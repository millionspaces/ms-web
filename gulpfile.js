var gulp 	= require('gulp');
var uglify 	= require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var concat 	= require('gulp-concat');
var del 	= require('del');
var plugins = require('gulp-load-plugins')();
var replace = require('gulp-replace');
var argv 	= require('yargs').argv;
var inlinesource = require('gulp-inline-source');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');


//image compress
var imagemin = require('gulp-imagemin');
var imageminJpedRecompress = require('imagemin-jpeg-recompress');

//var DIST_PATH = (argv.prod === undefined) ? 'src/build' : 'dist';
var DIST_PATH;

switch (argv.prod) {
    case 'dev':
        DIST_PATH = "dev-build/dist";
        break;
    case 'qae':
        DIST_PATH = "qae-build/dist";
        break;
    case 'trg':
        DIST_PATH = "trg-build/dist";
        break;
    case 'beta':
        DIST_PATH = "beta-build/dist";
        break;
    case 'live':
        DIST_PATH = "live-build/dist";
        break;
    default:
        DIST_PATH = "src/build";
}

var SRC_PATH = 'src/assets/**/*.*';

var BUNDLE1_CSS = [
	'src/assets/custom/css/app.css',
	'src/assets/custom/css/animate.css',
	'src/assets/custom/css/bespoke.css',
	'src/assets/custom/css/developer.css',
	'src/assets/custom/css/responsive.css',
	'src/assets/custom/css/elements.css'
];

var BUNDLE2_CSS = [
		'src/assets/bootstrap/dist/css/bootstrap.min.css',
		'src/assets/rating/css/rating.min.css',
		'src/assets/toastr/build/toastr.min.css',
    	'src/assets/full-calendar/fullcalendar.css',
		'src/assets/propit/protip.min.css',
    	'src/assets/owl/css/owl.carousel.min.css',
    	'src/assets/magnific-popup/css/magnific-popup.css',
	    'src/assets/owl/css/owl.theme.default.min.css',
    	'src/assets/animate/animate.min.css'
	];

var BUNDLE3_CSS = [
		'src/assets/font/flaticon.css',
		'src/assets/flag-icon/css/flag-icon.css',
		'src/assets/jquery-steps/jquery.steps.css',
		'src/assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
		'src/assets/croppie/dist/croppie.css',
    	'src/assets/timepicker/jquery.timepicker.css',
    	'src/assets/font/fontastic.custom.icons.css',
		'src/assets/ms-schedular/css/auxschedular.css',
    	'src/assets/ms-scheduler-v2/css/jquery-ui-ext.css',
    	'src/assets/ms-scheduler-v2/css/styles.css',
    	'src/assets/flipclock/css/flipclock.css'
    	/*'src/assets/blurry/css/blurry.load.css'*/

	];

var BUNDLE_JS = [
	'src/assets/jquery/jquery.min.js',
    'src/assets/jquery/jquery-ui.min.js',
    'src/assets/parallax/parallax.min.js',
	'src/assets/bootstrap/dist/js/bootstrap.min.js',
    'src/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
    'src/assets/jquery-steps/jquery.steps.js',
    'src/assets/moment/moment.min.js',
    'src/assets/moment/moment-range.min.js',
    'src/assets/rating/js/rating.min.js',
    'src/assets/toastr/build/toastr.min.js',
	'src/assets/socket/socket.io-1.2.0.js',
    'src/assets/croppie/dist/croppie.min.js',
    'src/assets/underscore/underscore-min.js',
	'src/assets/timepicker/jquery.timepicker.min.js',
	'src/assets/propit/protip.min.js',
	'src/assets/calendar/js/clndr.js',
    'src/assets/owl/js/owl.carousel.min.js',
    'src/assets/magnific-popup/js/jquery.magnific-popup.min.js',
    'src/assets/ms-scheduler-v2/js/jquery.ui.touch-punch.min.js',
    'src/assets/ms-scheduler-v2/js/selectableScroll.js',
    'src/assets/ms-scheduler-v2/js/jquery.msScheduler.js',
    'src/assets/flipclock/js/flipclock.min.js'
]

gulp.task('copy-fonts', function(){
    return gulp.src([
			'src/assets/font/Flaticon.ttf',
			'src/assets/font/Flaticon.woff',
			'src/assets/font/Flaticon.svg',
			'src/assets/font/Flaticon.eot',
      		'src/assets/font/avenir-roman.ttf',
			'src/assets/font/fontastic-custom.eot',
			'src/assets/font/fontastic-custom.svg',
			'src/assets/font/fontastic-custom.ttf',
			'src/assets/font/fontastic-custom.woff',
			'src/assets/font-awesome-4.7.0/fonts/*'
		], { base: ''} )
        .pipe(gulp.dest(DIST_PATH + '/fonts'));
});

gulp.task('copy-glyph-icons', function(){
    return gulp.src(['src/assets/bootstrap/fonts/*'], { base: '' })
        .pipe(gulp.dest(DIST_PATH+ '/fonts'));
});

gulp.task('copy-images', function(){
    return gulp.src([
		'src/assets/img/3D.jpg',
		'src/assets/img/processing.gif',
		'src/assets/img/bg-2.jpg',
		'src/assets/img/profile-bg.jpg',
		'src/assets/img/log-w.png',
		'src/assets/img/home_icon1.png',
		'src/assets/img/home_icon2.png',
		'src/assets/img/home_icon3.png',
		'src/assets/img/bg.gif',
		'src/assets/img/signup-bg.jpg',
		'src/assets/img/listing/about-m.jpg',
		'src/assets/img/listing/about-m2.jpg',
		'src/assets/img/listing/about-m3.jpg',
		'src/assets/img/listing/fee-structure-icon1.png',
		'src/assets/img/listing/fee-structure-icon2.png',
		'src/assets/img/listing/fee-structure-icon3.png',
		'src/assets/img/mobile-img.jpg'
	], { base: ''} ).pipe(imagemin([
		imagemin.gifsicle(),
		imagemin.jpegtran(),
		imageminJpedRecompress()
	])).pipe(gulp.dest(DIST_PATH + '/img'));
});

gulp.task('bundle1-css', function () {
	return gulp.src(BUNDLE1_CSS)
		.pipe(concat('style.bundle.1.css'))
        .pipe(replace('url(../../img/listing', 'url(img/'))
        .pipe(replace('url(../../img/', 'url(img/'))
        .pipe(replace('url(../../../assets/img', 'url(img/'))
		.pipe(minifyCss({processImport: false}))
		.pipe(gulp.dest(DIST_PATH))
});

gulp.task('bundle2-css', function () {
	return gulp.src(BUNDLE2_CSS)
		.pipe(concat('style.bundle.2.css'))
        .pipe(replace('url(../fonts/glyphicons-halflings-regular', 'url(fonts/glyphicons-halflings-regular'))
        .pipe(replace('url(\'../fonts/fontawesome-webfont', 'url(\'fonts/fontawesome-webfont'))
		.pipe(gulp.dest(DIST_PATH))
});

gulp.task('bundle3-css', function () {
	return gulp.src(BUNDLE3_CSS)
		.pipe(concat('style.bundle.3.css'))
        .pipe(replace("./Flaticon.eot", 'fonts/Flaticon.eot'))
        .pipe(replace("./Flaticon.eot?#iefix", 'fonts/Flaticon.eot?#iefix'))
        .pipe(replace("./Flaticon.woff", 'fonts/Flaticon.woff'))
        .pipe(replace("./Flaticon.ttf", 'fonts/Flaticon.ttf'))
        .pipe(replace("./Flaticon.svg#Flaticon", 'fonts/Flaticon.svg#Flaticon'))
        .pipe (replace ("./fontastic-custom.eot", "fonts/fontastic-custom.eot"))
        .pipe (replace ("./fontastic-custom.svg", "fonts/fontastic-custom.svg"))
        .pipe (replace ("./fontastic-custom.ttf", "fonts/fontastic-custom.ttf"))
        .pipe (replace ("./fontastic-custom.woff", "fonts/fontastic-custom.woff"))
		.pipe(minifyCss({processImport: false}))
		.pipe(gulp.dest(DIST_PATH))
});

gulp.task('bundle-all-css', function () {
    return gulp.src([DIST_PATH + '/style.bundle.3.css' , DIST_PATH + '/style.bundle.2.css', DIST_PATH + '/style.bundle.1.css' ] )
        .pipe(concat('style.bundle.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest(DIST_PATH))
});

gulp.task('bundle-js', function () {
    return gulp.src(BUNDLE_JS)
        .pipe(concat('scripts.bundle.js'))
		.pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest(DIST_PATH))
});

gulp.task('index-replace', function () {
	if(!argv.prod)
		return null;

	return gulp.src(DIST_PATH + '/index.html')
		.pipe(replace('build/',''))
		.pipe(gulp.dest(DIST_PATH))
});

gulp.task('clean', function(){
	return del.sync([
        DIST_PATH
	])
});

gulp.task('inline-code', function () {
    return gulp.src(DIST_PATH + '/index.html')
        .pipe(inlinesource())
        .pipe(gulp.dest(DIST_PATH));
});

gulp.task('develop', [
	'copy-fonts',
	'copy-glyph-icons',
	'copy-images',
	'bundle1-css',
	'bundle2-css',
	'bundle3-css',
	'bundle-js',
	'index-replace'
	] ,
	function () {
		console.log('Finished develop tasks');
	}
);

gulp.task('default', function(done) {
    runSequence('develop', 'bundle-all-css', function() {
        console.log('All Done');
        done();
    });
});
