## EVENTSPACE(FRONTEND)
This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## To run this project locally, you have to
- Install node [https://nodejs.org/en/download/]
- Install node (linux based) [https://nodejs.org/en/download/package-manager/]
- setup 'Angular CLI' globally
```sh
$ npm install -g @angular/cli
```

- setup 'Gulp' globally
```sh
$ npm install -g gulp
```

- go the project directory (/Frontend) and run
```sh
$ npm i
```

## Development server
Run `npm run local` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `npm run prod` to build the project for **production**. The build artifacts will be stored in the `dist/` directory.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## fixing fallbackLoader and loader option has been deprecated
go to 'node_modules/angular-cli/models/webpack-configs' and open 'styles.js'
find the global style path section and change it from:
```
loader: [
      ("css-loader?" + JSON.stringify({ sourceMap: cssSourceMap })),
    ].concat(commonLoaders, loaders),

    fallbackLoader: 'style-loader',

    publicPath: ''
```
to

```
use: [
      ("css-loader?" + JSON.stringify({ sourceMap: cssSourceMap })),
    ].concat(commonLoaders, loaders),

    fallback: 'style-loader',

    publicPath: ''
```

## Further help
To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
