export const environment = {
  production: true,
  host: 'https://dev.millionspaces.com',
  bucket: 'https://res.cloudinary.com/dz4u73trs/image/upload/q_50/development',
  cloudName: 'dz4u73trs',
  preset: 'xgnvzwig',
  cldFolder: 'development',
  apiHost: 'https://dapi.millionspaces.com/api',
  apiHostNotification: 'https://dnotification.millionspaces.com',
  apiHostBase: 'https://dapi.millionspaces.com',
  paymentCaptureFlag: 'M',
  envName: 'dev',
  apiPayment: 'https://qpayment.millionspaces.com',
  apiBooking: 'https://booking.millionspaces.com'
};
