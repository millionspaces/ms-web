export const environment = {
    production: true,
    host: 'https://trg.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/millionspaces',
    cloudName: 'dz4u73trs',
    preset: 'xgnvzwig',
    cldFolder: 'millionspaces',
    apiHost: 'https://tapi.millionspaces.com/api',
    apiHostNotification: 'https://tnotification.millionspaces.com',
    apiHostBase: 'https://tapi.millionspaces.com',
    paymentCaptureFlag: 'M',
    envName: 'trg',
    apiPayment: 'https://qpayment.millionspaces.com'
};
