export const environment = {
    production: true,
    host: 'https://millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/w_400,g_south_west,y_400,l_msk_mskalx/q_50,fl_progressive/millionspaces',
    thumbnailPath: 'https://res.cloudinary.com/dgcojyezg/image/upload/w_600,fl_progressive/f_auto/millionspaces',
    cloudName: 'dgcojyezg',
    preset: 'hglnuurt',
    cldFolder: 'millionspaces',
    apiHost: 'https://api.millionspaces.com/api',
    apiHostNotification: 'https://notification.millionspaces.com',
    apiHostBase: 'https://api.millionspaces.com',
    paymentCaptureFlag: 'A',
    envName: 'live',
    hostDomain: 'http://host.millionspaces.com',
    apiPayment: 'https://payment.millionspaces.com'
};


document.write(`
    <script src="https://www.googletagmanager.com/gtag/js?id=UA-110933663-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-110933663-1');
    </script>
`
);