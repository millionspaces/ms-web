// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  host: 'https://dev.millionspaces.com',
  bucket:
    'https://res.cloudinary.com/dz4u73trs/image/upload/q_50,fl_progressive/development',
  thumbnailPath:
    'https://res.cloudinary.com/dz4u73trs/image/upload/w_600,fl_progressive/f_auto/millionspaces',
  cldFolder: 'development',
  cloudName: 'dz4u73trs',
  preset: 'xgnvzwig',
  apiHost: 'https://dapi.millionspaces.com/api',
  apiHostNotification: 'https://dnotification.millionspaces.com',
  apiHostBase: 'https://dapi.millionspaces.com',
  paymentCaptureFlag: 'M',
  envName: 'dev',
  hostDomain: 'http://qhost.millionspaces.com',
  apiPayment: 'https://qpayment.millionspaces.com',
  apiBooking: 'https://qbooking.millionspaces.com'
};

// local port
// apiBooking: 'http://localhost:3002'
