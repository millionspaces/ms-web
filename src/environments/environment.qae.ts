export const environment = {
    production: true,
    host: 'https://qae.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dz4u73trs/image/upload/q_50,fl_progressive//millionspaces',
    thumbnailPath: 'https://res.cloudinary.com/dz4u73trs/image/upload/w_600,fl_progressive/f_auto/millionspaces',
    cloudName: 'dz4u73trs',
    preset: 'xgnvzwig',
    cldFolder: 'millionspaces',
    apiHost: 'https://qapi.millionspaces.com/api',
    apiHostNotification: 'https://qnotification.millionspaces.com',
    apiHostBase: 'https://qapi.millionspaces.com',
    paymentCaptureFlag: 'M',
    envName: 'qae',
    hostDomain: 'https://qhost.millionspaces.com',
    apiPayment: 'https://qpayment.millionspaces.com',
    apiBooking: 'https://qbooking.millionspaces.com'
};
