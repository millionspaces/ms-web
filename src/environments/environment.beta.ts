export const environment = {
    production: true,
    host: 'https://beta.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dz4u73trs/image/upload/q_50/millionspaces',
    cloudName: 'dz4u73trs',
    preset: 'xgnvzwig',
    cldFolder: 'millionspaces',
    apiHost: 'https://bapi.millionspaces.com/api',
    apiHostNotification: 'https://bnotification.millionspaces.com',
    apiHostBase: 'https://bapi.millionspaces.com',
    paymentCaptureFlag: 'M',
    envName: 'beta',
    apiPayment: 'https://qpayment.millionspaces.com'
};
