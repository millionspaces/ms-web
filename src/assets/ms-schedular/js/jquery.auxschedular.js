;( function( $, window, document, undefined ) {

    "use strict";

    // Plugin defaults
    var pluginName = "auxScheduler",
        defaults = {
            startTime: 12,
            endTime: 23,
            selectionClass: 'selection-wrapper',
            timeSlotClass: 'time-slot',
            timeSlotSelector: '.time-slot',
            activeSlotClass: 'ms-active',
            droppedSlotClass: 'dropped',
            selectedDateSelector: '.schedular-selected-date',
            selectedDate: '',
            disabledHours: [],
            getHours: null,
            handleTimeSlotClick: null,
            clearActive: null
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {

        this.element = element;

        // Extending defaults
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this._registeredHours = [];

        this.init ();
    }

    // Avoid Plugin.prototype conflicts
    $.extend( Plugin.prototype, {

        options: function (option, val) {
            this.settings[option] = val;
            this.init ()
        },

        init: function () {

            this.setSelectedDate ();
            this.constructSlots ();
            this.initDragAndDrop ();

        },

        // Append slots to Doc
        constructSlots: function () {
            $(this.element).find('.schedular').html('');

            for (var i = this.settings.startTime; i<this.settings.endTime; i++) {
                $(this.element).find('.schedular').append(this.constructTimeSlot(i))
            }

            $(this.element).find('.schedular').append('<p>'+this.formatTime (i)+'</p>');

        },

        // Creating time slots and appending to DOM
        constructTimeSlot: function (time) {

            var disabledHours = this.disabledHours (),
                className = '',
                disabledText = this.disabledText (),
                text = '',
                disabledId = this.disabledId (),
                id = null,
                bookingTypes = this.getBookingType ()

            // Determining disable hours and enabled hours
            if ($.inArray(time, disabledHours) !==-1) {

                var bookingType = bookingTypes[time]?' guest-booking': ' host-booking'
                className = '"ms-disabled '  + bookingType + '"';

            } else  {
                className = this.settings.timeSlotClass;
            }

            disabledText[time]?text = ' - '+disabledText[time]: '';

            disabledId[time]?id = disabledId[time]: id=null;

            var timeSlot = '<p>'+this.formatTime (time)+'</p>'
                + '<div class='+className+' data-fromtime='+time+ ' data-totime='+(time+1)+ ' data-id='+id +'><p></p>'
                + '</div>';
            return timeSlot;

        },

        // Get disabled hours array
        disabledHours: function  () {

            var disabledHours = [];

            this.settings.disabledHours.forEach(function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledHours.push(i)
                }
            });

            return disabledHours;

        },

        clearActive: function () {
            $('.dropped').removeClass('droped');
        },

        disabledText: function () {
            var disabledText = {};

            this.settings.disabledHours.forEach(function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledText[i] = obj.text;
                }
            });

            return disabledText;

        },

        disabledId: function () {
            var disabledId = {};

            this.settings.disabledHours.forEach (function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledId[i] = obj.id;
                }
            });

            return disabledId;
        },

        getBookingType: function () {
            var bookingType = {};

            this.settings.disabledHours.forEach (function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    bookingType[i] = obj.guest;
                }
            });

            return bookingType;
        },

        // Format time string to 12 hours
        formatTime: function (time) {

            var formattedTime = ((time + 11) % 12 + 1).toString ();

            time < 12 ? formattedTime += ':00 AM' : formattedTime += ':00 PM'

            return formattedTime;

        },

        // Initializing drag and drop
        initDragAndDrop: function () {

            var $this = this;

            $($this.element).drag('start', function () {

                $('.time-slot').removeClass('dropped');
                $('.' + $this.settings.selectionClass).remove();
                return $('<div class='+$this.settings.selectionClass+' />').css('opacity', .65 ).appendTo(document.body);

            }).drag (function(ev, dd) {
                $(dd.proxy).css({
                    top: Math.min(ev.pageY, dd.startY),
                    left: Math.min(ev.pageX, dd.startX),
                    height: Math.abs(ev.pageY - dd.startY),
                    width: Math.abs(ev.pageX - dd.startX),
                    zIndex: 9999999999
                });
                console.log($('.'+$this.settings.selectionClass).offset().top + $('.'+$this.settings.selectionClass).height(), $('.aux-shedular-wrapper').offset().top + $('.aux-shedular-wrapper').height());
                // if ($('.'+$this.settings.selectionClass).offset().top + $('.'+$this.settings.selectionClass).height() > $('.aux-shedular-wrapper').offset().top + $('.aux-shedular-wrapper').height()) {
                //
                //   $('.host-scheduler').animate({
                //         scrollTop: $('.'+$this.settings.selectionClass).offset().top + $('.'+$this.settings.selectionClass).height(),
                //   }, 100, function () {
                //     $(this).stop();
                //   });
                // }
            }).drag ("end", function (ev, dd) {
                $(dd.proxy).remove ();

                if ($('.dropped:first').hasClass('time-slot')) {
                    $this.settings.getHours?$this.settings.getHours($('.dropped:first').attr('data-fromtime'), $('.dropped:last').attr('data-totime')):null;
                }

            });

            $(this.settings.timeSlotSelector).drop("start", function () {

                if (!$(this).hasClass('disabled-yellow')) {
                    $(this).addClass ($this.settings.activeSlotClass);
                }
                $(this).nextAll('div.ms-disabled').first().nextAll('.time-slot').removeClass('time-slot').addClass('disabled-yellow');
                $(this).prevAll('div.ms-disabled').first().prevAll('.time-slot').removeClass('time-slot').addClass('disabled-yellow');

            }).drop(function (ev, dd) {

                if (!$(this).hasClass('disabled-yellow')) {
                    $(this).toggleClass($this.settings.droppedSlotClass);
                }

            }).drop("end",function (ev, dd, cc) {

                $('.disabled-yellow').addClass('time-slot').removeClass('disabled-yellow');

                if (!$(this).hasClass('disabled-yellow')) {
                    $(this).removeClass ($this.settings.activeSlotClass);
                }

            });

            $.drop({multi: true});

            $(this.settings.timeSlotSelector).on('click', function () {
                $('.dropped').removeClass('dropped');
                $(this).toggleClass($this.settings.droppedSlotClass);
                $this.settings.handleTimeSlotClick?$this.settings.handleTimeSlotClick ($(this).data('fromtime'), $(this).data('totime'), null):null;
            });

            $('.ms-disabled').on('click', function () {
                $this.settings.handleTimeSlotClick?$this.settings.handleTimeSlotClick ($(this).data('fromtime'), $(this).data('totime'),  $(this).data('id')):null;
            });


        },

        // Setting selected date to DOM element
        setSelectedDate: function () {
            $(this.element).find(this.settings.selectedDateSelector).text(this.settings.selectedDate);
        }

    });

    // A plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {

        var args = $.makeArray(arguments),
            after = args.slice(1);

        return this.each(function () {

            var instance = $.data(this, pluginName);

            if (instance) {
                if (instance[options]) {
                    instance[options].apply(instance, after);
                } else {
                    console.error('Method ' + options + ' does not exist on Plugin');
                }
            } else {
                var plugin = new Plugin(this, options);

                $.data(this, pluginName, plugin);
                return plugin;
            }
        });

    };

}) (jQuery, window, document);
