;( function( $, window, document, undefined ) {

    "use strict";

    // Plugin defaults
    var pluginName = "msScheduler",
        defaults = {
            title: '',
            startTime: 0,
            endTime: 23,
            cssScope: 'ms-scheduler-scope',
            getHours: null,
            onClicked: null,
            clearActive: null,
            handleBookedSlotClick: null,
            type: 'guest'
        };

    // Plugin constructor
    function Plugin ( element, options ) {

        this.element = element;

        // Extending defaults
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this._registeredHours = [];
        this.init ();
    }

    // Avoid Plugin.prototype conflicts
    $.extend( Plugin.prototype, {

        options: function (option, val) {
            this.settings[option] = val;
            this.init ();
        },

        // Initializing scheduler
        init: function () {
            var $this = this;

            $(this.element).html('');

            this.constructSlots ();
            
            //console.log($this.settings.type);

            if ($this.settings.type ==='host') {
                $('.booked').selectableScroll({
                    stop: function () {
                        $this.settings.handleBookedSlotClick?$this.settings.handleBookedSlotClick(+$(this).data('time'), +$(this).data('time')+1, +$(this).data('id')):null;
                    }
                });
            }
    
            // Initializing selectable
            $(".time-slots-wrapper").selectableScroll({
            filter: ".ms-scheduler-time-slot:not(.booked, .free-space)", // Filtering booked hours
            selecting: function(event, ui) {

                if ($('.ui-selecting:last').next().hasClass('booked')) {
                    $('.ui-selecting:last').nextAll().not('.booked').addClass('prevented').removeClass('ui-selected');
                }
                if ($('.ui-selecting:first').prev().hasClass('booked')) {
                    $('.ui-selecting:first').prevAll().not('.booked').addClass('prevented').removeClass('ui-selected');
                }
            },
            stop: function (event, ui) {
                $('.prevented').removeClass('prevented').removeClass('ui-selected');
                console.log('start time',+$('.ui-selected:first').data('time'));
                console.log('end time',+$('.ui-selected:last').data('time'));
                if (+$('.ui-selected:first').data('time') === +$('.ui-selected:last').data('time')) {
                    $this.settings.getHours?$this.settings.getHours(+$('.ui-selected:first').data('time'), +$('.ui-selected:last').data('time') + 1, false, $('.ui-selected:last').data('id')):null
                } else if ($('.ui-selected:first').data('time') || +$('.ui-selected:first').data('time') === 0) {
                    $this.settings.getHours?$this.settings.getHours(+$('.ui-selected:first').data('time'), +$('.ui-selected:last').data('time') + 1, true):null
                }
                // guest calendar handle disable click
                else if($this.settings.type !== 'host' && !+$('.ui-selected:first').data('time') && !+$('.ui-selected:first').data('time')){
                    $this.settings.getHours?$this.settings.getHours(null, null, false): null;
                }

            }
          });
          

        },

        clearActive: function() {
            $('.ui-selected').removeClass('ui-selected');
        },

        // Construct a single time slot
        constructSlot: function (time) {

          var disabledHours = this.getDisabledHours (),
              className = '',
              disabledText = this.getDisabledText (),
              text = '',
              disabledId = this.getDisabledId (),
              id = null,
              bookingTypes = this.getBookingType ()

          // Determining disable hours and enabled hours
          if ($.inArray(time, disabledHours) !==-1) {

              var bookingType = bookingTypes[time]?' guest-booking': ' host-booking';
                  className = 'booked '  + bookingType;

          }

          disabledText[time]?text = ' - '+disabledText[time]: '';

          disabledId[time]?id = disabledId[time]: id=null;

          var slot = '<div class="ms-scheduler-time-slot '+className+'" data-id="'+id+'" data-time="'+time+'">'
                   +    '<p>'+this.formatTime(time)+'</p>'
                   +    '<div></div>'
                   +  '</div>';

          return slot;
        },



        // construct all the slots
        constructSlots: function () {

          var slots = ''

          for (var i=this.settings.startTime; i<this.settings.endTime;i++) {
            slots += this.constructSlot (i);
          }

          $(this.element).append (
              '<section class="'+this.settings.cssScope+'" ms-scheduler-css">'
              +  '<header>'
              +     '<p>'+this.settings.title+'</p>'
              +  '</header>'
              +  '<div class="time-slots-wrapper">'
              +     slots
              +     '<div class="ms-scheduler-time-slot free-space">'
              +      '<p>'+this.formatTime(this.settings.endTime)+'</p>'
              +      '<div></div>'
              +     '</div>'
              +   '</div>'
              + '</section>'
          )
        },

        // Format time string to 12 hours
        formatTime: function (time) {

            var formattedTime = ((time + 11) % 12 + 1).toString ();

            time < 12 ? formattedTime += ':00 AM' : formattedTime += ':00 PM'

            return formattedTime;

        },

        // Get disabled hours array
        getDisabledHours: function  () {

            var disabledHours = [];

            this.settings.disabledHours.forEach(function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledHours.push(i)
                }
            });

            return disabledHours;

        },

        getDisabledText: function () {
            
            var disabledText = {};

            this.settings.disabledHours.forEach(function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledText[i] = obj.text;
                }
            });

            return disabledText;

        },

        getDisabledId: function () {
            var disabledId = {};

            this.settings.disabledHours.forEach (function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    disabledId[i] = obj.id;
                }
            });

            return disabledId;
        },

        getBookingType: function () {
            var bookingType = {};

            this.settings.disabledHours.forEach (function (obj) {
                for (var i = obj.from; i < obj.to; i++) {
                    bookingType[i] = obj.guest;
                }
            });

            return bookingType;
        },


    });

    // A plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {

        var args = $.makeArray(arguments),
            after = args.slice(1);

        return this.each(function () {

            var instance = $.data(this, pluginName);

            if (instance) {
                if (instance[options]) {
                    instance[options].apply(instance, after);
                } else {
                    console.error('Method ' + options + ' does not exist on Plugin');
                }
            } else {
                var plugin = new Plugin(this, options);

                $.data(this, pluginName, plugin);
                return plugin;
            }
        });

    };

}) (jQuery, window, document);
