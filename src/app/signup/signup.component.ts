import {Component, OnInit, OnDestroy} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { User } from "../user/user.model";
import { BasicValidators } from "../shared/validators/basic.validators";
import { PasswordValidators } from "../shared/validators/password.validators";
import { CryptoService } from "../shared/services/crypto.service";
import { UserService } from "../user/user.service";
import { Router } from "@angular/router";
import { DeviceService } from "../shared/services/device.service";
import { AuthenticationService } from "../shared/services/authentication.service";

declare let $: any;

@Component ({
  templateUrl: './signup.component.html',
  styles: [`
    .sign-up-form-ctrl-ico {
      height: 42px;
    }
    .sign-up-form-ctrl-input {
      padding-left: 50px;
    }

    .login-box-body{
      background-color:#ffffff !important;
      min-height: 476px;
   }        
  
  .user-input {
         border: 1px solid #95d5f1;
         height: 38px;
         padding: 6px 12px !important;
         margin-bottom:1rem;
  }

  .btn-common{
      color: #252424;
      background-color: #ffdd57;
      border: 1px solid #fefefe;
      text-transform: uppercase;
      text-align: center;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      font-size: 1.4rem !important;
      width: 100%;
      margin: 10px 0;
      font-weight:500;
      outline: 0;
  }
  
  .btn-common:hover, .btn-common:active, .btn-common:focus{
        color: #252424 !important;
        background-color: #ffdd57 !important;
        outline: 0;
  }

  .has-error .form-control {
    border: 1px solid #c40000!important;
    box-shadow: none;
    background-color:transparent;
}

.field-error{
    color: #c40000 !important;
    text-align:left !important;
    font-size:1.2rem;
    margin-top:-1rem;
}
  `]
})

export class SignupComponent implements OnInit, OnDestroy {

  form: FormGroup;
  user: User = new User();
  signupAgreement: boolean = false;
  showTAndC: boolean = false;
  showPP: boolean = false;
  showMS: boolean = false;
  hideLogin: boolean = false;
  emailToBeRegistered: string = '';
  imageUrl = 'assets/img/signup-bg.jpg';

  constructor(
      fb: FormBuilder,
      private _crpto: CryptoService,
      private _userService: UserService,
      private _router: Router,
      private _deviceService: DeviceService,
      private _authService: AuthenticationService,
  ){

    this.form = fb.group({
      email : ['', BasicValidators.email, new BasicValidators(this._authService).emailExistent.bind(this)],
      name : ['', Validators.required],
        password : ['', Validators.compose([
          Validators.required,
          PasswordValidators.shouldHaveMinimumCharacters
        ])],
      },
    );
  }

  registerNow(){
    let result;

    let userToBeRegistered : User = {
          email: this.user.email,
          name: this.user.name,
          password: this._crpto.sha256Hex(this.user.password),
          mobileNumber: this.user.mobileNumber,
        };

    result = this._userService.registerUser(userToBeRegistered);

    result.subscribe(
        resp => {
            if(resp.status === 201){
                this.showMailSentSuccessMessage ();
                this.hideLoginWhenSuccess ();
                this.emailToBeRegistered = userToBeRegistered.email;
            }
        },
        error => {
          console.log('error signup ',error);
        },
    );

  }

  previewLogin(){
    this._router.navigate(['user/signin']);
  }

    showTermsAndConditions () {
        var win = window.open("/#/terms/privacy", '_blank');
        win.focus();
    }

    showPrivacyPolicy () {
        this.showPP = !this.showPP;
    }
    
    showMailSentSuccessMessage () {
      this.showMS = true;
    }

    hideLoginWhenSuccess () {
      this.hideLogin = true;
    }

    ngOnInit(){
        $('body').css('background-image', 'url(' + this.imageUrl + ')');
    }

    ngOnDestroy(){
        $('body').css('background-image', 'none');
    }

}
