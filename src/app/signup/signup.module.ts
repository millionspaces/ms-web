import { NgModule } from "@angular/core";
import { SignupComponent } from "./signup.component";
import { CryptoService } from "../shared/services/crypto.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { EmailSuccessComponent } from "./email-success.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        SharedModule
    ],
    declarations: [SignupComponent, EmailSuccessComponent],
    exports: [SignupComponent, EmailSuccessComponent],
    providers: [
        CryptoService,
    ]
})
export class SignupModule{

}