import {Component, Input} from '@angular/core';

@Component ({
  styles: [`
    .email-success-wrapper {
      margin: 50px auto 0;
      padding-top: 40px!important;
      width: 360px; 
      background: rgba(255,255,255,0.8);
      text-align: center;
      padding: 30px 10px;
      position: relative;
      top: 40px;
    }
    .email-success-wrapper h1 {
        font-size: 26px;
        margin-bottom: 15px;
    }
    .email-success-wrapper p {
        margin-bottom: 15px;
    }
    .icon-wrapper {
        font-size: 32px;
    }
  `],
  template: `
    <div  [style.display]="showMS?'block':'none'" class="email-success-wrapper">
        <div class="email-success-wrapper-inner">
            <span class="icon-wrapper"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
            <h1>Account Activation</h1>
            <p>The account activation link has been sent to your email address ({{emailToBeRegistered}}).</p>
        </div>
    </div>
  `,
  selector: 'email-success'
})

export class EmailSuccessComponent  {
  @Input() showMS;
  @Input() emailToBeRegistered;
}