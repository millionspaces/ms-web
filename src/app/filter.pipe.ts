import {Pipe, PipeTransform} from '@angular/core';
import {isNullOrUndefined} from "util";

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(spaces: any, args?: any[]): any {
        if (isNullOrUndefined(args) || args.length == 0) {
            return spaces;
        }
        else {
            var result = new Set();
            for (let space of spaces) {
                //var num=0;
                for (let arg of args) {
                    if (space.eventType.indexOf(arg) > -1) {
                        // num++;
                        result.add(space);
                    }
                }
                //if(num >= args.length)
                // result.push(space);
            }
            return Array.from(result);
        }
    }


}
