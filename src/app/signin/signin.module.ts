import {NgModule} from "@angular/core";
import {SignInComponent} from "./signin.component";
import {AuthenticationService} from "../shared/services/authentication.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {GoogleServicesAPIModule} from "../google-services-api/google-services-api.module";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {FbServicesAPIModule} from "../fb-auth/fb-services-api.module";
import {CommonModule} from "@angular/common";

@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        GoogleServicesAPIModule,
        SharedModule,
        FbServicesAPIModule],
    declarations: [SignInComponent],
    exports: [SignInComponent],
    providers:[AuthenticationService]

})
export class SignInModule{

}