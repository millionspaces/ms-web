import {Component, OnInit} from "@angular/core";
import { AuthenticationService } from "../shared/services/authentication.service";
import {Router, ActivatedRoute} from "@angular/router";
import { User } from "../user/user.model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BasicValidators } from "../shared/validators/basic.validators";
import { environment } from "../../environments/environment";

declare var $;
declare var io: any;

@Component({
    selector: 'signin',
    templateUrl:'./signin.component.html',
    styleUrls : ['./signin.component.css'],
})
export class SignInComponent implements OnInit {
    isLoading:Boolean = false;
    user:User = new User();
    form: FormGroup;
    socket: any;
    imageUrl = 'assets/img/signup-bg.jpg';
    returnUrl: string;
    navigateTo: string;

    emailPattern = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";

    constructor(
        private authService: AuthenticationService,
        private _router: Router,
        private route: ActivatedRoute,
        fb: FormBuilder,
        private _authService: AuthenticationService,
        private router: Router
    ){
        this.form = fb.group({
            email: ['', BasicValidators.email],
            password: ['', Validators.required]
        });
        this.socket = io(environment.apiHostNotification);
    }


    login(user) {

        this.isLoading = true;
        this.authService.login(user)
            .subscribe(
                user => {
                    this.socket.emit('intiate', user.id);
                    this._router.navigateByUrl(this.returnUrl);
                },
                error => {
                    if(error.code == "22"){
                        this.form.controls['password'].setErrors({
                            osxIssue: true
                        });
                    }else {
                        this.form.controls['password'].setErrors({
                            invalidLogin: true
                        });
                    }
                    this.isLoading = false;
                    console.log(this.form);

                },
                () => {
                    this.isLoading = false;
                });
    }

    previewSignup(){
        this._router.navigate(['user/signup']);
    }

    showForgotPasswordScreen = false;
    showForgotPasswordForm(){
        $('.login-box').hide();
        this.showForgotPasswordScreen = true;
    }

    showSignInForm(){
        $('.login-box').show();
        $("#loginModal").modal('show');
        this.showForgotPasswordScreen = false;
    }

    toggleScreen(visible){
        console.log('visible', visible);
        this.showForgotPasswordScreen = visible;
        $('.login-box').show();
    }

    sentResetEmail = false;
    sentEmailApiFetch = false;

    sendResetLink(email: HTMLInputElement){
        this.sentEmailApiFetch = false;

        if(email.value.trim() === '') return;

        this.authService.isRegisteredEmail(email.value.trim()).subscribe(emailSent => {
            if(emailSent){
                this.showForgotPasswordScreen = false;
                this.sentResetEmail = true;
            }else{
                this.sentResetEmail = false;
            }
            this.sentEmailApiFetch = true;
        });

    }

    ngOnInit(){
        $('body').css('background-image', 'url(' + this.imageUrl + ')');
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    ngOnDestroy(){
        $('body').css('background-image', 'none');
    }

    guestProceed = false;

    porceedAsGuest(){
        this.guestProceed = true;
    }

    submitEmailForm(form) {
        this._authService.guestLogin(form.email)
            .subscribe(
                user => {
                    if (this.returnUrl) this._router.navigateByUrl(this.returnUrl);
                    else this._router.navigate(['/home']);
                },
                error => {
                    console.error(error)
                });


    }
}