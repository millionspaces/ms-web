import {Component, OnInit, OnDestroy} from "@angular/core";
declare var $;
@Component({
    template: `
        <div id="notfountc">
            <!--image here-->
            <img src="../../assets/img/notfound.png" alt="not-found" border="1" width="20%" />
        </div>
    `,
    styles: [`
        #notfountc{
            margin-top: 120px;
        }
        #notfountc img {
            display:block;
            margin:0px auto;
        }
    `]
})
export class NotFoundComponent{

}