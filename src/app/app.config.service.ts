import {Injectable} from "@angular/core";

@Injectable()
export class AppConfigService{

    private app_currency = 'LKR';

    getCurrency(){
        return this.app_currency;
    }


}