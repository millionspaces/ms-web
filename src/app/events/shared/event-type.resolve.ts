import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {EventService, EventType} from "./event.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class EventTypeResolve implements Resolve<any>{
    constructor(
        private eventService: EventService
    ){}

    resolve() : Observable<EventType[]>{
        return this.eventService.getEventTypes();
    }
}