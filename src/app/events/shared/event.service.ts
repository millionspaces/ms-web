import {Injectable} from "@angular/core";
import {ServerConfigService} from "../../server.config.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class EventService{

    eventTypesUrl: string;
    eventSubTypesUrl: string;

    constructor(
        private _serverConfigService: ServerConfigService,
        private _http: Http
    ){
        this.eventTypesUrl = environment.apiHost+'/common/eventTypes';
        this.eventSubTypesUrl = environment.apiHost+'/common/eventSubTypes';
    }

    getEventTypes(): Observable<EventType[]>{
        return this._http
            .get(this.eventTypesUrl, this._serverConfigService.getCookieHeader())
            .map((response: Response) => <Event[]>response.json())
            .catch(this.handleError);
    }

    getEventSubTypes(): Observable<EventSubType[]>{
        return this._http
            .get(this.eventSubTypesUrl, this._serverConfigService.getCookieHeader())
            .map((response: Response) => <EventSubType[]>response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }

}

export class EventType{
    id:number;
    name:string;
    icon:string;
    selected: boolean;
}

export class EventSubType{
    id:number;
    name:string;
}