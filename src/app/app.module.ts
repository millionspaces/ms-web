import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './pages/not-found.component';
import { SignupModule } from './signup/signup.module';
import { ServerConfigService } from './server.config.service';
import { SignInModule } from './signin/signin.module';
import { AuthGuard } from './shared/services/auth-guard.service';
import { AWSS3Service } from './shared/services/aws-s3.service';
import { FooterModule } from './footer/footer.module';
import { AngularFireModule } from 'angularfire2';
import { EventTypeResolve } from './events/shared/event-type.resolve';
import { AmenitiesResolve } from './amenities/shared/amenities.resolve';
import { CancellationPolicyResolve } from './core/cancellation-policy/shared/cancellation-policy.resolve';
import { AppConfigService } from './app.config.service';
import { SpaceService } from './spaces/shared/space.service';
import { SpacesResolve } from './spaces/spaces.resolve';
import { UserResolve } from './user/user.resolve';
import { ToasterService } from './shared/services/toastr.service';
import { CryptoService } from './shared/services/crypto.service';
import { NotificationService } from './shared/services/notification.service';
import { DeviceService } from './shared/services/device.service';
import { PagerService } from './spaces/shared/pager.service';
import { AmenityUnitsResolve } from './amenities/shared/amenity-units.resolve';
import { SpaceRulesResolve } from './spaces/shared/space-rules.resolve';
import { SeatingOptionsResolver } from './spaces/shared/seating-options.resolver';
import { SpaceResolve } from './spaces/space.resolve';
import { RouteDataService } from './shared/services/route.data.service';
import { FeaturedSpacesResolve } from './spaces/featured-spaces.resolve';
import { MainAlertComponent } from './shared/components/main-alert-component';
import { MainModalComponent } from './shared/components/main-modal-component';
import { SharedModule } from './shared/shared.module';
import { LocalStorageService } from './shared/services/local-storage.service';
import { SessionStorageService } from './shared/services/session-storage.service';
import { UserService } from './user/user.service';
import { BookingService } from './user/my-activities/booking.service';
import { MSErrorHandler } from './common/errors/ms-error-handler';
import { MomentService } from './shared/services/moment.service';
import { MSModalService } from './shared/services/ms-modal.service';
import { AboutUsComponent } from './about/about-us.component';
import { PrivacyTermsComponent } from './privacy/privacy-terms.component';
import { FutureBookingsResolve } from './spaces/future-bookings.resolve';
import { SimilarSpacesResolve } from './spaces/similar-spaces.resolve';
import { PressPageComponent } from './press/press-page.component';
import { NavbarV2Component } from './navbar-v2/navbar-v2.component';
import { FormsModule } from '@angular/forms';
import { BookingModule } from './booking/booking.module';
import { HomeModule } from './homepage-v2/home.module';
import { SocketService } from './socket.service';

export const firebaseConfig = {
  /*apiKey: "AIzaSyC76SFP0AGZjkTAeA0XsjhTelVu-Be5LCg",
  authDomain: "eventspace-154706.firebaseapp.com",
  databaseURL: "https://eventspace-154706.firebaseio.com",
  storageBucket: "eventspace-154706.appspot.com",
  messagingSenderId: "78233837973"*/

  apiKey: 'AIzaSyC3YSuvMILOURi67dG1RMWC2RJXYmvC71Q',
  authDomain: 'millionspaces-186806.firebaseapp.com',
  databaseURL: 'https://millionspaces-186806.firebaseio.com',
  projectId: 'millionspaces-186806',
  storageBucket: 'millionspaces-186806.appspot.com',
  messagingSenderId: '524002174668'
};

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    SignupModule,
    SignInModule,
    HomeModule,
    BookingModule,
    FooterModule,
    AppRoutingModule,
    SharedModule
  ],
  declarations: [
    AppComponent,
    NotFoundComponent,
    MainModalComponent,
    MainAlertComponent,
    AboutUsComponent,
    PrivacyTermsComponent,
    PressPageComponent,
    NavbarV2Component
  ],

  providers: [
    ServerConfigService,
    MSErrorHandler,
    AppConfigService,
    AuthGuard,
    AWSS3Service,
    EventTypeResolve,
    AmenitiesResolve,
    AmenityUnitsResolve,
    CancellationPolicyResolve,
    SpaceService,
    SpacesResolve,
    SpaceResolve,
    UserService,
    UserResolve,
    ToasterService,
    CryptoService,
    NotificationService,
    DeviceService,
    SpaceRulesResolve,
    FutureBookingsResolve,
    SimilarSpacesResolve,
    SeatingOptionsResolver,
    PagerService,
    RouteDataService,
    FeaturedSpacesResolve,
    LocalStorageService,
    SessionStorageService,
    BookingService,
    MomentService,
    MSModalService,
    SocketService,
    { provide: ErrorHandler, useClass: MSErrorHandler }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
