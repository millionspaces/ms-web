import {Component, OnInit} from "@angular/core";
import {RouteDataService} from "../../shared/services/route.data.service";
import {SessionStorageService} from "../../shared/services/session-storage.service";

@Component({
    templateUrl: './book-now-pay-later.component.html',
    styleUrls: ['./book-now-pay-later.component.css']
})
export class BookNowPayLaterComponent implements OnInit {

    paymentSummaryData: any;

    constructor(
        private routeDataService: RouteDataService,
        private storageService: SessionStorageService,
    ) {}

    ngOnInit() {
        if(this.routeDataService.paymentSummaryData){
            this.paymentSummaryData = this.routeDataService.paymentSummaryData;
            this.storageService.removeItem('PSD'); // payment summary data
            this.storageService.addItem('PSD', this.routeDataService.paymentSummaryData); // payment summary data
        }else{
            this.paymentSummaryData = this.storageService.getItem('PSD');
        }
    }

}