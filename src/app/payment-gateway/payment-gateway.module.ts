import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { CoreModule } from "../core/core.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PaymentGatewayRouteModule } from "./payment-gateway.route.module";
import { PaymentStatusComponent } from "./payment-status/payment-status.component";
import { CommonModule } from "@angular/common";
import {PaymentOptionsComponent} from "./payment-options/payment-options.component";
import { PaymentService } from "./common/payment.service";
import { PaymentManualComponent } from "./payment-options/manual/payment-manual.component";
import { PaymentResponseComponent } from "./payment-response/payment-response.component";
import { PaymentManualResponseComponent } from "./payment-options/manual/payment-manual-response-component";
import { InfinityWarPromoComponent } from "./payment-options/promotions/infinity-war-promo.component";
import {FreeHotDeskBookingComponent} from "./payment-options/promotions/free-hotdesk-booking/free-hotdesk-booking.component";
import {BookNowPayLaterComponent} from "./book-now-pay-later/book-now-pay-later.component";
import {PaymentOptions2Component, SafePipe} from "./payment-options/payment-options2.component";
import {SessionExpiredComponent} from "./payment-options/session-expired.compoent";
import {PaymentSummaryGuard} from "./payment-options/payment-summary-guard.service";

@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        CoreModule,
        PaymentGatewayRouteModule
    ],
    declarations:[
        PaymentStatusComponent,
        PaymentOptionsComponent,
        PaymentOptions2Component,
        PaymentManualComponent,
        PaymentResponseComponent,
        PaymentManualResponseComponent,
        InfinityWarPromoComponent,
        FreeHotDeskBookingComponent,
        BookNowPayLaterComponent,
        SessionExpiredComponent,
        SafePipe
    ],
    providers: [PaymentService, PaymentSummaryGuard]
})
export class PaymentGatewayModule{

}