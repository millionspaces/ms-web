import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PlatformLocation } from "@angular/common";

@Component({
    templateUrl: './payment-response.component.html',
    styles: [`
        div.response-box{
             width: 100%;
             margin: 0 auto;
             background-color: #f5f5f5;
             padding: 20px 20px 30px 20px;
             margin-top: 50px;
             font-size: 14px;
             text-align: center;
        }
        .navigation-links-wrapper {
            padding: 0;
            list-style: none;
        }
        .navigation-links-wrapper > li {
              display: inline-block;
              padding: 10px;
              margin: 5px;
        }
        .navigation-links-wrapper > li > a {
            color: #5a5a5a;
        }

    `]
})
export class PaymentResponseComponent implements OnInit{

    orderId: string;
    code: number;
    status: string;

    constructor(private route: ActivatedRoute, private location: PlatformLocation){
        //user not allowed to go back
        location.onPopState(() => {
            location.forward();
            return;
        });
    }

    ngOnInit(){

        let qParams: any = this.route.snapshot.queryParams;

        if(qParams.hasOwnProperty('code')){
            this.orderId = qParams['orderId'];
            this.code = +qParams['code'];
            this.status = (qParams['status']) ? qParams['status'].replace(/\+/g, ' ') : '';
        }
    }


}
