import {Component, OnInit,AfterViewInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AppConfigService} from "../../app.config.service";

declare var $;
declare var moment: any;

@Component({
    templateUrl: './payment-status.component.html'
})
export class PaymentStatusComponent implements OnInit,AfterViewInit{

    userSpace: any;
    statusUpdated: string;
    sellingCurency = '';
    constructor(private route: ActivatedRoute, private appConfigService: AppConfigService){
        this.sellingCurency = appConfigService.getCurrency();
    }

    ngOnInit(){
        this.userSpace = this.route.snapshot.data['userSpace'];
        this.statusUpdated = moment().format('MMMM Do YYYY');
    }

    ngAfterViewInit(){
         $( "html" ).removeClass( "o-hidden");
    }
    getTotalRate() {
        let totalRate = this.userSpace.space.ratePerHour * this.getNumberOfHours();

        this.userSpace.extraAmenityList.forEach(amenity => {
            totalRate += amenity.rate * amenity.count;
        });

        return totalRate;
    }

    getNumberOfHours() {
        let start: any = new Date(this.userSpace.fromDate);
        let end: any = new Date(this.userSpace.toDate);
        let hoursCount: any = Math.abs(end - start) / 36e5;

        return hoursCount.toFixed(2);
    }
}