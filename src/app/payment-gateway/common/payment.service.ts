import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { ServerConfigService } from "../../server.config.service";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';

@Injectable()
export class PaymentService{

    private IPG_REDIRECT_URL = "https://www.hnbpg.hnb.lk/SENTRY/PaymentGateway/Application/ReDirectLink.aspx";
    baseUrl = environment.apiHost + '/book/space';

    constructor(
        private http: Http,
        private serverConfig: ServerConfigService
    ){}

    callTo(requestBody: any){
        return this.http
            .post(this.IPG_REDIRECT_URL, requestBody)
            .map((response: Response) => {
                console.log("response.text()", response.text())
            })
            .catch(this.handleError);
    }


    bookSpace(bookingRequest){
        return this.http
            .post(this.baseUrl, JSON.stringify(bookingRequest), this.serverConfig.getJsonHeader())
            .map(resp => resp.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }

}