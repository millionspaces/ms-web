const NUMBER_OF_KEYS = 12;

export const PASSWORD: string      = "j9vA4Z05";
export const MERCHANT_ID: string   = "14481900";
export const ACQUIRED_ID: string   = "415738";
export const CURRENCY: string      = "LKR";
export const CURRENCY_CODE: string = "144";
export const SENTRY_PREFIX: string = "SENTRYORD";
export const PURCHASE_CURRENCY_EXPONENT: string = "2";
export const CAPTURE_FLAG: string = "M";
export const SIGNATURE_METHOD: string = "SHA1";

function getFormattedAmount(amount){
    if(amount)
        return pad(amount, NUMBER_OF_KEYS);
}

function pad (amt, numberOfKeys) {
    amt = amt.toString();
    return amt.length < numberOfKeys ? pad("0" + amt, numberOfKeys) : amt;
}

export { getFormattedAmount }