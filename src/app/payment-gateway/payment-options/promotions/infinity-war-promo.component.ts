import { Component, AfterViewInit } from "@angular/core";
import { PlatformLocation } from "@angular/common";

declare let $: any;

@Component({
    templateUrl: './infinity-war-promo.component.html',
    styles: [`
        h1 {
            font-size: 46px;
        }
        .wrapper-marvel {
                background-image: url(http://res.cloudinary.com/dgcojyezg/image/upload/v1521533511/offers/marvel-bg_2.jpg);
                height: 100%;
                width: 100%;
                position: fixed;
                background-size: cover;
        }  
        .marvel-text-content {
            margin-left: 5px;
            margin-top: 145px;
            position: static;
            text-align: left;
            color: #fff;
        }

        @media only screen and (min-width: 992px) {
           .marvel-text-content {
               width: 400px;
               height: 400px;
               position: absolute;
               top:  50%;
               left: 24%;
               margin-left: -200px;
               margin-top: -150px;
            }
        }
         .wrapper-marvel h1 {
            font-family: 'Berkshire Swash', cursive;
            color: #ffffff;
            margin-bottom: 30px;
            display: inline-block;
            font-weight: bolder;
         }
         .wrapper-marvel h1 + p {
            font-size: 22px;
            width: 300px;
            display: inline-block;
         }
         .wrapper-marvel a {
            background: #03a3d3;
            color: #fff;
            padding: 10px 20px;
            border-radius: 20px;
            margin-top: 20px;
            display: inline-block;
         }
    `]
})
export class InfinityWarPromoComponent implements AfterViewInit {

    constructor(private location: PlatformLocation) {
        location.onPopState(() => {
            location.forward();
            return;
        });
    }

    ngAfterViewInit() {

    }

}