import {Component} from "@angular/core";

@Component({
    templateUrl: './free-hotdesk-booking.component.html',
    styles: [`
        .wrapper-promo {
            background-image: url(http://res.cloudinary.com/dgcojyezg/image/upload/q_50/v1521533511/offers/promo-banner-bg_2.jpg);
            background-repeat: no-repeat;
            height: 100%;
            width: 100%;
            position: fixed;
            background-size: cover;
            background-position: top;
            margin-top: 50px;
        }
        
        h1 {
            color: #FFFFFF;
        }
        
        .content-wrapper {
            position: relative;
            width: 100%;
            height: 100%;
        }
        
        .promo-text-content {
            text-align: center;
            position: absolute;
            color: #fff;
            bottom: 6%;
            width: 100%;
            height: 130px;
            background: #2196F3;
            border-top-left-radius: 100%;
            border-top-right-radius: 100%;
            padding-top: 5px;
        }
    `]
})
export class FreeHotDeskBookingComponent {

}