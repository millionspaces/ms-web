import {
  Component,
  Pipe,
  PipeTransform,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  HostListener,
  OnDestroy
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RouteDataService } from '../../shared/services/route.data.service';
import { environment } from 'environments/environment';

declare const $: any;

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  templateUrl: './payment-options2.component.html',
  styles: [
    `
      .payment-wrapper {
        height: 825px;
        width: 320px;
        margin: 50px auto;
      }
      .paymentGateway {
        margin-top: 20px;
        height: 100%;
        width: 100%;
        -webkit-box-shadow: 0px 0px 13px 0px rgba(227, 227, 227, 1);
        -moz-box-shadow: 0px 0px 13px 0px rgba(227, 227, 227, 1);
        box-shadow: 0px 0px 13px 0px rgba(227, 227, 227, 1);
      }
    `
  ]
})
export class PaymentOptions2Component
  implements OnInit, AfterViewInit, OnDestroy {
  url: string;
  paymentSummaryData: any;
  paymentAPI: string;
  isLoading: boolean = true;

  @ViewChild('form') formElementRef: ElementRef;

  // handle browser refresh
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler(event: Event) {
    event.returnValue = true;
  }

  constructor(private routeDataService: RouteDataService) {}

  ngOnInit() {
    // handle page refresh => navigate to step 1
    // if (!this.bookingService.selectedDate) {
    //     this.router.navigate(['/booking/select-date']);
    // }

    this.paymentSummaryData = this.routeDataService.paymentSummaryData;
    console.log('this.paymentSummaryData', this.paymentSummaryData);

    this.paymentAPI = environment.apiPayment;
  }

  ngAfterViewInit() {
    this.formElementRef.nativeElement.submit();
    $('body').css('background-color', '#f9f9f9');
  }

  get isiOSDevice() {
    console.log('navigator.userAgent', navigator.userAgent);
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
      return 'true';
    }
    if (navigator.platform === 'MacIntel') {
      return 'true';
    }
    return 'false';
  }

  ngOnDestroy() {
    this.routeDataService.paymentSummaryData = null;
    $('body').css('background-color', '#FFF');
  }

  onLoad() {
    window.top.scrollTo(0, 0);
    this.isLoading = false;
  }
}
