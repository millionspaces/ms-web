import { Component, OnInit, HostListener, ViewChild, ElementRef } from "@angular/core";
import { CryptoService } from "../../shared/services/crypto.service";
import { Router } from "@angular/router";
import { environment } from "environments/environment";
import { RouteDataService } from "../../shared/services/route.data.service";
import { SessionStorageService } from "../../shared/services/session-storage.service";

import { BookingService } from "../../user/my-activities/booking.service";
import { ToasterService } from "../../shared/services/toastr.service";
import { UserService } from "../../user/user.service";

import * as PaymentUtils from '../common/payment-utils';

let Buffer = require('buffer').Buffer;
declare let $;
declare let moment;

@Component({
    templateUrl: './payment-options.component.html',
    styleUrls: ['./payment-options.component.css']
})
export class PaymentOptionsComponent implements OnInit{

    paymentOptions = [
        {
            id: 1,
            option: 'Ipg'
        },
        {
            id: 2,
            option: 'Online Bank Transfer'
        },
        {
            id: 3,
            option: 'Bank Account Deposit'
        },
        {
            id: 4,
            option: 'Infinity War Promo'
        },
        {
            id: 5,
            option: 'Book Now Pay Later'
        }
    ];
    selectedPaymentOption = null;
    dateDifference:any;
    userSpace:any;

    /*IPG data*/
    merID = PaymentUtils.MERCHANT_ID;
    acqID = PaymentUtils.ACQUIRED_ID;
    merRespURL = environment.apiHost + "/payment/callback";
    purchaseCurrency = PaymentUtils.CURRENCY_CODE;
    purchaseCurrencyExponent = PaymentUtils.PURCHASE_CURRENCY_EXPONENT;
    orderId = '';
    signatureMethod = PaymentUtils.SIGNATURE_METHOD;
    signature = '';
    captureFlag = environment.paymentCaptureFlag;
    purchaseAmount = 0;
    /*IPG data*/

    isLoading = false;
    showAddContactModal = false;

    @ViewChild('form') formElementRef: ElementRef;

    constructor(
        private crypto: CryptoService,
        private router: Router,
        private routeDataService: RouteDataService,
        private storageService: SessionStorageService,
        private bookingService: BookingService,
        private toaster: ToasterService,
        private userService: UserService
    ){}

    paymentSummaryData: any;
    ipgTimer : any;

    ngOnInit(){

        if(this.routeDataService.paymentSummaryData){
            this.paymentSummaryData = this.routeDataService.paymentSummaryData;
            this.storageService.removeItem('PSD'); // payment summary data
            this.storageService.addItem('PSD', this.routeDataService.paymentSummaryData); // payment summary data
        }else{
            this.paymentSummaryData = this.storageService.getItem('PSD');
        }

        //767676



        if(this.paymentSummaryData.dateDiff) {
            this.dateDifference = this.paymentSummaryData.dateDiff;
        }

        if (this.paymentSummaryData.total != 0) {
            this.selectedPaymentOption = this.paymentOptions[0];

        } else if (this.paymentSummaryData.total == 0) {
            this.selectedPaymentOption = this.paymentOptions[3];

        } else {
            this.selectedPaymentOption = this.paymentOptions[0];
        }
        this.purchaseAmount = PaymentUtils.getFormattedAmount(this.paymentSummaryData.total.replace( '.', '' ));
     }

    ngAfterViewInit(){

        this.ipgTimer = $('.ipgClock').FlipClock(1200,{
            clockFace: 'MinuteCounter',
            countdown: true,
            stop: function() {
                $('.paymentBtnOption').prop('disabled', true);
                alert("Session Timeout. Redirecting to main page.....");
                $(location).attr('href', '/#/home');
            }
        });

        $('.ipgClock').click(function (e){
            e.preventDefault();
        });

        if(this.storageService.getItem('ipgTime')){
            this.ipgTimer.setTime(this.storageService.getItem('ipgTime').value);
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    savingIpgTime($event) {
        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();
    }

    @HostListener('window:popstate', ['$event'])
    savingIpgTimePopstate($event) {
        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();
    }

    onSelectionChange(selectedPaymentOption){
        this.selectedPaymentOption = selectedPaymentOption;
    }

    proceedWithManually(){

        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();
        let { total, referenceId, bookingId } = this.paymentSummaryData;

        this.storageService.removeItem('MPD'); // manual payment data
        this.storageService.addItem('MPD', { total, referenceId, bookingId });
        this.router.navigate(['payments/offline/transfer', referenceId]);
    }

    getEncodingSignature(orderId: string, amount: string){

        amount = PaymentUtils.getFormattedAmount(amount);

        let requestBody = PaymentUtils.PASSWORD + PaymentUtils.MERCHANT_ID +
            PaymentUtils.ACQUIRED_ID + PaymentUtils.SENTRY_PREFIX + orderId +
            amount + PaymentUtils.CURRENCY_CODE;

        let shaVal = this.crypto.sha1Hex(requestBody);
        let packval = this.crypto.packHex(shaVal);
        let val64 = Buffer.from(packval, 'binary').toString('base64');

        return val64;
    }

    submitData(form){
        this.isLoading = true;

        this.signature = this.getEncodingSignature(this.paymentSummaryData.referenceId, this.paymentSummaryData.total.replace( '.', '' ));
        this.orderId = PaymentUtils.SENTRY_PREFIX + this.paymentSummaryData.referenceId;

        setTimeout(() => {
            form.submit();
        },3000)

    }

    getFormattedDate(dateString){
        return moment(dateString).format('h:00 A')
    }

    setIpgTime(){
        this.removeIpgTime();
        this.storageService.addItem('ipgTime', {"value" : ""+this.ipgTimer.getTime()});
    }

    removeIpgTime(){
        if(this.storageService.getItem('ipgTime')){
            this.storageService.removeItem('ipgTime');
        }
    }

    proceedWithPromo(){
        this.isLoading = true;
        let updateRequest = {
            booking_id: this.paymentSummaryData.bookingId,
            event: 1,
            method: 'MANUAL',
            status: 4
        }

        this.bookingService.updateBooking(updateRequest).subscribe(response => {
            if(response.old_state === 'PAYMENT_DONE' && response.new_state === 'PAYMENT_DONE'){
                this.isLoading = false;
                this.router.navigate(['payments/offers/hot-desk-booking-promo']);
            }else{
                this.toaster.warning('Something went wrong', 'Sorry!');
            }
        }, error => {
            this.toaster.error(error, "Error");
        });
    }

    proceedToConfirm() {
        this.showAddContactModal = true;
    }

    get user(){
        return this.userService.getUser();
    }

    submitContactForm(form) {
        this.isLoading = true;
        let updateUser = this.user;
        updateUser.mobileNumber = form.contactNumber;

        this.userService.updateUser(updateUser).subscribe(response => {
            if(response.status === 200) {
               this.isLoading = false;
               switch (this.selectedPaymentOption.id) {
                   case 1:
                       this.submitData(this.formElementRef.nativeElement);
                       break;
                   case 2:
                       this.proceedWithManually();
                       break;
                   case 3:
                       this.proceedWithManually();
                       break;
                   case 4:
                       this.proceedWithPromo();
                       break;
                   case 5:
                       this.proceedWithPayLater();
                       break;
               }
            }else {
                console.error(response);
                this.toaster.warning('Something went wrong', 'Sorry!');
            }
        })
    }

    proceedWithPayLater() {
        this.isLoading = true;

        const updateRequest = {
            booking_id: this.paymentSummaryData.bookingId,
            event: 1,
            method: 'MANUAL',
            status: 5
        }

        this.bookingService.updateBooking(updateRequest).subscribe(response => {
            if(response.old_state === 'INITIATED' && response.new_state === 'PENDING_PAYMENT'){
                this.isLoading = false;
                this.router.navigate(['payments/offline/pay-later']);
            }else{
                this.toaster.warning('Something went wrong', 'Sorry!');
                this.router.navigate(['home']);
            }
        }, error => {
            this.toaster.error(error, "Error");
        });
    }
}
