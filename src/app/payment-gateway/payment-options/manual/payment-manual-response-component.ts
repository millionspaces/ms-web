import {Component, AfterViewInit} from "@angular/core";
import { PlatformLocation } from "@angular/common";

declare let $:any;

@Component({
    templateUrl: './payment-manual-response-component.html',
    styles: [`
        .tentative-booking-success-wrapper {
            width: 100%;
            margin: 120px auto 0;
            text-align: center;
        }
        .tentative-booking-success-wrapper h1 {
            color: #00B409;
            font-weight: 100;
        }
        .tentative-booking-success-wrapper h1 + p {
            font-weight: 300;
        }
        @media only screen and (min-width: 768px) {
            .tentative-booking-success-wrapper {
                width: 60%;
            }
        }
        button {
            border: 1px solid #5b92f1;
            background: #5b92f1;
            color: #fff;
            padding: 10px 20px;
        }
    `]
})

export class PaymentManualResponseComponent implements AfterViewInit {

    constructor(private location: PlatformLocation){
        //user not allowed to go back
        location.onPopState(() => {
            location.forward();
            return;
        });
    }

    ngAfterViewInit() {
        $('body').css('overflow', 'hidden');
        window.scrollTo(0, 0);
    }

}