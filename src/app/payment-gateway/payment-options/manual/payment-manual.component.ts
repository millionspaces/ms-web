import {Component, OnInit, HostListener} from "@angular/core";
import { Router } from "@angular/router";
import { ToasterService } from "../../../shared/services/toastr.service";
import { SessionStorageService } from "../../../shared/services/session-storage.service";
import {BookingService} from "../../../user/my-activities/booking.service";

declare let $;

@Component({
    templateUrl: './payment-manual.component.html',
    styles: [`
        h2 {
            font-weight: 300;
        }
        p {
            font-weight: 300;
        }
        .manual-booking-info-price-tag {
            font-weight: bold;
            background: tomato;
            color: #fff;
            border-radius: 3px;
            padding: 1px 3px;
            
        }
        .manual-booking-info-table {
            border: 1px solid #e2e2e2;
            margin-bottom: 30px;
            margin-top: 30px;
            font-weight: 100;
            font-style: 22px;
            font-weight: 300;
        }
        .manual-booking-payment-info-wrapper-inner {
            width: 100%;
            margin: 80px auto 0;
        }
        @media only screen and (min-width: 768px) {
            .manual-booking-payment-info-wrapper-inner  {
                width: 60%;
            }
        }
        .table-row > div {
            display: inline-block;
            width: 50%;
        }
        .table-row {
            border-bottom: 1px solid #e2e2e2;
        }
        .table-row:nth-child(odd) {
            background: #fbfbfb;
        }
        .table-row:last-child {
            border-bottom: none;
        }
        .table-data {
            padding: 10px;
        }
        .table-data:last-child {
            border-left: 1px solid #e2e2e2;
        }
        .button-wrapper {
            text-align: right;
        }
        button {
            border: 1px solid #5b92f1;
            background: #5b92f1;
            color: #fff;
            padding: 10px 20px;
        }
        .ipgClock {
            zoom: 0.3;
            -moz-transform: scale(0.3);
        }
        /*.flip-clock-divider .flip-clock-label {*/
            /*display: none!important;*/
        /*}*/
    `]
})
export class PaymentManualComponent implements OnInit{

    bookingId: number;
    grandTotal: string;
    referenceId: string;
    accountDetails: any;
    ipgTimer : any;

    message = "You can have only one booking with a pending payment at a given time.";

    constructor(
        private router: Router,
        private bookingService: BookingService,
        private toaster: ToasterService,
        private storageService: SessionStorageService,
    ){}

    ngOnInit(){
        this.accountDetails = {
            accountHolderName: 'Millionspaces Pvt Ltd',
            accountNumber: '021910010333',
            bank: 'Sampath Bank',
            address: 'Sampath Bank PLC, World Trade Center, Unit L03/EB/01, Level 03, East Block, Echelon Square, Colombo 01, Sri Lanka',
            branch: 'City Office',
            swiftCode: 'BSAMLKLX'
        }

        let mpd = this.storageService.getItem('MPD'); // manual payment data
        if(!mpd){
            this.toaster.error("Something went wrong!", "Error");
        }else{
            this.bookingId = mpd.bookingId;
            this.grandTotal = mpd.total;
            this.referenceId = mpd.referenceId;
        }
    }

    ngAfterViewInit(){
        this.ipgTimer = $('.ipgClock').FlipClock(1200,{
            clockFace: 'MinuteCounter',
            countdown: true,
            stop: function() {
                $('#confirmBtn').prop('disabled', true);
                alert("Session Timeout. Redirecting to main page.....");
                $(location).attr('href', '/#/home');
            }
        });

        if(this.storageService.getItem('ipgTime')){
            this.ipgTimer.setTime(this.storageService.getItem('ipgTime').value);
        }

        $('.ipgClock').click(function (e){
            e.preventDefault();
        });
    }

    @HostListener('window:beforeunload', ['$event'])
    savingIpgTime($event) {
        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();
    }

    @HostListener('window:popstate', ['$event'])
    savingIpgTimePopstate($event) {
        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();
    }

    proceedWithBankTransfer(){

        this.setIpgTime();
        this.ipgTimer.timer._destroyTimer();

        let updateRequest = {
            booking_id: this.bookingId,
            event: 1,
            method: 'MANUAL',
            status: 1
        }

        console.log('updateRequest', updateRequest);

        this.bookingService.updateBooking(updateRequest).subscribe(response => {
            if(response.old_state === 'INITIATED' && response.new_state === 'PENDING_PAYMENT'){
                this.router.navigate(['payments/offline/transfer/status/success']);
            }else{
                this.toaster.warning(`${this.message}`, 'Sorry!');
            }
        }, error => {
            this.toaster.error(error, "Error");
        });

    }

    setIpgTime(){
        this.removeIpgTime();
        this.storageService.addItem('ipgTime', {"value" : ""+this.ipgTimer.getTime()});
    }

    removeIpgTime(){
        if(this.storageService.getItem('ipgTime')){
            this.storageService.removeItem('ipgTime');
        }
    }
}