import {Component} from "@angular/core";

@Component({
    template:  `
        <section class="section">
            <div class="container">
                <div class="box">
                    <span>Sorry! Your session has expired.</span>
                </div>
            </div>
        </section>
    `,
    styles: [`
        .section { 
            margin: 20% auto;
            text-align: center;
        }
        .box {
            font-size: 2rem;
            padding: 1rem;
           
        }
    `]
})
export class SessionExpiredComponent {

}