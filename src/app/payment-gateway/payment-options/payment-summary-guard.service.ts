import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {RouteDataService} from "../../shared/services/route.data.service";
import {Injectable} from "@angular/core";

@Injectable()
export class PaymentSummaryGuard implements CanActivate {

    constructor(
        private routeService: RouteDataService,
        private router: Router
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        if (this.routeService.paymentSummaryData) {
            return true;
        }
        this.router.navigate(['/']);
        return false;
    }

}