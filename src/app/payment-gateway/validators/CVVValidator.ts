import {FormControl} from "@angular/forms";


export function cvvValidator(control: FormControl ): { [s: string]: boolean }{

    if(control.value){
        let cvvNumber: string = control.value;

        if(cvvNumber.length === 3 || cvvNumber.length === 4){
            return null;
        }
        return { 'invalid': true };
    }


}