 import {FormControl} from "@angular/forms";

 export function expireValidator( control: FormControl ): { [s: string]: boolean }
 {
   if(control.value){
     var expire: string = control.value.replace(/ /g, '');
     var month: string  = "00";
     var year: string   = "00";

     if( expire.length > 0 )
     {
       // length test
       if( expire.length < 4 )
         return { 'minLength': true };

       if( expire.length == 4 )
       {
         month = expire.substr(0, 2);
         year  = expire.substr(2, 2);
       }
       else if (expire.length == 5)
       {
         month = expire.substr(0, 2);
         year  = expire.substr(3, 2);
       }

       if( !isExpireValid(month, year) )
         return { 'invalid': true };
     }
   }

 }

 export function isExpireValid(month: string, year: string): boolean
 {
   var m: number = parseInt(month);
   var y: number = parseInt(year);

   if( isNaN(m) || isNaN(y) )
     return false;

   if( m < 1 || m > 12 )
     return false;

   if( y < 0 || y > 99 )
     return false;

   var date: Date       = new Date();
   var curMonth: number = date.getMonth();
   var curYear: number  = parseInt( date.getFullYear().toString().substr(2, 2) );
   
   if( y < curYear )
     return false;
   else if( y == curYear )
     return m >= curMonth;
   else
     return true;
 }