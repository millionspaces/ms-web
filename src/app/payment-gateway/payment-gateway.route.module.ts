import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PaymentOptions2Component } from "./payment-options/payment-options2.component";
import { SessionExpiredComponent } from "./payment-options/session-expired.compoent";
import { PaymentSummaryGuard } from "./payment-options/payment-summary-guard.service";

const IPG_ROUTES = [

    {
        path: 'order/summary',
        canActivate: [PaymentSummaryGuard],
        component: PaymentOptions2Component,
    },
    {
        path: 'expired',
        component: SessionExpiredComponent
    }
   /* {
        path: 'offline/transfer/:reference',
        component: PaymentManualComponent
    },
    {
        path: 'response',
        component: PaymentResponseComponent,
    },
    {
        path: 'offline/transfer/status/success',
        component: PaymentManualResponseComponent,
    },
    {
        path: 'offers/hot-desk-booking-promo',
        component: FreeHotDeskBookingComponent,
    },
    {
        path: 'offline/pay-later',
        component: BookNowPayLaterComponent
    }*/

]


@NgModule({
    imports: [RouterModule.forChild(IPG_ROUTES)],
    exports: [RouterModule]
})
export class PaymentGatewayRouteModule{
}