import {Component} from "@angular/core";

@Component({
    templateUrl: './privacy-terms.component.html',
    styles: [`
        .privacy-content-wrapper {
            margin: 80px 40px;
             background: #fff; 
             padding: 15px;
             -webkit-box-shadow: 2px 2px 5px 2px rgba(50, 50, 50, 0.10);
            -moz-box-shadow:    2px 2px 5px 2px rgba(50, 50, 50, 0.10);
            box-shadow:         2px 2px 5px 2px rgba(50, 50, 50, 0.10);
        }
    `]
})
export class PrivacyTermsComponent {

}