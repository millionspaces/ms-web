import { Component } from "@angular/core";

@Component({
    templateUrl: './press-page.component.html',
    styles: [`
        .container-press {
            width: 90%;
            margin: 0 auto;
        }
        .press-page-wrapper {
            margin-top: 50px;
        }
        .press-page-cover {
            height: 300px;
            background: url(../../assets/img/press/press-main.jpg);
            background-size: cover;
            background-position: center;
            margin-bottom: 50px;
            text-align: center;
        }
        .press-page-cover img {
            padding-top: 60px;
            width: 100%;
            max-width: 600px;
        }
        .press-img {
            width: 100%;
            margin-bottom: 50px;
        }
        h1 {
            margin-top: 0px;
            font-size: 22px;
        }
        .container-press a {
            display: inline-block;
            margin-bottom: 50px;
            margin-top: 0px;
            font-size: 20px;
        }
        .container-press a:hover, .container-press a:active, .container-press a:visited {
            color: blue;
        }
    `]
})
export class PressPageComponent {

}