import {Component, OnInit, OnDestroy, HostListener} from '@angular/core';
import {SessionStorageService} from '../../shared/services/session-storage.service';
import {BookingFlowService} from '../booking-flow.service';
import {Router} from '@angular/router';
import {BookingService} from '../../user/my-activities/booking.service';

@Component({
    selector: 'app-select-amenities',
    templateUrl: './select-amenities.component.html',
    styleUrls: ['./select-amenities.component.css']
})
export class SelectAmenitiesComponent implements OnInit, OnDestroy {

    bookingData: any;

    amenityCost: number = 0;
    totalHoursCount: number = 0;
    perHourAmenityCost: number = 0;

    extraAmenities = [];

    selectedEvent;
    _selectedSeating;
    _selectedGuestCount;
    maxGuestCount;
    minGuestCount;

    showModal = false;
    togglePanel = true;

    // handle browser refresh
    @HostListener("window:beforeunload", ["$event"])
    unloadHandler(event: Event) {
        event.returnValue = true;
    }

    constructor(
        private storageService: SessionStorageService,
        private bookingService: BookingFlowService,
        private booking_service: BookingService,
        private router: Router
    ) {
    }


    ngOnInit() {

        // handle page refresh => navigate to step 1
        if (!this.bookingService.selectedDate) {
            this.router.navigate(['/booking/select-date']);
        }

        this.bookingData = this.storageService.getItem('b_data');
        const extraAmenities = this.bookingService.extraAmenities;

        // bind max guest count
        this.maxGuestCount = this.bookingData.participantCount;
        // bind min guest count
        this.minGuestCount = this.bookingData.minGuestCount;

        // bind guest count first time for service. otherwise total will be NAN for per guest base charges
        this.bookingService.guestCount = (this.bookingService.guestCount) ? this.bookingService.guestCount : this.bookingData.participantCount;

        // initialize the input fields, refill if exsits
        this.selectedGuestCount = (this.bookingService.guestCount) ? this.bookingService.guestCount : this.bookingData.participantCount;
        this.selectedEvent = (this.bookingService.selectedEventType) ? this.bookingService.selectedEventType : this.bookingData.eventTypes[0].id;
        this.selectedSeating = (this.bookingService.selectedSeating) ? this.bookingService.selectedSeating : this.bookingData.seatingTypes[0].id;

        // remove applied discount amount (promo code)
        this.bookingService.discountAmount = 0;

        if (extraAmenities && extraAmenities.length > 0) {
            this.extraAmenities = extraAmenities;
            this.reCalculatePerHourAmenitiesCost();
            return;
        }

        this.extraAmenities = this.bookingData.extraAmenities;
        this.bookingService.extraAmenities = this.bookingData.extraAmenities;
    }

    getAmenityUnitName(unitId) {
        let name = '';
        switch (unitId) {
            case 1:
                name = 'Per hour';
                break;
            case 2:
                name = 'Per person';
                break;
            case 3:
                name = 'Per unit';
                break;
            default:
                name = 'Per unit';
                break;
        }
        return name;
    }

    calculateAmenityCost(exAmenity) {

        if (exAmenity.amenityUnit === 1) {
            if (exAmenity.selected) {
                return exAmenity.rate * this.bookingService.numberOfHours;
            } else {
                return 0;
            }
        }

        return exAmenity.count * exAmenity.rate;
    }

    increaseAmenityCount(exAmenity) {

        console.info('amenity', exAmenity);
        exAmenity.count++;

        // per person or per unit
        if (exAmenity.amenityUnit === 2 || exAmenity.amenityUnit === 3) {
            this.bookingService.amenityCost += exAmenity.rate;
        }
    }

    decreaseAmenityCount(exAmenity) {

        if (exAmenity.count === 0) {
            return;
        }
        exAmenity.count--;
        // per person
        if (exAmenity.amenityUnit === 2 || exAmenity.amenityUnit === 3) {
            this.bookingService.amenityCost -= exAmenity.rate;
        }
    }

    onChangeAmenity(exAmenity) {

        exAmenity.selected = exAmenity.selected || null;

        if (exAmenity.selected) {
            exAmenity.selected = false;
            this.bookingService.perHourAmenityCost -= exAmenity.rate * this.bookingService.numberOfHours;
        } else {
            exAmenity.selected = true;
            this.bookingService.perHourAmenityCost += exAmenity.rate * this.bookingService.numberOfHours;
        }
    }

    get totalCharge() {
        return this.bookingService.totalCharge;
    }

    get numberOfHours() {
        return this.bookingService.numberOfHours;
    }

    reCalculatePerHourAmenitiesCost() {
        const perHourAmenityCost = this.extraAmenities
            .filter(amenity => amenity.selected)
            .map(amenity => amenity.rate * this.bookingService.numberOfHours)
            .reduce((total, charge) => total + charge, 0);

        this.bookingService.perHourAmenityCost = perHourAmenityCost;
    }

    continueBooking() {
        if (!this.selectedGuestCount || !this.selectedSeating || !this.selectedEvent) {
            this.showModal = true;
            return;
        }

        const bookingChargeRequest = {
            space: this.bookingData.spaceId,
            eventType: this.selectedEvent,
            guestCount: this.selectedGuestCount,
            advanceOnly: (this.bookingData.advanceOnlyEnable == 1) ? true : false,
            dates: this.getSelectedTimePeriod(),
            bookingWithExtraAmenityDtoSet: this.getSelectedAmenityUnits()
        }

        try {
            this.booking_service.getBookingCharge(bookingChargeRequest)
                .subscribe(resp => {
                    console.info('resp', resp);
                    this.bookingService.paymentSummaryData = resp;
                    this.router.navigate(['/booking/summary']);
                }, error => {
                    this.bookingService.paymentSummaryData = null;
                })

        } catch (error) {
            console.error(error);
        }

        console.info('bookingChargeRequest', bookingChargeRequest);

        // this.router.navigate(['/booking/summary'])
    }

    ngOnDestroy() {
        this.bookingService.selectedEventType = this.selectedEvent;
        this.bookingService.selectedSeating = this.selectedSeating;
        this.bookingService.guestCount = this.selectedGuestCount;
    }

    getSelectedTimePeriod() {
        const chosenBlocks = this.bookingService.dayAvailabilityBlocks
            .filter(block => block.chosen)
            .map(block => {
                return {
                    fromDate: this.bookingService.selectedDate.date + 'T' + block.from + ':' + '00Z',
                    toDate: this.bookingService.selectedDate.date + 'T' + block.to + ':' + '00Z'
                };
            })

        return chosenBlocks;
    }

    getSelectedAmenityUnits() {

        const selectedPerHourAmenities = this.bookingService.extraAmenities
            .filter(amenity => amenity.amenityUnit === 1 && amenity.selected)
            .map(amenity => {
                return {
                    amenityId: amenity.amenity.id,
                    number: this.bookingService.numberOfHours
                };
            })

        const selectedOtherAmenities = this.bookingService.extraAmenities
            .filter(amenity => amenity.amenityUnit !== 1 && amenity.count > 0)
            .map(amenity => {
                return {
                    amenityId: amenity.amenity.id,
                    number: amenity.count
                };
            })

        return selectedPerHourAmenities.concat(selectedOtherAmenities);
    }

    get selectedSeating() {
        return this._selectedSeating;
    }

    set selectedSeating(seating) {
        this._selectedSeating = +seating;
        this.selectSeating(+seating);
    }

    get selectedGuestCount() {
        return this._selectedGuestCount;
    }

    set selectedGuestCount(guestCount) {
        this._selectedGuestCount = +guestCount;
        this.bookingService.guestCount = +guestCount;
    }

    selectSeating(seatingID: number) {
        let seating = this.bookingData.seatingTypes.find(item => item.id === seatingID);

        if (this.selectedGuestCount > seating.participantCount)
            this.selectedGuestCount = seating.participantCount;

        this.maxGuestCount = seating.participantCount;
    }



}
