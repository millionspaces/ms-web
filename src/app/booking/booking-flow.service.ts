import { Injectable } from '@angular/core';

@Injectable()
export class BookingFlowService {

  blockChargeType: number;

  spaceCharge: number = 0;
  amenityCost: number = 0;
  perHourAmenityCost: number = 0;
  discountAmount: number = 0;

  selectedDate = null;
  dayAvailabilityBlocks = [];
  numberOfHours: number = 0;

  extraAmenities = [];

  selectedEventType: number;
  selectedSeating: number;
  guestCount: number;
  promoCode: string;

  paymentSummaryData: any;

  constructor() { }

  get totalCharge() {
    /*console.info('this.spaceCharge', this.spaceCharge);
    console.info('this.amenityCost', this.amenityCost);
    console.info('this.perHourAmenityCost', this.perHourAmenityCost);
    console.info('this.discountAmount', this.discountAmount);
    console.info('this.guestCount', this.guestCount);*/
    return ((this.blockChargeType === 2 ? this.guestCount * this.spaceCharge : this.spaceCharge) + this.amenityCost + this.perHourAmenityCost) - this.discountAmount;
  }

}
