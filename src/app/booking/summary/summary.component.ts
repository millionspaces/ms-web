import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { BookingFlowService } from '../booking-flow.service';
import { Router } from '@angular/router';
import { SessionStorageService } from '../../shared/services/session-storage.service';
import { SpaceService } from '../../spaces/shared/space.service';
import { MSError } from '../../common/errors/ms.error';
import { BookingService } from '../../user/my-activities/booking.service';
import { UserService } from '../../user/user.service';
import { RouteDataService } from '../../shared/services/route.data.service';
import 'rxjs/add/operator/filter';

declare let moment: any;
declare let $: any;
declare let fbq: any;

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit, AfterViewInit {
  bookingData: any;

  validPromo = false;
  promocodeApplied = false;

  showContactModal = false;
  showBalancePayable = false;

  showErrorModal = false;
  bookingErrorMessage = '';

  togglePanel = true;

  // handle browser refresh
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler(event: Event) {
    event.returnValue = true;
  }

  constructor(
    private bookingService: BookingFlowService,
    private booking_service: BookingService,
    private router: Router,
    private storageService: SessionStorageService,
    private spaceService: SpaceService,
    private userService: UserService,
    private routeDataService: RouteDataService
  ) {}

  ngAfterViewInit() {
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      // hide footer on promo input is hover
      $('#promo-input').focus(function() {
        $('.payment-flow-footer').hide();
      });

      $('#promo-input').focusout(function() {
        $('.payment-flow-footer').show();
      });
    }
  }

  ngOnInit() {
    // handle page refresh => navigate to step 1
    if (!this.bookingService.selectedDate) {
      this.router.navigate(['/booking/select-date']);
    }

    this.bookingData = this.storageService.getItem('b_data');
  }

  verifyPromo(form) {
    this.bookingService.discountAmount = 0;

    let payload = {
      space: this.bookingData.spaceId,
      promoCode: form.promoCode
    };

    this.spaceService.verifyPromoCode(payload).subscribe(resp => {
      this.promocodeApplied = true;

      if (resp.status === 1) {
        //promo code is valid

        this.bookingService.promoCode = form.promoCode;

        this.validPromo = true;

        const flatDiscount = resp.promoDetails.isFlatDiscount;
        let discountAmount = 0;

        if (flatDiscount === 0) {
          discountAmount = Math.floor(
            this.bookingService.totalCharge * (resp.promoDetails.discount / 100)
          );
        } else if (flatDiscount === 1) {
          discountAmount = this.getFlatDiscountAmount(
            resp.promoDetails.discount
          );
        }
        this.bookingService.discountAmount = discountAmount;
      } else if (resp.status === 2) {
        this.validPromo = false;
        this.bookingService.discountAmount = 0;
      } else if (resp.status === 3) {
        this.validPromo = false;
        this.bookingService.discountAmount = 0;
      }
    });
  }

  getFlatDiscountAmount(discountAmount: number) {
    let calAmount = 0;
    let totalAmount = this.bookingService.totalCharge;

    if (totalAmount < discountAmount) {
      calAmount = totalAmount;
    } else {
      calAmount = discountAmount;
    }
    return calAmount;
  }

  continueBooking() {
    try {
      const bookingRequest = {
        space: this.bookingData.spaceId,
        eventType: +this.bookingService.selectedEventType,
        promoCode: this.bookingService.promoCode,
        guestCount: this.bookingService.guestCount,
        bookingCharge: this.showBalancePayable
          ? this.advancePayment
          : this.totalCost,
        seatingArrangement: +this.bookingService.selectedSeating,
        dates: this.getSelectedTimePeriod(),
        bookingWithExtraAmenityDtoSet: this.getSelectedAmenityUnits(),
        advanceOnly: this.showBalancePayable ? true : false
      };

      const paymentSummaryData = {
        bookingId: null,
        referenceId: null,
        space: this.bookingData.spaceName,
        organization: this.bookingData.orgName,
        address: this.bookingData.address,
        date: moment(new Date(this.bookingService.selectedDate.date)).format(
          'dddd Do MMMM YYYY'
        ),
        reservationTime: this.getReservationTime(),
        total: (this.showBalancePayable
          ? this.advancePayment
          : this.totalCost
        ).toFixed(2)
      };

      console.info('bookingRequest', bookingRequest);
      console.info('paymentSummaryData', paymentSummaryData);

      this.bookNow(bookingRequest, paymentSummaryData);
    } catch (error) {
      console.error('error', error);
    }
  }

  bookNow(bookingRequest: any, paymentSummaryData: any) {
    this.booking_service.bookSpace(bookingRequest).subscribe(
      response => {
        if (response.id) {
          if (
            response.bookingCharge === 0 &&
            response.promotion &&
            response.promotion.type === 'NORMAL'
          ) {
            this.promotionPay(response.id).subscribe(res => {
              if (
                res.old_state === 'INITIATED' &&
                res.new_state === 'PAYMENT_DONE'
              ) {
                this.router.navigate(['booking/promoted/success']);
                return;
              } else {
                alert('Something went wrong');
                this.router.navigate(['home']);
                return;
              }
            });
          }

          paymentSummaryData.bookingId = response.id;
          paymentSummaryData.referenceId = response.orderId;
          paymentSummaryData.payLaterEnabled = response.isPayLaterEnabled;
          paymentSummaryData.manualPaymentAllowed =
            response.isBookingTimeEligibleForManualPayment;
          paymentSummaryData.promotion = response.promotion
            ? JSON.stringify(response.promotion)
            : null;

          this.routeDataService.clearData();
          this.routeDataService.paymentSummaryData = paymentSummaryData;

          this.showContactModal = false;

          this.router.navigate(['payments/order/summary']);
        } else if (response.error) {
          console.error(response.error);
          this.showContactModal = false;
          this.showErrorModal = true;
          this.bookingErrorMessage = response.error;
        } else {
          console.error('ERROR');
        }
      },
      (error: MSError) => {
        console.error('error', error);
      }
    );
  }

  getSelectedTimePeriod() {
    const chosenBlocks = this.bookingService.dayAvailabilityBlocks
      .filter(block => block.chosen)
      .map(block => {
        return {
          fromDate:
            this.bookingService.selectedDate.date +
            'T' +
            block.from +
            ':' +
            '00Z',
          toDate:
            this.bookingService.selectedDate.date + 'T' + block.to + ':' + '00Z'
        };
      });

    return chosenBlocks;
  }

  getSelectedAmenityUnits() {
    const selectedPerHourAmenities = this.bookingService.extraAmenities
      .filter(amenity => amenity.amenityUnit === 1 && amenity.selected)
      .map(amenity => {
        return {
          amenityId: amenity.amenity.id,
          number: this.bookingService.numberOfHours
        };
      });

    const selectedOtherAmenities = this.bookingService.extraAmenities
      .filter(amenity => amenity.amenityUnit !== 1 && amenity.count > 0)
      .map(amenity => {
        return {
          amenityId: amenity.amenity.id,
          number: amenity.count
        };
      });

    return selectedPerHourAmenities.concat(selectedOtherAmenities);
  }

  getReservationTime() {
    return this.bookingService.dayAvailabilityBlocks
      .filter(block => block.chosen)
      .map(
        block =>
          this.get12HourTime(+block.from) + '-' + this.get12HourTime(+block.to)
      );
  }

  get12HourTime(value) {
    if (value === 12) return '12 PM';
    return value >= 12 ? value - 12 + ' PM' : value + ' AM';
  }

  submitContactForm(form) {
    let updateUser = this.user;

    try {
      // if user doesn't have an mobile number or new number is different from previous
      if (
        !updateUser.mobileNumber ||
        updateUser.mobileNumber != form.contactNumber
      ) {
        // assign new number and save
        updateUser.mobileNumber = form.contactNumber;

        this.userService.updateUser(updateUser).subscribe(response => {
          if (response.status === 200) {
            this.continueBooking();
          } else {
            alert('Something went wrong!');
          }
        });
      } else {
        fbq('track', 'InitiateCheckout');
        this.continueBooking();
      }
    } catch (error) {
      console.error('error', error);
    }
  }

  continueToPayment() {
    // now host can book their own spaces
    /*if (this.bookingData.ownerId === this.userService.getUser().id) {
      alert('You can not request your own spaces');
      return;
    }*/
    this.showContactModal = true;
  }

  toggleAdvancePayment() {
    this.showBalancePayable = !this.showBalancePayable;
  }

  get spaceCost() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.slotCharge
      : 0;
  }

  get amenityCost() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.extraAmenityCharge
      : 0;
  }

  get spaceDiscount() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.discount
      : 0;
  }

  get totalCost() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.charge -
          this.bookingService.discountAmount
      : 0;
  }

  get user() {
    return this.userService.getUser();
  }

  get advanceOnlyEnable() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.advanceOnlyEnable
      : 0;
  }

  get advancePayment() {
    return this.bookingService.paymentSummaryData
      ? this.bookingService.paymentSummaryData.payableAdvance
      : 0;
  }

  get discountAmount() {
    return this.bookingService.discountAmount;
  }

  promotionPay(bookingId: number) {
    return this.booking_service.updateBooking({
      booking_id: bookingId,
      event: 1,
      method: 'MANUAL',
      status: 4
    });
  }
}
