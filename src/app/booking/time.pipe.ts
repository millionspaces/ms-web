import {PipeTransform, Pipe} from "@angular/core";

@Pipe({
    name: 'formatTime'
})
export class TimeFormat implements PipeTransform {
    transform(value:any):any {
        if (+value === 12) return '12 PM';
        return (+value >= 12) ? (value - 12)  + ' PM' : value + ' AM'
    }
}