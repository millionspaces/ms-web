import {Component} from '@angular/core';

@Component({
    templateUrl: './promotion-page.component.html',
    styles: [`
        
        .promotion-wrapper {
            width: 50%;
            margin: 10% auto;
            background: #f7f7f7;
        }
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .container {
            text-align: center;
            padding: 2px 16px;
        }
        
        .card-wrapper {
            text-align: center;
            padding: 2rem;
        }
        
        .card-wrapper .title {
            color: #0d6632;
            font-size: 2.5rem;
        }
        @media screen and (max-width: 768px) {
            .promotion-wrapper {
                width: 90%;
                margin: 40% auto;
            }
            .card-wrapper .title {
                font-size: 2rem;
            }

        }
    
    `]
})
export class PromotionPageComponent {

    constructor() {}





}