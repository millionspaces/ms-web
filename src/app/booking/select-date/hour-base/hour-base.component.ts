import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { BookingFlowService } from "../../booking-flow.service";

declare let moment: any;

@Component({
  selector: 'app-hour-base',
  templateUrl: './hour-base.component.html',
  styleUrls: ['./hour-base.component.css', '../../spinner.css']
})
export class HourBaseComponent implements OnInit, OnChanges {

  @Input('selectedDate') selectedDate;
  @Input('bookingData') bookingData;

  spaceAvailability: any;
  dayAvailabilityBlocks = [];

  spaceFutureBookingDates = [];
  spaceId: number;

  takenBlocks = [];

  blocksLoading = false;
  toggleLoadMoreBlock = false;

  displayBlockCount = 5;

  @Output('spaceCharge') spaceCharge = new EventEmitter<number>();
  @Output('fullBookedDates') fullBookedDates = new EventEmitter<Array<any>>();

  constructor(private bookingService: BookingFlowService) { }

  ngOnInit() {

    // create 7 days availability
    this.spaceAvailability = this.bookingData.availability
        .map(dayAvailability => {
          return {
            day: (dayAvailability.day === 7) ? 0 : dayAvailability.day,
            from: +dayAvailability.from.split(':')[0],
            to: +dayAvailability.to.split(':')[0],
            charge: +dayAvailability.charge,
          }
        });

    // create future booked dates
    const blockTimesForPeriod = [];
    this.bookingData.futureBookingDates.forEach(date => {
      blockTimesForPeriod.push(this.getBlockTimeRange(
          moment(date.from).format('YYYY-MM-DDTHH:00'),
          moment(date.to).format('YYYY-MM-DDTHH:00'))
      )
      // merge blockTimesForPeriod
      this.spaceFutureBookingDates = (blockTimesForPeriod.reduce((a,b) => a.concat(b) , []))
          .reduce((a,b) => a.concat(b), []);
    });

    //get spaceId
    this.spaceId = this.bookingData.spaceId;

    // console.info('fbd', this.spaceFutureBookingDates);

    // collect => select-date - refill availability
    if (this.bookingService.selectedDate) {
      this.selectedDate = this.bookingService.selectedDate;

      if (this.bookingService.dayAvailabilityBlocks.length > 0) {
        this.dayAvailabilityBlocks = this.bookingService.dayAvailabilityBlocks;
      }
    }

    // set full day event dates for calendar dots
    const fullBookedDates = this.getCalendarFulldayBlocks(this.spaceFutureBookingDates);
    this.fullBookedDates.emit(fullBookedDates);

  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['selectedDate'] || !changes['selectedDate'].currentValue) return;

    // clear existing day blocks and set new blocks for a new day
    this.blocksLoading = true;
    this.dayAvailabilityBlocks = [];
    this.takenBlocks = [];
    this.spaceCharge.emit(0);

    setTimeout(() => {
      this.setDayAvailability(changes['selectedDate'].currentValue);
      this.blocksLoading = false;
    }, 200)
  }

  setDayAvailability(selectedDate: any) {

    const dayAvailability = this.spaceAvailability.find(availability => availability.day === selectedDate.day);

    console.info('dayAvailability', dayAvailability)

    // create all 1 hour blocks from day availability
    for (let i = dayAvailability.from; i < dayAvailability.to; i++) {
      this.dayAvailabilityBlocks.push({
        date: selectedDate.date,
        day: dayAvailability.day,
        charge: dayAvailability.charge,
        from: i,
        to: i+1,
        blocked: false,
        chosen: false
      })
    }

    //console.info('this.dayAvailabilityBlocks', this.dayAvailabilityBlocks)

    this.tempBlockSameDayPast();

    // disabled matching blocks based on future booking dates
    if (this.spaceFutureBookingDates.length > 0) {
      this.spaceFutureBookingDates.forEach(bookedDate => {
        if (bookedDate.date === selectedDate.date) {
          this.dayAvailabilityBlocks.forEach(dayAvailabilityBlock => {
            if (dayAvailabilityBlock.from >= bookedDate.from && dayAvailabilityBlock.to <= bookedDate.to) {
              dayAvailabilityBlock.blocked = true;
            }
            else if (dayAvailabilityBlock.to > bookedDate.from && dayAvailabilityBlock.from < bookedDate.to) {
              dayAvailabilityBlock.blocked = true;
            }
          });
        }
      });
    }

    this.bookingService.dayAvailabilityBlocks = this.dayAvailabilityBlocks;

  }

    tempBlockSameDayPast() {
        this.dayAvailabilityBlocks.forEach(block => {
            if (moment(block.date).format('YYYY-MM-DD') === moment(new Date()).format('YYYY-MM-DD')) {
                const hours = new Date().getHours();
                if (hours >= block.from || hours >= block.to) {
                    block.blocked = true;
                }
            }
        })
    }

  toggleBlock(selectedBlock) {

      this.dayAvailabilityBlocks
          .forEach(block => {
              if (block.from === selectedBlock.from && block.to === selectedBlock.to) {
                  block.chosen = !block.chosen;
              }
          })

      // find chosen blocks
      const chosenBlocks = this.dayAvailabilityBlocks.filter(block => block.chosen === true)

      const total = chosenBlocks
          .map(block => block.charge)
          .reduce((total, charge) => total + charge, 0);

      // console.info('total', total)
      this.bookingService.dayAvailabilityBlocks = this.dayAvailabilityBlocks;
      this.bookingService.numberOfHours = chosenBlocks.length;
      this.spaceCharge.emit(total);
  }

  getBlockTimeRange(fromDateTime: any, toDateTime: any) { // fromDateTime => 2018-09-26T14:00, toDateTime => 2018-09-27T23:00
    const fromDate = moment(fromDateTime).format('YYYY-MM-DD'); // 2018-09-26
    const endDate = moment(toDateTime).format('YYYY-MM-DD'); // 2018-09-27

    const fromHour = +moment(fromDateTime).format('HH'); // 14
    const endHour = +moment(toDateTime).format('HH'); // 23

    const blockTimes = [];

    // same day
    if (fromDate === endDate) {
      blockTimes.push({
        date: fromDate,
        from: fromHour,
        to: endHour
      })
    }

    // not same day
    else {
      const allDates = this.getEnumerateDaysBetweenDates(fromDate, endDate);
      allDates[0].from = fromHour;
      allDates[allDates.length -1].to = endHour;
      blockTimes.push(allDates);
    }

    return blockTimes;
  }

  // returns an array of dates between the two dates
  getEnumerateDaysBetweenDates(startDate, endDate) {
    startDate = moment(startDate);
    endDate = moment(endDate);

    var now = startDate, dates = [];

    while (now.isBefore(endDate) || now.isSame(endDate)) {
      dates.push({ date: now.format('YYYY-MM-DD'), from: 0, to: 24});
      now.add(1, 'days');
    }
    return dates;
  }

  getCalendarFulldayBlocks(futureBookingDates: Array<any>) {
    return futureBookingDates
        .filter(futureBooking => futureBooking.from === 0 && futureBooking.to === 24)
        .map(futureBooking => { return { date: futureBooking.date} })
  }

  toggleLoadMore() {
    if (this.toggleLoadMoreBlock) {
      this.toggleLoadMoreBlock = false;
      this.displayBlockCount = 5;
    } else {
      this.toggleLoadMoreBlock = true;
      this.displayBlockCount = this.dayAvailabilityBlocks.length;
    }
  }


}
