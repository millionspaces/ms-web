import {Component, OnInit, HostListener, OnDestroy} from '@angular/core';
import { RouteDataService } from "../../shared/services/route.data.service";
import { SessionStorageService } from "../../shared/services/session-storage.service";
import { SpaceService } from "../../spaces/shared/space.service";
import { Router } from "@angular/router";
import { BookingFlowService } from "../booking-flow.service";

@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.css']
})
export class SelectDateComponent implements OnInit, OnDestroy {

  bookingData: any;
  eventDaysCalendar: any;

  // selected date
  selectedDate: any;

  chosenBlocks = [];

  showModal = false;
  isWithinNotice = false;

  fullBookedDates = [];


  constructor(
      private routeDataService: RouteDataService,
      private storageService: SessionStorageService,
      private spaceService: SpaceService,
      private router: Router,
      private bookingService: BookingFlowService
  ) { }

    // handle browser refresh
    @HostListener("window:beforeunload", ["$event"])
    unloadHandler(event: Event) {
        event.returnValue = true;
    }

  ngOnInit() {

      const bookingData = this.storageService.getItem('b_data');

      if (bookingData) {
          this.bookingData = bookingData;
      } else {
          this.bookingData = this.routeDataService.bookingData;
      }

    this.bookingService.blockChargeType = +this.bookingData.blockChargeType;
    this.storageService.addItem('b_data', this.bookingData);
  }

  setSelectedDate(selectedDate) {
    this.selectedDate = selectedDate;
    this.bookingService.selectedDate = selectedDate;
    this.storageService.addItem('s_date', this.selectedDate);
  }

  setSpaceCharge(spaceCharge) {
    this.bookingService.spaceCharge = spaceCharge;
  }

  continueBooking() {

    const chosenBlocks = this.bookingService.dayAvailabilityBlocks
        .filter(block => block.chosen)
        .map(block => {
          return {
              fromDate: this.bookingService.selectedDate.date + 'T' +  block.from + ':' +'00Z',
              toDate: this.bookingService.selectedDate.date + 'T' +  block.to + ':' +'00Z'
          }
        })

     const availabilityRequest = {
       space: this.bookingData.spaceId,
       dates: chosenBlocks
     }

    // console.info('availabilityRequest', availabilityRequest);

    this.spaceService.getSpaceAvailablility(availabilityRequest)
        .subscribe(availability => {
          if (availability.response) {
            this.isWithinNotice = false;
            this.router.navigate(['/booking/collect'])
          }
          else {
            this.isWithinNotice = true;
            this.showModal = true;
          }
        });
  }

  get spaceCharge() {
    return this.bookingService.spaceCharge;
  }

    setFullBookedDates(fullBookedDates) {
      this.fullBookedDates = fullBookedDates;
    }

    clearPaymentSummaryData() {
        if (this.routeDataService.paymentSummaryData) {
            this.routeDataService.paymentSummaryData = null;
        }
    }


    ngOnDestroy() {
      this.clearPaymentSummaryData();
    }


}
