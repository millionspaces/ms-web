import {PipeTransform, Pipe} from "@angular/core";

@Pipe({
    name: 'filterBlock',
    pure: false
})
export class FilterBlocksPipe implements PipeTransform {
    transform(items: any[], numberOfBlocks: string): any {

        if (items.length === 0) return [];

        return items.slice(0, (+numberOfBlocks + 1))

    }
}