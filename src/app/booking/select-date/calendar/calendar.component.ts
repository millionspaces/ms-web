import { Component, AfterViewInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit } from "@angular/core";
import { SessionStorageService } from "../../../shared/services/session-storage.service";

declare let clndr: any;
declare let moment: any;
declare let $: any;

@Component({
  selector: 'booking-calendar',
  template: `
        <div class="book-cal"></div>
        `,
  styles: [`
        :host >>> .clndr-controls {
            font-size: 20px;
            padding-top: 20px;
        }
        :host >>> .clndr-controls {
            font-size: 14px;
            margin-bottom: 20px;
        }
        :host >>> .clndr-control-button, :host >>> .clndr-control-button {
            display: inline-block;
            width: 22%;
        }
        :host >>> .month {
          display: inline-block;
          width: 52%;
        }
        /*:host >>> .month {*/
            /*text-align: center;*/
        /*}*/
        /*:host >>> .clndr-next-btn-wrapper {*/
            /*text-align: right;*/
        /*}*/
        :host >>> .header-days {
            /*display: inline-block;*/
        }
        :host >>> .header-days > div {
            display: inline-block;
            width: 14.2%;
        }
        :host >>> .clndr-weeks-wrapper > div {

        }
        :host >>> .clndr-weeks-wrapper > div > div {
            display: inline-block;
            width: 14.2%;
            height: 50px;
        }
        :host >>> .book-cal {
            text-align: center;
            background: #ffffff;
            margin: 10px;
            border: 1px solid #e2e2e2;
        }
        :host >>> .day-contents {
            display: table;
            margin: 0 auto;
            height: 100%;
        }
        :host >>> .day-cell {
            display: table-cell;
            vertical-align: middle;
            height: 100%;
            position: relative;
            width: 15px;
        }
        :host >>> .adjacent-month {
            color: #e2e2e2;
        }
        :host >>> .day-contents {
            cursor: pointer;
        }
        :host >>> .month {
            font-size: 16px;
        }
        :host >>> .clndr-next-button, :host >>> .clndr-previous-button {
            cursor: pointer;
            font-size: 22px;
            padding: 10px 20px;
        }
        :host >>> .event > div > div > span {
            position: absolute;
            background: #388fff;
            bottom: 7px;
            left: 7px;
            width: 5px;
            height: 5px;
            display: inline-block;
            border-radius: 50%;
        }
        :host >>> .today {
            border-radius: 50%;
            color: #388fff;
            width: 49px;
            height: 40px;
            font-weight: bold;
        }

        /*:host >>> .today.event > div > div > span {*/
            /*left: 23px;*/
        /*}*/

        :host >>> .inactive {
            color: #989898;
        }
        :host >>> .selected {
            background: #FFDD57;
            border-radius: 10px;
        }
        :host >>> .past {
            color: #989898;
            background: transparent;
        }
        

    `]
})
export class CalendarComponent implements AfterViewInit, OnChanges, OnInit {

  bookingCalendar: any;

  @Input('fullBookedDates') fullBookedDates;
  @Input('calendarEnd') calendarEnd;
  @Output('selectedDate') selectedDate: EventEmitter<any> = new EventEmitter<any>();

  todayDate;

  clndrTemplate = `
        <div class='clndr-controls'>
            <div class='clndr-control-button'>
                <span class='clndr-previous-button'><i class="fa fa-angle-left" aria-hidden="true"></i></span>
            </div
            ><div class='month'><%= month %> <%= year %></div
            ><div class='clndr-control-button clndr-next-btn-wrapper'>
                <span class='clndr-next-button'><i class="fa fa-angle-right" aria-hidden="true"></i></span>
            </div>
        </div>
        <div>
            <div>
                <div class='header-days'><% for(var i = 0; i < daysOfTheWeek.length; i++) { %><div class='header-day'><%= daysOfTheWeek[i] %></div><% } %></div>
            </div>
            <div class="clndr-weeks-wrapper"><% for(var i = 0; i < numberOfRows; i++){ %><div><% for(var j = 0; j < 7; j++){ %><% var d = j + i * 7; %><div class='<%= days[d].classes %>'><div class='day-contents'><div class="day-cell"><%= days[d].day %><span></span></div></div></div><% } %></div><% } %></div>
        </div>
    `

  constructor(private storageService: SessionStorageService) {}

  ngOnInit() {
    this.todayDate = moment();
  }

  ngOnChanges(changes: SimpleChanges) {
      if (!changes['fullBookedDates'] || !changes['fullBookedDates'].currentValue) return;

      if (this.bookingCalendar) {
          this.bookingCalendar.setEvents(changes['fullBookedDates'].currentValue);
      }

  }

  ngAfterViewInit(){

    this.bookingCalendar = (<any>$('.book-cal')).clndr({
      constraints: {
        startDate: moment().format('YYYY-MM-DD'),
        endDate: this.calendarEnd,
      },
      events: this.fullBookedDates,
      template: this.clndrTemplate,
      trackSelectedDate: true,
      daysOfTheWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
      weekOffset: 1,
      selectedDate: this.initializedDay,
      ignoreInactiveDaysInSelection: true,
      classes: {
        past: "past",
        today: "today",
        event: "event",
        inactive: "inactive",
        selected: "selected",
        lastMonth: "last-month",
        nextMonth: "next-month",
        adjacentMonth: "adjacent-month"
      },
      clickEvents: {
        click:  (target) => {
          // ignore event from inactive cells
          if ($(target.element).hasClass('inactive')) {
            return;
          }
          this.selectedDate.emit({ day: target.date.weekday(), date: moment(target.date).format('YYYY-MM-DD') });
           // time-block-wrapper
            $('html, body').animate({
                scrollTop: $('.time-block-wrapper').offset().top
            }, 1000);
        }
      },
      showAdjacentMonths: true,
      adjacentDaysChangeMonth: false
    });

    this.bookingCalendar.setYear(this.initializedYear);
    this.bookingCalendar.setMonth(this.initializedMonth);

  }

  get initializedDay() {

    if (this.storageService.getItem('s_date')) {
      return this.storageService.getItem('s_date').date; // YYYY-MM-DD
    }

    return null;
  }

  get initializedYear() {

    if (this.storageService.getItem('s_date')) {
      return +this.storageService.getItem('s_date').date.split('-')[0]; // YYYY
    }

    return this.todayDate.year();
  }

  get initializedMonth() {

    if (this.storageService.getItem('s_date')) {
      return +this.storageService.getItem('s_date').date.split('-')[1] - 1; // MM
    }

    return this.todayDate.month();
  }
}
