import { NgModule } from '@angular/core';
import { SelectDateComponent } from './select-date/select-date.component';
import { CalendarComponent } from './select-date/calendar/calendar.component';
import {Routes, RouterModule} from "@angular/router";
import { HourBaseComponent } from './select-date/hour-base/hour-base.component';
import { BlockBaseComponent } from './select-date/block-base/block-base.component';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import { SelectAmenitiesComponent } from './select-amenities/select-amenities.component';
import {BookingFlowService} from "./booking-flow.service";
import { SummaryComponent } from './summary/summary.component';
import {TimeFormat} from "./time.pipe";
import {SharedModule} from "../shared/shared.module";
import {BookingGuard} from "./booking-guard";
import {PromotionPageComponent} from "./promotions/promotion-page.component";
import {BookingPageComponent} from "./booking-page/booking-page.component";

const BOOKING_ROUTES: Routes = [
  {
    path: 'booking/select-date',
    component: SelectDateComponent
  },
  {
    path: 'booking/collect',
    component: SelectAmenitiesComponent
  },
  {
    path: 'booking/summary',
    component: SummaryComponent,
    canActivate: [BookingGuard]
  },
    {
        path: 'booking/promoted/success',
        component: PromotionPageComponent
    },
  {
    path: 'booking/space/:id',
    component: BookingPageComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      SharedModule,
      RouterModule.forChild(BOOKING_ROUTES)
  ],
  declarations: [
    SelectDateComponent,
    CalendarComponent,
    HourBaseComponent,
    BlockBaseComponent,
    SelectAmenitiesComponent,
    SummaryComponent,
    TimeFormat,
      PromotionPageComponent,
    BookingPageComponent
  ],
  providers: [BookingFlowService, BookingGuard]
})
export class BookingModule { }
