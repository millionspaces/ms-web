import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { RouteDataService } from "../shared/services/route.data.service";
import {Injectable} from "@angular/core";

@Injectable()
export class BookingGuard implements CanActivate {

    constructor(
        private routeData: RouteDataService,
        private router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.hasPaymentSummaryData();
    }

    hasPaymentSummaryData(): boolean {
        if(!this.routeData.paymentSummaryData) {
            return true;
        }

        this.router.navigate(['/booking/select-date']);
        return false;
    }

}