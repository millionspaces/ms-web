import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.css']
})
export class BookingPageComponent implements OnInit {
  bookingPageLink: any;

  constructor(
    private domSanitizer: DomSanitizer,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const routeParams = this.activedRoute.snapshot.params;
    console.log('routeParams.id', routeParams.id);
    this.loadIfame(routeParams.id);
  }

  loadIfame(spaceId) {
    this.bookingPageLink = this.domSanitizer.bypassSecurityTrustResourceUrl(
      environment + '/booking/' + spaceId
    );
  }
}
