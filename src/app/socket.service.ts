import { Injectable } from '@angular/core';
import io from 'socket.io-client';

@Injectable()
export class SocketService {
  private _socket: any;

  constructor() {
    this._socket = io('https://qnserver.millionspaces.com');
  }

  get socket() {
    return this._socket;
  }
}
