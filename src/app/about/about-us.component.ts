import {Component, OnInit} from "@angular/core";

@Component({
    templateUrl: './about-us.component.html',
    styles: [`
         .about-ms {
            background:  #f7f7f7;
            margin-top: 50px;
        }
        .about-ms-inner > div {
            width: 100%;
            display: inline-block;
            padding: 10px;
            vertical-align: top;
        }
        @media only screen and (min-width: 992px) {
            .about-ms-inner > div {
                width: 50%;
                padding: 30px;
            }
        }
        .about-ms-inner > div:first-child {
            padding-left: 20px;
            margin-bottom: 40px;
        }
        @media only screen and (min-width: 992px) {
            .about-ms-inner > div:first-child {
                padding-left: 100px;
                margin-bottom: 0;
            }
        }
        .about-ms-inner > div:last-child {
            background: url(assets/img/about/image.jpg);
            background-size: cover;
            background-position: bottom;
            color: #fff;
        }
        .about-ms-inner > div:last-child h3 {
            font-weight: bold;
            color: #fff;
        }
        .about-ms-inner > div:last-child ul {
            margin-left: 20px;
            padding-left: 0;
        }
        .about-ms-inner > div:last-child ul li {
            margin-bottom: 10px;
        }
        .abt-ms-title {
            font-size: 30px;
            font-weight: bold;
            margin-bottom: 0;
        }
        .abt-ms-title + p {
            font-size: 18px;
        }
        .abt-ms-title + p + p {
            font-size: 18px;
            font-weight: 300;
        }
        .download-email-dk {
            background: #ffce4b;
            text-decoration: none;
            color: #000;
            padding: 10px;
            margin-top: 5px;
            display: inline-block;
        }
        .download-email-dk:hover {    
            text-decoration: none;
            color: #000;
        }
        .million-spaces-team-wrapper {
            padding: 60px 0;
        }
        .million-spaces-team-wrapper header {
            text-align: center;
        }
        .million-spaces-team-wrapper header h1 {
            font-weight: bold;
            margin-top: 50px;
            margin-bottom: 0;
        }
        .million-spaces-team-wrapper header h1 + p {
            font-size: 18px;
        }
        .images-wrapper {
            text-align: center;
            padding: 30px 0;
        }
        .images-wrapper img {
            width: 100%;
        }
        .images-wrapper > div {
            display: inline-block;
            width: 100%;
        }
        .images-wrapper > div > div {
            display: inline-block;
            width: 33%;
            vertical-align: top;
        }
        .images-wrapper.images-wrapper-ceo > div > div {
            width: 84%;
        }
        .images-wrapper > div > div > h3 {
            text-transform: uppercase;
            font-weight: bold;
            margin-bottom: 0;
        }
        .images-wrapper > div > div > p {
            text-transform: capitalize;
            margin-bottom: 20px;
        }
        .dev-team > div {
            width: 50%;
            display: inline-block;
            vertical-align: middle;
        }
        .dev-team > div > img {
            width: 100%;
        }
        .team-p-w {
            font-style: italic;
            padding: 20px;
            text-align: center;
            font-size: 20px;
            font-weight: 300;
        }
        .team-photo {
            width: 100%;
            margin-top: 20px;
        }
        .section-heading {
            display: block;
            font-size: 4rem;
            font-weight: bold;
            padding: 3rem;
        }
        .profile-wrapper {
            padding: 2rem 5rem;
        }
        .text-center {
            text-align:center;
        }
        .profile-container {
            padding: 1rem 0 6rem 0;
        }
        .profile li {
            text-align: left;
            padding: 0.5rem 0;
        }
        .profile-name {
            display: block;
            font-size: 2rem;
            font-weight: bold;
            padding: 1rem 0;
        }
        .ms-c {
            color: #00adee;
        }
    `]
})
export class AboutUsComponent implements OnInit{

    ngOnInit(){
        window.scrollTo(0, 0);
    }

}