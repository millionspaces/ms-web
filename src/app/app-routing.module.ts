import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from "./pages/not-found.component";
import { NgModule} from '@angular/core';
import { EventTypeResolve } from "./events/shared/event-type.resolve";
import { AuthGuard } from "./shared/services/auth-guard.service";
import { AboutUsComponent } from "./about/about-us.component";
import { PrivacyTermsComponent } from "./privacy/privacy-terms.component";
import { SysInfoComponent } from "./footer/sysinfo/sysinfo.component";
import { PressPageComponent } from "./press/press-page.component";
import { HomepageV2Component } from "./homepage-v2/homepage-v2.component";

const routes: Routes = [
    {
        path: 'home', component: HomepageV2Component,
        resolve: {
            eventTypes: EventTypeResolve,

        }
    },
    {
        path: 'spaces',
        loadChildren: 'app/spaces/spaces.module#SpacesModule'
    },
    {
        path: 'user',
        loadChildren: 'app/user/user.module#UserModule'
    },
    {
        path: 'payments',
        canActivate:[AuthGuard],
        loadChildren: 'app/payment-gateway/payment-gateway.module#PaymentGatewayModule'
    },
    {
        path: 'about',
        component: AboutUsComponent
    },
    {
        path: 'press',
        component: PressPageComponent
    },
    {
        path: 'terms/privacy',
        component: PrivacyTermsComponent,
    },
    {
        path: 'ms/sysinfo',
        component: SysInfoComponent,
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: NotFoundComponent
    }
]


@NgModule({
    imports: [RouterModule.forRoot(routes ,{ useHash: true })],
    exports: [RouterModule]
})

export class AppRoutingModule{

}