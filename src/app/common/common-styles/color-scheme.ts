export const colorTheme = {
    darkBlue: '#090913',
    lightGray: '#e2e2e2',
    midGray: '#d6d6d6'
}