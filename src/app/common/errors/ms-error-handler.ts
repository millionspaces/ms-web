import { ErrorHandler, Injectable } from "@angular/core";
import { ToasterService } from "../../shared/services/toastr.service";
import { environment } from "../../../environments/environment";

/**
 *
 *  Main error handler(handle all unexpected errors)
 *
 */
@Injectable()
export class MSErrorHandler implements ErrorHandler{

    constructor(private toaster: ToasterService){}

    handleError(error){

        if(!environment.production)
            this.toaster.error('An unexpected error occurred.', "Error");

        console.error('UNEXPECTED ERROR', error);
    }

}