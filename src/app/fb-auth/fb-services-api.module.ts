import {NgModule} from "@angular/core";
import {FbAuthComponent} from "./fb-auth.component";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [FbAuthComponent],
    exports : [FbAuthComponent]
})

export class FbServicesAPIModule{}