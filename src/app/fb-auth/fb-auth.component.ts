import {Component, OnInit, Input} from "@angular/core";
import {AuthenticationService} from "../shared/services/authentication.service";
import {Router} from "@angular/router";
import {AngularFire, AuthProviders, AuthMethods} from "angularfire2";

declare const FB: any;
declare var $;

@Component({
    selector: 'fb-sign-in',
    template:`
        <a (click)="onFacebookLoginClick()" id="facebookSignInBtn" class="btn btn-block btn-wrap-social btn-facebook btn-flat"><i class="fa fa-facebook fa-icon"></i> Sign in using Facebook</a>
        <div *ngIf="isLoading">
             <main-pre-loader></main-pre-loader>
        </div>
`,
styles: [`    
      .btn-facebook {
        color: #fcf5f4;
        background-color: #3c5a9a;
        border: 1px solid #788cb8;
      }
      .btn-wrap-social {
        position: relative;
        padding-left: 4.3rem !important;
        text-align: left;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        border-radius: 0;
        font-size: 1.4rem;
      }
    
      .btn-wrap-social > .fa-icon{
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        width: 32px;
        line-height: 1.9rem;
        text-align: center;
        border-right: 1px solid rgba(199, 199, 199, 0.5);
        display: block;
        margin: 10px 0;
        font-size: 1.5rem;
      }
    `]  
})
export class FbAuthComponent implements OnInit{

    isLoading = false;
    @Input() navigateTo;
    //private hrefFb = "https://www.facebook.com/v2.8/dialog/oauth?client_id=767624696718278&redirect_uri=http://localhost:4200/&scope=email&response_type=token"
    constructor(
        private _authService: AuthenticationService,
        private _router: Router,
        private af: AngularFire
    )
    {
        /*FB.init({
            appId   : '767624696718278',
            cookie  : false,
            xfbml   : true,
            version : 'v2.8'
        });*/
    }

    ngOnInit(){
        this.af.auth.subscribe(authState => {
            if(!authState){
                console.log("NOT LOGGED IN");
            }else{
                if(authState.facebook && authState.facebook.hasOwnProperty("accessToken"))
                    this.loginWithFB(authState);
            }
        });
    }

    onFacebookLoginClick(){

        this.af.auth.login({
            provider : AuthProviders.Facebook,
            method :AuthMethods.Popup,
            scope: ['public_profile']
        }).then(authState => {
            this.loginWithFB(authState);
            console.log("After Login facebook", authState);
        });

    }

    /*callbackFn(response) {
        if (response.status === 'connected')
            this.loginWithFB(response);
        else if (response.status === 'not_authorized')
            console.log('not_authorized',response);
        else
            console.log('else',response);
    };*/

    loginWithFB(authState){
        this.isLoading = true;
        this._authService.loginWithFB(authState.facebook.accessToken).subscribe(
            user => {
                this.isLoading = false;
                console.log("LOGIN SUCCESS");
                //this._routerAlertService.success("Login successful", true);
                this._router.navigateByUrl(this.navigateTo);
            },
            error => {
                console.log('error fb',error)
                //this._routerAlertService.error(error);
            },
            () => {
                if($('#loginModal')){
                    $('#loginModal').modal('hide');
                }
            }
        );
    }


}