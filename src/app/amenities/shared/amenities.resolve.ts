import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {AmenitiesService, Amenity} from "./amenities.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AmenitiesResolve implements Resolve<any>{
    constructor(
        private amentiesService: AmenitiesService
    ){}

    resolve() : Observable<Amenity[]>{
        return this.amentiesService.getAmenitiesList();
    }
}