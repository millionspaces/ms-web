import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AmenitiesService} from "./amenities.service";

@Injectable()
export class AmenityUnitsResolve implements Resolve<any>{
    constructor(
        private amentiesService: AmenitiesService
    ){}

    resolve() : Observable<any[]>{
        return this.amentiesService.getAmenityUnits();
    }
}