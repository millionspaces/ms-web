import {Injectable} from "@angular/core";
import {ServerConfigService} from "../../server.config.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AmenitiesService{

    baseUrl: string;
    private amenities: Array<any>;

    constructor(
        private _serverConfigService: ServerConfigService,
        private _http: Http
    ){
        this.baseUrl = environment.apiHost+'/common';
    }

    getAmenitiesList(): Observable<Amenity[]>{
        return this._http
            .get(this.baseUrl+'/amenities', this._serverConfigService.getCookieHeader())
            .map((response: Response) => <Amenity[]>response.json())
            .catch(this.handleError);
    }

    getAmenityUnits(): Observable<any[]>{
        return this._http
            .get(this.baseUrl+'/amenityUnits', this._serverConfigService.getCookieHeader())
            .map((response: Response) => <any[]>response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }

}

export class Amenity{
    id:number;
    name:string;
    icon: string;
    selected: boolean;
    amenityUnit: number;
}