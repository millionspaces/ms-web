import {Injectable} from '@angular/core';
import {Headers, RequestOptions, Http, Response} from "@angular/http";
import { environment } from '../environments/environment';
import {Observable} from "rxjs/Observable";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class ServerConfigService{

  private _gApiHost = "www.googleapis.com";

  private _jsonHeader: RequestOptions;
  private _loginHeader: RequestOptions;
  private _cookieHeader: RequestOptions;
  private _corsHeader: RequestOptions;
  private _notificationHeader:RequestOptions;

  private _geoCodePath: string = "https://maps.googleapis.com/maps/api/geocode";

  constructor(private http: Http) {
    this.initRequestHeaders();
  }

  initRequestHeaders(){
    let headersJson = new Headers();
    headersJson.append("Content-Type", "application/json");
    // headersJson.append("Access-Control-Allow-Origin", "*");

    let headersLogin = new Headers();
    headersLogin.append("Content-Type", "application/x-www-form-urlencoded");

    let headersCORS = new Headers();
    headersCORS.append("Access-Control-Allow-Origin", "*");
    headersCORS.append("Access-Control-Allow-Methods", "DELETE, HEAD, GET, OPTIONS, POST, PUT");
    headersCORS.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    headersCORS.append("Access-Control-Allow-Headers", "Content-Type");

    this._jsonHeader = new RequestOptions({ headers: headersJson, withCredentials: true });
    this._loginHeader = new RequestOptions({ headers: headersLogin, withCredentials: true });
    this._notificationHeader=new RequestOptions({ headers: headersJson, withCredentials: false });
    this._corsHeader = new RequestOptions({headers : headersCORS});
    this._cookieHeader = new RequestOptions({withCredentials: true});
  }

  getSystemInfo(){
    return this.http
        .get(environment.apiHost + '/common/system', this.getCookieHeader())
        .map((response: Response) => response.json())
        .catch(this.handleError);
  }

  getJsonHeader(){
    return this._jsonHeader;
  }

  getLoginHeader(){
    return this._loginHeader;
  }

  getCookieHeader(){
    return this._cookieHeader;
  }

  getGeoCodeJsonPath(){
    return this._geoCodePath+"/json?";
  }

  getNotificationHeader(){
    return this._notificationHeader;
  }

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json().error || 'Server Error');
  }

}

