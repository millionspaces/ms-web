import {Component, Input, OnInit} from "@angular/core";
import {environment} from "../../../environments/environment";
import {MsUtil} from "../../shared/util/ms-util";
import {RouteDataService} from "../../shared/services/route.data.service";
import {Router} from "@angular/router";
import {SigninModalComponent} from "../../core/modal/signin-modal.component";
import {UserService} from "../../user/user.service";

@Component({
    selector: 'category-spaces',
    templateUrl: './category-spaces.component.html',
    styleUrls: ['../homepage-v2.component.css']
})
export class CategorySpacesComponent implements OnInit {

    @Input() eventType;

    @Input() spaces;

    imagePath: string;

    // modal data
    modalComponentData = null;

    constructor(
        private router: Router,
        private routeDataService: RouteDataService,
        private userService: UserService
    ) {}

    ngOnInit() {
        this.imagePath = environment.bucket;
    }

    showSpace(space, event?: any) {

        event.stopPropagation();
        const spaceName = MsUtil.replaceWhiteSpaces(space.name, '-');

        if (!this.userService.getUser()) {
            this.showLoginModal(space.id, spaceName);
            return;
        }

        window.open(`#/spaces/${space.id}/${spaceName}`, '_blank');
    }

    goToEvent (eventId) {

        this.routeDataService.clearData();

        let searchDataRequest = {
            "currentPage": 0,
            "participation": null,
            "budget": null,
            "amenities": null,
            "events": <any> [eventId],
            "location": {address: null, latitude: null, longitude: null},
            "available": null,
            "rules": null,
            "seatingArrangements": null,
            "spaceType": []
        }

        this.routeDataService.searchDataRequest = searchDataRequest;
        this.router.navigate (['spaces/list']);

    }

    showLoginModal(spaceId, spaceName) {
        this.modalComponentData = {
            component: SigninModalComponent,
            inputs: {
                navigateTo: 'spaces/' + spaceId + '/' + MsUtil.replaceWhiteSpaces(spaceName, '-')
            }
        }
    }

    getSportEventTypes() {

    }


}