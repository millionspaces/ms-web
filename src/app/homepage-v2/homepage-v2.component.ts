import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {SpaceService} from "../spaces/shared/space.service";
import {RouteDataService} from "../shared/services/route.data.service";
import {GoogleMapService} from "../google-services-api/shared/google-map.service";
import {SigninModalComponent} from "../core/modal/signin-modal.component";
import {MsUtil} from "../shared/util/ms-util";
import { environment } from "../../environments/environment";
import {UserService} from "../user/user.service";

declare let $: any;
declare let Typed: any;
declare let google: any;

@Component({
  selector: 'app-homepage-v2',
  templateUrl: './homepage-v2.component.html',
  styleUrls: ['./homepage-v2.component.css']
})
export class HomepageV2Component implements OnInit, AfterViewInit {

  //resolves
  eventTypes: any[];
  homePageSpaces: any[];
  spaceOrOrgName: string;
  autoComplete: any;
  env: string;
  eventLocation = { address:null, latitude: null, longitude: null };
  completeOptions = {
    componentRestrictions: {country: "lk"}
  };

  // modal data
  modalComponentData = null;

  eventTypeImagePath = 'https://res.cloudinary.com/dgcojyezg/image/upload/q_80,fl_progressive/ms-mobile-web/event-types/'

    eventTypeList = [];
    showNumberOfEventTypes = '2';
    showMoreEventTypes = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private spaceService: SpaceService,
      private routeDataService: RouteDataService,
      private mapService: GoogleMapService,
      private userService: UserService
  ) {
    this.eventTypes = this.route.snapshot.data['eventTypes'];
  }

  setMoreEventTypes() {
        this.showMoreEventTypes = true;
        this.showNumberOfEventTypes = '12';
  }

  ngOnInit() {

      this.eventTypeList = [
          { id: 4, name: 'Meeting', image: this.eventTypeImagePath + '/1.Meeting.jpg' },
          { id: 1, name: 'Party', image: this.eventTypeImagePath + '/4.Party.jpg' },
          { id: 8, name: 'Coworking Space', image: this.eventTypeImagePath + '/3.Coworking.jpg' },
          { id: 9, name: 'Sports', image: this.eventTypeImagePath + '/2.Sports.jpg' },
          { id: 3, name: 'Conference', image: this.eventTypeImagePath + '/5.Conference.jpg' },
          { id: 5, name: 'Interview', image: this.eventTypeImagePath + '/6.Interview.jpg' },
          { id: 7, name: 'Photoshoot', image: this.eventTypeImagePath + '/8.Photoshoot.jpg' },
          { id: 6, name: 'Training', image: this.eventTypeImagePath + '/7.Training.jpg' },
          { id: 2, name: 'Wedding', image: this.eventTypeImagePath + '/9.Wedding.jpg' },
          { id: 10, name: 'Concert', image: this.eventTypeImagePath + '/10.Concert.jpg' },
          { id: 12, name: 'Day Outing', image: this.eventTypeImagePath + '/12.DayOut.jpg' },
          { id: 11, name: 'Pop Up Event', image: this.eventTypeImagePath + '/11.PopUp.jpg' },
          { id: 14, name: 'Entertainment', image: this.eventTypeImagePath + '/13.Entertainment.jpg' }
      ];

    const request = [
      { //sports
        "location":null,
        "events":[9],
        "limit":9
      },
      /*{ // meeting
        "location":null,
        "events":[4],
        "limit":4,
        "orderBy": [324, 204, 17, 365]
      },
      { //party
        "location":null,
        "events":[1],
        "limit":4,
        "orderBy": [55, 120, 404, 386]
      },
      { //press con
        "location":null,
        "events":[3],
        "limit":4,
        "orderBy": [134, 40, 129, 216]
      },
      { //interview
        "location":null,
        "events":[5],
        "limit":4,
        "orderBy": [14, 227, 29, 307]
      },
      { //training
        "location":null,
        "events":[6],
        "limit":4,
        "orderBy": [189, 32, 52, 62]
      },
      { // photoshoot
        "location":null,
        "events":[7],
        "limit":4,
        "orderBy": [368, 65, 38, 392]
      },
      {
        //hot desk
        "location":null,
        "events":[8],
        "limit":4,
        "orderBy": [5, 217, 335, 352]
      },{
      //concert
        "location":null,
        "events":[10],
        "limit":4,
        "orderBy": [75, 198, 7, 309]
      }*/
    ]

    this.spaceService.getHomepageSpaces(request).subscribe(spaces => {
      this.homePageSpaces = spaces['9'];

    })
  }

  ngAfterViewInit() {
    new Typed('.typehead', {
      strings: this.eventTypes.map(eventType => eventType.name),
      typeSpeed: 30,
      backDelay: 1000,
      loop: true
    });

    // initialize address
    let input: HTMLInputElement = (<HTMLInputElement>document.getElementById("search-box"));
    this.initAutoComplete(input);


    $('body').on('click', function(e) {
      var target = e.target; //target div recorded
      if (!$(target).is('input') && !$(target).is('#event-search-wrapper') ) {
        this.toggleEventTypes = false
      }
    }.bind(this))


    // initialize banners slider
    // this.cycleBackgrounds();

  }

  cycleBackgrounds() {
    let index = 0;

    const imageEls = $('.slider-container .slide'); // Get the images to be cycled.

    setInterval(function () {
      // Get the next index.  If at end, restart to the beginning.
      index = index + 1 < imageEls.length ? index + 1 : 0;

      // Show the next
      imageEls.eq(index).addClass('show');

      // Hide the previous
      imageEls.eq(index - 1).removeClass('show');
    }, 5000);
  };

  searchSpaces() {
      const requestBody = {
        "currentPage": 0,
        "participation": null,
        "budget": null,
        "amenities": null,
        "events": null,
        "location": null,
        "available": null,
        "rules": null,
        "seatingArrangements": null,
        "spaceType": [],
        "sortBy": null,
        "organizationName": this.spaceOrOrgName
      }
      this.routeDataService.searchDataRequest = requestBody;

      if(this.spaceOrOrgName) {
        this.router.navigate(['spaces/list', { q: this.spaceOrOrgName } ]);
      } else {
        this.router.navigate(['spaces/list']);
      }

  }

  private initAutoComplete(input: HTMLInputElement): void {

    this.autoComplete = new google.maps.places.Autocomplete(input, this.completeOptions);
    google && google.maps && google.maps.event && google.maps.event.addListener(this.autoComplete, 'place_changed', () => {

      this.eventLocation.address =  input.value;
      this.mapService.findGeoCode(input.value).subscribe(
          geoCode => {
            this.eventLocation.latitude = geoCode.lat;
            this.eventLocation.longitude = geoCode.lng;
            this.homePageSpaces = null;
            this.filterByLocation();
          },
          error => { console.log(error)}
      );
    });
  }

  showLoginModal(spaceId, spaceName) {
    this.modalComponentData = {
      component: SigninModalComponent,
      inputs: {
        navigateTo: 'spaces/' + spaceId + '/' + MsUtil.replaceWhiteSpaces(spaceName, '-')
      }
    }
  }

  filterByLocation() {
    const location = {
          address: this.eventLocation.address,
          latitude: this.eventLocation.latitude,
          longitude: this.eventLocation.longitude
    };
    const requestBody = [
      {
        location,
        "events":[1],
        "limit":4
      },
      {
        location,
        "events":[2],
        "limit":4
      },
      {
        location,
        "events":[3],
        "limit":4
      },
      {
        location,
        "events":[4],
        "limit":4
      },
      {
        location,
        "events":[5],
        "limit":4
      },
      {
        location,
        "events":[6],
        "limit":4
      },
      {
        location,
        "events":[7],
        "limit":4
      },
      {
        location,
        "events":[8],
        "limit":4
      },
      {
        location,
        "events":[9],
        "limit":4
      },
      {
        location,
        "events":[10],
        "limit":4
      }
    ]

    this.spaceService.getHomepageSpaces(requestBody)
        .subscribe(spaces => {
          this.homePageSpaces = spaces;
    })

  }

  getFeaturedImage(space){
    return environment.thumbnailPath+'/'+space.thumbnailImage;
  }

  showSpace(space, event?: any) {

    event.stopPropagation();
    const spaceName = MsUtil.replaceWhiteSpaces(space.name, '-');

    if (!this.userService.getUser()) {
      this.showLoginModal(space.id, spaceName);
      return;
    }

    window.open(`#/spaces/${space.id}/${spaceName}`, '_blank');
  }


  goToEvent (eventId) {

    this.routeDataService.clearData ();

    let searchDataRequest = {
      "currentPage": 0,
      "participation": null,
      "budget": null,
      "amenities": null,
      "events": <any> [+eventId],
      "location": {address: null, latitude: null, longitude: null},
      "available": null,
      "rules": null,
      "seatingArrangements": null,
      "spaceType": []
    }
    this.routeDataService.searchDataRequest = searchDataRequest;

    this.router.navigate (['spaces/event', eventId ]);
  }

  getDiscountValue(space): number {
    return Math.abs(space.discount);
  }

  toggleEventTypes = false;

  toggleExploreEventTypes(e) {
    this.toggleEventTypes = !this.toggleEventTypes;
  }


}
