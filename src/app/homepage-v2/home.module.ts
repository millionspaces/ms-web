import { NgModule } from "@angular/core";
import { AmenitiesService } from "../amenities/shared/amenities.service";
import { EventService } from "../events/shared/event.service";
import { BrowserModule } from "@angular/platform-browser";
import { FooterModule } from "../footer/footer.module";
import { RouterModule } from "@angular/router";
import { GoogleMapService } from "../google-services-api/shared/google-map.service";
import { SharedModule } from "../shared/shared.module";
import { FormsModule } from "@angular/forms";
import {HomepageV2Component} from "./homepage-v2.component";
import {CategorySpacesComponent} from "./category-spaces/category-spaces.component";
import {CoreModule} from "../core/core.module";

@NgModule({
    imports:[BrowserModule, FooterModule, RouterModule, SharedModule, FormsModule, CoreModule],
    declarations:[HomepageV2Component, CategorySpacesComponent],
    providers: [AmenitiesService, EventService, GoogleMapService]
})
export class HomeModule{

}