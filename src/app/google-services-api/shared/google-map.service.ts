import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";

import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

import { ServerConfigService } from "../../server.config.service";

declare var google: any;

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class GoogleMapService{

    mapOnSpaceCreate;
    mapOnSpaceShow;
    private subject = new Subject<any>();

    private subject1 = new Subject<number>();
    private subject2 = new Subject<number>();

    selectedGeoCodes : any = null;
    marker: any;

    private _geoCodeApiKey = "AIzaSyDMLLTB3j6Y__YZfJwdoHzKkQIE8QyrIKA";

    constructor( private _http: Http, private _serverConfig: ServerConfigService ){

    }

    setMarkerHighlighted(index){
        this.subject1.next(index);
    }

    getMarkerHighlightedAsObservable(): Observable<number>{
        return this.subject1.asObservable();
    }

    setMarkerLeave(index){
        this.subject2.next(index);
    }

    getMarkerMarkerLeaveAsObservable(): Observable<number>{
        return this.subject2.asObservable();
    }

    setMap(mapOptions, id){
        if(id === 'map-on-create'){
            this.mapOnSpaceCreate = new google.maps.Map(document.getElementById(id), mapOptions);
        }else if(id === 'map-on-spaces'){
            this.mapOnSpaceShow = new google.maps.Map(document.getElementById(id), mapOptions);
        }
        setTimeout(function() {
            google.maps.event.trigger(this.mapOnSpaceCreate, "resize");
        }, 100);
    }

    setMapViewOnMobile(){
        this.subject.next(new Object());
    }

    getMapViewObservableOnMobile(): Observable<any>{
        return this.subject.asObservable();
    }

    setMarkers(){
        /*let markers = [];
        let markerData = [   // the order of these markers must be the same as the <div class="marker"></div> elements
            {lat: 6.920466, lng: 79.862423, title: 'Place 1'},
            {lat: 6.920086, lng: 79.862543, title: 'Place 2'},
            {lat: 6.920547, lng:79.862119, title: 'Place 3'}
        ];
        for (var i=0; i< markerData.length; i++) {
            markers.push(
                new google.maps.Marker({
                    position: new google.maps.LatLng(markerData[i].lat, markerData[i].lng),
                    title: markerData[i].title,
                    map: this.map,
                })
            );
        }*/
    }

    setMarker(){
        new google.maps.Marker({
            position: new google.maps.LatLng(this.getSelectedGeoCodes().lat. this.getSelectedGeoCodes().lng),
            map: this.mapOnSpaceCreate,
            icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
    }

    setGeoCode(lat, lng){
        //this.geoCode = {lat: lat, lng: lng};
    }

    findGeoCode(address: string): Observable<any>{
        return this._http
            .get(`${this._serverConfig.getGeoCodeJsonPath()}address=${address}&key=${this._geoCodeApiKey}`)
            .map((response: Response) => response.json().results)
            .map(results => {
                return {
                    lat : results[0].geometry.location.lat,
                    lng : results[0].geometry.location.lng
                }
            });
    }

    // used for 'mapOnSpaceCreate'
    targetLocation(geocode: any) {

        if(this.marker)
            this.marker.setMap(null);

        this.marker = new google.maps.Marker({
            position: geocode,
            draggable: false,
            animation: google.maps.Animation.DROP,
            icon: 'https://millionspaces.com/assets/img/map/32-32-normal.png'
        });
        google.maps.event.addListener(this.marker, 'dragend', function (marker) {
            this.setSelectedGeoCodes({
                lat: marker.latLng.lat(),
                lng: marker.latLng.lng(),
            })
        }.bind(this));
        this.marker.setMap(this.mapOnSpaceCreate);
        this.mapOnSpaceCreate.panTo(geocode);
    }

    clickroute(lat, lng, map) {
        var latLng = new google.maps.LatLng(lat, lng);
        map.panTo(latLng);
    }

    setSelectedGeoCodes(geoCodes){
        this.selectedGeoCodes = geoCodes;
    }

    getSelectedGeoCodes(){
        return this.selectedGeoCodes;
    }

    spacesMap = null;

    setSpacesMap(map){
        this.spacesMap = map;
    }

    getSpacesMap(){
        return this.spacesMap;
    }


    clearGeoCodes(){
        this.subject1.next();
        this.subject2.next();
    }

}
