import {Component, NgZone, AfterViewInit, OnInit, Input} from "@angular/core";
import { Router } from "@angular/router";

import { AuthenticationService } from "../../shared/services/authentication.service";
import {AngularFire, AuthProviders, AuthMethods} from "angularfire2";

declare var gapi: any;
declare var $;

@Component({
    selector: 'google-sign-in',
    template: `
<!--<button id="customBtn" type="submit" class="btn btn-default btn-circle white google-login"><i class="flaticon flaticon-google-plus"></i></button>-->
    <a  (click)="onGoogleLoginClick()" id="googleSignInBtn" class="btn btn-block btn-social  btn-wrap-social btn-google btn-flat"><i class="fa fa-google-plus fa-icon"></i> Sign in using Google+</a>
      <div *ngIf="isLoading">
    <main-pre-loader></main-pre-loader>
</div>
`,
styles: [` 
 .btn-google {
    color: #fcf5f4;
    background-color: #de4b39;
    border: 1px solid #e2685a;
    margin-bottom: 10px;
  }
  .btn-wrap-social {
    position: relative;
    padding-left: 4.3rem !important;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    border-radius: 0;
    font-size: 1.4rem;
  }

  .btn-wrap-social > .fa-icon{
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    width: 32px;
    line-height: 1.6rem;
    text-align: center;
    border-right: 1px solid rgba(199, 199, 199, 0.5);
    display: block;
    margin: 10px 0;
    font-size: 1.5rem;
  }    `]  
})
export class GoogleAuthComponent implements OnInit{
    isLoading = false;
    @Input() navigateTo;

    constructor(
        private zone: NgZone,
        private _authService: AuthenticationService,
        private _router: Router,
        private af: AngularFire
    ){}

    ngOnInit(){
        this.af.auth.subscribe(authState => {
            if(!authState){
                console.log("NOT LOGGED IN");
            }else{
                if(authState.google && authState.google.hasOwnProperty("accessToken"))
                    this.loginWithGoogle(authState);
            }
        });
    }

    onGoogleLoginClick(){
        this.af.auth.login({
            provider : AuthProviders.Google,
            method :AuthMethods.Popup
        }).then(authState => {
            console.log("After Login google", authState);
            this.loginWithGoogle(authState);
        });
    }

    loginWithGoogle(authState){
        this.isLoading = true;
        this._authService.loginWithGoogle(authState.google.accessToken).subscribe(
        user => {
            console.log("LOGIN SUCCESS");
            this.isLoading = false;
            //this._routerAlertService.success("Login successful", true);
            this._router.navigateByUrl(this.navigateTo);
        },
        error => {
            console.log('error google',error)
            //this._routerAlertService.error(error);
        },
        () => {
            if($('#loginModal')){
                $('#loginModal').modal('hide');
            }
        }
    );
}



}