import { NgModule } from "@angular/core";

import { GoogleAuthComponent } from "./google-auth/google.auth.component";
import { GoogleMapComponent } from "./google-map/google-map.component";
import { GoogleMapService } from "./shared/google-map.service";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [GoogleAuthComponent, GoogleMapComponent],
    providers : [GoogleMapService],
    exports : [GoogleAuthComponent, GoogleMapComponent]
})

export class GoogleServicesAPIModule{}