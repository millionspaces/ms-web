import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Injectable()
export class RefundCalModalService{

    private requestSource = new Subject<any>();

    setRequest(resp){
        this.requestSource.next(resp);
    }

    getRequestObservable() : Observable<any>{
        return this.requestSource.asObservable();
    }
}