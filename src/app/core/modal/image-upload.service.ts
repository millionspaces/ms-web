import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Subject} from 'rxjs/Subject';
import {AWSS3Service} from "../../shared/services/aws-s3.service";

@Injectable()
export class ImageUploadService{

    private imageSource = new Subject<any>();

    constructor(private s3Service: AWSS3Service){

    }

    getImageAsObservable(): Observable<any>{
        return this.imageSource.asObservable();
    }

    setImage(resp){
        this.imageSource.next(resp);
    }

    uploadFile(img){
        this.s3Service.uploadData({
            Key: getImageId(),
            Body: img,
            ACL: 'public-read',
            ContentType: img.type
        }, 'menus').
        then(
            res => {
                let imageData = {
                    response: res,
                    window: 'menus'
                }
                console.log('imageData',imageData);
                this.setImage(imageData);
            }
        ).catch(err => console.log('upload error', err));

        function getImageId() {
            return Math.floor(Math.random() * (1000000 - 100 + 1)) + 100 +
                new Date().toJSON().slice(0, 19).replace(/-|:/g, '') + 'IMG';
        }
    }


}