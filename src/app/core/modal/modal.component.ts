import {
    Component, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver,
    ReflectiveInjector
} from "@angular/core";
import {ErrorModalComponent} from "./error-modal.component";
import {SuccessModalComponent} from "./success-modal.component";
import {SigninModalComponent} from "./signin-modal.component";
import {InfoModalComponent} from "./info-modal.component";
import {WarningModalComponent} from "./warning-modal.component";
import {UnsavedDataModalComponent} from "./unsaved-data-moda.component";
import {RefundCalModalComponent} from "./refund-cal-modal.component";
import {SpaceReviewModalComponent} from "./space-review-modal.component";
import {ImageUploadModalComponent} from "./image-upload-modal.component";

@Component({
    selector: 'modal-component',
    entryComponents: [
        ErrorModalComponent,
        SuccessModalComponent,
        SigninModalComponent,
        InfoModalComponent,
        WarningModalComponent,
        RefundCalModalComponent,
        SpaceReviewModalComponent,
        ImageUploadModalComponent,
    ],
    template: `<div #modalComponentContainer></div>`
})
export class ModalComponent{
    currentComponent = null;
    @ViewChild('modalComponentContainer', { read: ViewContainerRef }) modalComponentContainer: ViewContainerRef;

    constructor(private resolver: ComponentFactoryResolver){
    }

    @Input() set modalComponentData(data: {component: any, inputs: any }) {
        if (!data)
            return;

        let inputProviders = Object.keys(data.inputs).map((inputName) => {return {provide: inputName, useValue: data.inputs[inputName]};});
        let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

        let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.modalComponentContainer.parentInjector);

        let factory = this.resolver.resolveComponentFactory(data.component);

        let component = factory.create(injector);

        this.modalComponentContainer.insert(component.hostView);

        if (this.currentComponent) {
            this.currentComponent.destroy();
        }
        this.currentComponent = component;
    }

}