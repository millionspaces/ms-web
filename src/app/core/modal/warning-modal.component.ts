import {Component, Injector, AfterViewInit} from "@angular/core";
declare var $;
@Component({
    selector: 'warning-modal',
    template: `
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-yellow"> <!-- bg-blue bg-yellow bg-green -->
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="flaticon flaticon-zerror"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning" role="alert">
                                <div class="modal-icon"><span><i class="flaticon flaticon-warning"></i></span></div>
                                <div class="modal-text">
                                    <h4>{{heading}}</h4>
                                    <p>{{body}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" (click)="reloadPage()">OK</button>
                        </div>
                    </div>
                </div>
            </div>
    `
})
export class WarningModalComponent implements AfterViewInit{

    heading: string;
    body: string;

    constructor(private injector: Injector) {
        this.heading = this.injector.get('heading');
        this.body = this.injector.get('body');
    }

    ngAfterViewInit(){
        $("#myModal").modal();
    }

    reloadPage(){
        $("#myModal").hide();
        location.reload();
    }

}