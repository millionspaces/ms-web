import {Component, AfterViewInit, Injector} from "@angular/core";
import {SuccessModalComponent} from "./success-modal.component";

declare var $;
declare var rating: any;
declare var moment: any;

@Component({
    templateUrl: './space-review-modal.component.html'
})
export class SpaceReviewModalComponent implements AfterViewInit{
    bookingId : any;
    modalComponentData = null;

    reviewData = {
        createdAt : <string> null,
        title: <string> null,
        description: <string> null,
        rate: {
            overall_rate : <number> 0,
            cleanliness_rate : <number> 0,
            amenities_rate : <number> 0,
            service_rate : <number> 0
        },
        bookingId: <number> null,
    }

    constructor(
        private injector: Injector,

    ) {
        this.bookingId = this.injector.get('bookingId');
    }

    ngAfterViewInit(){
        $("#reviewModal").modal();

        // current rating, or initial rating
        let currentRating = 0;

        // max rating, i.e. number of stars you want
        let maxRating = 5;

        let el_overall = document.getElementById('overall-rating');
        let el_cleanliness = document.getElementById('cleanliness-rating');
        let el_amenities = document.getElementById('amenities-rating');
        let el_service = document.getElementById('service-rating');

        var callbackOverall = function(rating) {
            this.reviewData.rate.overall_rate = rating;
        }.bind(this);

        var callbackCleanliness = function(rating) {
            this.reviewData.rate.cleanliness_rate = rating;
        }.bind(this);

        var callbackAmenities = function(rating) {
            this.reviewData.rate.amenities_rate = rating;
        }.bind(this);

        var callbackService = function(rating) {
            this.reviewData.rate.service_rate = rating;
        }.bind(this);

        // rating instance
        rating(el_overall, currentRating, maxRating, callbackOverall);
        rating(el_cleanliness, currentRating, maxRating, callbackCleanliness);
        rating(el_amenities, currentRating, maxRating, callbackAmenities);
        rating(el_service, currentRating, maxRating, callbackService);
    }

    submitReview() {

        this.reviewData.createdAt = moment().format('YYYY-MM-DD');
        this.reviewData.bookingId = this.bookingId;

        let rate = `${this.reviewData.rate.overall_rate}|${this.reviewData.rate.cleanliness_rate}|${this.reviewData.rate.amenities_rate}|${this.reviewData.rate.service_rate}`

        //console.log('this.reviewData', this.reviewData);

        /*this.bookingService.addReview({
         "rate" : rate,
         "title": this.reviewData.title,
         "description" : this.reviewData.description,
         "createdAt" : this.reviewData.createdAt,
         "bookingId" : this.reviewData.bookingId
         }).subscribe(state => {
         console.log('state', state);
         $("#reviewModal").modal('hide');
         this.bookingService.setReview(state);
         this.createSuccessModalComponent('','Thank you for your time and feedback!');
         });
         }*/

        /*createSuccessModalComponent(heading, body) {
         this.modalComponentData = {
         component: SuccessModalComponent,
         inputs: {
         heading: heading,
         body: body
         }
         }
         }*/
    }
}