import {Component, Injector, AfterViewInit, OnInit} from "@angular/core";
import {RefundCalModalService} from "./refund-cal-modal.service";
import {AppConfigService} from "../../app.config.service";
declare var $;
@Component({
    selector: 'refund-cal',
    templateUrl: './refund-cal-modal-component.html'
})
export class RefundCalModalComponent implements OnInit, AfterViewInit{

    sellingCurency = '';
    bookingData: any;
    spaceRateForHours: number = 0;
    amenitiesRate: number = 0;
    totalRate: number = 0;
    selectDate: string = new Date().toString();
    flexibleHours: number = 48;
    moderateHours: number = 120;
    strictHours: number = 168;
    refund:any;
    percentage:any;
    value:any;

    constructor(
        private injector: Injector,
        private refundService: RefundCalModalService,
        private appConfigService: AppConfigService
    ) {
        this.bookingData = this.injector.get('bookingData');
        this.sellingCurency = appConfigService.getCurrency();
    }

    ngAfterViewInit(){
        $("#refund").modal();

        $(".form_datetime").datetimepicker({
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left",
            showMeridian: true,
            startDate: new Date(),
            todayHighlight: true,
            forceParse: false,
            endDate: new Date(this.bookingData.startTime),
        }).on('changeDate', function(ev){
            this.selectDate = ev.date.toString();

            let startTimeAsDate :any = new Date(this.bookingData.startTime);
            let hours:any = Math.abs( startTimeAsDate - ev.date) / 36e5;
            console.log('hours',hours);

            if(this.bookingData.cancellationPolicy.name === "Flexible"){
                if(hours > this.flexibleHours || hours == this.flexibleHours){
                    this.percentage = '100%';
                    this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
                }else if(hours < this.flexibleHours){
                    this.percentage = '0%';
                    this.value = 0;
                }
            }

            else if(this.bookingData.cancellationPolicy.name === "Moderate"){
                if(hours > this.moderateHours || hours == this.moderateHours){
                    this.percentage = '100%';
                    this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
                }else if(hours < this.moderateHours){
                    this.percentage = '0%';
                    this.value = 0;
                }
            }

            else if(this.bookingData.cancellationPolicy.name === "Strict"){
                if(hours > this.strictHours || hours == this.strictHours){
                    this.percentage = '50%';
                    this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
                }else if(hours < this.strictHours){
                    this.percentage = '0%';
                    this.value = 0;
                }
            }


        }.bind(this));
    }



    ngOnInit(){

        this.spaceRateForHours = this.bookingData.numOfHours * this.bookingData.spaceRate;

        if(this.bookingData.extraAmenityList.length > 0){
            this.bookingData.extraAmenityList.forEach(amenity => {
                this.amenitiesRate += amenity.rate * amenity.count;
            });
        }

        this.totalRate = this.spaceRateForHours + this.amenitiesRate;

        let start:any = new Date();
        let end: any = new Date(this.bookingData.startTime);
        let hours:any = Math.abs(end - start) / 36e5;

        if(this.bookingData.cancellationPolicy.name === "Flexible"){
            if(hours > this.flexibleHours || hours == this.flexibleHours){
                this.percentage = '100%';
                this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
            }else if(hours < this.flexibleHours){
                this.percentage = '0%';
                this.value = 0;
            }
        }

        else if(this.bookingData.cancellationPolicy.name === "Moderate"){
            if(hours > this.moderateHours || hours == this.moderateHours){
                this.percentage = '100%';
                this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
            }else if(hours < this.moderateHours){
                this.percentage = '0%';
                this.value = 0;
            }
        }

        else if(this.bookingData.cancellationPolicy.name === "Strict"){
            if(hours > this.strictHours || hours == this.strictHours){
                this.percentage = '50%';
                this.value = this.totalRate * this.bookingData.cancellationPolicy.refundRate;
            }else if(hours < this.strictHours){
                this.percentage = '0%';
                this.value = 0;
            }
        }
    }

    cancelRequest(){
        this.refundService.setRequest({bookingId: this.bookingData.bookingId});

        $('#refund').modal('hide');
    }
}