import { Component, AfterViewInit, Injector } from "@angular/core";
import { ImageUploadService } from "./image-upload.service";
import { AWSS3Service } from "../../shared/services/aws-s3.service";

declare var $;
declare var croppie;

@Component({
    selector: 'upload-image',
    styles: [`
        
 .thumbnail-w{
    width: 450px;
 }
 .cover-w{
    width: 790px;
 }
 .menu-w{
    width: 500px;
 }
 
 /* fancy upload
-------------------------------------------------------- */
.form-control{
    border: #ddd 1px solid;
    box-shadow: none;
    border-radius: 0px;
}

.fancy-file-upload {
    position:relative;
    overflow:hidden;
    display:block;
    margin-bottom:3px;
}
.fancy-file-upload>span.button {
    color:#fff;
    background-color:#333;
    font-weight: 400;
    position: absolute;
    top: 4px;
    right: 4px;
    top:4px;
    bottom:4px;
    line-height: 38px;
    padding: 0 16px;
    z-index: 10;
}


.fancy-file-upload.fancy-file-primary>span.button {
    background-color:#333;
}
.fancy-file-upload.fancy-file-success>span.button {
    background-color:#4cae4c;
}
.fancy-file-upload.fancy-file-danger>span.button {
    background-color:#d43f3a;
}
.fancy-file-upload.fancy-file-warning>span.button {
    background-color:#eea236;
}
.fancy-file-upload.fancy-file-info>span.button {
    background-color:#46b8da;
}
.fancy-file-upload.fancy-file-default>span.button {
    color:#666;
    background-color:rgba(0,0,0,0.1);
}

.fancy-file-upload>input[type=text] {
    background-color:transparent;
    padding-left: 36px;
    border-radius: 0px;
}
.fancy-file-upload>input[type=file] {
    width: 100%;
    height: 100%;
    cursor: pointer;
    padding: 8px 10px;
    position: absolute;
    -moz-opacity: 0;
    opacity: 0;
    z-index: 11;
    bottom: 0;
    right: 0;
 border-radius: 0px;
}
.fancy-file-upload>i {
    position:absolute;
    top: -1px;
    width: 42px;
    height: 42px;
    color: inherit;
    line-height: 42px;
    position: absolute;
    text-align: center;
    color:#888;
    z-index: 10;
}

.btn-dirtygreen {
    background-color: #1693A5;
    color: #FFF !important;
    border-radius: 0px;
    font-size: 12px !important;
    margin-left: -22px;
    margin-right: 0;
    width: 113%;
    line-height: 23px;
    float: left;
}

.modal-body{
    padding: 0px 15px 5px 15px;
}
.upload-demo-wrap{
    padding-top: 55px;
    padding-bottom: 15px;
}
 
 
    `],
    template: `
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
           <div class="modal-dialog" role="document" [class.thumbnail-w]="window === 'thumbnail'" [class.cover-w]="window === 'cover'" [class.menu-w]="window === 'menus'">
              <div class="modal-content">
                    <div class="modal-header bg-blue"> <!-- bg-blue bg-yellow bg-green -->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div>  
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <!-- custom file upload -->
                                        <div class="fancy-file-upload fancy-file-warning">
                                            <i class="fa fa-file"></i>
                                            <input type="file" class="form-control" id="upload" value="Choose a file" (change)="readFile($event)" accept="image/*" />
                                            <input type="text" class="form-control" [placeholder]="fileName" readonly="">
                                            <span class="button">Choose File</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3" *ngIf="disableUpload">
                                        <a class="btn btn-3d btn-dirtygreen" disabled><i class="fa fa-upload p-r-10"></i>Upload</a>
                                    </div>
                                    <div class="col-sm-3" *ngIf="!disableUpload">
                                        <a (click)="uploadImage()" class="btn btn-3d btn-dirtygreen"><i class="fa fa-upload p-r-10"></i>Upload</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="upload-demo-wrap">
                                    <div id="upload-demo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
        <div *ngIf="isLoading">
            <main-pre-loader></main-pre-loader>
        </div>
    `
})
export class ImageUploadModalComponent implements AfterViewInit {
    window: string;
    uploadCrop: any;
    isLoading = false;
    fileName: string = 'No file selected';
    disableUpload=true;

    constructor(
        private injector: Injector,
        private s3Service: AWSS3Service,
        private uploadService: ImageUploadService
    ) {
        this.window = this.injector.get('window');
    }

    ngAfterViewInit() {

        $("#myModal").modal();

        if (this.window === 'thumbnail') {
            this.uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 280,
                    height: 400,
                    type: 'square'
                },
                boundary: {
                    width: 400,
                    height: 390
                },
                showZoomer: false,
                enableZoom: false
            });
        }
        else if (this.window === 'cover') {
            this.uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 700,
                    height: 400,
                    type: 'square'
                },
                boundary: {
                    width: 720,
                    height: 390
                },
                showZoomer: false,
                enableZoom: false
            })
        }
        else if (this.window === 'profile') {
            this.uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 300,
                    height: 300,
                    type: 'circle'
                },
                boundary: {
                    width: 320,
                    height: 320
                },
                showZoomer: false,
                enableZoom: false
            })
        }
        else if (this.window === 'menus') {
            this.uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: {
                    width: 320,
                    height: 440,
                    type: 'square'
                },
                boundary: {
                    width: 400,
                    height: 450
                }
            });
        }
    }

    readFile(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            this.fileName = event.target.files[0].name;
            reader.onload = function (e: any) {

                var img = new Image;
                img.src = reader.result;
                img.onload = function () {
                    this.disableUpload = false;
                }.bind(this);

                $('.upload-img').addClass('ready');

                this.uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            }.bind(this);
            reader.readAsDataURL(event.target.files[0]);
        }
        else {
            //swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    uploadImage() {
        this.isLoading = true;
        this.uploadCrop.croppie('result', {
            type: 'blob',
            size: this.getImageSize(this.window),
            quality: (this.window === 'cover') ? 0.5 : 1,
            format: 'jpeg'

        }).then(function (img) {

            this.s3Service.uploadData({
                Key: getImageId(),
                Body: img,
                ACL: 'public-read',
                ContentType: img.type
            }, this.window).
                then(
                res => {
                    //console.log('res',res);
                    $("#myModal").modal('hide');
                    let imageData = {
                        response: res,
                        window: this.window
                    }
                    this.uploadService.setImage(imageData);
                    this.isLoading = false;
                }
                ).catch(err => console.log('upload error', err));

            function getImageId() {
                return Math.floor(Math.random() * (1000000 - 100 + 1)) + 100 +
                    new Date().toJSON().slice(0, 19).replace(/-|:/g, '') + 'IMG';
            }
        }.bind(this));
    }

    getImageSize(window: string): any{
        if(window === 'cover' || window === 'thumbnail'){
            return 'original';
        }

        /*else if(window === 'thumbnail'){
            return {width: 560, height: 800};
        }*/

        else if(window === 'profile'){
            return 'viewport';
        }
    }




}