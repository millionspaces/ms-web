import {Component, AfterViewInit, OnInit, Injector} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {BasicValidators} from "../../shared/validators/basic.validators";
import {PasswordValidators} from "../../shared/validators/password.validators";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {UserService} from "../../user/user.service";
import {Router} from "@angular/router";
import {CryptoService} from "../../shared/services/crypto.service";
import {User} from "../../user/user.model";

declare var $;
@Component({
    selector: 'signin-modal',
    templateUrl: './signin-modal.component.html',
    styles: [`
        .login-box-msg, .register-box-msg{
            border-bottom: none;
            margin-bottom: 2rem;
            font-weight:700;
        }
        .login-box-body{
         background-color:#ffffff !important;
         min-height: 476px;
        }        
        .user-input {
            border: 1px solid #95d5f1;
            height: 38px;
            padding: 6px 12px !important;
            margin-bottom:1rem;
        }
        .btn-common{
          color: #252424;
          background-color: #ffdd57;
          border: 1px solid #ffefb2t;
          text-transform: uppercase;
          text-align: center;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 1.4rem !important;
          width: 100%;
          margin: 10px 0;
          font-weight:500;
        }
        .btn-common:hover, .btn-common:active, .btn-common:focus{
            color: #252424 !important;
            background-color: #ffdd57 !important;
        }
        .guest-email {
            margin: 1.5rem 0;
        }
        .btn-guest {
        color: #fcf5f4;
        background-color: #29aae3;
        border: 1px solid #34aee4;
        text-transform: uppercase;
          text-align: center;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 1.4rem !important;
          width: 100%;
          margin: 10px 0;
          font-weight:500;
        }
        .btn-wrap-social {
            position: relative;
            padding-left: 4.3rem !important;
            text-align: left;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            border-radius: 0;
            font-size: 1.4rem;
          }
        
          .btn-wrap-social > .fa-icon{
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            width: 32px;
            line-height: 1.9rem;
            text-align: center;
            border-right: 1px solid rgba(199, 199, 199, 0.5);
            display: block;
            margin: 10px 0;
            font-size: 1.5rem;
          }
        .text-account{
            text-align: center;
            display: block;
            font-weight: 700;
            color:#252424;
        }
        .text-link{
          color: #03a3d3;
        }
        .text-account:hover{
            color:#252424;
        }
        .text-forgot {
            color: #4eb9e8;
            text-align: center;
            margin-top: 10px;
            cursor: pointer;
            font-weight:500;
        }
        .divider {
            margin-top: 10px;
            text-align: center;
            color: #818286;
        }
        .social-auth-links{
            border-bottom: 0 !important;
        }
        .has-error .form-control {
            border: 1px solid #c40000!important;
            box-shadow: none;
            background-color:transparent;
        }
        .field-error{
            color: #c40000 !important;
            text-align:left !important;
            font-size:1.2rem;
            margin-top: -1rem;
        }
      
        .login-box{
            font-size: 16px;
        }
        .form-control{
            font-size: 16px;
            color: #5a5a5a;
        }
        .form-control-icon{
            padding: 14px 12px 8px 6px;
        }
        .hoverEft:hover {
                color: #51c5e8;
                cursor: pointer;
         }
         .proceed-as-guest-link {
            margin-bottom: 3rem;
            display: block;
            background: dodgerblue;
            color: #FFFFFF;
            padding: 1rem;
            width: 100%;
            cursor: pointer;
         }
         .guest-email {
            margin: 1rem 0;
            padding: 12px 10px;
         }
        .user-icon {
            float: left;
            position: relative;
            left: 5px;
            top: 2px;
        }
    `]
})
export class SigninModalComponent implements OnInit, AfterViewInit{

    user;
    loginForm: FormGroup;
    signupForm: FormGroup;
    formData: any = { email: null, password: null }
    signupFormData: any = { email: null, password: null, name: null }
    showSignin = true;
    showSignup = false;
    guestProceed = false;
    navigateTo: string;
    showForgotPasswordScreen = false;

    emailPattern = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";

    constructor(
        private fb: FormBuilder,
        private _authService: AuthenticationService,
        private userService: UserService,
        private router: Router,
        private crptoService: CryptoService,
        private injector: Injector
    ) {
        this.navigateTo = this.injector.get('navigateTo');
        this.loginForm = fb.group({
            email: ['', BasicValidators.email],
            password: ['', Validators.required]
        });
        this.signupForm = fb.group({
                email : ['', BasicValidators.email,
                    new BasicValidators(this._authService).emailExistent.bind(this)],
                name : ['', Validators.required],
                password : ['', Validators.compose([
                    Validators.required,
                    PasswordValidators.shouldHaveMinimumCharacters
                ])],
            },
        );
    }

    ngOnInit(){
        /*let user = this.userService.getUser();

        if(user){
            this.user = user;
            return;
        }
        let isLoggedIn = localStorage.getItem('ES_USR');

        if(!user && isLoggedIn && isLoggedIn === 'Y')
            this.authService.getLoggedUser().subscribe(
                user => {
                    this.user = user;
                    this.userService.setLoggedUser(user);
                }
            );*/
    }

    ngAfterViewInit(){
        $('html').addClass('hidden-scroll');
        $("#loginModal").modal();

        $('#loginModal').on('hidden.bs.modal', function () {
            $('html').removeClass('hidden-scroll');
        })
        $('#loginModal').on('show.bs.modal', function () {
            $('html').addClass('hidden-scroll');
        })
    }

    login(userInput){
        this._authService.login(userInput)
            .subscribe(
                user => {
                    $("#loginModal").modal('hide');
                    this.router.navigate([this.navigateTo]);
                },
                error => {
                    this.loginForm.controls['password'].setErrors({
                        invalidLogin: true
                    })
                });
    }

    registerNow(){
        let result;

        let userToBeRegistered : User = {
            email: this.signupFormData.email,
            name: this.signupFormData.name,
            password: this.crptoService.sha256Hex(this.signupFormData.password),
        }

        result = this.userService.registerUser(userToBeRegistered);
        result.subscribe(
            resp => {
                this._authService.login({
                    email: userToBeRegistered.email,
                    password: this.signupFormData.password
                })
                    .subscribe(
                        user => {
                            $("#loginModal").modal('hide');
                            this.router.navigate([this.navigateTo]);
                        });
            },
            error => {
                console.log('error signup ',error);
            }
        );
    }

    showSignupForm(){
        this.showSignup = true;
        this.showSignin = false;
        this.guestProceed = false;
    }

    showSigninForm(){
        this.showSignin = true;
        this.showSignup = false;
        this.guestProceed = false;
    }

    porceedAsGuest(){
        this.guestProceed = true;
        this.showSignup = false;
        this.showSignin = false;
    }

    showForgotPasswordScreenForm(){
        $("#loginModal").modal('hide');
        this.showForgotPasswordScreen = true;

    }

    toggleScreen(visible){
        this.showForgotPasswordScreen = visible;
        $("#loginModal").modal('show');
    }

    submitEmailForm(form) {
        this._authService.guestLogin(form.email)
            .subscribe(
                user => {
                    $("#loginModal").modal('hide');
                    this.router.navigate([this.navigateTo]);
                },
                error => {
                   console.error(error)
                });


    }

}