import {Component, Injector, AfterViewInit} from "@angular/core";
declare var $;
@Component({
    selector: 'info-modal',
    template: `
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-blue"> <!-- bg-blue bg-yellow bg-green -->
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="modal-icon"><span><i class="fa fa-times"></i></span></div>
                                <div class="modal-text">
                                    <h4>{{heading}}</h4>
                                    <p>{{body}}</p>
                                </div>
                            </div>
                        </div>
                        <!--<div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>-->
                    </div>
                </div>
            </div>
    `
})
export class InfoModalComponent implements AfterViewInit{

    heading: string;
    body: string;

    constructor(private injector: Injector) {
        this.heading = this.injector.get('heading');
        this.body = this.injector.get('body');
    }

    ngAfterViewInit(){
        $("#myModal").modal();
    }

}