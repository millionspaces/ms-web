import {Injectable} from "@angular/core";

declare let moment: any;

@Injectable()
export class FifaMascotService {

    spacesList: Array<any>;

    constructor() {
        this.spacesList = [
            { id: 204, date: '07/02/18'},
            { id: 323, date: '07/03/18'},
            { id: 422, date: '07/04/18'},
            { id: 272, date: '07/05/18'},
            { id: 276, date: '07/06/18'},
            { id: 382, date: '07/07/18'},
            { id: 391, date: '07/08/18'},
            { id: 425, date: '07/09/18'},
            { id: 433, date: '07/10/18'},
            { id: 237, date: '07/11/18'},
            { id: 283, date: '07/12/18'},
            { id: 302, date: '07/13/18'},
            { id: 331, date: '07/14/18'},
            { id: 411, date: '07/15/18'}
        ]
    }

    isValidPromotedTime(id): boolean {
        const validSpaces = this.spacesList.filter(space => space.id === id);

        if(validSpaces.length === 0) return false;

        if(moment().format("MM/DD/YY") !== validSpaces[0].date) return false;

        return true;
    }

}