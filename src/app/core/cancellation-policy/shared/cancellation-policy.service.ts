import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {ServerConfigService} from "../../../server.config.service";
import {environment} from "../../../../environments/environment";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CancellationPolicyService{
    baseUrl: string;

    constructor(
        private _serverConfigService: ServerConfigService,
        private _http: Http
    ){
        this.baseUrl = environment.apiHost+'/common/cancellationPolicies';
    }

    getpolicyList() : Observable<CancellationPolicy[]>{
        return this._http
            .get(this.baseUrl, this._serverConfigService.getCookieHeader())
            .map((response: Response) => <CancellationPolicy[]>response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }
}

export class CancellationPolicy{
    id: number;
    name: string;
    description: string;
}

