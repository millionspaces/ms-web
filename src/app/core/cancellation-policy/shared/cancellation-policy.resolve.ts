import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {CancellationPolicy, CancellationPolicyService} from "./cancellation-policy.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CancellationPolicyResolve implements Resolve<any>{

    constructor(
        private policyService: CancellationPolicyService,
    ){}

    resolve() : Observable<CancellationPolicy[]>{
        return this.policyService.getpolicyList();
    }

}