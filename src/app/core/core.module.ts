import { NgModule } from "@angular/core";
import { ModalComponent } from "./modal/modal.component";
import { ErrorModalComponent } from "./modal/error-modal.component";
import { SuccessModalComponent } from "./modal/success-modal.component";
import { SigninModalComponent } from "./modal/signin-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GoogleServicesAPIModule } from "../google-services-api/google-services-api.module";
import { FbServicesAPIModule } from "../fb-auth/fb-services-api.module";
import { SharedModule } from "../shared/shared.module";
import { CancellationPolicyComponent } from "./cancellation-policy/cancellation-policy.component";
import { CancellationPolicyResolve } from "./cancellation-policy/shared/cancellation-policy.resolve";
import { CancellationPolicyService } from "./cancellation-policy/shared/cancellation-policy.service";
import { CoreRouteModule } from "./core.route.module";
import { InfoModalComponent } from "./modal/info-modal.component";
import { WarningModalComponent }  from "./modal/warning-modal.component";
import { RefundCalModalComponent } from "./modal/refund-cal-modal.component";
import { RefundCalModalService } from "./modal/refund-cal-modal.service";
import { SpaceReviewModalComponent } from "./modal/space-review-modal.component";
import { ImageUploadModalComponent } from "./modal/image-upload-modal.component";
import { ImageUploadService } from "./modal/image-upload.service";
import { CommonModule } from "@angular/common";
import { UnsavedDataModalComponent } from "./modal/unsaved-data-moda.component";
import {FifaMascotService} from "./fifa-mascot.service";

@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        GoogleServicesAPIModule,
        FbServicesAPIModule,
        CoreRouteModule,
        SharedModule,
    ],
    declarations: [
        ModalComponent,
        ErrorModalComponent,
        SuccessModalComponent,
        SigninModalComponent,
        InfoModalComponent,
        WarningModalComponent,
        RefundCalModalComponent,
        CancellationPolicyComponent,
        SpaceReviewModalComponent,
        ImageUploadModalComponent,
        UnsavedDataModalComponent,
    ],
    exports: [
        ModalComponent,
        ErrorModalComponent,
        SuccessModalComponent,
        SigninModalComponent,
    ],
    providers: [
        CancellationPolicyResolve,
        CancellationPolicyService,
        RefundCalModalService,
        ImageUploadService,
        FifaMascotService
    ]
})
export class CoreModule{

}