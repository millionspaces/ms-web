import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CancellationPolicyComponent} from "./cancellation-policy/cancellation-policy.component";


const CORE_ROUTES = [
    {
        path: 'booking/cancellation-policy',
        component: CancellationPolicyComponent ,
    }]

@NgModule({
    imports: [RouterModule.forChild(CORE_ROUTES)],
    exports: [RouterModule]
})

export class CoreRouteModule{

}