import {Component, Input} from '@angular/core';
import {AuthenticationService} from "../shared/services/authentication.service";
import {Router} from "@angular/router";
import {UserService} from "../user/user.service";

declare let $: any;

@Component({
  selector: 'app-navbar-v2',
  templateUrl: './navbar-v2.component.html',
  styleUrls: ['./navbar-v2.component.css']
})
export class NavbarV2Component  {

  @Input() user;

  constructor(
      private authService: AuthenticationService,
      public router: Router,
      private userService: UserService,
  ) { }

  signInUser(){
    this.router.navigate(['user/signin']);
  }

  signOutUser(){
    this.authService.logout().subscribe(
        resp => {
          console.log(resp);
        },
        error => {
          console.log('error from nav', error);
        },
        () => {
          this.router.navigate(['home']);
          this.userService.clearUser();
        });
  }

  goToProfile(){
    this.router.navigate(['user/profile/my-profile']);
  }
}
