import { Component } from '@angular/core';
import { RouteDataService } from "../shared/services/route.data.service";
import { Router} from "@angular/router";

@Component({
    selector: 'page-footer',
    templateUrl: './footer.component.html',
    styles: [`
    .main-footer {
        background: #4c4c4c;
        padding: 60px 0;        
        color: #b7b2b2;
    }
    .footer-details-block h4 {
        text-transform: uppercase;
        font-size: 20px;
        font-weight: bold;                
        color: #b7b2b2;
    }
    .contact-details h4 {
        margin-bottom: 0;
        margin-top: 15px;
    }
    .footer-social-icons-wrapper {
        padding: 0;
        margin-bottom: 15px;
    }
    .footer-social-icons-wrapper > li {
        display: inline-block;
        font-size: 22px;
        padding-right: 15px;
    }
    .footer-social-icons-wrapper > li > a {
        color: #e2e2e2;
    }
    .copyrights-info > a {
        color: #bbd4e5;
    }
    .event-types-list {
        list-style: none;
        padding: 0;
    }
    .event-types-list > li {
        margin-bottom: 5px;    
    }
    .event-types-list > li > a {
        color: #b7b2b2;
        font-size: 16px;
    }
    .event-types-list > li > a:hover {
        color: #ffffff;
    }
    .events-style-lists-wrapper > ul {
        display: inline-block;
    }
    .events-style-lists-wrapper > ul:first-of-type {
        margin-right: 0px;
    }
    @media only screen and (min-width: 990px) {
        .events-style-lists-wrapper > ul:first-of-type {
            margin-right: 30px;
        }
    }
    .footer-contacts-list {
        list-style: none;
        padding: 0;
    }
    .footer-contacts-list > li {
        font-size: 16px;
        margin-bottom: 15px;
    }
    .footer-contacts-list > li {
        color: #b7b2b2;
    }
    .footer-contacts-list > li > span:last-of-type {
        padding-left: 35px;
    }
    .footer-contacts-list > li > span:only-of-type {
        padding: 0;
    }
    .footer-contacts-list > li > i {
        font-size: 26px;
        display: inline-block;
        width: 30px;
        color: #818181;
    }
    .footer-contacts-list > li:first-child > i {    
        position: relative;
        top: 10px;
    }
    .footer-contacts-list > li > a {
        padding-left: 2px;
        color: #b7b2b2;
    }
    
    @media screen and (max-width: 768px) {
      .footer-social-icons-wrapper > li > a {
        font-size: 32px;
        padding: 0 15px;
      }
      .footer-social-icons-wrapper > li {
        padding: 0;
      }
      .footer-details-block {
        text-align: center;
        margin-bottom: 30px;
      }
      .footer-contacts-list > li > i {
        display: inline-block;
        width: 100%;
      }
      .footer-contacts-list > li {
        text-align: center;
      }
      .footer-contacts-list > li > span:last-of-type {
        padding: 0;
      }
    }
  `]
})

/**
 * Class representing FooterComponent
 */
export class FooterComponent{

  /**
   * Creates a FooterComponent
   */
  constructor (
      private routeDataService: RouteDataService,
      private router: Router,
  ){}



  /**
   * Handle footer link clicks
   */
   goToEvent (event) {

      this.routeDataService.clearData ();

      let searchDataRequest = {
          "currentPage": 0,
          "participation": null,
          "budget": null,
          "amenities": null,
          "events": <any> [event.id],
          "location": {address: null, latitude: null, longitude: null},
          "available": null,
          "rules": null,
          "seatingArrangements": null
      }

      this.routeDataService.searchDataRequest = searchDataRequest;
      this.router.navigate (['spaces/list']);

   };


}
