import {NgModule} from "@angular/core";
import {FooterComponent} from "./footer.component";
import {RouterModule} from "@angular/router";
import {SysInfoComponent} from "./sysinfo/sysinfo.component";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
    imports: [BrowserModule, RouterModule],
    declarations: [FooterComponent, SysInfoComponent],
    exports: [FooterComponent, SysInfoComponent]
})
export class FooterModule{

}