import {Component, OnInit} from "@angular/core";
import {ServerConfigService} from "../../server.config.service";
import { environment } from "../../../environments/environment";

@Component({
    styles: [`
        .system-info-wrapper {
            text-align: center;
            width: 600px;
            height: 400px;
            position: fixed;
            top: 50%;
            margin-top: -200px;
            left: 50%;
            margin-left: -300px;
            background: #fff;
            padding: 30px;
        }
        .data-list {
            list-style: none;
        }
        .data-list > li {
            margin-bottom: 5px;
        }
        .data-list > li > div {
            display: inline-block;
            width: 30%;
            text-align: left;
            vertical-align: top;
        }
        .data-list > li > div:last-child {
            font-weight: bold;
            width: 70%;
            word-wrap: break-word;
        }
    `],
    template:`
        <div class="system-info-wrapper" *ngIf="sysInfo">
            <div>
                <h4>System Info</h4>
                <ul class="data-list">
                    <li>
                        <div>1. apiUrl</div><div>{{sysInfo.apiUrl}}</div>
                    </li>
                    <li>
                        <div>2. webUrl</div><div>{{sysInfo.webUrl}}</div>
                    </li>
                    <li>
                        <div>3. ip</div><div>{{sysInfo.ip}}</div>
                    </li>
                    <li>
                        <div>4. enableCache</div><div>{{sysInfo.enableCache}}</div>
                    </li>
                    <li>
                        <div>5. dbUrl</div><div>{{sysInfo.dbUrl}}</div>
                    </li>
                    <li>
                        <div>6. captureFlag</div><div>{{captureFlag}}</div>
                    </li>
                    <li>
                        <div>7. cloudinaryUrl</div><div>{{cloudinaryUrl}}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div *ngIf="!sysInfo">
            <span>Loading....</span>
        </div>
    `
})
export class SysInfoComponent implements OnInit {

    sysInfo: any;
    captureFlag: string;
    cloudinaryUrl: string;

    constructor( private serverConfig: ServerConfigService ){}

    ngOnInit(){
        this.serverConfig.getSystemInfo().subscribe(sysInfo => {
            //console.log('sysInfo', sysInfo);
            this.sysInfo = sysInfo;
            this.captureFlag = environment.paymentCaptureFlag;
            this.cloudinaryUrl = environment.bucket;
        });
    }

}