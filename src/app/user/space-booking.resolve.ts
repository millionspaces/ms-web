import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { BookingService } from "./my-activities/booking.service";

@Injectable()
export class SpaceBookingResolve implements Resolve<any>{

    constructor(private bookingService: BookingService){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{

        let bookingId: number = +route.params['bookingId'];
        let isManual: boolean = JSON.parse(route.queryParams['m']);

        return this.bookingService.getSpaceBooking({bookingId, isManual});
    }

}