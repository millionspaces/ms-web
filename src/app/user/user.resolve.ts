import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {User} from "./user.model";
import {AuthenticationService} from "../shared/services/authentication.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserResolve implements Resolve<any>{

    constructor(
        private authService: AuthenticationService,
    ){}

    resolve() : Observable<User>{
        return this.authService.getLoggedUser();
    }

}