import {Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from '../../../environments/environment';
import {MsUtil} from "../../shared/util/ms-util";
import {BookingService} from "./booking.service";
import {RouteDataService} from "../../shared/services/route.data.service";
import {MSError} from "../../common/errors/ms.error";
import {BadRequestError} from "../../common/errors/bad-request-error";
import {ToasterService} from "../../shared/services/toastr.service";

declare let moment;

@Component({
    templateUrl: './my-activity-details.component.html',
    styles: [`
        /************* GO BACK LINK *************/
        .back-to-space {
            margin-bottom: 10px;
        }

        .back-to-space:hover > a {
            color: #5b92f1;
        }

        /*****************************************/
        .bookings-single-view-space-wrapper {
            background: #fff;
            padding: 10px;
            width: 100%;
            margin: 10px auto;
        }
        @media only screen and (min-width: 768px) {
            .bookings-single-view-space-wrapper {
                width: 60%;
            }
        }
        .bookings-single-view-space-wrapper-inner header > .titles-wrapper {
            vertical-align: middle;
        }
        .bookings-single-view-space-wrapper-inner header > button {
            display: inline-block;
            border: 1px solid #46a5cc;
            background: #fff;
            color: #46a5cc;
            padding: 10px;
        }
        .bookings-single-view-space-wrapper-inner header h1 {
            margin: 0;
        }
        .content-table {
            margin-top: 20px;
        }
        .content-table > div:nth-child(even) {
            background: #f7f7f7;
        }
        .content-table > div > div {
            display: inline-block;
            padding: 10px;
        }
        .content-table > div > div p {
            margin: 0;
            padding: 0;
        }
        .content-table > div > div:first-child {
            width: 30%;
            vertical-align: middle;
            font-weight: bold;
        }
        .content-table > div > div:last-child {
            width: 68%;
            vertical-align: middle;
            padding-left: 10px;
            border-left: 1px solid #e8e8e8;
        }
        .status-label {
            display: inline-block;
            padding: 3px 10px;
            background: #46a5cc;
            color: #fff;
            border-radius: 3px;
        }
        .guest-details-list {
            padding-left: 0;
            list-style: none;
        }
        .guest-details-list p, .guest-details-list i {
            display: inline-block;
        }
        .guest-details-list i {
            width: 20px;
        }
        .seating-content img, .seating-content p {
            display: inline-block;
        }
        .seating-content img {
            margin-right: 10px;
        }
        .content-table-menus button {
            border: 1px solid #46a5cc;
            background: #fff;
            color: #46a5cc;
            padding: 10px;
            margin-right: 10px;
            transition: all 0.3s ease;
        }
        .content-table-menus button:hover {
            background: #46a5cc;;
            color: #fff;
        }
        .extras-type > p {
            font-weight: bold;
            border-bottom: 20px;
            display: block;
        }
        .extras-type ul {
            list-style: none;
            padding-left: 0;
        }
        .extras-type ul > li > i {
            display: inline-block;
            vertical-align: middle;
            margin-right: 5px;
        }

        .extras-type ul > li > p {
            display: inline-block;
            vertical-align: middle;
        }

        textarea {
            width: 100%;
            resize: none;
            padding: 10px;
            border: 1px solid #e2e2e2;
        }
        .content-table {
            border: 1px solid #e8e8e8;
        }
        .cancellation-policy-content p {
            display: inline-block;
        }
        .cancellation-policy-content p:first-child {
            font-weight: bold;
        }
        .save-button-wrapper > button {
            border: 1px solid #e2e2e2;
            background: #46a5cc;
            color: #fff;
            padding: 10px 30px;
            margin: 10px 0;
        }
        .save-button-wrapper > button:hover {
            border: 1px solid #e2e2e2;
        }
        .my-space-booking-modal {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 50px;
            left: 0;
            z-index: 999;
            background: #fff;
            display: none;
            overflow-y: auto;
        }
        .my-space-booking-modal > img {
            width: 100%;
        }
        @media only screen and (min-width: 768px) {
            .my-space-booking-modal {
                width: 400px;
                height: 400px;
                top: 50%;
                margin-top: -200px;
                left: 50%;
                margin-left: -200px;
            }
        }
        .my-space-booking-modal-layover {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.6);
            display: none;
        }
        .close-my-space-booking-modal {
            position: absolute;
            top: 10px;
            right: 10px;
            font-size: 22px;
            color: tomato;
            cursor: pointer;
        }
        .my-space-booking-modal-active {
            display: inline-block;
        }
        .my-space-booking-modal-layover-active {
            display: inline-block;
        }
        .extras-type-chargeable ul li i:before {
            margin-left: 0;
        }
        .extras-type-chargeable ul li i {
            margin-right: 5px;
        }
        .go-back-link {
            position: relative;
            top: 12px;
            left: 50px;
        }
        .go-back-link:hover {
            color: #46a5cc;
        }
        @media only screen and (min-width: 992px) {
            .go-back-link {
                position: relative;
                top: 10px;
                left: 0;
            }
        }
        .activity-details-popup-overlay {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.5);
        }
        .activity-details-popup {
            position: fixed;
            width: 500px;
            height: 500px;
            background: #fff;
            top: 50%;
            left: 50%;
            margin-left: -150px;
            margin-top: -250px;
        }
        .activity-details-popup-close {
            position: absolute;
            right: 20px;
            top: 10px;
            color: tomato;
            cursor: pointer;
        }
        .activity-details-popup-inner {
            padding: 40px 10px 10px 10px;
            height: 100%;
            overflow-y: auto;
        }
        a.menu-item-link {
            margin-right: 10px;
            padding: 2px 10px;
        }
        .btn-wrapper {
            margin-top: 10px;
            display: flex; 
            justify-content: center;
        }
    `]
})
export class MyActivityDetailsComponent {

    myActivity;
    time;
    bucketUrl: string;
    isLoading = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private bookingService: BookingService,
        private routeDataService: RouteDataService,
        private toaster: ToasterService
    ){
        this.myActivity = this.route.snapshot.data['myActivity'];
        this.bucketUrl = environment.bucket;

        this.route.queryParams
            .subscribe(params => {
                this.time = params['time'];
            });
    }

    getBlockDate(timeBlock){
        return moment(timeBlock.fromDate).format('Do MMMM YYYY ');
    }

    getBlockStartTime(timeBlock){
        return moment(timeBlock.fromDate).format('h:mm A');
    }

    getBlockEndTime(timeBlock){
        return moment(timeBlock.toDate).format('h:mm A');
    }

    getBookedDate(booking){
        return moment(booking.bookedDate).format('Do MMMM YYYY');
    }

    toggleMenu: boolean = false;
    menuData: any;
    showMenu(menu: any){
        console.log(menu);
        this.menuData = menu;
        this.toggleMenu = !this.toggleMenu;
    }

    closeModal(){
       this.toggleMenu = false;
    }

    showSpace(space: any) {
        const spaceName = MsUtil.replaceWhiteSpaces(space.name, '-');
        window.open(`#/spaces/${space.id}/${spaceName}`, '_blank');
    }

    goToPayments(bookingId) {

        this.isLoading = true;

        let paymentSummaryData = {
            manageBooking: true,
            bookingId: null,
            referenceId: null,
            space: null,
            organization: null,
            address: null,
            date: null,
            reservationTime: null,
            total: null,
            dateDiff: null,
            payLaterEnabled: null,
            manualPaymentAllowed: false,
            promotion: ''
        }

        this.bookingService.continueToPayLater(bookingId).subscribe(resp => {
            console.log('resp', resp);

                const dateString = resp.reservationTime
                    .map(date => date.fromDate + ' - ' + date.toDate)
                    .join();

            paymentSummaryData.manageBooking = true;
            paymentSummaryData.bookingId = bookingId;
            paymentSummaryData.referenceId = resp.referenceId;
            paymentSummaryData.space = resp.spaceName;
            paymentSummaryData.organization = resp.organization;
            paymentSummaryData.address = resp.address;
            paymentSummaryData.date = moment(resp.reservationDate).format('Do MMMM YYYY dddd')
            paymentSummaryData.reservationTime = dateString;
            paymentSummaryData.total = resp.bookingCharge.toFixed(2);
            paymentSummaryData.dateDiff = null;
            paymentSummaryData.payLaterEnabled = resp.isPayLaterEnabled;
            paymentSummaryData.manualPaymentAllowed = resp.isBookingTimeEligibleForManualPayment;
            paymentSummaryData.promotion = (resp.promotion) ? JSON.stringify(resp.promotion) : '';

            this.routeDataService.clearData();
            this.routeDataService.paymentSummaryData = paymentSummaryData;
            this.router.navigate(['payments/order/summary']);
        },
        (error: MSError) => {
            console.error('error', error)
            if(error instanceof BadRequestError){
                this.toaster.error('Something went wrong with the request', 'Error');
            }
            else throw error;
        },
        () => {
            this.isLoading = false;
        });

    }
}