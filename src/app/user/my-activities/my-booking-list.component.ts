import {Component, Input, OnInit, OnChanges, SimpleChanges, AfterViewInit } from "@angular/core";
import {MsUtil} from "../../shared/util/ms-util";
import {MomentService} from "../../shared/services/moment.service";
import {environment} from "../../../environments/environment";
import {BookingService} from "./booking.service";
import {ToasterService} from "../../shared/services/toastr.service";
import {Router} from "@angular/router";

declare let moment: any;
declare let $: any;

@Component({
    selector: 'my-booking-list',
    templateUrl: 'my-booking-list.component.html',
    styles: [`
        .ratings-modal-window {
            position: fixed;
            left: 0;
            top: 0;
            z-index: 999;
            background: #fff;
            width: 100%;
            height: 100%;
            top: 60px;
            padding: 10px;
            text-align: center;
            display: none;
        }
        .ratings-modal-window h1 {
            font-weight: 300;
            margin-top: 0;
        }
        @media only screen and (min-width: 768px) {
            .ratings-modal-window {
                border: 1px solid #e2e2e2;
                width: 800px;
                height: 482px;
                left: 50%;
                margin-left: -400px;
                top: 50%;
                margin-top: -260px;
            }
        }
        .review-title-wrapper, .description-block {
            text-align: left;
        }
        .description-block {
            width: 48%;
            display: inline-block;
        }
        .description-blocks > .description-block:first-child {
            margin-right: 4%;
        }
        .review-title-wrapper label, .description-block label {
            display: block;
            font-size: 1.2rem;
            font-weight: bold;
        }
        .review-title-wrapper input {
            height: 40px;
            border: 1px solid #e2e2e2;
            width: 100%;
            padding: 5px;
        }
        textarea {        
            border: 1px solid #e2e2e2;
            width: 100%;
            resize: none;
            height: 140px;
            padding: 5px;
        }
        .review-block {
            display: inline-block;
            width: 24%;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .review-block i.fa.fa-star {
            font-size: 20px;
            color: #bfbfbf;
        }
        .review-block .active-star i.fa.fa-star {
            color: #f6de26;
        }
        .review-block h3 {
            font-size: 14px;
            margin-top: 0;
            text-decoration: underline;
        }
        .ratings-modal-submit-btn button  {
            border: 1px solid #43b0ff;
            background: #43b0ff;
            color: #fff;
            padding: 5px 10px;
            transition: all 0.3s;
            font-weight: bold;
        }
        .ratings-modal-window-overlay {
            position: fixed;
            display: none;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 888;
            background: rgba(0, 0, 0, 0.6);
        }
        .ratings-modal-window-active {
            display: inline-block;
        }
        .ratings-modal-window-overlay-active {
            display: inline-block;
        }
        .manage-booking-cancellation-popup-details {
            overflow-y: auto;
        }
        .manage-booking-cancellation-block {
            overflow: auto;
            height: 80%;
        }
        .manage-booking-btn {
            margin-bottom: 20px;
        }
        .submit-review-button-disabled {
            background-color: #99d6ff!important;
        }
        .space-count-zero-validation {
            border: 1px solid red!important;
        }
        .disabled-submit-btn {
            background: #fff!important;
            color: #9c9c9c!important;;
            border: 1px solid #9c9c9c!important;;
            cursor: not-allowed;
            font-weight: bold;
        }
        .disabled-submit-btn:hover {
            color: #9c9c9c!important;
            border: 1px solid #9c9c9c;
            background: #fff;
        }
        .today-indi-text {
            position: relative;
            top: -27px;
            left: -12px;
            font-size: 10px;
        }
        .word-count {
            position: relative;
            display: inline-block;
            width: 100%;
            text-align: right;
            top: -25px;
            font-size: 12px;
            right: 4px;
        }
    `]

})
export class MyBookingListComponent implements OnInit, OnChanges, AfterViewInit {

    @Input('guestBookings') guestBookings;
    @Input('time') time;

    toBecancelledBooking: any;
    toBeReviewBooking: any;
    reviewedData: any;

    // 3 bookings for each row
    chunkBookings: any[] = [];

    flexibleHours: number = 48;
    moderateHours: number = 168;
    strictHours: number = 168;

    //Reviews
    myReview = {
        services: 0,
        cleanliness: 0,
        valueForMoney: 0,
        msExp: 0
    }

    review: any = {
        title : '',
        desc : '',
        msExp: ''
    }

    bucket: string;

    constructor(
        private momentService: MomentService,
        private bookingService: BookingService,
        private toaster: ToasterService,
        private router: Router
    ){}

    ngOnInit(){

       let myArray: any[] = this.guestBookings;
       this.chunkBookings = MsUtil.createGroupedArray(myArray, 3);

        this.bucket = environment.bucket;
    }

    ngAfterViewInit () {

        var $services = $('.services'),
            $cleanliness = $('.cleanliness'),
            $valueForMoney = $('.value-for-money'),
            $msExp = $('.ms-exp');

        let that = this;

        this.rateEvent ($services, 'services', that);
        this.rateEvent ($cleanliness, 'cleanliness', that);
        this.rateEvent ($valueForMoney, 'valueForMoney', that);
        this.rateEvent ($msExp, 'msExp', that);

    }

    rateEvent(DOMContainer, option, that){
        DOMContainer.find('span').each((index, elm) => {
            $(elm).attr('data-rating', index+1)
        })

        DOMContainer.find('span').on('click', function () {
            DOMContainer.find('span').each(function () {
                $(this).removeClass('active-star');
            })
            for (var i=1; i<=$(this).data('rating');i++) {
                $(this).addClass('active-star').prevAll().addClass('active-star')
            }
            if(option === 'services'){
                that.myReview.services = $(this).data('rating');
            }else if(option === 'cleanliness'){
                that.myReview.cleanliness = $(this).data('rating');
            }else if(option === 'valueForMoney'){
                that.myReview.valueForMoney = $(this).data('rating');
            }else if(option === 'msExp'){
                that.myReview.msExp = $(this).data('rating');
            }

        });
    }


    ngOnChanges(changes: SimpleChanges){
        let myArray: any[] = this.guestBookings;
        this.chunkBookings = MsUtil.createGroupedArray(myArray, 3);
    }

    onCancelBooking(event){

        this.toBecancelledBooking = event.cancelBooking;

        let beforeDateCount = moment().diff(moment(event.cancelBooking.bookedDate), 'days') + 1;


        console.log('datecount', beforeDateCount);

        $('.cancel-modal').show();

    }

    onReviewBooking(event){
        this.toBeReviewBooking = event.reviewBooking;
    }

    starServices: number;
    startCleanliness: number;
    starValueForMoney: number;
    startMSExp: number;

    starCountArray = [];

    onReviewedBooking(event) {
        this.reviewedData = event.reviewObject;

        console.log(event.reviewObject);

        const rateArray = event.reviewObject.rate.split('|');

        this.starServices = +rateArray[1];
        this.startCleanliness = +rateArray[2];
        this.starValueForMoney = +rateArray[3];
        this.startMSExp = +rateArray[4];

        this.starCountArray = Array(5).fill(0);

    }

    closeModal(){
        $('.cancel-modal').hide();
    }

    closeReviewModal(){
        this.toBeReviewBooking = null;

        //reset to defaults
        $('.services').find('span').each(function () {
            $(this).removeClass('active-star');
        })
        $('.cleanliness').find('span').each(function () {
            $(this).removeClass('active-star');
        })
        $('.value-for-money').find('span').each(function () {
            $(this).removeClass('active-star');
        })
        $('.ms-exp').find('span').each(function () {
            $(this).removeClass('active-star');
        })
        this.myReview.services = 0;
        this.myReview.cleanliness = 0;
        this.myReview.valueForMoney = 0;
        this.myReview.msExp = 0;
        $('.mandatoryReviewField').val("");
    }

    closePreview() {
        this.reviewedData = null;
    }

    getFromTime(booking){
        return moment(booking.dates[0].fromDate).format('h:mm A');
    }

    getEndTime(booking){
        return moment(booking.dates[0].toDate).format('h:mm A');
    }

    getEventDay(booking){
        return moment(booking.dates[0].fromDate).format('DD MMM YY');
    }

    getToday(){
        return moment().format('DD MMM YY');
    }

    getBookedDate(booking){
        return moment(booking.bookedDate).format('DD MMM YYYY');
    }

    getLastRefundDate(booking) {

        if (booking.space.cancellationPolicy.id === 1) {
            // flexible
            return moment(booking.dates[0].fromDate).subtract(2, "days").format('DD MMM YY');

        } else if (booking.space.cancellationPolicy.id === 2) {
            // moderate
            return moment(booking.dates[0].fromDate).subtract(7, "days").format('DD MMM YY');

        }
        return moment(booking.dates[0].fromDate).subtract(2, "days").format('DD MMM YY');
    }

    isTodayWithinNoRefundSide(booking) {
        const dateFormat = moment().format('YYYY-MM-DD');
        const today = moment(dateFormat);
        const eventDate = moment(booking.dates[0].fromDate);

        const dateDiff = eventDate.diff(today, 'days');

        if(booking.space.cancellationPolicy.id === 1 && dateDiff <= 2){
            return true;
        }else if(booking.space.cancellationPolicy.id === 2 && dateDiff <= 7){
            return true;
        }else {
            return false;
        }
    }

    getCancellablePortian(booking) {
        if (booking.space.cancellationPolicy.id === 1) {
            const daysBeforeLastRefund = moment(booking.dates[0].fromDate).subtract(2, "days").diff(moment(), 'days') + 1;
            const daysAfterLastRefund = moment(booking.dates[0].fromDate).diff(moment(booking.dates[0].fromDate).subtract(2, "days"), 'days');
            const daysCount = daysBeforeLastRefund + daysAfterLastRefund;

            return (daysBeforeLastRefund / daysCount) * 100;
        }
        else if (booking.space.cancellationPolicy.id === 2) {
            const daysBeforeLastRefund = moment(booking.dates[0].fromDate).subtract(7, "days").diff(moment(), 'days') + 1;
            const daysAfterLastRefund = moment(booking.dates[0].fromDate).diff(moment(booking.dates[0].fromDate).subtract(7, "days"), 'days');
            const daysCount = daysBeforeLastRefund + daysAfterLastRefund;

            return (daysBeforeLastRefund / daysCount) * 100;
        }
    }

    getNoRefundPortian(booking) {
        if (booking.space.cancellationPolicy.id === 1) {
            const daysBeforeLastRefund = moment(booking.dates[0].fromDate).subtract(2, "days").diff(moment(), 'days') + 1;
            const daysAfterLastRefund = moment(booking.dates[0].fromDate).diff(moment(booking.dates[0].fromDate).subtract(2, "days"), 'days');
            const daysCount = daysBeforeLastRefund + daysAfterLastRefund;

            return (daysAfterLastRefund / daysCount) * 100;
        }
        else if (booking.space.cancellationPolicy.id === 2) {
            const daysBeforeLastRefund = moment(booking.dates[0].fromDate).subtract(7, "days").diff(moment(), 'days') + 1;
            const daysAfterLastRefund = moment(booking.dates[0].fromDate).diff(moment(booking.dates[0].fromDate).subtract(7, "days"), 'days');
            const daysCount = daysBeforeLastRefund + daysAfterLastRefund;

            return (daysAfterLastRefund / daysCount) * 100;
        }
    }

    getRefundAmount(booking){

        if(booking.reservationStatus === 2) return 0;

        if(this.isTodayWithinNoRefundSide(booking)){
            return 0;
        }

        let refundAmount = 0;

        let hours = this.momentService.getDuration(booking.dates[0].fromDate, this.momentService.getMoment());

        if(booking.space.cancellationPolicy.id === 1){
            if(hours > this.flexibleHours){
                refundAmount = booking.bookingCharge * booking.space.cancellationPolicy.refundRate;
            }else if(hours <= this.flexibleHours){
                refundAmount = 0;
            }
        }

        else if(booking.space.cancellationPolicy.id === 2){
            if(hours > this.moderateHours){
                refundAmount = booking.bookingCharge * booking.space.cancellationPolicy.refundRate;
            }else if(hours <= this.moderateHours){
                refundAmount = 0;
            }
        }

        else if(booking.space.cancellationPolicy.id === 3){
            refundAmount = 0;
        }

        return refundAmount;
    }

    goToSpace(space: any){
        var win = window.open(environment.host+"/#/spaces/"+space.id+'/'+MsUtil.replaceWhiteSpaces(space.name, '-'), '_blank');
        win.focus();
    }

    cancelThisBooking(booking: any){

        let res = confirm("Are you sure, you want to cancel this booking?");
        if (res !== true) return;

        let cancelRequest = {
            booking_id: booking.id,
            event: 3,
            refund: this.getRefundAmount(booking)
        }

        this.bookingService.updateBooking(cancelRequest).subscribe(response => {
            if(response.new_state === 'CANCELLED' && (response.old_state === 'PAYMENT_DONE' || response.old_state === 'PENDING_PAYMENT') ){
                this.closeModal();
                this.toaster.success('Booking was successfully cancelled', 'Success');
                booking.reservationStatus.id = 4;
                booking.reservationStatus.label = 'Cancelled';
            }else{
                this.toaster.error('Something went wrong', 'Error');
            }
        });
    }

    submitReview(review){

        let reviewTitle = review.title.trim();
        let reviewDesc = review.desc.trim();
        let msExp = review.msExp.trim();

        if(reviewTitle === '' || reviewDesc === ''){
            return;
        }

        let serviceRate = +this.myReview.services;
        let cleanlinessRate = +this.myReview.cleanliness;
        let valueForMoneyRate = +this.myReview.valueForMoney;
        let msExpRate = +this.myReview.msExp;

        /*if(!serviceRate || !cleanlinessRate || !valueForMoneyRate || !msExpRate){
            return;
        }*/

        let averageRating = Math.round((serviceRate + cleanlinessRate + valueForMoneyRate + msExpRate) / 4);

        let reviewRequest = {
            rate: averageRating+'|'+this.myReview.services+'|'+this.myReview.cleanliness+'|'+this.myReview.valueForMoney+'|'+this.myReview.msExp,
            title: reviewTitle,
            description: reviewDesc,
            msExperience: msExp,
            createdAt: moment().format('YYYY-MM-DD'),
            bookingId: this.toBeReviewBooking.id
        }

        this.bookingService.saveReview(reviewRequest).subscribe(response => {
            console.log('response', response);
            if(response === 'review sucessfully added'){
                this.toBeReviewBooking.isReviewed = true;
                this.toaster.success(`${this.toBeReviewBooking.space.name} has been reviewed successfully`,'Thank you!');
                reviewTitle = "";
                reviewDesc = "";
                msExp = "";
                $('.mandatoryReviewField').val("");
                this.closeReviewModal();
                location.reload();
            }else{
                $('.mandatoryReviewField').val("");
                this.toaster.error('','Something went wrong!');
            }
        })

    }

    allFieldsRated() {

        let serviceRate = +this.myReview.services;
        let cleanlinessRate = +this.myReview.cleanliness;
        let valueForMoneyRate = +this.myReview.valueForMoney;
        let msExpRate = +this.myReview.msExp;

        if(!serviceRate || !cleanlinessRate || !valueForMoneyRate || !msExpRate){
            return false;
        }

        return true;


    }


}