import { Component, ViewEncapsulation, OnInit, AfterViewInit } from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import { MomentService } from "../../shared/services/moment.service";

@Component({
    templateUrl: './my-activities.component.html',
    styles: [`
        .manage-booking-top-bar {
            background: #fdfdfd;
            position: fixed;
            top: 50px;
            left: 0px;
            border-left: 1px solid #f7f7f7;
            border-bottom: 1px solid #f7f7f7;
            font-weight: 300;
            width: 100%;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-top-bar {
                top: 50px;
                left: 200px;
                width: calc(100% - 200px);
            }
        }
        .manage-booking-top-bar > div {
            display: inline-block;
            vertical-align: middle;
        }
        .manage-booking-top-bar {
            z-index: 99;
        }
        .manage-booking-topbar-title {
            border-right: 1px solid #f7f7f7;
            margin-left: 30px;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-title {
                margin-left: 0;
            }
        }
        .manage-booking-topbar-title h1 {
            margin: 0;
            padding: 5px;
            font-weight: 300;
        }
        .manage-booking-topbar-title {
            width: 100%;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-title {
                width: 70%;
            }
        }
        .manage-booking-topbar-past {
            width: 33%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
            user-select: none;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-past {
                width:  15%;
            }
        }
        .manage-booking-topbar-upcoming {
            width:  33%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
            user-select: none;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-upcoming {
                width:  15%;
            }
        }
        .manage-booking-topbar-switch {
            width: 10%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
        }
        .active-topbar-button {
            background: #43b0ff;
            color: #fff;
        }
        .manage-booking-main-content {
            margin-top: 120px;
        }
        @media only screen and (min-width: 992px) {
            .manage-booking-main-content {
                margin-top: 60px;
            }
        }
        .manage-booking-filters-bar {
            background: #fff;
            margin-bottom: 10px;
            font-weight: 300;
            text-align: right;
        }
        .manage-booking-filters-bar > div {
            display: inline-block;
        }
        .manage-booking-filters-bar > div:last-child {
            text-align: right;
        }
        .manage-booking-filters-bar > div > p {
            padding: 0;
            margin: 0;
        }
        .manage-booking-topbar-past active-topbar-button > i {
            margin-right: 10px;
        }
        /** Upcoming **/
        .manage-booking-block-upcoming {
            background: #ffffff;
            margin-bottom: 10px;
            margin-top: 10px;
            font-weight: 300;
            z-index: 10;
            border: 1px solid #e2e2e2;
        }
        .manage-booking-block-image-wrapper {
            position: relative;
        }
        .booking-status-manage-booking-upcoming {
            position: absolute;
            top: 0;
            display: inline-block;
            color: #fff;
            padding: 3px 10px;
            left: 0;
            font-weight: bold;
            font-size: 20px;
        }
        .booking-status-manage-booking-upcoming-confirmed {
            background: #22A464;
        }
        .booking-status-manage-booking-upcoming-cancelled {
            background: #E1544D;
        }
        .booking-status-manage-booking-upcoming-expired {
            background: #005EA4;
        }
        .booking-status-manage-booking-upcoming-pending {
            background: #FDAF01;
        }
        .manage-booking-block-image-wrapper > img {
            width: 100%;
        }
        .manage-booking-block-upcoming-header > h1 {
            margin: 0;
            font-weight: 300;
            font-size: 22px;
            padding: 10px;
        }
        .manage-booking-block-upcoming-header > p {
            font-size: 14px;
            padding: 0 10px;
            font-weight: 300;
        }
        .manage-booking-block-upcoming-header i {
            color: #43b0ff;
            display: inline-block;
            padding-right: 15px;
        }
        .manage-booking-block-info-block {
            background: #f7f7f7;
        }
        .manage-booking-block-info-block > div {
            padding: 10px;
            display: inline-block;
            width: 50%;
            vertical-align: middle;
            font-size: 14px;
        }
        .manage-booking-block-info-block > div i {
            color: #43b0ff;
            padding-right: 5px;
        }
        .manage-booking-block-upcoming-btns-holder {
            text-align: center;
            padding-right: 5px;

        }
        .manage-booking-block-upcoming-btns-holder > a {
            padding: 10px;
            display: inline-block;
            margin-top: 10px;
            color: #fff;
            width: 100%;
        }
        .manage-booking-block-upcoming-btns-holder > a:first-child {
            color: #43b0ff;
            border: 1px solid #43b0ff;
        }
        .manage-booking-block-upcoming-btns-holder > a:first-child:hover {
            background: #43b0ff;
            color: #fff;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-block-upcoming-btns-holder > a {
                width: 45%;
                padding: 5px 0;
                text-align: center;
            }
        }
        .manage-booking-block-upcoming-btns-holder > a.manage-booking-btn-cancel {
            border: 1px solid #ef5b5b;
            color: #ef5b5b;
            font-weight: bold;
            transition: all 0.3s;
        }
        .manage-booking-block-upcoming-btns-holder > a.manage-booking-btn-cancel:hover {
            background: #ef5b5b;
        }
        .manage-booking-block-upcoming-btns-holder > a:hover {
            color: #fff!important;
            background: #ef5b5b;
        }
        /** Cancellation Block **/
        .manage-booking-cancellation-block {
            margin-top: 10px;
            margin-bottom: 15px;
            background: #fff;
            display: none;
        }
        .manage-booking-cancellation-image-wrapper {
            position: relative;
        }
        .manage-booking-cancellation-image-wrapper img {
            width: 100%;
        }
        .manage-booking-cancellation-header {
        }
        .manage-booking-cancellation-header h1 {
            margin: 0;
            font-size: 22px;
            padding: 10px;
            font-weight: 300;
        }
        .manage-booking-cancellation-header h1 a {
            font-size: 16px;
        }
        .manage-booking-cancellation-header h1 a:hover {
            color: #03a3d3;
        }
        .manage-booking-cancellation-header > p {
            font-size: 16px;
            font-weight: 300;
            padding-left: 10px;
        }
        .manage-booking-cancellation-header > p > i {
            margin-right: 15px;
            color: #43b0ff;
        }
        .manage-booking-block-info-block > div {
            font-weight: 300;
        }
        .cancellation-policy-block-info {
            padding: 10px;
        }
        .manage-booking-block-charge {
            padding: 10px;
            font-weight: bold;
            font-size: 20px;
            color: #48aef9;
            text-align: left!important;
        }
        .cancellation-policy-block-info > p {
            margin-bottom: 0;
            font-weight: bold;
            margin-top: 10px;
        }
        .cancellation-policy-block-info > div {
            display: inline-block;
            margin-top: 10px;
            font-weight: 300;
        }
        .refund-policy-level {
            padding: 3px 10px;
            color: #fff;
            margin-right: 10px;
            border-radius: 3px;
        }
        .refund-policy-moderate {
            background: rgb(31, 179, 70);
        }
        .event-cancellation-time-line {
            margin: 20px 60px 20px 60px;
            display: block;
        }
        .event-cancellation-time-line > div {
            display: inline-block;
            height: 5px;
            magrin: 10px;
            vertical-align: top;
        }
        .event-cancellation-time-line > div > div  {
            display: inline-block;
        }
        .date-left {
            position: absolute;
            top: 13px;
            left: -22px;
            font-size: 10px;
        }
        .date-today {
            position: absolute;
            top: -20px;
            left: 130px;
            font-size: 10px;
        }
        .date-right {
            position: absolute;
            top: 13px;
            right: -22px;
            font-size: 10px;
        }
        .event-notice {
            position: absolute;
            bottom: 13px;
            right: -22px;
            font-size: 10px;
        }
        .last-date {
            position: absolute;
            top: 13px;
            right: -40px;
            font-size: 10px;
        }
        .cancellable-portion {
            width: 70%;
            background: rgb(31, 179, 70);
            position: relative;
        }
        .cancellable-portion:before {
            content: '';
            width: 12px;
            height: 12px;
            background: rgb(31, 179, 70);
            position: absolute;
            border-radius: 50%;
            top: -4px;
            left: -5px;
        }
        .cancellable-portion:after {
            content: '';
            width: 12px;
            height: 12px;
            background: rgb(31, 179, 70);
            position: absolute;
            border-radius: 50%;
            top: -4px;
            right: -6px;
            z-index: 99;
        }
        .locked-portion {
            width: 30%;
            background: #ef5b5b;
            position: relative;
        }
        .locked-portion:after {
            content: '';
            width: 12px;
            height: 12px;
            background: #ef5b5b;
            position: absolute;
            border-radius: 50%;
            top: -4px;
            right: -5px;
        }
        .temp-class-attr:before {
            content: '';
            width: 12px;
            height: 12px;
            background: #ef5b5b;
            position: absolute;
            border-radius: 50%;
            top: -4px;
            left: -5px;
        }
        .event-cancellation-pricing-info {
            padding: 10px;
        }
        .event-cancellation-pricing-info > div > div {
            display: inline-block;
            width: 50%;
            font-weight: 400;
            font-size: 16px;
            color: #000000;
            margin: 5px 0;
        }
        .event-cancellation-pricing-info > div > div:last-child {
            text-align: right;
        }
        .cancellation-block-popup-overlay {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.5);
            z-index: 9999;
            display: none;
        }
        .cancellation-block-popup-overlay.cancellation-block-popup-overlay-active {
            display: block;
        }
        .manage-booking-cancellation-block.manage-booking-cancellation-block-active {
            display: block;
            position: fixed;
            top: 12%;
            left: 0;
            z-index: 99999;
            width: 100%;
            overflow-y: auto;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-cancellation-block.manage-booking-cancellation-block-active {
                width: 40%;
                left: 50%;
                margin-left: -20%;
            }
        }
        .cancellation-block-active-close {
            position: absolute;
            top: 8px;
            right: 10px;
            z-index: 9;
            font-size: 22px;
            color: #fb7d7d;
        }
        .cancellation-block-active-close:hover {
            color: #fb7d7d!important;
        }
        .price-block-wrapper > div {
            display: inline-block;
            width: 50%;
            text-align: right;
        }
        .dropdown {
            width: 200px;
            position: relative;
            margin-top: 1px;
            margin-right: 1px;
        }
        .dropdown > button {
            width: 200px;
            position: relative;
            text-align: left;
            background: #f3f3f3;
            margin-bottom: 1px;
        }
        .dropdown > button > .caret {
            position: absolute;
            top: 18px;
            right: 10px;
        }
        .dropdown-menu {
            width: 100%;
            border-radius: 0;
        }
        .time-line-legend-wrapper {
            margin-top: 10px;
        }
        .time-line-legend-wrapper > ul {
            padding-left: 10px;
            list-style: none;
        }
        .time-line-legend-wrapper > ul > li {
            padding-left: 0;
            display: flex;
            align-items: center;
        }
        .time-line-legend-wrapper > ul > li > span {
            display: inline-block;
            background: #e2e2e2;
            width: 25px;
            height: 5px;
            margin-right: 5px;
        }
        .time-line-legend-wrapper > ul > li:first-child > span {
            background: rgb(31, 179, 70);
        }
        .time-line-legend-wrapper > ul > li:last-child > span {
            background: #ef5b5b;
        }
    `],
    encapsulation: ViewEncapsulation.None
})
export class MyActivities {

    userBookings: any [];
    guestBookingsInitial: any [];
    guestBookings: any []; //use for the view

    upcomingBookings: any [];
    pastBookings: any [];

    time: string; // past or upcoming

    statusList = [
        { label: 'All', id: 0 },
        { label: 'Pending Payment', id: 2 },
        { label: 'Confirmed', id: 5 },
        { label: 'Cancelled', id: 4 },
        { label: 'Expired', id: 6 },
    ];
    selectedStatus: any;

    constructor(
        private route: ActivatedRoute,
        private momentService: MomentService,
    ){
        this.guestBookingsInitial = this.route.snapshot.data['guestBookings'];

        this.route.queryParams
            .subscribe(params => {
                this.selectedStatus = this.statusList[0];
                this.time = params['time'];
                this.filterBookings(params['time']);
            });
    }

    filterBookings(time: string){

        // prevent first time attempt before ngOnInt
        if(!this.guestBookingsInitial) return;

        this.time = time;

        if(time === 'upcoming'){
            this.guestBookings = this.upcomingBookings = this.guestBookingsInitial.filter(booking => {
                return this.momentService.getDuration(booking.dates[0].toDate, this.momentService.getMoment()) >= 0;
            });

        }else if(time === 'past'){
            this.guestBookings = this.pastBookings = this.guestBookingsInitial.filter(booking => {
                return this.momentService.getDuration(booking.dates[0].toDate, this.momentService.getMoment()) < 0;
            });
        }

    }

    selectStatus(status: any){
        this.selectedStatus = status;

        if(this.time === 'upcoming'){
            if(status.id === 0){
                this.guestBookings =  this.upcomingBookings;
                return;
            }

            else if(status.id === 5){
                this.guestBookings = this.upcomingBookings.filter(booking => {
                    return (booking.reservationStatus.id === 3 || booking.reservationStatus.id === 5);
                });
            }

            else {
                this.guestBookings = this.upcomingBookings.filter(booking => {
                    return status.id === booking.reservationStatus.id;
                });
            }


        }
        else if(this.time === 'past'){
            if(status.id === 0) {
                this.guestBookings = this.pastBookings;
                return;
            }

            else if(status.id === 5){
                this.guestBookings = this.pastBookings.filter(booking => {
                    return (booking.reservationStatus.id === 3 || booking.reservationStatus.id === 5);
                });
            }

            else {
                this.guestBookings = this.pastBookings.filter(booking => {
                    return status.id === booking.reservationStatus.id;
                });
            }

        }
    }


}
