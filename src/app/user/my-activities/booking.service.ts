import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Http, Response } from "@angular/http";
import { ServerConfigService } from "../../server.config.service";
import { MSError } from "../../common/errors/ms.error";
import { NotFoundError } from "../../common/errors/not-found-error";
import { BadRequestError } from "../../common/errors/bad-request-error";

//every service should use below imports
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class BookingService {

    baseUrl: string;

    constructor(
        private http: Http,
        private serverConfigService: ServerConfigService
    ){
        this.baseUrl = environment.apiHost+'/book';
    }

    bookSpace(bookingRequest){
        return this.http
            .post(
                this.baseUrl+'/space',
                JSON.stringify(bookingRequest),
                this.serverConfigService.getJsonHeader()
            )
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    blockSpaceManual(bookingRequest){
        return this.http
            .post(
                this.baseUrl+'/manual',
                JSON.stringify(bookingRequest),
                this.serverConfigService.getJsonHeader()
            )
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getBookingCharge(bookingRequest){
        return this.http
            .post(
                this.baseUrl+'/bookingCharge',
                JSON.stringify(bookingRequest),
                this.serverConfigService.getJsonHeader()
            )
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    deleteManualBooking(id: number){
        return this.http
            .delete(
                this.baseUrl+'/manual/'+id,
                this.serverConfigService.getJsonHeader()
            )
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    updateManualBlock(block){
        return this.http
            .put(this.baseUrl+'/manual', JSON.stringify(block), this.serverConfigService.getJsonHeader())
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    getUserBookings(){
        return this.http
            .get(this.baseUrl+'/spaces', this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getSpaceBookings(spaceId){
        return this.http
            .get(this.baseUrl+'/spaces/space/'+spaceId, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getGuestBookings(){
        return this.http
            .get(this.baseUrl+'/spaces/guest', this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getSpaceBooking(bookingData){
        return this.http
            .post(this.baseUrl+'/spaces', JSON.stringify(bookingData) ,this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getBlockedSpace(blockedId){
        return this.http
            .get(this.baseUrl+'/manual/'+blockedId, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getUserBooking(bookingId){
        return this.http
            .get(this.baseUrl+'/spaces/'+bookingId, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    updateBooking(updateRequest){
        return this.http
            .put(this.baseUrl + '/space', JSON.stringify(updateRequest), this.serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    saveRemarks(remarkRequest){
        return this.http
            .put(this.baseUrl+'/addRemark', JSON.stringify(remarkRequest) ,this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    saveReview(reviewRequest){
        return this.http
            .post(this.baseUrl+'/addReview', JSON.stringify(reviewRequest) ,this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.text())
            .catch(this.handleError);
    }

    continueToPayLater(bookingId) {
        return this.http.get(this.baseUrl + '/paylater/'+bookingId, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response): Observable<any> {

        if(error.status === 404)
            return Observable.throw(new NotFoundError());

        if(error.status === 400)
            return Observable.throw(new BadRequestError(error.json()));

        return Observable.throw(new MSError(error))
    }

}