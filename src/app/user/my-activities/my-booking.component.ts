import {Component, Input, Output, EventEmitter} from "@angular/core";
import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";

declare let moment;

@Component({
    selector: 'my-booking',
    styles: [`
        .manage-booking-reviewed-btn {
            background: #fff;
            color: #9c9c9c;
            border: 1px solid #9c9c9c;
            font-weight: bold;
        }
        .manage-booking-reviewed-btn:hover {
            color: #9c9c9c!important;
            border: 1px solid #9c9c9c;
            background: #fff;
        }
        
    `],
    template: `
        <div class="col-md-4"> <!--not showing discard bookings -->
            <div class="manage-booking-block-upcoming">
                <div class="manage-booking-block-image-wrapper">
                    <img src="{{bucket}}/{{booking.space.image}}" alt="">
                    <div *ngIf="booking.reservationStatus.id === 2" class="booking-status-manage-booking-upcoming booking-status-manage-booking-upcoming-pending">{{booking.reservationStatus.label}}</div>
                    <div *ngIf="booking.reservationStatus.id === 3" class="booking-status-manage-booking-upcoming booking-status-manage-booking-upcoming-confirmed">{{booking.reservationStatus.label}}</div>
                    <div *ngIf="booking.reservationStatus.id === 4" class="booking-status-manage-booking-upcoming booking-status-manage-booking-upcoming-cancelled">{{booking.reservationStatus.label}}</div>
                    <div *ngIf="booking.reservationStatus.id === 6" class="booking-status-manage-booking-upcoming booking-status-manage-booking-upcoming-expired">{{booking.reservationStatus.label}}</div>
                    <div *ngIf="booking.reservationStatus.id === 5" class="booking-status-manage-booking-upcoming booking-status-manage-booking-upcoming-confirmed">{{booking.reservationStatus.label}}</div>
                </div>
                <div class="manage-booking-block-upcoming-header">
                    <h1>{{booking.space.name | summary : 22}}</h1>
                    <p><i class="fa fa-map-marker"></i>{{booking.space.addressLine1 | summary : 40}}</p>
                </div>
                <div class="manage-booking-block-info-block">
                    <div><i class="fa fa-clock-o"></i> {{ getFromTime() }} - {{ getEndTime() }} {{(booking.dates.length > 1) ? '+' : '' }}</div
                    ><div><i class="fa fa-calendar"></i> {{ getEventDay() }}</div>
                </div>
                <div class="price-block-wrapper">
                    <div class="manage-booking-block-charge">
                        {{booking.bookingCharge | currency : 'LKR' : false : '.0-0'}}
                    </div><div class="manage-booking-block-upcoming-btns-holder">
                        <a (click)="showActivityDetails(booking.id)">View</a>
                        <a *ngIf="(booking.reservationStatus.id === 2 || booking.reservationStatus.id === 3) && time === 'upcoming' " (click)="onClickCancelBooking(booking)" class="manage-booking-btn manage-booking-btn-cancel">Cancel</a>
                        <a *ngIf="booking.reservationStatus.id === 5 && !booking.isReviewed && time === 'past'" (click)="onClickReviewBooking(booking)" class="manage-booking-btn manage-booking-btn-cancel">Leave a Review</a>
                        <a *ngIf="booking.reservationStatus.id === 5 && booking.isReviewed" class="manage-booking-btn manage-booking-reviewed-btn" (click)="onClickedReviewed(booking)">Reviewed</a>
                    </div>
                </div>
            </div>
        </div>
        
    `,
})
export class MyBookingComponent {

    @Input('booking') booking;
    @Input('time') time;
    @Output('onCancelBooking') onCancelBooking = new EventEmitter();
    @Output('onReviewBooking') onReviewBooking = new EventEmitter();
    @Output('onReviewedBooking') onReviewedBooking = new EventEmitter();

    bucket: string;
    constructor(private router: Router){
        this.bucket = environment.bucket;
    }

    getFromTime(){

        return moment(this.booking.dates[0].fromDate).format('h:mm A');
    }

    getEndTime(){
        return moment(this.booking.dates[0].toDate).format('h:mm A');
    }

    getEventDay(){
        return moment(this.booking.dates[0].toDate).format('Do MMMM YYYY');
    }

    onClickCancelBooking(booking: any){
        this.onCancelBooking.emit({cancelBooking: booking});
    }

    onClickReviewBooking(booking: any){
        this.onReviewBooking.emit({reviewBooking: booking});
    }

    onClickedReviewed(booking: any) {
        this.onReviewedBooking.emit({reviewObject: booking.review});
    }

    showActivityDetails(bookingId: number){
        this.router.navigate(['/user/profile/my-activities/details', bookingId], { queryParams: {m : false, time: this.time} });
    }
}