export class User{
    id?: number;
    email: string;
    name?: string;
    password: string;
    verify?: string;
    mobileNumber?: string;
    companyPhone?: string;
    dob?: string;
    anniversary?: string
    gTokenId?: string;
    isTrustedUser?: Boolean;
    isLogged?: Boolean = false;
    imageUrl?: string;
    companyName?: string;
    about?:string;
    jobTitle?: string;
    lastName?: string;
}