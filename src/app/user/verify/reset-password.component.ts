import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CryptoService} from "../../shared/services/crypto.service";
import { AuthenticationService } from "../../shared/services/authentication.service";
import { ToasterService } from "../../shared/services/toastr.service";

@Component({
    template: `


        <form #f="ngForm">
            <div class="forgot-password-reset-window forgot-password-reset-window-active">
               <h3 class="reset-pw-heading">Reset Your Password</h3>
                
                <input class="password-reset" ngModel type="password" name="password" placeholder="New Password" #newPassword="ngModel" minlength="8" maxlength="50" (change)="onChangeNewPassword(newPassword)" autofocus required/>
                <div *ngIf="newPassword.touched && !newPassword.valid">
                    <span class="pw-reset-screen-error" *ngIf="newPassword.errors['required']" >Password can not be empty</span>
                    <span class="pw-reset-screen-error" *ngIf="newPassword.errors['minlength']" >Use at least 8 characters</span>
                </div>
                <p class="reset-pw-tips">Tips: Use at least 8 characters. Don't re-use passwords from other websites or include obvious words like your name or email.</p>
                
                <input class="password-confirm" type="password" ngModel name="cpassword" placeholder="Confirm Password" #confirmPassword="ngModel" (keyup)="matchPassword(confirmPassword)" required />
                <div *ngIf="confirmPassword.touched && newPassword.valid">
                    <span class="pw-reset-screen-error" *ngIf="!confirmPassword.valid && confirmPassword.errors['required']">Confirm password</span>
                    <span class="pw-reset-screen-error" *ngIf="confirmPassword.valid && passwordMismatch">Please make sure your passwords match </span>
                </div>
                <p class="reset-pw-tips">By clicking "Save & Continue" you confirm that you accept the <a href="/#/terms/privacy" target="_blank">Terms of Service & Privacy Policy</a></p>
                
                <a class="reset-button" [class.reset-button-disabled]="!f.valid || passwordMismatch" (click)="saveAndContinue(f.valid)" >Save and Continue</a>
            </div>
        </form>
        <div class="reset-password-overlay reset-password-overlay-active"></div>
    `,
    styles: [`
        /*forgot password styles*/
        
        .reset-password-overlay {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999;
            background: url(../../../assets/img/bg-image.jpg);
            background-size: cover;
        }
        .reset-password-overlay-active {
            display: inline-block;
        }
        /** wrapper class: Reset **/
        .forgot-password-reset-window {
            display: none;
            width: 100%;
            height: 100%;
            padding: 20px;
            margin-top: 50px;
            z-index: 1000;
            position: fixed;
            background: #fff;
        }
        .forgot-password-reset-window-active {
            display: inline-block;
        }
        @media only screen and (min-width: 768px) {
            .forgot-password-reset-window {
                width: 460px;
                height: 410px;
                top: 50%;
                left: 50%;
                margin-top: -195px;
                margin-left: -230px;
            }
        }
        .forgot-password-reset-window input[type=password] {
            width: 100%;
            height: 50px;
            padding: 10px;
            border: 1px solid #e0e0e0;
        }
        .reset-pw-heading {
            font-size: 26px;
            font-weight: bold;
            margin-top: 0;
            padding: 0px;
            color: #969696;
        }
        .reset-pw-tips {
            color: #9b9b9b;
            font-weight: 400;
        }
        .reset-pw-tips > a {
            color: #7b7b7b;
            border-bottom: 1px dotted #7b7b7b;
            text-decoration: none;
            font-style: italic;
        }
        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            font-weight: 300;
            color: #9b9b9b;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            font-weight: 300;
            color: #9b9b9b;
        }
        :-ms-input-placeholder { /* IE 10+ */
            font-weight: 300;
            color: #9b9b9b;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-weight: 300;
            color: #9b9b9b;
        }
        .reset-button {
            text-align: center;
            border: 1px solid #5b92f1;
            color: #ffffff;
            width: 100%;
            display: inline-block;
            margin-top: 20px;
            padding: 15px 10px;
            background: #5b92f1;
            transition: all 0.3s;
        }
        /** Send email **/
        .forgot-password-send-email-wrapper {
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            padding: 20px;
            margin-top: 50px;
            z-index: 1000;
            background: #fff;
        }
        .forgot-password-send-email-wrapper-active {
            display: inline-block;
        }
        @media only screen and (min-width: 768px) {
            .forgot-password-send-email-wrapper {
                width: 460px;
                height: 280px;
                top: 50%;
                left: 50%;
                margin-top: -140px;
                margin-left: -230px;
            }
        }
        .forgot-password-send-email-wrapper input[type=email] {
            width: 100%;
            height: 50px;
            margin-bottom: 10px;
            padding: 10px;
            border: 1px solid #e0e0e0;
        }
        .reset-pw-heading {
            font-size: 26px;
            font-weight: bold;
            margin-top: 0;
            padding: 0px;
            color: #969696;
        }
        .forgot-password-send-email-wrapper h3{
            font-size: 26px;
            font-weight: bold;
            margin-top: 0;
            padding: 0px;
            color: #969696;
        }
        .send-email-btns-wrapper {
            display: inline-block;
            width: 100%;
            text-align: center;
        }
        .send-email-btns-wrapper > a {
            text-align: center;
            border: 1px solid #5b92f1;
            color: #ffffff;
            width: 100%;
            display: inline-block;
            margin-top: 20px;
            padding: 15px 10px;
            background: #5b92f1;
            transition: all 0.3s;
            width: 48%;
        }
        .send-email-btns-wrapper > a:last-child {
            margin-left: 2%;
        }
        .send-email-btns-wrapper > a:first-child {
            margin-right: 2%;
            border: 1px solid #5b92f1;
            background: #ffffff;
            color: #5b92f1;
        }
        .reset-button-disabled {    
            background: #e2e2e2;
            color: #888888;
            cursor: not-allowed;
            border: 1px solid #888888;
        }
        .pw-reset-screen-error {
            color: #dd6859;
            font-size: 10.5px;
            font-weight: bold;
        }

        
    `]

})
export class ResetPasswordComponent {

    key: string;
    newPassword: string;
    passwordMismatch = false;

    constructor(
        private route: ActivatedRoute,
        private crpto: CryptoService,
        private authService: AuthenticationService,
        private toastr: ToasterService,
        private router: Router

    ){
        this.key = this.route.snapshot.queryParams['key'];
    }

    onChangeNewPassword(control){
        this.newPassword = control.value.trim();
    }

    matchPassword(control){
        if(control.value !== this.newPassword){
            this.passwordMismatch = true;
        }else {
            this.passwordMismatch = false;
        }
    }

    saveAndContinue(isFormValid){
       if(!isFormValid || this.passwordMismatch) return;

       this.authService.resetUserPassword(this.key, {
           newPassWord: this.crpto.sha256Hex(this.newPassword)
       }).subscribe(isSuccessfullyUpdated => {
           //console.log('response', isSuccessfullyUpdated);
           if(isSuccessfullyUpdated){
                this.toastr.success('', 'Password reset successfully!');
                this.router.navigate(['/user/signin']);
           }else{
               this.toastr.error('Please contact support', 'Something went wrong!');
           }

       })


    }



}