import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../user.service";
import { ToasterService } from "../../shared/services/toastr.service";

@Component({
    template: ``
})
export class UserVerifyComponent implements OnInit{

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private router: Router,
        private toaster: ToasterService
    ){

    }

    ngOnInit(){
        let key = "";

        this.route.queryParams.subscribe(params => {
            key = decodeURIComponent(params['key']);
        })

        this.userService.verifyUser({key}).subscribe(resp => {
            if(resp === true){
                this.router.navigate(['user/registration/success']);
            }else{
                this.toaster.error ('Something went wrong!', 'Error')
            }
        });
    }





}