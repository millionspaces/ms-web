import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    template: `
        <div class="col-xs-12 p-x-0" style="margin-top: 80px;">
           <div class="inner-message-box p-t-20">
              <div align="center"><img src="../../../assets/img/users/succefull-sent-icon.png"></div>
              <h2 align="center">Thank you</h2>
              <p align="center" class="font-16">Your account has been created successfully.</p>
           </div>
           <div class="p-t-10" align="center"> <button class="btn btn-primary" style="width: 250px" type="button" (click)="goToLogin()"><span style="padding-left: 10px; padding-right: 10px;">SIGN IN NOW</span><i class="fa fa-angle-right" aria-hidden="true"></i></button></div>
        </div>
    `
})
export class RegistrationSuccessComponent {

    constructor(private router: Router){}

    goToLogin(){
        this.router.navigate(['user/signin'])
    }
}