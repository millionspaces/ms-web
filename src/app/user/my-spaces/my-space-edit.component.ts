import { Component } from "@angular/core";

@Component({
    templateUrl: './my-space-edit.component.html',
    styles: [`
        .back-to-space {
            margin-bottom: 10px;
        }
        .back-to-space {
            margin-left: 30px;
        }
        @media only screen and (min-width: 992px) {
            .back-to-space {
                margin-left: 0;
            }
        }
        .back-to-space > a:hover {
            color: #5b92f1;
        }
        
        .my-space-title-wrapper {
            background: #fff;
            padding: 10px;
            margin-top: 10px;
        }
        .my-space-title-wrapper h1 {
            margin-top: 0;
            margin-bottom: 0;
        }
        .contact-details-wrapper {
            text-align: center;
            margin-top: 100px;
        }
        .contact-details-wrapper i {
            color: #46a5cc;
            font-size: 100px;
        }
        .contact-details-wrapper i:first-child {            
            font-size: 60px;
            position: relative;
            top: -20px;
        }
        .contact-details-wrapper h1 {
            font-weight: 300;
            color: #46a5cc;
        }
    `]
})
export class MySpaceEditComponent {

}