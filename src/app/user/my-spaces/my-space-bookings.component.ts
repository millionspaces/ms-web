import { Component, OnInit } from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MomentService} from "../../shared/services/moment.service";
import {MsUtil} from "../../shared/util/ms-util";

declare let moment: any;

@Component({
    templateUrl: './my-space-bookings.component.html',
    styles: [`

        .back-to-space {
            margin-bottom: 10px;
        }
        
        .back-to-space:hover > a {
            color: #5b92f1;
        }
        
        .my-spaces-bookings-table-wrapper {
            margin: 10px 0;
        }
        .my-spaces-bookings-table {            
            overflow-y: auto;
            background: #fff;
        }
        .my-spaces-bookings-table > ul {
            list-style: none;
            min-width: 1000px;
            padding: 10px 10px 0;
            margin-bottom: 0;
        }
        .my-spaces-bookings-table > ul > li {
            list-style: none;
            display: inline-block;
            width: 16%;
            padding: 3px;
            text-align: center;
        }
        .my-spaces-bookings-table-body {
            padding: 0 10px 10px;
            min-width: 1000px;
        }
        .my-spaces-bookings-table-body > div > div {
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            padding: 3px;
            height: 45px;
        }
        .my-spaces-bookings-table-header > li {
            border: 1px solid #e2e2e2;
        }
        .my-spaces-bookings-table > ul > li:first-child, .my-spaces-bookings-table-body > div > div:first-child {
            width: 26%;
        }
        .my-spaces-bookings-table > ul > li:nth-child(2), .my-spaces-bookings-table-body > div > div:nth-child(2) {
            width: 12%;
        }
        .my-spaces-bookings-table > ul > li:nth-child(3), .my-spaces-bookings-table-body > div > div:nth-child(3) {
            width: 14%;
        }
        .my-spaces-bookings-table > ul > li:nth-child(4), .my-spaces-bookings-table-body > div > div:nth-child(4) {
            width: 16%;
        }        
        .my-spaces-bookings-table > ul > li:nth-child(5), .my-spaces-bookings-table-body > div > div:nth-child(5) {
            width: 16%;
        }       
        .my-spaces-bookings-table > ul > li:last-child, .my-spaces-bookings-table-body > div > div:last-child {
            width: 16%;
        }
        .my-spaces-bookings-table-body > div:nth-child(even) {
            background: #f9f9f9;
        }
        .my-spaces-bookings-table-header > li {
            font-weight: bold;
        }
        .my-space-booking-table-header > li, .my-spaces-bookings-table-body > div > div {        
            border: 1px solid #e2e2e2;
        }
        .label-confirmed {
            display: inline-block;
            padding: 2px 3px;
            background: #22A464;
            color: #fff;
            border-radius: 3px;
        }
        .view-btn-wrapper > button {
            border: 1px solid #46a5cc;
            background: #fff;
            color: #46a5cc;
        }        
        .my-spaces-bookings-table-header > li {
            border-bottom: none;
        }
        .manage-booking-top-bar-bookings {
            background: #fdfdfd;
            position: fixed;
            top: 50px;
            left: 0px;
            border-bottom: 2px solid #43b7de;
            font-weight: 300;
            width: 100%;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-top-bar-bookings {
                top: 50px;
                left: 200px;
                width: calc(100% - 200px);
            }
        }
        .manage-booking-top-bar-bookings > div {
            display: inline-block;
            vertical-align: middle;
        }
        .manage-booking-top-bar-bookings {        
            z-index: 99;
        }
        .manage-booking-topbar-bookings-title {
            border-right: 1px solid #f7f7f7;
            margin-left: 30px;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-bookings-title {
                margin-left: 0;
            }
        }
        .manage-booking-topbar-bookings-title h1 {
            margin: 0;
            padding: 5px;
            font-weight: 300;
        }
        .manage-booking-topbar-bookings-title {
            width: 100%;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-bookings-title {
                width: 70%;
            }
        }
        .manage-booking-topbar-bookings-past {
            width: 33%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
            user-select: none;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-bookings-past {
                width:  15%;
            }
        }        
        .manage-booking-topbar-bookings-upcoming {
            width:  33%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
            user-select: none;
        }
        @media only screen and (min-width: 768px) {
            .manage-booking-topbar-bookings-upcoming {
                width:  15%;
            }
        }
        .manage-booking-topbar-bookings-switch {
            width: 10%;
            text-align: center;
            cursor: pointer;
            padding: 15px;
        }
        .active-topbar-bookings-button {
            background: #43b0ff;
            color: #fff;
        }
        .manage-booking--bookings-filters-bar {
            background: #fff;
            margin-bottom: 10px;
            font-weight: 300;
            text-align: right;
        }
        .my-space-title-wrapper-bookings {
            background: #fff;
            padding: 10px;
            margin-top: 60px;
            margin-bottom: 10px;
        }
        .my-space-title-wrapper-bookings h1 {
            margin-top: 0;
            margin-bottom: 0;
        }
        
        /** Status labels **/
        .status-label {
            display: inline-block;
            padding: 0 3px;
            border-radius: 3px;
        }
        .status-label-confirmed {
            color: #ffffff;
            background: #22A464;
        }
        .status-label-expired {
            color: #ffffff;
            background: #005EA4;
        }        
        .status-label-cancelled {
            color: #ffffff;
            background: #E1544D;
        }                
        .status-label-pending {
            color: #ffffff;
            background: #FDAF01;
        }
        .view-space-booking-btn {
            border: 1px solid #43b0ff;
            padding: 3px 10px;
            color: #43b0ff;
            background: #fff;
            transition: all 0.3s;
        }
        .view-space-booking-btn:hover {
            color: #fff;
            background: #43b0ff;
        }
        .table-bold-text  {
            font-weight: 600;
            width: 40px;
            display: inline-block;
        }
        .block-time-block {
            padding: 3px;
            display: inline-block;
        }
        .block-time-block p {
            margin-bottom: 0;
        }
        .block-time-block:nth-child(odd) {
            display: inline-block;
            width: 100%;
        }
        .no-bookings {
            padding: 20px;
            margin: 0 auto;
            text-align: center;
        }
    `]
})
export class MySpaceBookingsComponent implements OnInit {

    userBookings: any [];
    spaceBookingsAll: any [];
    spaceBookings;
    spaceId: number;
    spaceName;
    spaceAddress;
    spaceImage: string;
    time: string;

    upcomingBookings: any [];
    pastBookings: any [];

    selectedStatus = '0';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private momentService: MomentService
    ){
        this.spaceBookingsAll = this.route.snapshot.data['spaceBookings'];
        this.spaceId = +route.snapshot.params['spaceId'];

        this.route.queryParams
            .subscribe(params => {
                this.time = params['time'];
                this.filterBookings(params['time']);
            });
    }

    ngOnInit(){

        console.log('spaceBookingsAll', this.spaceBookingsAll);

        if(this.spaceBookingsAll.length === 0){
            return;
        }

        this.spaceName = this.spaceBookingsAll[0].space.name;
        this.spaceAddress = this.spaceBookingsAll[0].space.addressLine1;
        this.spaceImage = this.spaceBookingsAll[0].space.image;

    }

    filterBookings(time: string){

        if(time === 'upcoming'){
            //filter upcoming bookings
            this.spaceBookings = this.spaceBookingsAll.filter(booking => {
                if(booking.dates.length === 0) return;
                return this.momentService.getDuration(booking.dates[0].toDate, this.momentService.getMoment()) >= 0;
            });

        }else if(time === 'past'){
            this.spaceBookings = this.spaceBookingsAll.filter(booking => {
                if(booking.dates.length === 0) return;
                return this.momentService.getDuration(booking.dates[0].toDate, this.momentService.getMoment()) < 0;
            });
        }
    }

    getFromTime(booking, index){
        if(!booking.dates[index]) return '';
        return moment(booking.dates[0].fromDate).format('Do MMMM YYYY h:mm A');
    }

    getEndTime(booking, index){
        if(!booking.dates[index]) return '';
        return moment(booking.dates[0].toDate).format('Do MMMM YYYY h:mm A');
    }

    getEventDay(booking){
        if(!booking.dates[0]) return '';
        return moment(booking.dates[0].fromDate).format('Do MMMM YYYY');
    }

    showBooking(spaceId: number, bookingId: number, isManual: boolean ){
        this.router.navigate(['/user/profile/my-spaces/space', spaceId, 'booking', bookingId], {
            queryParams: { m : isManual, time: this.time }
        });
    }


    getBlockStartTime(timeBlock){
        return moment(timeBlock.fromDate).format('Do MMMM YYYY h:mm A');
    }

    getBlockEndTime(timeBlock){
        return moment(timeBlock.toDate).format('Do MMMM YYYY h:mm A');
    }

    sortBy: string = '-dates[0].fromDate';

    sortByRef() {
        if(this.sortBy === '-id') {
            this.sortBy = 'id';
        }else {
            this.sortBy = '-id';
        }
        this.selectedStatus = '0';
    }

    sortByReservationTime() {
        if(this.sortBy === '-dates[0].fromDate') {
            this.sortBy = 'dates[0].fromDate';
        }else {
            this.sortBy = '-dates[0].fromDate';
        }
        this.selectedStatus = '0';
    }

    sortByBookingCharge() {
        if(this.sortBy === '-bookingCharge') {
            this.sortBy = 'bookingCharge';
        }else {
            this.sortBy = '-bookingCharge';
        }
        this.selectedStatus = '0';
    }

    sortByGuestName() {
        if(this.sortBy === '-user.name') {
            this.sortBy = 'user.name';
        } else {
            this.sortBy = '-user.name';
        }
        this.selectedStatus = '0';
    }

    changeStatus() {
        this.sortBy = this.selectedStatus;
    }

    showSpace(spaceId, nameOfTheSpace) {
        const spaceName = MsUtil.replaceWhiteSpaces(nameOfTheSpace, '-');
        window.open(`#/spaces/${spaceId}/${spaceName}`, '_blank');
    }


}