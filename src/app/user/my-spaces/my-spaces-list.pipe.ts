import {PipeTransform, Pipe} from "@angular/core";

@Pipe({
    name: 'sortBookings'
})
export class MySpacesListPipe implements PipeTransform {

    transform(array: Array<any>, currentField: string) {
        if(!array){
            return;
        }

        if(currentField === 'id') {
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return -1;
                } else if (a[currentField] > b[currentField]) {
                    return 1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === '-id') {
            currentField = currentField.split('-')[1];
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return 1;
                } else if (a[currentField] > b[currentField]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === 'dates[0].fromDate') {
            array.sort((a: any, b: any) => {
                if (a['dates'][0]['fromDate'] < b['dates'][0]['fromDate']) {
                    return -1;
                } else if (a['dates'][0]['fromDate'] > b['dates'][0]['fromDate']) {
                    return 1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === '-dates[0].fromDate') {
            array.sort((a: any, b: any) => {
                if (a['dates'][0]['fromDate'] < b['dates'][0]['fromDate']) {
                    return 1;
                } else if (a['dates'][0]['fromDate'] > b['dates'][0]['fromDate']) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === 'bookingCharge') {
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return -1;
                } else if (a[currentField] > b[currentField]) {
                    return 1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === '-bookingCharge') {
            currentField = currentField.split('-')[1];
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return 1;
                } else if (a[currentField] > b[currentField]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === 'user.name') {
            array.sort((a: any, b: any) => {
                if (a['user']['name'] < b['user']['name']) {
                    return -1;
                } else if (a['user']['name'] > b['user']['name']) {
                    return 1;
                } else {
                    return 0;
                }
            });
            return array;
        }
        else if(currentField === '-user.name') {
            array.sort((a: any, b: any) => {
                if (a['user']['name'] < b['user']['name']) {
                    return 1;
                } else if (a['user']['name'] > b['user']['name']) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return array;
        }

        else if(currentField === '0') {
            return array;
        }

        else if(currentField === '4') {
            return array.filter(space => {
                if(space.reservationStatus) {
                    return space.reservationStatus.id === 4;
                }
            })
        }

        else if(currentField === '6') {
            return array.filter(space => {
                if(space.reservationStatus) {
                    return space.reservationStatus.id === 6;
                }
            })
        }

        else if(currentField === '2') {
            return array.filter(space => {
                if(space.reservationStatus) {
                    return space.reservationStatus.id === 2;
                }
            })
        }

        else if(currentField === '3,5') {
            return array.filter(space => {
                if(space.reservationStatus) {
                    return space.reservationStatus.id === 3 || space.reservationStatus.id === 5;
                }
            })
        }

    }

}