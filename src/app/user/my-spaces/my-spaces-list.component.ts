import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MsUtil} from "../../shared/util/ms-util";
import {environment} from "../../../environments/environment";
import {SpaceService} from "../../spaces/shared/space.service";
import {ToasterService} from "../../shared/services/toastr.service";
import {UserService} from "../user.service";

@Component({
    templateUrl: './my-spaces-list.component.html',
    styles: [`
        form {
            height: 100%;
        }
        .my-spaces-wrapper {
            margin: 0 10px;
        }
        .my-spaces-wrapper:first-child {
            margin-top: 45px;
        }
        .my-space-block {
            background: #fff;
            width: 100%;
            border: 1px solid #e2e2e2;
            margin-bottom: 10px;
        }
        .my-space-block-img-wrapper {
            position: relative;
        }
        .my-space-block-img-wrapper > img {
            width: 100%;
        }
        .my-space-block-info {
            padding: 5px;
        }
        .my-space-block-info > h1 {
            font-weight: 300;
            margin-top: 10px;
            font-size: 22px;
            margin-bottom: 0;
        }
        .my-space-block-info > h1 + p {
            color: #717171;
            margin-bottom: 0;
        }
        .my-space-buttons-wrapper {
            text-align: center;
            padding: 5px 0 10px;
        }
        .btn-my-space {
            border: none;
            width: 30%;
            padding: 10px 0;
            color: #46a5cc;
            border: 1px solid #46a5cc;
            background: #fff;
            transition: all 0.3s;
        }
        .btn-my-space:hover {
            background: #46a5cc;
            color: #fff;
        }
        .btn-my-space > i {
            display: inline-block;
            width: auto;
        }
        @media only screen and (min-width: 768px){
            .btn-my-space > i {
                width: 100%;
            }
        }
        @media only screen and (min-width: 1200px) {
            .btn-my-space > i {
                width: auto;
            }
        }
        .btn-my-space > p {
            display: inline-block;
            font-size: 12px;
            margin-bottom: 0;
        }
        .my-space-block-inner {
            position: relative;
        }

        /** linked space */
        .linked-space-indi {
            position: absolute;
            text-align: right;
            width: 100%;
            z-index: 89;
        }
        .linked-space-indi > div {
            display: inline-block;
            padding: 5px;
            margin: 5px;
            color: #fff;
        }
        .link-space-btn {
            background: #43b7de;
            cursor: pointer;
        }
        .promote-as-parent {
            background: #109a06;
            cursor: pointer;
        }
        .link-space-modal {
            position: fixed;
            top: 0;
            width: 400px;
            height: 400px;
            background: #fff;
            left: 50%;
            top: 50%;
            margin-left: -200px;
            margin-top: -200px;
            z-index: 100;
        }
        .link-space-modal-inner {
            position: relative;
            height: 100%;
        }
        .linked-space-title {
            padding: 20px;
            font-size: 18px;
            margin-top: 20px;
        }
        .close-linked-space-modal {
            position: absolute;
            right: 20px;
            top: 0;
            color: #2e2e2e;
            border: 1px solid #515151;
            padding: 1px 5px;
        }
        .close-linked-space-modal:hover {
            color: tomato;
        }
        .linked-spaces-space-list {
            list-style: none;
            font-size: 16px;
            height: 230px;
            overflow-y: auto;
        }
        .linked-spaces-space-list input[type=checkbox] {
            position: relative;
            top: 1px;
        }
        .link-spaces-btns-wrapper {
            position: absolute;
            bottom: 10px;
            text-align: center;
            width: 100%;
            padding: 20px;
        }
        .link-spaces-btns-wrapper > a {
            display: inline-block;
            padding: 10px 20px;
        }
        .link-space-confirm-btn {
            color: #43b7de;
            border: 1px solid #43b7de;
        }
        .link-space-confirm-btn:hover {
            color: #43b7de;
        }
        .link-space-cancel-btn {             
            color: tomato;
            border: 1px solid tomato;
        }
        .link-space-cancel-btn:hover {
            color: tomato;
        }
        /** linked space */
        .linked-space-indi {
            position: absolute;
            text-align: right;
            width: 100%;
        }
        .linked-space-indi > div {
            display: inline-block;
            padding: 5px;
            margin: 5px;
            color: #fff;
        }
        .link-space-btn {
            background: #43b7de;
        }
        .promote-as-parent {
            background: #109a06;
        }
        .link-space-modal {
            position: fixed;
            top: 0;
            width: 400px;
            height: 400px;
            left: 50%;
            top: 50%;
            margin-left: -200px;
            margin-top: -200px;
        }
        .link-space-modal-inner {
            position: relative;
            height: 100%;
        }
        .linked-space-title {
            padding: 20px;
            font-size: 18px;
            margin-top: 20px;
        }
        .close-linked-space-modal {
            position: absolute;
            right: 20px;
            top: 0;
            color: #2e2e2e;
            border: 1px solid #515151;
            padding: 1px 5px;
        }
        .close-linked-space-modal:hover {
            color: tomato;
        }
        .linked-spaces-space-list {
            list-style: none;
            font-size: 16px;
        }
        .linked-spaces-space-list input[type=checkbox] {
            position: relative;
            top: 1px;
        }
        .link-spaces-btns-wrapper {
            position: absolute;
            bottom: 10px;
            text-align: center;
            width: 100%;
            padding: 20px;
        }
        .link-spaces-btns-wrapper > a {
            display: inline-block;
            padding: 10px 20px;
        }
        .link-space-confirm-btn {
            color: #43b7de;
            border: 1px solid #43b7de;
        }
        .link-space-confirm-btn:hover {
            color: #43b7de;
        }
        .link-space-cancel-btn {             
            color: tomato;
            border: 1px solid tomato;
        }
        .link-space-cancel-btn:hover {
            color: tomato;
        }
        .modal-overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.6);
            z-index: 99;
        }
        
        .linked-space-level-indi {
            position: absolute;
            bottom: -19px;
            left: 20px;
            padding: 1px 6px;
            color: #fff;
            border-radius: 3px;
        }
        .linked-space-level-indi-parent {
            background: #6f6f6f;
        }
        .linked-space-level-indi-child {
            background: #d2a031;
        }
        
        
        /**** Spinkit ****/
        .inline-preloader {
            display: inline-block;
            width: 100%;
            height: 100%;
        }
        .sk-circle {
            width: 40px;
            height: 40px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -20px;
            margin-left: -20px;
        }
        .sk-circle .sk-child {
          width: 100%;
          height: 100%;
          position: absolute;
          left: 0;
          top: 0;
        }
        .sk-circle .sk-child:before {
          content: '';
          display: block;
          margin: 0 auto;
          width: 15%;
          height: 15%;
          background-color: #333;
          border-radius: 100%;
          -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
                  animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
        }
        .sk-circle .sk-circle2 {
          -webkit-transform: rotate(30deg);
              -ms-transform: rotate(30deg);
                  transform: rotate(30deg); }
        .sk-circle .sk-circle3 {
          -webkit-transform: rotate(60deg);
              -ms-transform: rotate(60deg);
                  transform: rotate(60deg); }
        .sk-circle .sk-circle4 {
          -webkit-transform: rotate(90deg);
              -ms-transform: rotate(90deg);
                  transform: rotate(90deg); }
        .sk-circle .sk-circle5 {
          -webkit-transform: rotate(120deg);
              -ms-transform: rotate(120deg);
                  transform: rotate(120deg); }
        .sk-circle .sk-circle6 {
          -webkit-transform: rotate(150deg);
              -ms-transform: rotate(150deg);
                  transform: rotate(150deg); }
        .sk-circle .sk-circle7 {
          -webkit-transform: rotate(180deg);
              -ms-transform: rotate(180deg);
                  transform: rotate(180deg); }
        .sk-circle .sk-circle8 {
          -webkit-transform: rotate(210deg);
              -ms-transform: rotate(210deg);
                  transform: rotate(210deg); }
        .sk-circle .sk-circle9 {
          -webkit-transform: rotate(240deg);
              -ms-transform: rotate(240deg);
                  transform: rotate(240deg); }
        .sk-circle .sk-circle10 {
          -webkit-transform: rotate(270deg);
              -ms-transform: rotate(270deg);
                  transform: rotate(270deg); }
        .sk-circle .sk-circle11 {
          -webkit-transform: rotate(300deg);
              -ms-transform: rotate(300deg);
                  transform: rotate(300deg); }
        .sk-circle .sk-circle12 {
          -webkit-transform: rotate(330deg);
              -ms-transform: rotate(330deg);
                  transform: rotate(330deg); }
        .sk-circle .sk-circle2:before {
          -webkit-animation-delay: -1.1s;
                  animation-delay: -1.1s; }
        .sk-circle .sk-circle3:before {
          -webkit-animation-delay: -1s;
                  animation-delay: -1s; }
        .sk-circle .sk-circle4:before {
          -webkit-animation-delay: -0.9s;
                  animation-delay: -0.9s; }
        .sk-circle .sk-circle5:before {
          -webkit-animation-delay: -0.8s;
                  animation-delay: -0.8s; }
        .sk-circle .sk-circle6:before {
          -webkit-animation-delay: -0.7s;
                  animation-delay: -0.7s; }
        .sk-circle .sk-circle7:before {
          -webkit-animation-delay: -0.6s;
                  animation-delay: -0.6s; }
        .sk-circle .sk-circle8:before {
          -webkit-animation-delay: -0.5s;
                  animation-delay: -0.5s; }
        .sk-circle .sk-circle9:before {
          -webkit-animation-delay: -0.4s;
                  animation-delay: -0.4s; }
        .sk-circle .sk-circle10:before {
          -webkit-animation-delay: -0.3s;
                  animation-delay: -0.3s; }
        .sk-circle .sk-circle11:before {
          -webkit-animation-delay: -0.2s;
                  animation-delay: -0.2s; }
        .sk-circle .sk-circle12:before {
          -webkit-animation-delay: -0.1s;
                  animation-delay: -0.1s; }
        
        @-webkit-keyframes sk-circleBounceDelay {
          0%, 80%, 100% {
            -webkit-transform: scale(0);
                    transform: scale(0);
          } 40% {
            -webkit-transform: scale(1);
                    transform: scale(1);
          }
        }
        
        @keyframes sk-circleBounceDelay {
          0%, 80%, 100% {
            -webkit-transform: scale(0);
                    transform: scale(0);
          } 40% {
            -webkit-transform: scale(1);
                    transform: scale(1);
          }
        }
    `]
})
export class MySpacesListComponent implements OnInit {
    userSpaces: any = [];

    // 3 spaces for each row
    chunkSpaces: any[] = [];
    bucket: string;
    isModalVisible:boolean = false;
    isLoading:boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private spaceService: SpaceService,
        private toastr: ToasterService
    ){
        this.userSpaces = this.route.snapshot.data['userSpaces'];
        this.bucket = environment.bucket;

        console.log('this.userSpaces', this.userSpaces);
    }

    ngOnInit(){

        let myArray: any[] = this.userSpaces;

        myArray.sort(function(a, b){
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        })

        this.chunkSpaces = MsUtil.createGroupedArray(myArray, 3);
    }

    showListingHome(){
        this.router.navigate(['/spaces/listing/home'])
    }

    imageLoaded(space){
        space.visible = true;
    }

    goToMySpace(id: number){
        this.router.navigate(['/user/profile/spaces/show', id]);
    }

    goToBookings(id: number){
        this.router.navigate(['/user/profile/my-spaces/space', id, 'bookings'], {queryParams: {time: 'upcoming'}} );
    }

    goToEditSpace(id: number){
        //window.location.href = environment.hostDomain;
        this.router.navigate(['/user/profile/my-spaces/space', id, 'update']);
    }

    goToScheduler(id: number){
        this.router.navigate(['/user/profile/my-spaces/space', id, 'schedule']);
    }

    closeModalWindow(){
        this.isModalVisible = !this.isModalVisible;
    }

    selectedSpace;
    showModalWindow(selectedSpace){
        this.isModalVisible = !this.isModalVisible;
        this.isLoading = true;
        this.spaceService.getSpace(selectedSpace.id).subscribe((space:any) => {
            this.selectedSpace = space;
            this.selectedChilds = space.childSpaces.map(space => space.id);
            this.isLoading = false;
            //console.log('selectedSpace', this.selectedSpace);
            //console.log('selectedChilds', this.selectedChilds);
        });
    }

    selectedChilds = [];
    selectChild(id: number){

        let idx = this.selectedChilds.indexOf(id);
        if(idx > -1){
            this.selectedChilds.splice(idx, 1);
        }else {
            this.selectedChilds.push(id);
        }

        //console.log('this.selectedChilds', this.selectedChilds);
    }

    submitChildSpaces(){

        if(this.selectedChilds.length === 0 && this.selectedSpace.childSpaces.length === 0){
            alert('Please select at least one space');
            return;
        }

        let requestData = {
            originSpace: this.selectedSpace.id,
            childs: this.selectedChilds
        }

        this.spaceService.addChildSpace(requestData).subscribe(resp => {
            console.log('resp', resp);
            if(resp){
                this.toastr.success('Successfully Updated!');
                setTimeout(() => {
                    this.isModalVisible = false;
                    location.reload();
                }, 500)


            }else{
                this.toastr.error('Something went wrong!');
                this.isModalVisible = false;
            }
        });
    }

    isSelected(space, childSpaces){
        if(childSpaces.length > 0){
            return childSpaces.find(childSpace => childSpace.id === space.id )
        }else{
            return false;
        }
    }


}
