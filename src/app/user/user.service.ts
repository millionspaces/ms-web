import { Injectable } from "@angular/core";
import { ServerConfigService } from "../server.config.service";
import { Http, Response } from "@angular/http";
import { User } from "./user.model";
import { Observable } from "rxjs/Observable";
import { environment } from '../../environments/environment';
import { Subject } from "rxjs/Subject";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UserService{

    private subject = new Subject<User>();
    private user: User;
    baseUrl: string;

    constructor(
        private _serverConfig: ServerConfigService,
        private _http: Http
    ) {
        this.baseUrl = environment.apiHost+'/user';
    }

    registerUser(user: User){
        return this._http.post(
            this.baseUrl,
            JSON.stringify(user),
            this._serverConfig.getJsonHeader()
        ).map(resp => resp);
    }

    updateUser(user: User) : Observable<any>{
        return this._http.put(
            this.baseUrl,
            JSON.stringify(user),
            this._serverConfig.getJsonHeader()
        ).map(resp => resp);
    }

    getUserSpaces(){
        return this._http
            .get(this.baseUrl+'/spaces', this._serverConfig.getCookieHeader())
            .map((response: Response) => <any[]>response.json())
            .catch(this.handleError);
    }

    setLoggedUser(user: User){
        this.subject.next(user);
        this.user = user;
    }

    getUserObservable(): Observable<User>{
        return this.subject.asObservable();
    }

    getUser(){
        return this.user;
    }

    verifyUser(keyObject: any){
        return this._http.post(
            this.baseUrl + '/verify',
            JSON.stringify(keyObject),
            this._serverConfig.getJsonHeader()
        )
            .map((response: Response) => <any[]>response.json())
            .catch(this.handleError);
    }

    clearUser(){
        this.user = null;
        this.subject.next();
    }

    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }








}