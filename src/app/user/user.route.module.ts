import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SignupComponent } from "../signup/signup.component";
import { SignInComponent } from "../signin/signin.component";
import { AuthGuard } from "../shared/services/auth-guard.service";
import { UserResolve } from "./user.resolve";
import { UserSpacesResolve } from "./user-spaces.resolve";
import { MyActivities } from "./my-activities/my-activities.component";
import { UserVerifyComponent } from "./verify/user-verify.component";
import { RegistrationSuccessComponent } from "./verify/registration-success.component";
import { ProfileContainerComponent } from "./profile-container/profile-container.component";
import { MyProfileComponent } from "./my-profile/my-profile.component";
import { MySpacesAvailabilityComponent } from "./space-availability/my-spaces-availability.component";
import { SpaceResolve } from "../spaces/space.resolve";
import { MySpaceBookingsComponent } from "./my-spaces/my-space-bookings.component";
import { MySpacesListComponent } from "./my-spaces/my-spaces-list.component";
import { MySpaceEditComponent } from "./my-spaces/my-space-edit.component";
import { SeatingOptionsResolver } from "../spaces/shared/seating-options.resolver";
import { EventTypeResolve } from "../events/shared/event-type.resolve";
import {MySpaceBookingDetailsComponent} from "./my-spaces/my-space-booking-details.component";
import {SpaceBookingsResolve} from "./space-bookings.resolve";
import {SpaceBookingResolve} from "./space-booking.resolve";
import {GuestBookingsResolve} from "./guest-booking.resolve";
import {ResetPasswordComponent} from "./verify/reset-password.component";
import {MyActivityDetailsComponent} from "./my-activities/my-activity-details.component";
import {UserBookingResolve} from "./user-booking.resolve";
import {ForgotPasswordComponent} from "../shared/components/forgotpassword.component";
import {LoggedInGuard} from "../shared/services/loggedin.guard";

const userRoutes = [
    { path: 'login', component: SignInComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'signin', component: SignInComponent, canActivate:[LoggedInGuard] },
    { path: 'signout', component: SignInComponent },
    { path: 'reset-password', component: ResetPasswordComponent },
    { path: 'verify', component: UserVerifyComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'registration/success', component: RegistrationSuccessComponent },
    {
        path: 'profile',
        component: ProfileContainerComponent,
        canActivate:[AuthGuard],
        resolve: { user: UserResolve },
        children: [
            {
                path: '',
                redirectTo: 'my-profile',
                pathMatch: 'full'
            },
            {
                path: 'my-profile',
                component: MyProfileComponent
            },
            {
                path: 'my-spaces/space/:spaceId/booking/:bookingId',
                component: MySpaceBookingDetailsComponent,
                /*resolve: {userBooking: UserBookingResolve}*/
                resolve: {spaceBooking: SpaceBookingResolve}
            },
            {
                path: 'my-spaces/space/:spaceId/bookings',
                component: MySpaceBookingsComponent,
                /*resolve: {userBookings: UserBookingsResolve}*/
                resolve: {spaceBookings: SpaceBookingsResolve}
            },
            {
                path: 'my-spaces/space/:id/schedule',
                component: MySpacesAvailabilityComponent,
                resolve: {
                    space: SpaceResolve,
                    seatingOptions: SeatingOptionsResolver,
                    eventTypes: EventTypeResolve
                }
            },
            {
                path: 'my-spaces/space/:id/update',
                component: MySpaceEditComponent,
                resolve: {space: SpaceResolve}
            },
            {
                path: 'my-spaces/availability/space/:id',
                component: MySpacesAvailabilityComponent
            },
            {
                path: 'my-spaces',
                component: MySpacesListComponent,
                resolve: {userSpaces: UserSpacesResolve}
            },
            {
                path: 'my-activities',
                component: MyActivities,
                resolve: {
                    guestBookings: GuestBookingsResolve,
                }
            },
            {
                path: 'my-activities/details/:bookingId',
                component: MyActivityDetailsComponent,
                resolve: {
                    myActivity: UserBookingResolve
                }
            }
            /*{
                path: 'availability',
                component: MySpacesAvailabilityComponent,
                resolve: {
                    userSpaces: UserSpacesResolve,
                    seatingOptions: SeatingOptionsResolver,
                    eventTypes: EventTypeResolve
                }
            },*/
        ]
    },
]

@NgModule({
    imports: [RouterModule.forChild(userRoutes)],
    exports: [RouterModule]
})

export class UserRouteModule{}