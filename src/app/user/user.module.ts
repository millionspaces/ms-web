import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { CryptoService}  from "../shared/services/crypto.service";
import { UserRouteModule } from "./user.route.module";
import { UserResolve } from "./user.resolve";
import { CoreModule } from "../core/core.module";
import { SignInModule } from "../signin/signin.module";
import { CommonModule } from "@angular/common";
import { SignupModule } from "../signup/signup.module";
import { UserSpacesResolve } from ".//user-spaces.resolve";
import { MyActivities } from "./my-activities/my-activities.component";
import { SharedModule } from "../shared/shared.module";
import { UserVerifyComponent } from "./verify/user-verify.component";
import { RegistrationSuccessComponent } from "./verify/registration-success.component";
import { ProfileContainerComponent } from "./profile-container/profile-container.component";
import { MyProfileComponent } from "./my-profile/my-profile.component";
import { MySpacesAvailabilityComponent } from "./space-availability/my-spaces-availability.component";
import { HostCalendarComponent } from "./calendar/host-calendar.component";
import { MySpaceComponent } from "./spaces/my-space.component";
import { AvailabilityService } from "./space-availability/availability.service";
import { CalendarDayViewComponent } from "./space-availability/calendar-day-view.component";
import { ManualBookingComponent } from "./space-availability/manual-booking.component";
import { SchedulerComponent } from "./space-availability/scheduler.component";
import { BlockSpaceComponent } from "./space-availability/block-space.component";
import { MyBookingListComponent } from "./my-activities/my-booking-list.component";
import { MyBookingComponent } from "./my-activities/my-booking.component";
import { UserBookingsResolve } from "./user-bookings.resolve";
import { UserBookingResolve } from "./user-booking.resolve";
import { MySpacesListComponent } from "./my-spaces/my-spaces-list.component";
import { MySpaceEditComponent } from "./my-spaces/my-space-edit.component";
import { MySpaceBookingsComponent } from "./my-spaces/my-space-bookings.component";
import {MySpaceBookingDetailsComponent} from "./my-spaces/my-space-booking-details.component";
import {SpaceBookingsResolve} from "./space-bookings.resolve";
import {SpaceBookingResolve} from "./space-booking.resolve";
import {GuestBookingsResolve} from "./guest-booking.resolve";
import {ResetPasswordComponent} from "./verify/reset-password.component";
import {HostSchedulerComponent} from "./space-availability/host-scheduler.component";
import {MyActivityDetailsComponent} from "./my-activities/my-activity-details.component";
import {MySpacesListPipe} from "./my-spaces/my-spaces-list.pipe";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        SignInModule,
        SignupModule,
        UserRouteModule,
        CoreModule,
        SharedModule
    ],
    declarations: [
        ProfileContainerComponent,
        MySpaceComponent,
        MySpacesAvailabilityComponent,
        MyProfileComponent,
        HostCalendarComponent,
        MyActivities,
        UserVerifyComponent,
        RegistrationSuccessComponent,
        CalendarDayViewComponent,
        ManualBookingComponent,
        SchedulerComponent,
        BlockSpaceComponent,
        MyBookingListComponent,
        MyBookingComponent,
        MySpacesListComponent,
        MySpaceEditComponent,
        MySpaceBookingsComponent,
        MySpaceBookingDetailsComponent,
        ResetPasswordComponent,
        HostSchedulerComponent,
        MyActivityDetailsComponent,
        MySpacesListPipe
    ],
    providers: [
        CryptoService,
        UserResolve,
        UserSpacesResolve,
        AvailabilityService,
        UserBookingsResolve,
        UserBookingResolve,
        SpaceBookingsResolve,
        SpaceBookingResolve,
        GuestBookingsResolve
    ]
})
export class UserModule{

}