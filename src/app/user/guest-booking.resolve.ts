import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { BookingService } from "./my-activities/booking.service";

@Injectable()
export class GuestBookingsResolve implements Resolve<any>{

    constructor(private bookingService: BookingService){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{
        return this.bookingService.getGuestBookings();
    }

}