import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
    templateUrl: './my-space.component.html',
    styles: [`
        label {
            margin: 0;
            padding: 0;
        } 
        input[type=text], textarea {
            border: 1px solid #e2e2e2;
        }
        .manage-bookings-wrapper {
            padding: 0 10px 10px;
            background: #ffffff;
        }
        .manage-bookings-wrapper-inner h3 {
            margin: 0;
            font-size: 32px;
        }
        .manage-booking-title {
            margin-bottom: 10px;
        }
        .manage-booking-title > label, .manage-booking-title > input {
            display: block;
            width: 100%;
        }
        .manage-booking-title > input {
            height: 40px;
            padding: 0 10px;
        }
        .manage-booking-description > textarea, .manage-booking-description > label {
            width: 100%;
        }
        .manage-booking-description > textarea {
            height: 80px;
            padding: 0 10px;
            width: 100%;
        }
        .manage-booking-address > input, .manage-booking-address > label {
            width: 100%;
        }
        .manage-booking-address > input {
            height: 40px;
            padding: 0 10px;
            margin-bottom: 10px;
        }
        .manage-booking-map {
            border: 1px solid #e2e2e2;
            width: 100%;
            height: 100px;
            background: #e2e2e2;
        }
        .manage-booking-company-name > label, .manage-booking-company-name > input, .manage-booking-footage > label, .manage-booking-footage > input, .manage-booking-rate-per-hour > input, .manage-booking-rate-per-hour > label, .manage-booking-participant-count > label, .manage-booking-participant-count > input {
            width: 100%;
        }
        .manage-booking-footage > label, .manage-booking-rate-per-hour > label, .manage-booking-participant-count > label {
            margin-top: 10px;
        }
        .manage-booking-footage > input, .manage-booking-rate-per-hour > input, .manage-booking-participant-count > input {
            height: 40px;
        }
        .sub-title {
            margin-top: 30px;
        }
        .manage-booking-company-name > input {
            height: 40px;
            margin-bottom: 15px;
            padding-left: 10px;
        }
        .sub-title > i {
            color: #0D3349;
            cursor: pointer;
        }
        .manage-booking-space-details input[type=text] {
            padding-left: 10px;
        }
        .manage-booking-space-details-block ul {
            padding-left: 0;
            list-style: none;
        }
        .manage-booking-space-details-block label {
            margin-top: top;
            margin-left: 5px;
        }
        .manage-booking-space-details-block input {
            position: relative;
            top: 1px;
        }
        .extra-amenities-list > li {
            margin-bottom: 10px;
        }        
        .extra-amenities-list > li > span {
            display: inline-block;
        }
        .extra-amenities-list > li > span:not(:last-child) {
            padding-right: 10px;
        }
        .extra-amenities-list > li> span:first-child {
            width: 35%;
        }
        .extra-amenities-list > li > span:nth-child(2n) {
            width: 20%;
        }
        .extra-amenities-list > li > span:nth-child(2n) > input {
            width: 100%;
        }        
        .extra-amenities-list > li > span:nth-child(3n) {
            width: 30%;
        }    
        .extra-amenities-list > li > span:nth-child(3n) > select {
            width: 100%;
        }
        .extra-amenities-list > li > span:last-child {
            width: 15%;
            text-align: right;
        }
        .save-btn-wrapper {
            text-align: right;
        }
        .save-btn-wrapper > .save-btn {
            padding: 10px 20px;
            background: #4585f1;
            color: #fff;
        }
    `]
})
export class MySpaceComponent implements OnInit{

    space: any;

    constructor(private route: ActivatedRoute){}

    ngOnInit(){
        this.space = this.route.snapshot.data['space'];
    }

}