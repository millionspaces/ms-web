import {
    Component, ElementRef,  Input, OnChanges, SimpleChanges, OnInit,
    Output, EventEmitter
} from "@angular/core";
import {BookingService} from "../my-activities/booking.service";

declare let moment: any;
declare let $;

@Component({
    selector: 'event-scheduler',
    templateUrl: './scheduler.component.html',
})
export class SchedulerComponent implements  OnChanges, OnInit{

    @Input('dayViewData') dayViewData;
    @Output('showBookingForm') showBookingForm = new EventEmitter<any>();
    @Output('showBookingFormUpdated') showBookingFormUpdated = new EventEmitter<any>();
    auxShe: any;

    constructor(
        private bookingService: BookingService,
        private _elementRef: ElementRef)
    {}

    ngOnInit(){

        if(this.auxShe){
            return;
        }

        /*let scheduler = this._elementRef.nativeElement.querySelector('#auxScheduler');
        this.auxShe = (<any>$(scheduler)).auxScheduler({
            /!*handleTimeSlotClick: (from, to) => {
                setTimeout(() => {
                    this.showBookingForm.emit({time: {from, to}});
                    this.reRenderScheduler(this.getSchedulerData());
                }, 100);
            },*!/
            getHours: (from, to) => {
                setTimeout(() => {
                    this.showBookingForm.emit({time: {from, to}});
                    this.reRenderScheduler(this.getSchedulerData());
                }, 100);
            }
        });*/


    }

    dayData;
    selectedDate = moment();
    disabledPeriods = [];
    startTime: number = 0;
    endTime: number = 24;

    ngOnChanges(changes: SimpleChanges){
        this.resetGlobalValues();

        console.log('dayViewData', changes['dayViewData'].currentValue);

        this.dayData = changes['dayViewData'].currentValue;

        if(!this.dayData){
            return;
        }

        let dayData = this.dayData;

        if(dayData.eventsPerDay.length > 0){
            this.disabledPeriods = dayData.eventsPerDay.map(event => {
                return {
                    text: event.note,
                    from: +event.from.split(':')[0],
                    to: +event.to.split(':')[0],
                    id: event.id,
                    guest: !event.isManual
                }
            });
        }

        console.log('disabledPeriods', this.disabledPeriods);

        //this.startTime = +dayData.availability.from.split(':')[0];
        //this.endTime = +dayData.availability.to.split(':')[0];
        this.startTime = 0;
        this.endTime = 24;

        //console.log('disabledPeriods', disabledPeriods);
        let scheduler = this._elementRef.nativeElement.querySelector('#auxScheduler');
        if(!this.auxShe) {

            this.auxShe = (<any>$(scheduler)).auxScheduler({
                // one cell click
                handleTimeSlotClick: (from, to, id) => {
                    if(id){
                       this.bookingService.getBlockedSpace(id).subscribe(blockedSpace => {
                           if(blockedSpace.isManual === 0) return;
                           this.showBookingFormUpdated.emit({blockedSpace});
                       }, error => {
                           console.log('error', error);
                       })
                    }else{
                        setTimeout(() => {
                            this.showBookingForm.emit({time: {from, to}});
                        }, 400);
                    }
                },
                // multiple cells drag select
                getHours: (from, to) => {
                    setTimeout(() => {
                        this.showBookingForm.emit({time: {from, to}});
                    }, 400);
                }
            });
        }

        this.reRenderScheduler(this.getSchedulerData());

    }

    reRenderScheduler({selectedDate, startTime, endTime, disabledPeriods}){
        let scheduler = this._elementRef.nativeElement.querySelector('#auxScheduler');

        (<any>$(scheduler)).auxScheduler("options", 'selectedDate', moment(selectedDate).format('Do MMMM YYYY - dddd'));
        (<any>$(scheduler)).auxScheduler("options", 'startTime', startTime);
        (<any>$(scheduler)).auxScheduler("options", 'endTime', endTime);
        (<any>$(scheduler)).auxScheduler("options", 'disabledHours', disabledPeriods);
    }

    resetGlobalValues(){
        this.selectedDate = moment();
        this.startTime = 0;
        this.endTime = 24;
        this.disabledPeriods = [];
    }

    getSchedulerData(){
        return {
            selectedDate: this.dayData.date,
            startTime: this.startTime,
            endTime: this.endTime,
            disabledPeriods: this.disabledPeriods
        }
    }

    clearActiveSelectors(){
        let scheduler = this._elementRef.nativeElement.querySelector('#auxScheduler');
        (<any>$(scheduler)).auxScheduler('options', 'clearActive')
    }

    addNewEvent(event){
        this.disabledPeriods.push(event);
        this.reRenderScheduler(this.getSchedulerData());
    }

    removeBlock(blockId){
        let x = this.disabledPeriods.find(disabledPeriod => disabledPeriod.id === blockId);
        let index = this.disabledPeriods.indexOf(x);
        this.disabledPeriods.splice(index, 1);

        this.reRenderScheduler(this.getSchedulerData());
    }
}