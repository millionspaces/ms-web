import {Component, Input, AfterViewInit} from "@angular/core";

@Component({
    selector: 'day-view',
    templateUrl: './calendar-day-view.component.html',
    styles: [`
        .event-block-event-title {
            display: inline-block;
            width: 100%;
            padding-left: 20px;
            text-align: left;
            color: #8c8c8c;
        }
        .events-blocks-date-heading {
            font-size: 16px;
            padding-left: 20px;
            font-weight: bold;
        }
        .fully-booked {
            margin-top: 30px;
            padding: 50px 0;
            background: #c8ffc8;
            border: 1px solid #e2e2e2;
            color: #2cab2c;
        }
    `]
})
export class CalendarDayViewComponent implements AfterViewInit{

    @Input('dayViewData') dayViewData;


    ngAfterViewInit () {
        console.log('test dayViewData', this.dayViewData);
    }

}