import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AvailabilityService {
    private spacesSubject = new Subject<any>();

    setSpace(space: any) {
        this.spacesSubject.next({ space: space });
    }

    clearMessage() {
        this.spacesSubject.next();
    }

    getSpace(): Observable<any> {
        return this.spacesSubject.asObservable();
    }
}