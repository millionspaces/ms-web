import {Component, Output, EventEmitter, Input} from "@angular/core";
import {SpaceService} from "../../spaces/shared/space.service";
import {BookingService} from "../my-activities/booking.service";

declare let $: any;
declare let moment: any;

@Component({
    selector: 'block-space',
    templateUrl: './block-space.component.html',
    styles: [`
        input[type="text"], textarea {
            border: 1px solid #e2e2e2;
        }
        .add-new-space-modal-wrapper {
            display: inline-block;
            position: fixed;
            top: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.4);
            z-index: 99999;
            width: 100%;
            height: 100%;
            text-align: center;
        }
        .add-new-space-modal {
            display: inline-block;
            width: 90%;
            background: #ffffff;
            margin-top: 10px;
            overflow-y: auto;
            overflow-x: hidden;
            text-align: left;
            padding: 10px 40px;
        }
        @media only screen and (min-width: 768px) {
            .add-new-space-modal {
                width: 50%;
            }
        }
        .modal-window-header > h4 {
            text-align: center;
            font-weight: bold;
        }
        .add-new-modal-selected-date {
            text-align: center;
            margin-bottom: 40px;
        }
        .input-wrapper-full-width input, .input-wrapper-full-width textarea {
            display: inline-block;
            width: 100%;
            max-width: 100%;
            height: 40px;
        }
        .dropdown-full-width button{
            width: 100%;
            padding: 10px;
            position: relative;
            margin-top: 31px;
        }
        .input-row {
            margin-top: 30px;
            margin-bottom: 30px;
        }
        .list-item-seating {
            margin-bottom: 10px;
        }
        .cost-details-wrapper {
            margin-bottom: 30px;
        }
        .seating-arrangement-wrapper {
            margin-bottom: 30px;
        }
        .extra-details-wrapper {
            margin-bottom: 30px;
            margin-top: 30px;
        }
        .guest-details-wrapper {
            margin-bottom: 30px;
            margin-top: 30px;
        }
        textarea {
            height: 100px!important;
        }
        .buttons-wrapper > button {
            background:
        }
        .modal-buttons-wrapper {
            text-align: right;
        }
        .modal-close-btn {
            background: #fb9f8f;
            color: #fff;
            border-radius: 0px;
            border: none;
            padding: 6px 30px;
            display: inline-block;
        }
        .modal-save-btn {
            background: #4079f9;
            color: #fff;
            border-radius: 0px;
            border: none;
            padding: 6px 30px;
            display: inline-block;
        }
        /*.seating-arr-heading {*/
            /*font-weight: bold;*/
        /*}*/
        /*label {*/
            /*font-weight: bold;*/
        /*}       */
        .error-message {
            padding: 10px;
            background: tomato;
            color: #fff;
        }
        .modal-window-close-button {
            position: absolute;
            top: 10px;
            right: 25px;
            color: tomato;
            font-size: 20px;
        }
        .add-new-space-modal {
            position: relative;
        }
        .space-name-selector-wrapper {
            border: 1px solid #e2e2e2;
            margin-bottom: 20px;
        }
        .space-name-selector-wrapper button {
            width: 100%;
            text-align: left;
        }
        .space-name-selector-wrapper button .caret {
            position: absolute;
            right: 10px;
            top: 20px;
        }
        .space-name-selector-wrapper .dropdown-menu {
            width: 100%;
            height: 300px;
            overflow-y: auto;
        }
    `]
})
export class BlockSpaceComponent{

    @Input('seatingOptions') seatingOptions;
    @Input('eventTypes') eventTypes;
    @Input('userSpaces') userSpaces;
    @Output('onSuccessBlock') onSuccessBlock = new EventEmitter<any>();

    @Output('onClickCloseBlockSpace') onClickCloseBlockSpace = new EventEmitter<boolean>();

    blockModel = {
        spaceId: <number> null,
        eventStart: <string> null,
        eventEnd: <string> null,
        cost: <string> '',
        note: <string> '',
        guestContactNumber: <string> '',
        guestEmail: <string> '',
        guestName: <string> '',
        maximumGuest: <string> '',
        eventTitle: <string> '',
        eventTypeId: <number> null,
        eventTypeName: <string> '',
        extrasRequested: <string> '',
        seatingArrangementIcon: <string> '',
        seatingArrangementId: <number> null,
        seatingArrangementName: <string> '',
    }

    eventStart;
    eventEnd;

    selectedSpace: any;
    selectedEvent: any;
    bookingDate = moment();

    constructor(private spaceService: SpaceService, private bookingService: BookingService){}

    ngAfterViewInit(){
        $(".event-start").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            minView: 1,
        }).on('changeDate', function (ev) {
            this.eventStart = ev.date;
            this.blockModel.eventStart = moment(ev.date).format("YYYY-MM-DDTHH:00")+'Z';
            $('.event-end').datetimepicker('setStartDate', this.eventStart);
        }.bind(this));

        $(".event-end").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            minView: 1,
        }).on('changeDate', function (ev) {
            this.eventEnd = ev.date;
            this.blockModel.eventEnd = moment(ev.date).format("YYYY-MM-DDTHH:00")+'Z';
        }.bind(this));
    }

    selectSpace(space){
        this.spaceService.getSpace(space.id).subscribe(space => {
            this.selectedSpace = space;
            this.blockModel.spaceId = space.id;
        }, error => {
            console.log('error', error);
            this.selectedSpace = null;
            this.blockModel.spaceId = null;
            alert('something went wrong');
        })
    }

    selectEventType(event: any){
        this.selectedEvent = event;
        this.blockModel.eventTypeId = event.id;
        this.blockModel.eventTypeName = event.name
    }

    selectSeatingOption(option){
        this.blockModel.seatingArrangementIcon = option.icon;
        this.blockModel.seatingArrangementId = option.id;
        this.blockModel.seatingArrangementName= option.name;
    }

    closeForm(){
        this.onClickCloseBlockSpace.emit(false);
    }

    spaceAvailableForBooking = false;
    checkedAvailability = false;

    //check availability at millionspaces
    checkAvailability(){

        let availabilityRequest = {
            space: this.selectedSpace.id,
            dates:[ {
                fromDate: this.blockModel.eventStart,
                toDate: this.blockModel.eventEnd,
            }],
        }

        this.spaceService.getSpaceAvailablility(availabilityRequest).subscribe(available => {
            if(available){
                this.spaceAvailableForBooking = true;
                this.blockSpace();

            }else{
                this.spaceAvailableForBooking = false;
                this.checkedAvailability = true;
            }

        }, err => {
            console.log('network error!!', err);
        });
    }

    blockSpace(){
        let blockModel = this.blockModel;

        let blockRequest = {
            space: blockModel.spaceId,
            fromDate: blockModel.eventStart,
            toDate: blockModel.eventEnd,
            dateBookingMade: moment().format('YYYY-MM-DDTHH:00')+'Z',
            cost: blockModel.cost,
            note: blockModel.note,
            guestContactNumber: blockModel.guestContactNumber,
            guestEmail: blockModel.guestEmail,
            guestName: blockModel.guestName,
            noOfGuests: blockModel.maximumGuest,
            eventTitle: blockModel.eventTitle,
            eventTypeId: blockModel.eventTypeId,
            eventTypeName: blockModel.eventTypeName,
            extrasRequested: blockModel.extrasRequested,
            seatingArrangementIcon: blockModel.seatingArrangementIcon,
            seatingArrangementId: blockModel.seatingArrangementId,
            seatingArrangementName: blockModel.seatingArrangementName
        }

        //console.log('blockRequest', blockRequest);

        this.bookingService.blockSpaceManual(blockRequest).subscribe(blockId => {
            if(blockId != 0 && blockId > 0){
                alert('successfully blocked '+ blockId);

                let blockStart = blockModel.eventStart.split('T')[0];
                let blockEnd = blockModel.eventEnd.split('T')[0];
                let schedulerEvent = {text: '', from : 0, to: 24, id: 0, guest: false};

                if(blockStart === blockEnd){
                    schedulerEvent = {
                        text: blockModel.note,
                        from: +blockModel.eventStart.split('T')[1].split(':')[0],
                        to: +blockModel.eventEnd.split('T')[1].split(':')[0],
                        id: blockId,
                        guest: false
                    }
                }else{
                    schedulerEvent = {
                        text: blockModel.note,
                        from: +blockModel.eventStart.split('T')[1].split(':')[0],
                        to: 24,
                        id: blockId,
                        guest: false
                    }
                }

                let myDates = this.getDates(new Date(blockStart), new Date(blockEnd));
                let blockSet = [];

                if(blockStart === blockEnd){
                    let block = {
                        from: moment().format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                        to: moment().format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.to, 2)+':00',
                        note: schedulerEvent.text,
                        isManual: true,
                        id: blockId
                    }
                    blockSet.push(block)
                }else{
                    let blocks = [];
                    blocks = myDates.map((blockDate, index) => {
                        if(index === 0){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }else if(index === (myDates.length -1)){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT')+blockModel.eventEnd.split('T')[1].split(':')[0]+':00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }else{
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }
                    })
                    blockSet.push(...blocks);
                }

                this.onSuccessBlock.emit({schedulerEvent, blockSet});

            }else{
                alert('something went wrong');
            }
        })
    }

    addDays(days, currentDate) {
        var dat = new Date(currentDate.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
    }

    getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(currentDate)
            currentDate = this.addDays(1, currentDate);
        }
        return dateArray;
    }

    zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }
}
