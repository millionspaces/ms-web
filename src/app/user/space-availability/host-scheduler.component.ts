import {
    Component, Input, Output, EventEmitter, ElementRef, OnInit, OnChanges, SimpleChanges,
    NgZone
} from "@angular/core";
import {BookingService} from "../my-activities/booking.service";

declare let $: any;
declare let moment: any;


@Component({
    selector: 'host-event-scheduler',
    template: `
        <h1 class="events-blocks-date-heading">{{dayViewData?.date | formatDate: 'date'}}</h1>   
        <div id="host-scheduler-wrapper">
        
        </div>
    `,
    styles: [`
        .events-blocks-date-heading { 
            font-size: 16px;
            padding-left: 20px;
            font-weight: bold;
            margin-top: 0;
        }
    `]
})
export class HostSchedulerComponent implements OnInit, OnChanges{

    @Input('dayViewData') dayViewData;
    @Output('showBookingForm') showBookingForm = new EventEmitter<any>();
    @Output('showBookingFormUpdated') showBookingFormUpdated = new EventEmitter<any>();
    auxShe: any;
    dayData;
    selectedDate = moment();
    disabledPeriods = [];
    startTime: number = 0;
    endTime: number = 24;

    constructor(
        private _elementRef: ElementRef,
        private bookingService: BookingService,
        private ngZone: NgZone
    ) {}

    ngOnInit(){
        if(this.auxShe){
            return;
        }
    }

    ngOnChanges(changes: SimpleChanges){
        this.resetGlobalValues();

        console.log('dayViewData', changes['dayViewData'].currentValue);

        this.dayData = changes['dayViewData'].currentValue;

        if(!this.dayData){
            return;
        }

        let dayData = this.dayData;

        if(dayData.eventsPerDay.length > 0){
            this.disabledPeriods = dayData.eventsPerDay.map(event => {
                return {
                    text: event.note,
                    from: +event.from.split(':')[0],
                    to: +event.to.split(':')[0],
                    id: event.id,
                    guest: !event.isManual
                }
            });
        }

        console.log('disabledPeriods', this.disabledPeriods);

        //this.startTime = +dayData.availability.from.split(':')[0];
        //this.endTime = +dayData.availability.to.split(':')[0];
        this.startTime = 0;
        this.endTime = 24;


        //console.log('disabledPeriods', disabledPeriods);
        let scheduler = this._elementRef.nativeElement.querySelector('#host-scheduler-wrapper');
        if(!this.auxShe) {

            this.auxShe = (<any>$(scheduler)).msScheduler({
                type: 'host',
                disabledHours: [],

                handleBookedSlotClick: (from, to, id) => {
                    let now = moment().format('YYYY-MM-DD');
                    var bookingDate = moment(this.dayData.date).format('YYYY-MM-DD');
                    if (now > bookingDate) {
                        alert('Please select a future date');
                        this.clearActiveSelectors();
                        return;
                    }

                    if(id){
                        this.bookingService.getBlockedSpace(id).subscribe(blockedSpace => {
                            console.log('blockedSpace', blockedSpace);
                            if(blockedSpace.isManual === 0) return;
                            this.ngZone.run(() => {
                                this.showBookingFormUpdated.emit({blockedSpace: blockedSpace});
                            });
                        }, error => {
                            console.log('error', error);
                        })
                    }else{
                        this.ngZone.run(() => {
                            this.showBookingForm.emit({time: {from, to}});
                        });
                    }
                },
                getHours:  (from, to, isRange, id) => {
                    let now = moment().format('YYYY-MM-DD');
                    var bookingDate = moment(this.dayData.date).format('YYYY-MM-DD');
                    if (now > bookingDate) {
                        alert('Please select a future date');
                        this.clearActiveSelectors();
                        return;
                    }

                    this.ngZone.run(() => {
                        this.showBookingForm.emit({time: {from, to}});
                    });

                }
            });
        }

        this.reRenderScheduler(this.getSchedulerData());

    }


    reRenderScheduler({selectedDate, startTime, endTime, disabledPeriods}){
        let scheduler = this._elementRef.nativeElement.querySelector('#host-scheduler-wrapper');

        console.log('scheduler', scheduler);

        (<any>$(scheduler)).msScheduler("options", 'selectedDate', moment(selectedDate).format('Do MMMM YYYY - dddd'));
        (<any>$(scheduler)).msScheduler("options", 'startTime', startTime);
        (<any>$(scheduler)).msScheduler("options", 'endTime', endTime);
        (<any>$(scheduler)).msScheduler("options", 'disabledHours', disabledPeriods);
    }

    resetGlobalValues(){
        this.selectedDate = moment();
        this.startTime = 0;
        this.endTime = 24;
        this.disabledPeriods = [];
    }

    getSchedulerData(){
        return {
            selectedDate: this.dayData.date,
            startTime: this.startTime,
            endTime: this.endTime,
            disabledPeriods: this.disabledPeriods
        }
    }

    clearActiveSelectors(){
        let scheduler = this._elementRef.nativeElement.querySelector('#host-scheduler-wrapper');
        (<any>$(scheduler)).msScheduler('options', 'clearActive')
    }

    addNewEvent(event){
        this.disabledPeriods.push(event);
        this.reRenderScheduler(this.getSchedulerData());
    }

    removeBlock(blockId){
        let x = this.disabledPeriods.find(disabledPeriod => disabledPeriod.id === blockId);
        let index = this.disabledPeriods.indexOf(x);
        this.disabledPeriods.splice(index, 1);

        this.reRenderScheduler(this.getSchedulerData());
    }






}