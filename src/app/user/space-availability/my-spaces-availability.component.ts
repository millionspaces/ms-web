import {Component, AfterViewInit, ViewChild, OnInit} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HostCalendarComponent } from "../calendar/host-calendar.component";
import { SpaceService } from "../../spaces/shared/space.service";
import {HostSchedulerComponent} from "./host-scheduler.component";
import {MsUtil} from "../../shared/util/ms-util";

declare let $: any;
declare let moment: any;

@Component({
    templateUrl: './my-spaces-availability.component.html',
    styles: [`
        .panel-animated-wrapper {
            display: inline-block;
            width: 100%;
            height: 537px;
            position: relative;
            margin-bottom: 30px;
        }
        @media only screen and (min-width: 992px) {
            .panel-animated-wrapper {
                height: 450px;
            }
        }        
        .back-to-space {
            margin-left: 30px;
        }
        @media only screen and (min-width: 992px) {
            .back-to-space {
                margin-left: 0;
            }
        }
        .panel-underneath, .panel-top {
          width: 100%;
          display: inline-block;
          background: #fff;
          top: 0;
          vertical-align: top;
          position: absolute;
          z-index: 1;
          border: 1px solid #e2e2e2;
          margin-top: 10px;
          text-align: center;         
          min-height: 100%;
          background: #fbfbfb;
        }
        
        @media only screen and (min-width: 992px) {
          .panel-underneath, .panel-top {
            width: 50%;
            position: static;
          }
        }

        .panel-underneath.hide-on-first-sight {
            display: none;
        }
        .panel-top {
          background: #fff;
          z-index: 2;
          border: 1px solid #e2e2e2;
        }
        
        .event-block {
          margin: 10px;
          padding: 10px;
          background: #fff;
          opacity: 0; }
        
        .close {
          position: absolute;
          right: 0; }
        
        .schedular-wrapper {
          position: absolute;
          background: tomato;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          z-index: 9;
        }
        .dropdown {
          width: 100%;
          text-align: center;  
        }
        .dropdown > button {
          width: 100%;
          left: 0;
          position: relative;
          max-height: 400px;
        }
        

        @media only screen and (min-width: 768px) {
            .dropdown > button {                
                width: 50%;
            }
        }
        .availability-wrapper {
            margin-top: 10px;
        }
        
        @media only screen and (min-width: 768px) {
            .availability-wrapper {
                margin-top: 15px;
            }
        }
        .availability-wrapper .dropdown-menu {
            max-height: 400px;
            overflow-y: auto;
        }
        .dropdown-menu {
          width: 50.2%;
          top: 100%;
          left: 24.9%;
        }
        
        .pan-u-tabs {
            padding-left: 0;
            margin-left: 0;
            text-align: center;
            display: inline-block;
            width: 90%;
            background: #fff;
        }
        @media only screen and (min-width: 992px) {   
            .pan-u-tabs {
                 width: 100%;
            }           
        }
        .pan-u-tabs > li {
            display: inline-block;
            width: 50%;
            cursor: pointer;   
            padding: 10px 0;
        }
        .pan-u-tabs > li.pan-u-selected-tab {
            border-bottom: 3px solid #7ca7f7;
        }
        .add-new-space-btn {
            background: #fd4c4c;
            display: inline-block;
            color: #fff;
            padding: 11px 16px;
            border-radius: 50%;
            margin-right: 60px;
            position: relative;
        }
        .add-new-space-btn:hover > .add-new-space-note {
           display: inline-block;
        }
        .add-new-space-note {
            display: inline-block;
            position: absolute;
            font-size: 10px;
            width: 100px;
            background: #6d6d6d;
            left: -115px;
            text-align: center;
            top: 10px;
            padding: 4px 10px;
            display: none;
        }
        .add-new-space-note:after, .add-new-space-note:before {
            left: 100%;
            top: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }        
        .add-new-space-note:after {
            position: absolute;
            border-left-color: #6d6d6d;
            border-width: 10px;
            margin-top: -10px;
        }
        .add-new-space-btn:hover {
            color: #fff!important;
        }
        .add-new-space-btn-wrapper {
            text-align: right;
        }
        .events-wrapper .event-block {
            width: 100px;
            text-align: left;
            padding-left: 10px;
        }
        .events-wrapper .event-block > div {
            display: inline-block;
        }
        
        .mobile-panel-top {
            z-index: 999;
        }
        .back-to-calander {
            width: 10%;
            display: inline-block;
            background: #7ca7f7;
            color: #fff;
        }
        @media only screen and (min-width: 992px) {
          .back-to-calander {
            display: none;
          }
        }
        .back-to-calander:hover {
            color: #fff!important;
        }
        .back-to-calander {
            vertical-align: top;
            padding: 11.5px 0;  
        }
        .back-to-space {
            margin-bottom: 10px;
        }
        .back-to-space:hover > a {
            color: #5b92f1;
        }
        
        .my-space-title-wrapper {
            background: #fff;
            padding: 10px;
            margin-top: 10px;
        }
        .my-space-title-wrapper h1 {
            margin-top: 0;
            margin-bottom: 0;
        }
        
    `]
})
export class MySpacesAvailabilityComponent implements AfterViewInit, OnInit{

    userSpaces: any = [];
    seatingOptions: any = [];
    eventTypes: any = [];

    selectedSpace: any;
    dayViewData: any;

    @ViewChild(HostSchedulerComponent)
    scheduler: HostSchedulerComponent;

    @ViewChild(HostCalendarComponent)
    hostCalendar: HostCalendarComponent;

    bookingForm = {
        visible: false
    }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private spaceService: SpaceService,
    ){
        this.seatingOptions = this.route.snapshot.data['seatingOptions'];
        this.eventTypes = this.route.snapshot.data['eventTypes'];
        this.selectedSpace = this.route.snapshot.data['space'];
    }

    futureBookingDates;

    ngOnInit(){
        this.spaceService.getFutureBookingDates(this.selectedSpace.id).subscribe(futureBookingDates => {
            this.futureBookingDates = futureBookingDates;
        })
    }


    ngAfterViewInit () {

        // Caching wrappers
        // let $panelAnimatedWrapper = $('.panel-animated-wrapper'),
        //     $panelUnderneath = $('.panel-underneath'),
        //     $panelTop = $('.panel-top'),
        //     $animationTrigger = $panelTop,
        //     delay = 0;

        // if ($(window).innerWidth() > 768) {
        //     $panelUnderneath.css({
        //         marginLeft: -$panelUnderneath.outerWidth()/2,
        //         left: '50%'
        //     });

        //     $panelTop.css({
        //         marginLeft: -$panelUnderneath.outerWidth()/2,
        //         left: '50%'
        //     });

        //     $('.close').on ('click', function (e) {

        //         e.preventDefault ();

        //         $panelUnderneath.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function (e) {
        //             $('.event-block').each(function () {
        //                 $(this).css('opacity', 0);
        //             });
        //         });

        //         $panelTop.css({
        //             left: $panelTop.position().left + $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });

        //         $panelUnderneath.css({
        //             left: $panelUnderneath.position().left - $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });

        //         $panelUnderneath.removeClass('hidden-panel');

        //     });

        //     $('.up-edit-content').css({
        //         minHeight: $(document).innerHeight () - 20
        //     })
        // }

        // ------------- DONT DELETE -------------
        // var sortlists = $(".schedular").sortable({
        //     connectWith : ".schedular",
        //     items       : ".selection",
        //     tolerance   : 'pointer',
        //     revert      : 'invalid',
        //     forceHelperSize: true
        //
        // }).on('scroll', function() {
        //     sortlists.scrollTop($(this).scrollTop());
        // }).disableSelection();

        
    }

    selectSpace(space){
        this.spaceService.getSpace(space.id).subscribe(space => {
            this.selectedSpace = space;
        }, error => {
            console.log('error', error);
            this.selectedSpace = null;
            alert('something went wrong');
        })
        this.closeDayView();
    }

    showListingHome(){
        this.router.navigate(['/spaces/listing/home'])
    }

    showDayView(event){
        this.dayViewData = event.dayViewData;
        this.openDayView();
        $(".hide-on-first-sight").removeClass('hide-on-first-sight ')
    }

    closeDayView(){
        let $panelUnderneath = $('.panel-underneath'),
            $panelTop = $('.panel-top');

        // if ($(window).innerWidth () > 768) {

        //     if ($panelUnderneath.hasClass('hidden-panel')) {

        //         $panelUnderneath.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function (e) {
        //             $('.event-block').each(function () {
        //                 $(this).css('opacity', 0);
        //             });
        //         });

        //         $panelTop.css({
        //             left: $panelTop.position().left + $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });
        //         $panelUnderneath.css({
        //             left: $panelUnderneath.position().left - $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });

        //         $panelUnderneath.removeClass('hidden-panel');

        //     }


        // } else {
        //     $panelUnderneath.removeClass('mobile-panel-top');
        // }
        $panelUnderneath.removeClass('mobile-panel-top');

    }

    openDayView() {

        let $panelAnimatedWrapper = $('.panel-animated-wrapper'),
            $panelUnderneath = $('.panel-underneath'),
            $panelTop = $('.panel-top'),
            $animationTrigger = $panelTop,
            delay = 0;


        // if ($(window).innerWidth() > 768) {
        //     if (!$panelUnderneath.hasClass('hidden-panel')) {

        //         $panelTop.css({
        //             left: $panelTop.position().left - $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });

        //         $panelUnderneath.css({
        //             left: $panelUnderneath.position().left + $panelUnderneath.outerWidth()/2,
        //             transition: 'left 0.3s'
        //         });

        //         $panelUnderneath.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function (e) {
        //             $('.event-block').each(function (i) {
        //                 $(this).delay(100 * i).animate({ opacity: 1}, 400);
        //             });

        //             $panelUnderneath.addClass('hidden-panel');

        //         });
        //     }
        // } else {
            
        // }

        $($panelUnderneath).addClass('mobile-panel-top');

    }

    showBlockSpaceForm = false;
    blockSpace(){
        this.showBlockSpaceForm = true;
    }

    onClickCloseBlockSpace(state){
        this.showBlockSpaceForm = state;
    }

    displayAvailableEvents = false;
    displayScheduler = true;

    showAvailableEventsView(){
        this.displayScheduler = false;
        this.displayAvailableEvents = true;
    }

    showSchedulerView(){
        this.displayAvailableEvents = false;
        this.displayScheduler = true;
    }

    time: any;
    updatedFormData: any;

    showBookingForm(event){

        //reset form data
        this.time = null;
        this.updatedFormData = null;

        this.time = event.time;
        this.bookingForm.visible = true;

    }

    showBookingFormUpdated(event){

        //reset form data
        this.time = null;
        this.updatedFormData = null;

        this.updatedFormData = event.blockedSpace;
        this.bookingForm.visible = true;

    }


    onClickClose(state){
        if(state){
            this.bookingForm.visible = false;
            this.scheduler.clearActiveSelectors();
        }
    }

    onClickRemove(event){

        this.scheduler.removeBlock(event.blockId);

        this.hostCalendar.removeBlock(event.blockId);

        this.bookingForm.visible = false;

        location.reload();
    }

    onSuccessBlock(event){
        console.log('onSuccessBlock()');

        /*// add new event to scheduler(this will cause to re render scheduler)
        this.scheduler.addNewEvent(event.schedulerEvent);

        let eventDayData = {
            blocks: event.blockSet
        }
        console.log('eventDayData', eventDayData);

        //adding new event to existing 'eventsPerDay',
        //when click on the day cell, then user can see the new event both on scheduler and dayview
        this.dayViewData.eventsPerDay.push({
            from: event.schedulerEvent.from+':00:00',
            to: event.schedulerEvent.to+':00:00',
            note: event.schedulerEvent.text,
            isManual: true,
            id: event.schedulerEvent.id,
        });

        //re render calendar day with new event.(blue dot on date)
        this.hostCalendar.setEventDay(eventDayData);*/

        this.bookingForm.visible = false;

        console.log('event', event);

        location.reload();

    }

    onSuccessBlockPlus(event){

        this.showBlockSpaceForm = false; // if opened
    }

    onClickUpdate(event){
        console.log('onClickUpdate()');


        /*// add new event to scheduler(this will cause to re render scheduler)
        this.scheduler.addNewEvent(event.schedulerEvent);

        let eventDayData = {
            blocks: event.blockSet
        }

        //adding new event to existing 'eventsPerDay',
        //when click on the day cell, then user can see the new event both on scheduler and dayview
        this.dayViewData.eventsPerDay.push({
            from: event.schedulerEvent.from+':00:00',
            to: event.schedulerEvent.to+':00:00',
            note: event.schedulerEvent.text,
            isManual: true,
            id: event.schedulerEvent.id,
        });

        //re render calendar day with new event.(blue dot on date)
        this.hostCalendar.setEventDay(eventDayData);*/

        this.bookingForm.visible = false;

        location.reload();
    }

    zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    toggleCalendar(){
        this.closeDayView();
    }

    showSpace(space: any) {
        const spaceName = MsUtil.replaceWhiteSpaces(space.name, '-');
        window.open(`#/spaces/${space.id}/${spaceName}`, '_blank');
    }
}