import {Component, Input, AfterViewInit, OnInit, Output, EventEmitter} from "@angular/core";
import {SpaceService} from "../../spaces/shared/space.service";
import {BookingService} from "../my-activities/booking.service";

declare let moment: any;
declare let $: any;

@Component({
    selector: 'manual-booking',
    templateUrl: './manual-booking.component.html',
    styles: [`
        input[type="text"], textarea {
            border: 1px solid #e2e2e2;
        }
        .add-new-space-modal-wrapper .dropdown-full-width button {
            border: 1px solid #e2e2e2;
        }
        .add-new-space-modal-wrapper {
            display: inline-block;
            position: fixed;
            top: 43px;
            left: 0;
            background: rgba(0, 0, 0, 0.4);
            z-index: 99999;
            width: 100%;
            height: 100%;
            text-align: center;
            padding-top: 10px;
        }
        .add-new-space-modal-wrapper header {
            background: #43b7de;
            color: #fff;
            padding: 10px 0;
        }
        .add-new-space-modal-wrapper header h4 {
            margin: 0;
            padding: 0;
            color: #fff;
        }
        .add-new-space-modal {
            display: inline-block;
            width: 90%;
            background: #ffffff;
            height: 96%;
            overflow-y: auto;
            overflow-x: hidden;
            text-align: left;
        }
        .modal-window-close-button i {
            color: #fff;
        }
        @media only screen and (min-width: 768px) {
            .add-new-space-modal {
                width: 70%;
            }
        }
        .modal-window-header > h4 {
            text-align: center;
            font-weight: bold;
        }
        .add-new-modal-selected-date {
            text-align: center;
            margin-bottom: 40px;
        }
        .input-wrapper-full-width input, .input-wrapper-full-width textarea {
            display: inline-block;
            width: 100%;
            max-width: 100%;
            height: 40px;
        }
        .input-wrapper-full-width textarea {
            resize: none;
        }
        .dropdown-full-width button{
            width: 100%;
            padding: 10px;
            position: relative;
            margin-top: 31px;
        }
        .input-row {
            margin-top: 30px;
            margin-bottom: 30px;
        }
        .list-item-seating {
            margin-bottom: 10px;
        }
        .cost-details-wrapper {
            margin-bottom: 30px;
        }
        .seating-arrangement-wrapper {
            margin-bottom: 30px;
        }
        .extra-details-wrapper {
            margin-bottom: 30px;
            margin-top: 30px;
        }
        .guest-details-wrapper {
            margin-bottom: 30px;
            margin-top: 30px;
        }
        textarea {
            height: 100px!important;
        }
        .buttons-wrapper > button {
            background:
        }
        .modal-buttons-wrapper {
            text-align: right;
        }
        .modal-close-btn {
            background: #ff1a1a;
            color: #fff;
            border-radius: 0px;
            border: none;
            padding: 6px 30px;
            display: inline-block;
        }
        .modal-save-btn {
            background: #4079f9;
            color: #fff;
            border-radius: 0px;
            border: none;
            padding: 6px 30px;
            display: inline-block;
        }
        /*.seating-arr-heading {*/
            /*font-weight: bold;*/
        /*}*/
        /*label {*/
            /*font-weight: bold;*/
        /*}       */
        .error-message {
            padding: 10px;
            background: tomato;
            color: #fff;
        }
        .modal-window-close-button {
            position: absolute;
            top: 10px;
            right: 25px;
            color: tomato;
            font-size: 20px;
        }
        .add-new-space-modal {
            position: relative;
        }
        .edit-booking-modal-label, .seating-arr-heading {
            font-weight: bold;
        }
        .modal-body {
            padding: 0 30px 20px;
        }
        .add-new-modal-start-end-dates-wrapper {
            margin-top: 30px;
        }
        .block-space-validation-fail {
            border: 1px solid red!important;
        }
        .block-space-button-disabled{
            background-color: #99d6ff!important;
        }
    `]
})
export class ManualBookingComponent implements OnInit, AfterViewInit{

    eventStart: any;
    eventEnd: any;

    @Input('space') space;
    @Input('seatingOptions') seatingOptions;
    @Input('day') day;
    @Input('eventTypes') eventTypes;
    @Output('onClickClose') onClickClose = new EventEmitter<boolean>();
    @Output('onClickRemove') onClickRemove = new EventEmitter<any>();
    @Output('onClickUpdate') onClickUpdate = new EventEmitter<any>();
    @Output('onSuccessBlock') onSuccessBlock = new EventEmitter<any>();

    selectedEvent: any;
    bookingDate = moment();

    blockModel = {
        id: <number> null,
        spaceId: <number> null,
        eventStart: <string> null,
        eventEnd: <string> null,
        cost: <string> '',
        remark: <string> '',
        guestContactNumber: <string> '',
        guestEmail: <string> '',
        guestName: <string> '',
        maximumGuest: <string> '',
        eventTitle: <string> '',
        eventTypeId: <number> null,
        eventTypeName: <string> '',
        extrasRequested: <string> '',
        seatingArrangementIcon: <string> '',
        seatingArrangementId: <number> null,
        seatingArrangementName: <string> '',
    }

    constructor(
        private spaceService: SpaceService,
        private bookingService: BookingService
    ){

    }

    isUpdateForm = false;

    @Input()
    set time(time: any){
        if(!time) return;

        this.isUpdateForm = false;

        this.resetBlockModel();

        let from = this.day.format('YYYY-MM-DDT')+this.zeroPad(+time.from, 2)+':00';
        let end = this.day.format('YYYY-MM-DDT')+this.zeroPad(+time.to, 2)+':00';
        this.eventStart = from;
        this.eventEnd = end;
        this.blockModel.eventStart = from + 'Z';
        this.blockModel.eventEnd = end + 'Z';
    }

    @Input()
    set updatedFormData(updatedFormData: any){
        if(!updatedFormData) return;

        this.isUpdateForm = true;

        this.resetBlockModel();

        this.setBlockModel(updatedFormData);

    }

    setBlockModel(formData){
        console.log('formData', formData);

        this.eventStart =  moment(formData.fromDate).format('YYYY-MM-DDTHH:mm');
        this.eventEnd = moment(formData.toDate).format('YYYY-MM-DDTHH:mm');

        this.blockModel = {
            id: formData.id,
            spaceId: formData.space,
            eventStart: moment(formData.fromDate).format('YYYY-MM-DDTHH:mm')+'Z',
            eventEnd: moment(formData.toDate).format('YYYY-MM-DDTHH:mm')+'Z',
            cost: formData.cost,
            remark: formData.remark,
            guestContactNumber: formData.guestContactNumber,
            guestEmail: formData.guestEmail,
            guestName: formData.guestName,
            maximumGuest: formData.noOfGuests,
            eventTitle: formData.eventTitle,
            eventTypeId: formData.eventTypeId,
            eventTypeName: formData.eventTypeName,
            extrasRequested: formData.extrasRequested,
            seatingArrangementIcon: formData.seatingArrangementIcon,
            seatingArrangementId: formData.seatingArrangementId,
            seatingArrangementName: formData.seatingArrangementName,
        }



    }

    ngOnInit(){
        this.blockModel.spaceId = this.space.id;
    }

    ngAfterViewInit(){
        $(".event-start").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            minView: 1,
        }).on('show', () => {
            $('.event-start').datetimepicker('setStartDate', this.eventStart);
        }).on('changeDate', function (ev) {
            this.eventStart = ev.date;
            this.blockModel.eventStart = moment(ev.date).format("YYYY-MM-DDTHH:00")+'Z';
            $('.event-end').datetimepicker('setStartDate', this.eventStart);
        }.bind(this));

        $(".event-end").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            minView: 1,
        }).on('show', () => {
            $('.event-end').datetimepicker('setStartDate', this.eventStart);
        }).on('changeDate', function (ev) {
            this.eventEnd = ev.date;
            this.blockModel.eventEnd = moment(ev.date).format("YYYY-MM-DDTHH:00")+'Z';
        }.bind(this));
    }

    selectEventType(event: any){
        this.selectedEvent = event;
        this.blockModel.eventTypeId = event.id;
        this.blockModel.eventTypeName = event.name
    }

    selectSeatingOption(option){
        this.blockModel.seatingArrangementIcon = option.icon;
        this.blockModel.seatingArrangementId = option.id;
        this.blockModel.seatingArrangementName= option.name;
    }

    spaceAvailableForBooking = false;
    checkedAvailability = false;

    //check availability at millionspaces
    checkAvailability(){

        let availabilityRequest = {
            space: this.space.id,
            dates:[ {
                fromDate: this.blockModel.eventStart,
                toDate: this.blockModel.eventEnd,
            }],
        }

        this.spaceService.getSpaceAvailablility(availabilityRequest).subscribe(available => {
            if(available){
                this.spaceAvailableForBooking = true;
                this.blockSpace();

            }else{
                this.spaceAvailableForBooking = false;
                this.checkedAvailability = true;
            }

        }, err => {
            console.log('network error!!', err);
        });
        //this.blockSpace();
    }

    blockSpace(){
        let blockModel = this.blockModel;

        let blockRequest = {
            space: blockModel.spaceId,
            fromDate: blockModel.eventStart,
            toDate: blockModel.eventEnd,
            dateBookingMade: moment().format('YYYY-MM-DDTHH:00')+'Z',
            cost: blockModel.cost,
            remark: blockModel.remark,
            guestContactNumber: blockModel.guestContactNumber,
            guestEmail: blockModel.guestEmail,
            guestName: blockModel.guestName,
            noOfGuests: blockModel.maximumGuest,
            eventTitle: blockModel.eventTitle,
            eventTypeId: blockModel.eventTypeId,
            eventTypeName: blockModel.eventTypeName,
            extrasRequested: blockModel.extrasRequested,
            seatingArrangementIcon: blockModel.seatingArrangementIcon,
            seatingArrangementId: blockModel.seatingArrangementId,
            seatingArrangementName: blockModel.seatingArrangementName
        }

        console.log('blockRequest', blockRequest);

        this.bookingService.blockSpaceManual(blockRequest).subscribe(blockId => {
            if(blockId != 0 && blockId > 0){
                alert('successfully blocked '+ blockId);

                sessionStorage.setItem('e_date', moment(blockRequest.fromDate).month());

                let blockStart = blockModel.eventStart.split('T')[0];
                let blockEnd = blockModel.eventEnd.split('T')[0];
                let schedulerEvent = {text: '', from : 0, to: 24, id: 0, guest: false};

                if(blockStart === blockEnd){
                    schedulerEvent = {
                        text: blockModel.remark,
                        from: +blockModel.eventStart.split('T')[1].split(':')[0],
                        to: +blockModel.eventEnd.split('T')[1].split(':')[0],
                        id: blockId,
                        guest: false
                    }
                }else{
                    schedulerEvent = {
                        text: blockModel.remark,
                        from: +blockModel.eventStart.split('T')[1].split(':')[0],
                        to: 24,
                        id: blockId,
                        guest: false
                    }
                }

                let myDates = this.getDates(new Date(blockStart), new Date(blockEnd));
                let blockSet = [];

                if(blockStart === blockEnd){
                    let block = {
                        from: moment(this.day).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                        to: moment(this.day).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.to, 2)+':00',
                        note: schedulerEvent.text,
                        isManual: true,
                        id: blockId
                    }
                    blockSet.push(block)
                }else{
                    let blocks = [];
                    blocks = myDates.map((blockDate, index) => {
                        if(index === 0){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }else if(index === (myDates.length -1)){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT')+blockModel.eventEnd.split('T')[1].split(':')[0]+':00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }else{
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: blockId
                            }
                        }

                    })
                    blockSet.push(...blocks);
                }

                this.onSuccessBlock.emit({schedulerEvent, blockSet});

            }else{
                alert('something went wrong');
            }
        })
    }

    closeForm(){
        this.onClickClose.emit(true);
    }

    resetBlockModel(){
        this.eventStart = null;
        this.eventEnd = null;

        this.blockModel = {
            id: <number> null,
            spaceId: this.space.id,
            eventStart: <string> null,
            eventEnd: <string> null,
            cost: <string> '',
            remark: <string> '',
            guestContactNumber: <string> '',
            guestEmail: <string> '',
            guestName: <string> '',
            maximumGuest: <string> '',
            eventTitle: <string> '',
            eventTypeId: <number> null,
            eventTypeName: <string> '',
            extrasRequested: <string> '',
            seatingArrangementIcon: <string> '',
            seatingArrangementId: <number> null,
            seatingArrangementName: <string> ''
        }
    }

    updateBlock(block){
        if(!block){
           return;
        }

        let updateBlockRequest = {
            id: block.id,
            space: block.spaceId,
            fromDate: block.eventStart,
            toDate: block.eventEnd,
            dateBookingMade: moment().format('YYYY-MM-DDTHH:00')+'Z',
            cost: block.cost,
            remark: block.remark,
            guestContactNumber: block.guestContactNumber,
            guestEmail: block.guestEmail,
            guestName: block.guestName,
            noOfGuests: block.maximumGuest,
            eventTitle: block.eventTitle,
            eventTypeId: block.eventTypeId,
            eventTypeName: block.eventTypeName,
            extrasRequested: block.extrasRequested,
            seatingArrangementIcon: block.seatingArrangementIcon,
            seatingArrangementId: block.seatingArrangementId,
            seatingArrangementName: block.seatingArrangementName
        }

        this.bookingService.updateManualBlock(updateBlockRequest).subscribe(status => {
            console.log('status', status);

            if(+status === 1){
                alert('block updated successfully');

                let blockStart = block.eventStart.split('T')[0];
                let blockEnd = block.eventEnd.split('T')[0];
                let schedulerEvent = {text: '', from : 0, to: 24, id: 0, guest: false};

                if(blockStart === blockEnd){
                    schedulerEvent = {
                        text: block.note,
                        from: +block.eventStart.split('T')[1].split(':')[0],
                        to: +block.eventEnd.split('T')[1].split(':')[0],
                        id: block.id,
                        guest: false
                    }
                }else{
                    schedulerEvent = {
                        text: block.note,
                        from: +block.eventStart.split('T')[1].split(':')[0],
                        to: 24,
                        id: block.id,
                        guest: false
                    }
                }

                let myDates = this.getDates(new Date(blockStart), new Date(blockEnd));
                let blockSet = [];

                if(blockStart === blockEnd){
                    let myBlock = {
                        from: moment(this.day).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                        to: moment(this.day).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.to, 2)+':00',
                        note: schedulerEvent.text,
                        isManual: true,
                        id: block.id
                    }
                    blockSet.push(myBlock)
                }else{
                    let blocks = [];
                    blocks = myDates.map((blockDate, index) => {
                        if(index === 0){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT')+this.zeroPad(schedulerEvent.from, 2)+':00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: block.id
                            }
                        }else if(index === (myDates.length -1)){
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT')+block.eventEnd.split('T')[1].split(':')[0]+':00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: block.id
                            }
                        }else{
                            return {
                                from: moment(blockDate).format('YYYY-MM-DDT') + '00:00',
                                to: moment(blockDate).format('YYYY-MM-DDT') + '24:00',
                                note: schedulerEvent.text,
                                isManual: true,
                                id: block.id
                            }
                        }

                    })
                    blockSet.push(...blocks);
                }
                this.onClickUpdate.emit({schedulerEvent, blockSet});

            }
            else{
                alert('block update ERROR');
            }
        });
    }

    removeBlock(blockId){
        this.bookingService.deleteManualBooking(blockId).subscribe(status => {
            if(status === 1){
                alert('blocked removed');
                this.onClickRemove.emit({blockId});
            }else{
                console.log('not removed');
            }
        })
    }

    addDays(days, currentDate) {
        var dat = new Date(currentDate.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
    }

    getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(currentDate)
            currentDate = this.addDays(1, currentDate);
        }
        return dateArray;
    }

    zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }



}
