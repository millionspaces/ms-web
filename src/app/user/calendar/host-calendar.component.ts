import {
    Component, AfterViewInit, Input, OnChanges, SimpleChanges, Output, EventEmitter,
    OnDestroy, OnInit
} from "@angular/core";

declare let moment: any;
declare let $: any;

@Component({
    selector: 'host-calendar',
    template: `
        <div class="host-cal">

        </div>
    `,
    styles: [`
        :host >>> .clndr-controls {
            font-size: 20px;
            padding-top: 20px;
        }
        :host >>> .clndr-controls {
            font-size: 14px;
            margin-bottom: 20px;
        }
        :host >>> .clndr-control-button, :host >>> .month, :host >>> .clndr-control-button {
            display: inline-block;
            width: 33.33%
        }
        /*:host >>> .month {*/
            /*text-align: center;*/
        /*}*/
        /*:host >>> .clndr-next-btn-wrapper {*/
            /*text-align: right;*/
        /*}*/
        :host >>> .header-days {
            /*display: inline-block;*/
        }
        :host >>> .header-days > div {
            display: inline-block;
            width: 14.2%;
        }
        :host >>> .clndr-weeks-wrapper > div {

        }
        :host >>> .clndr-weeks-wrapper > div > div {
            display: inline-block;
            width: 14.2%;
            height: 50px;
        }
        :host >>> .book-cal {
            text-align: center;
            background: #ffffff;
            margin: 5px;
        }
        :host >>> .day-contents {
            display: table;
            margin: 0 auto;
            height: 100%;
        }
        :host >>> .day-cell {
            display: table-cell;
            vertical-align: middle;
            height: 100%;
            position: relative;
            width: 15px;
        }
        :host >>> .adjacent-month {
            color: #e2e2e2;
        }
        :host >>> .day-contents {
            cursor: pointer;
        }
        :host >>> .month {
            font-size: 16px;
        }
        :host >>> .clndr-next-button, :host >>> .clndr-previous-button {
            cursor: pointer;
            font-size: 22px;
            padding: 10px 20px;
        }
        :host >>> .event > div > div > span {
            position: absolute;
            background: #388fff;
            bottom: 7px;
            left: 7px;
            width: 5px;
            height: 5px;
            display: inline-block;
            border-radius: 50%;
        }
        :host >>> .today {
            border-radius: 50%;
            color: #388fff;
            width: 49px;
            height: 40px;
            font-weight: bold;
        }

        /*:host >>> .today.event > div > div > span {*/
            /*left: 23px;*/
        /*}*/

        :host >>> .inactive {
            color: #989898;
        }
        :host >>> .past {
            border-radius: 50%;
            width: 49px;
            height: 40px;
        }

    `]
})
export class HostCalendarComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy{

    hostCalendar: any;
    blockPeriods: any;
    eventDaysCalendar: any;
    events: any [];

    @Input('space') space;
    @Input('fBookingDates') fBookingDates;
    @Output('showDayView') showDayView = new EventEmitter();

    clndrTemplate = `
        <div class='clndr-controls'>
            <div class='clndr-control-button'>
                <span class='clndr-previous-button'><i class="fa fa-angle-left" aria-hidden="true"></i></span>
            </div
            ><div class='month'><%= month %> <%= year %></div
            ><div class='clndr-control-button clndr-next-btn-wrapper'>
                <span class='clndr-next-button'><i class="fa fa-angle-right" aria-hidden="true"></i></span>
            </div>
        </div>
        <div>
            <div>
                <div class='header-days'><% for(var i = 0; i < daysOfTheWeek.length; i++) { %><div class='header-day'><%= daysOfTheWeek[i] %></div><% } %></div>
            </div>
            <div class="clndr-weeks-wrapper"><% for(var i = 0; i < numberOfRows; i++){ %><div><% for(var j = 0; j < 7; j++){ %><% var d = j + i * 7; %><div class='<%= days[d].classes %>'><div class='day-contents'><div class="day-cell"><%= days[d].day %><span></span></div></div></div><% } %></div><% } %></div>
        </div>
    `

    ngOnInit(){
        this.blockPeriods = this.fBookingDates.map(block =>
            {
                return {
                    from: block.from,
                    to: block.to,
                    note: (block.note) ? block.note : '',
                    isManual: (block.isManual === 1) ? true : false,
                    id: block.id
                }
            }
        );

        let eventDaysCalendar = [];

        if(this.space.availabilityMethod === 'HOUR_BASE'){
            this.fBookingDates.forEach(date => {
                let startDate = moment(date.from).format('YYYY-MM-DD');
                let endDate = moment(date.to).format('YYYY-MM-DD');

                //same day booking
                if(startDate === endDate || moment(date.to).diff(moment(date.from),  'hours') === 24){
                    eventDaysCalendar.push({date: startDate});
                }else{
                    // not same day
                    let dates = getDates(new Date(startDate), new Date(endDate));
                    dates = dates.map(date =>  {
                        return { date: moment(date).format('YYYY-MM-DD') }
                    });
                    eventDaysCalendar.push(...dates);
                }
            })
        }
        else if(this.space.availabilityMethod === 'BLOCK_BASE'){
            this.fBookingDates.forEach(date => {
                let startDate = moment(date.from).format('YYYY-MM-DD');
                let endDate = moment(date.to).format('YYYY-MM-DD');

                //same day booking
                if(startDate === endDate || moment(date.to).diff(moment(date.from),  'hours') === 24){
                    eventDaysCalendar.push({date: startDate});
                }else{
                    // not same day
                    let dates = getDates(new Date(startDate), new Date(endDate));
                    dates = dates.map(date =>  {
                        return { date: moment(date).format('YYYY-MM-DD') }
                    });
                    eventDaysCalendar.push(...dates);
                }
            })
        }

        function addDays(days, currentDate) {
            var dat = new Date(currentDate.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = addDays(1, currentDate);
            }
            return dateArray;
        }

            //this.hostCalendar.setMonth(moment().month());
            //this.hostCalendar.setEvents(eventDaysCalendar);
            this.events = eventDaysCalendar;

    }

    ngAfterViewInit(){
        this.hostCalendar = (<any>$('.host-cal')).clndr({
            template: this.clndrTemplate,
            daysOfTheWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            weekOffset: 1,
            events: this.events,
            constraints: {
                endDate: this.getCalEndDate(),
                startDate: this.getCalStartDate()
            },
            classes: {
                past: "past",
                today: "today",
                event: "event",
                inactive: "inactive",
                selected: "selected",
                lastMonth: "last-month",
                nextMonth: "next-month",
                adjacentMonth: "adjacent-month"
            },
            clickEvents: {
                click: function (target) {
                    //console.log('target', target);

                    if ($(target.element).hasClass('inactive')) {
                        return;
                    }

                    let selectedDate = moment(target.date).format('DD/MM/YYYY');

                    let eventsPerDay = [];
                    let availabilityPerDayHourBase = { from: null, to: null, day: null, charge: null };
                    //let availabilityPerDayBlockBase = [];

                    //fill day availability
                    if(this.space.availabilityMethod === "HOUR_BASE"){
                        this.space.availability.forEach(dayAvailability => {
                            if(target.date.weekday() === dayAvailability.day){
                                availabilityPerDayHourBase = dayAvailability;
                                return;
                            }
                            else if(target.date.weekday() === 0 && dayAvailability.day === 7){
                                availabilityPerDayHourBase = dayAvailability;
                                return;
                            }
                        })
                    }

                    //fill day availability - per block
                    /*else if(this.space.availabilityMethod === "BLOCK_BASE"){
                        this.space.availability.forEach(dayAvailability => {
                            //handle weekdays blocks
                            if( (target.date.weekday() === 1 || target.date.weekday() === 2 || target.date.weekday() === 3
                                || target.date.weekday() === 4 || target.date.weekday() === 5 ) && dayAvailability.day === 8
                            ){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                            //handle saturday blocks
                            else if(target.date.weekday() === 6 && dayAvailability.day === 6){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                            //handle sunday blocks
                            else if(target.date.weekday() === 0 && dayAvailability.day === 7){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                        })
                    }*/

                    // fill schedularData
                    if(this.space.availabilityMethod === "HOUR_BASE"){

                        //fill events per day for both PH and PB
                        this.blockPeriods.forEach(block => {

                            let startDate = moment(block.from).format('DD/MM/YYYY');
                            let endDate = moment(block.to).format('DD/MM/YYYY');
                            let note = block.note;
                            let isManual = block.isManual;
                            let id = block.id;

                            // same date
                            if(selectedDate === startDate && selectedDate === endDate)
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: moment(block.to).format('HH:mm:00'),
                                    note,
                                    isManual,
                                    id
                                });

                            // partially booked from start date
                            else if(selectedDate === startDate && selectedDate !== endDate){
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: (isManual) ? '24:00:00' : availabilityPerDayHourBase.to,
                                    note,
                                    isManual,
                                    id
                                });
                            }

                            // fullday booked
                            else if(moment(selectedDate, 'DD/MM/YYYY').isBetween(moment(startDate, 'DD/MM/YYYY'), moment(endDate, 'DD/MM/YYYY'))){
                                eventsPerDay.push({
                                    fullDay: true,
                                    from: (isManual) ? '00:00:00' : availabilityPerDayHourBase.from,
                                    to: (isManual) ? '24:00:00' : availabilityPerDayHourBase.to,
                                    note,
                                    isManual,
                                    id
                                });
                            }

                            // partially booked from end date
                            else if(selectedDate === endDate && selectedDate !== startDate){
                                eventsPerDay.push({
                                    from: (isManual) ? '00:00:00' : availabilityPerDayHourBase.from,
                                    to: moment(block.to).format('HH:mm:00'),
                                    note,
                                    isManual,
                                    id
                                });
                            }
                        })
                    }


                    else if(this.space.availabilityMethod === "BLOCK_BASE"){

                        this.blockPeriods.forEach(block => {

                            let startDate = moment(block.from).format('DD/MM/YYYY');
                            let endDate = moment(block.to).format('DD/MM/YYYY');
                            let note = block.note;
                            let isManual = block.isManual;
                            let id = block.id;

                            // same date
                            if(selectedDate === startDate && selectedDate === endDate)
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: moment(block.to).format('HH:mm:00'),
                                    note,
                                    isManual,
                                    id
                                });

                            // partially booked from start date
                            else if(selectedDate === startDate && selectedDate !== endDate){
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: (isManual) ? '24:00:00' : availabilityPerDayHourBase.to,
                                    note,
                                    isManual,
                                    id
                                });
                            }

                            // fullday booked
                            else if(moment(selectedDate, 'DD/MM/YYYY').isBetween(moment(startDate, 'DD/MM/YYYY'), moment(endDate, 'DD/MM/YYYY'))){
                                eventsPerDay.push({
                                    fullDay: true,
                                    from: (isManual) ? '00:00:00' : availabilityPerDayHourBase.from,
                                    to: (isManual) ? '24:00:00' : availabilityPerDayHourBase.to,
                                    note,
                                    isManual,
                                    id
                                });
                            }

                            // partially booked from end date
                            else if(selectedDate === endDate && selectedDate !== startDate){
                                eventsPerDay.push({
                                    from: (isManual) ? '00:00:00' : availabilityPerDayHourBase.from,
                                    to: moment(block.to).format('HH:mm:00'),
                                    note,
                                    isManual,
                                    id
                                });
                            }
                        })

                    }

                    let dayViewData = {};

                    if(this.space.availabilityMethod === "HOUR_BASE"){
                        dayViewData = {
                            eventsPerDay,
                            availabilityType: 'HOUR',
                            //availability: availabilityPerDayHourBase,
                            date: moment(target.date)
                        }
                    }else if(this.space.availabilityMethod === "BLOCK_BASE"){
                        dayViewData = {
                            eventsPerDay,
                            availabilityType: 'BLOCK',
                            //availability: availabilityPerDayBlockBase,
                            date: moment(target.date)
                        }
                    }
                    this.showDayView.emit({dayViewData})

                }.bind(this)
            }
        });

        let month = sessionStorage.getItem('e_date') ? +sessionStorage.getItem('e_date') : moment().month();
        this.hostCalendar.setMonth(month);

    }

    getCalEndDate(){
        let clrArray = moment().format("YYYY-MM-DD").split('-');
        return (+clrArray[0] + 2) + '-' +clrArray[1] + '-' +clrArray[2];
    }

    getCalStartDate(){
        return moment(this.space.createdAt).format("YYYY-MM-DD");
    }


    ngOnChanges(changes: SimpleChanges){

        /*if(!this.space || !this.space.availabilityMethod) {
            return;
        }

        this.blockPeriods = this.fBookingDates.map(block =>
            {
                return {
                    from: block.from,
                    to: block.to,
                    note: (block.note) ? block.note : '',
                    isManual: (block.isManual === 1) ? true : false,
                    id: block.id
                }
            }
        );

        let eventDaysCalendar = [];

        if(this.space.availabilityMethod === 'HOUR_BASE'){
            this.fBookingDates.forEach(date => {
                let startDate = moment(date.from).format('YYYY-MM-DD');
                let endDate = moment(date.to).format('YYYY-MM-DD');

                //same day booking
                if(startDate === endDate){
                    eventDaysCalendar.push({date: startDate});
                }else{
                    // not same day
                    let dates = getDates(new Date(startDate), new Date(endDate));
                    dates = dates.map(date =>  {
                        return { date: moment(date).format('YYYY-MM-DD') }
                    });
                    eventDaysCalendar.push(...dates);
                }
            })
        }
        else if(this.space.availabilityMethod === 'BLOCK_BASE'){
            eventDaysCalendar = this.fBookingDates.map(date =>
                { return { date: date.from.split('T')[0] } }
            );
        }

        function addDays(days, currentDate) {
            var dat = new Date(currentDate.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = addDays(1, currentDate);
            }
            return dateArray;
        }

        if(this.hostCalendar){
            this.hostCalendar.setMonth(moment().month());
            this.hostCalendar.setEvents(eventDaysCalendar);
        }*/
    }

    ngOnDestroy(){
        console.log('hostCalendar destroy');
        sessionStorage.removeItem('e_date');
        this.hostCalendar.destroy();
    }

    setEventDay(eventDayData){
        this.blockPeriods.push(...eventDayData.blocks);

        let dates = eventDayData.blocks.map(eventDay => {
            return { date: eventDay.from.split('T')[0] }
        });

        // add blue dots for calendar
        this.hostCalendar.addEvents(dates);
    }

    removeBlock(blockId){

        let x = this.blockPeriods.find(bookingDate => bookingDate.id === blockId);
        let index = this.blockPeriods.indexOf(x);
        this.blockPeriods.splice(index, 1);

        // re initialize blue dots for calendar
        this.hostCalendar.setEvents(this.getCalendarDates());
    }


    getCalendarDates(){
        let eventDaysCalendar = [];

            this.blockPeriods.forEach(blockPeriod => {
                let startDate = blockPeriod.from.split('T')[0];
                let endDate = blockPeriod.to.split('T')[0];

                //same day booking
                if(startDate === endDate){
                    eventDaysCalendar.push({date: startDate});
                }else{
                    // not same day
                    let dates = getDates(new Date(startDate), new Date(endDate));
                    dates = dates.map(date =>  {
                        return { date: moment(date).format('YYYY-MM-DD') }
                    });
                    eventDaysCalendar.push(...dates);
                }
            })
        /*else if(this.space.availabilityMethod === 'BLOCK_BASE'){
            eventDaysCalendar = this.fBookingDates.map(date =>
                { return { date: date.from.split('T')[0] } }
            );
        }*/

        function addDays(days, currentDate) {
            var dat = new Date(currentDate.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = addDays(1, currentDate);
            }
            return dateArray;
        }
        return eventDaysCalendar;

    }

}
