import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { BookingService } from "./my-activities/booking.service";

@Injectable()
export class UserBookingResolve implements Resolve<any>{

    constructor(private bookingService: BookingService){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{

        let isManual: boolean = JSON.parse(route.queryParams['m']);

        if(isManual){
            return this.bookingService.getBlockedSpace(+route.params['bookingId']);
        }else{
            return this.bookingService.getUserBooking(+route.params['bookingId']);
        }

    }

}