import {Component, AfterViewInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../user.service";
import { ToasterService } from "../../shared/services/toastr.service";


declare let $: any;
declare let moment: any;

@Component({
    templateUrl: './my-profile.component.html',
    styles: [`

        .info-wrappers ul > li > input.ng-valid.ng-touched.ng-dirty {
          border-left: 5px solid green;
        }
        
        .info-wrappers ul > li > input.ng-invalid.ng-touched.ng-dirty {
          border-left: 5px solid red;
        }

        /*** Profile pic ***/
        .prof-pick-wrapper {
          text-align: center;
        }
        
        .profile-pick-holder {
          border-radius: 50%;
          overflow: hidden;
          display: inline-block;
          text-align: center;
          width: 200px;
          height: 200px;
        }
        
        .profile-pick-holder > img {
            width: 100%;
        }
        /**** Basic info ***/
        .basic-info-wrapper, .companay-info-wrapper, .companay-info-wrapper {
          background: #ffffff;
          margin-top: 20px;
          padding-left: 0;
          text-align: left;
        }
        
        @media only screen and (min-width: 768px) {
          .basic-info-wrapper, .companay-info-wrapper, .companay-info-wrapper {
            padding-left: 10px;
          }
        }
        
        .basic-info-wrapper ul, .companay-info-wrapper ul {
          list-style: none;
          padding-left: 0;
          padding: 10px;
        }
        
        .basic-info-wrapper ul > li, .companay-info-wrapper ul > li {
          margin-bottom: 10px;
        }
        
        .basic-info-wrapper ul > li > label, .companay-info-wrapper ul > li > label {
          display: inline-block;
          width: 100%;
          vertical-align: middle;
        }
        
               
        .basic-info-wrapper ul > li > input, .companay-info-wrapper ul > li > input {
          border: 1px solid #e2e2e2;
          height: 40px;
          outline: none;
          padding: 0 10px;
          width: 100%;
          vertical-align: middle;
        }
        
        @media only screen and (min-width: 768px) {
          .basic-info-wrapper ul > li > input, .companay-info-wrapper ul > li > input {
            width: 65%;
          }
        }
        
        .basic-info-wrapper ul > li > textarea, .companay-info-wrapper ul > li > textarea {
          border: 1px solid #e2e2e2;
          height: 100px;
          outline: none;
          padding: 0 10px;
          width: 100%;
          vertical-align: middle;
        }
        
        @media only screen and (min-width: 768px) {
          .basic-info-wrapper ul > li > textarea, .companay-info-wrapper ul > li > textarea {
            width: 65%;
          }
        }
        
        .basic-info-wrapper ul > li > p, .companay-info-wrapper ul > li > p {
          width: 100%;
          margin-left: 0;
          color: #7a7a7a;
          font-size: 14px;
          margin-top: 10px;
        }
        
        @media only screen and (min-width: 768px) {
          .basic-info-wrapper ul > li > p, .companay-info-wrapper ul > li > p {
            width: 70%;
            margin-left: 30%;
          }
        }
        
        .basic-info-wrapper > h2, .companay-info-wrapper > h2 {
          padding: 0;
          margin: 0;
          padding: 10px;
        }
        
        .btn-wrapper {
          text-align: right;
          margin: 20px 0;
        }
        
        .btn-wrapper > a {
          display: inline-block;
          border-radius: 0;
        }
        
        .input-group {
        width: 65%;
        }

    `]
})
export class MyProfileComponent implements AfterViewInit {

    user: any;

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private toaster: ToasterService
    ){
        this.user = route.parent.snapshot.data['user'];
    }

    saveUser(user){
        this.userService.updateUser(user).subscribe(resp => {

            if (resp.status === 200){
                this.toaster.success ("","Update successful")
            } else {
                this.toaster.error ('Something went wrong!', 'Error')
            }

        });
    }

    ngAfterViewInit() {
        $('#user-dob').datetimepicker({
            autoclose: true,
            pickerPosition: "top-right",
            howMeridian: true,
            forceParse: false,
            minView: '2',
            todayHighlight: true
        }).on('changeDate', function (ev) {
            console.log(ev);
            this.user.dateOfBirth = moment(ev.date).format("YYYY-MM-DD");
        }.bind(this));


        $('#user-anniv').datetimepicker({
            autoclose: true,
            pickerPosition: "top-right",
            howMeridian: true,
            forceParse: false,
            minView: '2',
            todayHighlight: true
        }).on('changeDate', function (ev) {
            console.log(ev);
            this.user.anniversary = moment(ev.date).format("YYYY-MM-DD");
        }.bind(this));
    }

}