import {Component, OnInit, OnDestroy, OnChanges, DoCheck} from "@angular/core";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
    templateUrl: './profile-container.component.html',
    styles: [`
        /*** Commmon **/
        .user-profile-edit-wrapper {
          padding-top: 10px;
          background: #ffffff;
        }

        .img-responsive {
          width: 100% !important;
        }

        /*** Side bar ***/
        .up-edit-sidebar {
          z-index: 99;
          background: #fff;
          position: fixed;
          top: 50px;
          height: 100%;
          overflow-x: hidden;
          background: #43b7de;
        }

        .up-edit-sidebar.sidebar-open {
          width: 100%;
          z-index: 9999;
        }

        .up-edit-sidebar.sidebar-close {
          width: 0%;
        }

        @media only screen and (min-width: 768px) {
          .up-edit-sidebar.sidebar-open {
            width: 200px;
          }
          .up-edit-sidebar.sidebar-close {
            width: 200px;
          }
        }

        .up-edit-sidebar > ul {
          list-style: none;
          padding-left: 0;
        }

        .up-edit-sidebar > ul > li {
          padding: 15px 10px;
          cursor: pointer;
        }

        .up-edit-sidebar > ul > li > a {
          color: #fff;
          padding: 10px 10px 5px;
          display: inline-block;
          width: 100%;
          text-transform: uppercase;
          font-size: 12px;
          border-radius: 3px;
        }
        .up-edit-sidebar > ul > li > a > i {
          vertical-align: middle;
          position: relative;
          top: -2px;
          font-size: 22px;
          margin-right: 5px;
        }
        .up-edit-sidebar > ul > li > a.activeLink  {
          color: #ffffff!important;
          font-weight: bold;
          background: rgba(255, 255, 255, 0.18);
        }
        .up-edit-sidebar > ul > li.up-link-active > a {
          color: #46a5cc;
        }
        .up-edit-sidebar > ul > li > i {
          margin-right: 5px;
        }

        .show-hide-sidebar {
            position: fixed;
            top: 53px;
            left: 0;
            border: 1px solid #d6d5d5;
            border-left: none;
            font-size: 17px;
            z-index: 999;
            color: #5ca5f3;
            display: inline-block;
            background: #fff;
            padding: 10px;
            line-height: 1;
        }

        .show-hide-sidebar.hide-btn {
          display: none;
        }

        @media only screen and (min-width: 768px) {
          .show-hide-sidebar {
            display: none;
          }
        }

        /*** edit ***/
        .up-edit-content {
          text-align: right;
          padding-top: 40px;
        }

        .up-edit-content-inner {
          display: inline-block;
          text-align: left;
          width: 100%;
        }

        @media only screen and (min-width: 768px) {
          .up-edit-content-inner {
            width: calc(100% - 210px);
            padding: 0 10px 0 0;
          }
        }

    `]
})

export class ProfileContainerComponent implements OnInit, OnDestroy, DoCheck {

    mobileSidebarOpen = false;
    profileActive = false;
    mySpacesActive = false;
    myActivitiesActive = false;
    routeSubscriptions: Subscription;

    constructor(public router: Router){
        this.routeSubscriptions = this.router.events.subscribe(event => {
            if (event.constructor.name === 'NavigationEnd') {
                this.profileActive = false;
                this.mySpacesActive = false;
                this.myActivitiesActive = false;
                this.activateLinks(event.url);
            }
        });
    }

    ngOnInit(){
        const url = this.router.url;
        this.activateLinks(url);
    }

     ngDoCheck() {
        const url = this.router.url;
        this.activateLinks(url);
     }

    activateLinks(url: string) {
        if (url.indexOf('my-profile') !== -1){
            this.profileActive = true;
            this.mySpacesActive = false;
            this.myActivitiesActive = false;
        }
        else if (url.indexOf('my-spaces') !== -1) {
            this.mySpacesActive = true;
            this.profileActive = false;
            this.myActivitiesActive = false;
        }
        else if (url.indexOf('my-activities') !== -1) {
            this.myActivitiesActive = true;
            this.profileActive = false;
            this.mySpacesActive = false;
        } else {
            this.profileActive = true;
        }
    }

    toggleSideBar(){
        this.mobileSidebarOpen = !this.mobileSidebarOpen;
    }

    goToMyProfile(){
        this.mobileSidebarOpen = false;
        this.profileActive = true;
        this.mySpacesActive = false;
        this.myActivitiesActive = false;
        this.router.navigate(['/user/profile/my-profile']);
    }

    goToMySpaces(){
        this.mobileSidebarOpen = false;
        this.mySpacesActive = true;
        this.profileActive = false;
        this.myActivitiesActive = false;
        this.router.navigate(['/user/profile/my-spaces'])
    }

    goToMyActivities(){
        this.mobileSidebarOpen = false;
        this.myActivitiesActive = true;
        this.profileActive = false;
        this.mySpacesActive = false;
        this.router.navigate(['/user/profile/my-activities'], { queryParams: { time: 'upcoming' } });
    }

    ngOnDestroy() {
        this.routeSubscriptions.unsubscribe();
    }


}
