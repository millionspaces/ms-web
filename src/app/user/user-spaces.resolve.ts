import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { UserService } from "./user.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class UserSpacesResolve implements Resolve<any>{

    constructor(private userService: UserService){}

    resolve(): Observable<any>{
        return this.userService.getUserSpaces();
    }

}