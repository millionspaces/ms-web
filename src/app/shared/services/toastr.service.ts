import { Injectable } from "@angular/core";

declare let toastr: any;

@Injectable()
export class ToasterService{

    success(message: string, title?: string){
        toastr.success(message, title, this.getOptions());
    }
    info(message: string, title?: string){
        toastr.info(message, title, this.getOptions());
    }
    warning(message: string, title?: string){
        toastr.warning(message, title, this.getOptions());
    }
    error(message: string, title?: string){
        toastr.error(message, title, this.getOptions());
    }

    getOptions(){
        return {
            closeButton: true,
            positionClass: 'toast-top-center'
        }
    }

}