import { Injectable } from "@angular/core";

@Injectable()
export class LocalStorageService{

    addItem(key: string, object: any): void {
        localStorage.setItem(key, JSON.stringify(object));
    }

    getItem(key: string): any {
        return JSON.parse(localStorage.getItem(key));
    }

    removeItem(key: string): void {
        localStorage.removeItem(key)
    }

    clear(): void {
        localStorage.clear();
    }

}