import { Injectable } from "@angular/core";

declare let moment: any;

@Injectable()
export class MomentService {

    /**
     * use this for get time duration between two dates
     * date format should be 'YYYY-MM-DDTHH:mm'
     * returns hours
     */
    getDuration(date1: string, date2: string): number {
        let diff_ms = moment(date1).diff(moment(date2));
        let dur_obj = moment.duration(diff_ms);
        return dur_obj.asHours();
    }

    getMoment(): any {
        return moment();
    }
}


