import {Injectable} from "@angular/core";
import { environment } from '../../../environments/environment';

var aws_sdk = require('aws-sdk');


@Injectable()
export class AWSS3Service {

    private accessKeyId = "AKIAIGK7FT7FOUOHLMBQ";
    private secretAccessKey = "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";
    private bucket;
    private imageWindow: string;

    constructor(){
        if(environment.host === 'https://lv.millionspaces.com'){
            this.bucket = 'millionspaces';
        }else{
            this.bucket = 's3eventspace';
        }
    }

    uploadData(params: any, window: string) : Promise<any>{
        this.imageWindow = window;
        return new Promise((resolve, reject) => {
            this.getBucket().upload(params, (err, data) => {
                if (err)
                    reject(err);
                 else
                    resolve(data);
            });
        });
    }

    removeData(params: any) : Promise<any>{
        return new Promise((resolve, reject) => {
            this.getBucket().deleteObject(params, (err, data) => {
                if (err)
                    reject(err);
                else
                    resolve(data);
            });
        });
    }

    getBucket() {
        let AWSService = (<any>window).AWS;
        AWSService.config.accessKeyId = this.accessKeyId;
        AWSService.config.secretAccessKey = this.secretAccessKey;
        return new AWSService.S3({
            params : { Bucket: this.bucket },
            /*endpoint: this.getBucketEndPoint()*/
        });
    }

    /*getBucketEndPoint(){
        if(this.imageWindow === 'thumbnail' || this.imageWindow === 'cover'){
            return 'https://s3-ap-southeast-1.amazonaws.com/space-images';
        }
        else if(this.imageWindow === 'menus'){
            return 'https://s3-ap-southeast-1.amazonaws.com/space-menus';
        }
        else if(this.imageWindow === 'profile'){
            return 'https://s3-ap-southeast-1.amazonaws.com/profile-images';
        }else {
            return null;
        }

    }*/


}
