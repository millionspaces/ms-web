import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { ServerConfigService } from "../../server.config.service";
import { NotFoundError } from "../../common/errors/not-found-error";
import { BadRequestError } from "../../common/errors/bad-request-error";
import { MSError } from "../../common/errors/ms.error";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class DataService {

    private url: string;

    constructor(
        private http: Http,
        private serverConfig: ServerConfigService
    ){}

    getAll(): Observable<any[]> {
       return this.http
           .get(this.url, this.serverConfig.getJsonHeader())
           .map((response: Response) => response.json())
           .catch(this.handleError);
    }

    getOne(id: number): Observable<any> {
        return this.http
            .get(this.url +'/'+ id, this.serverConfig.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    create(resource: any): Observable<any> {
        return this.http
            .post(this.url, JSON.stringify(resource), this.serverConfig.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    update(resource: any, method: string): Observable<any> {
        if(method === "PATCH") {
            return this.http
                .patch(this.url +'/'+ resource.id, JSON.stringify(resource), this.serverConfig.getJsonHeader())
                .map((response: Response) => response.json())
                .catch(this.handleError);
        }
        return this.http
            .put(this.url +'/'+ resource.id, JSON.stringify(resource), this.serverConfig.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    delete(id: number) {
        return this.http
            .delete(this.url +'/'+ id, this.serverConfig.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response): Observable<any> {

        if(error.status === 404)
            return Observable.throw(new NotFoundError());

        if(error.status === 400)
            return Observable.throw(new BadRequestError(error.json()));

        return Observable.throw(new MSError(error))
    }

}