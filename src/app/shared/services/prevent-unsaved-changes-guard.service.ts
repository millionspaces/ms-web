import {Injectable} from "@angular/core";
import {CanDeactivate} from "@angular/router";

export interface FormComponent{
   hasUnsavedChanges() : boolean;
}

@Injectable()
export class PreventUnsavedChangesGuard implements CanDeactivate<FormComponent>{

    canDeactivate(component: FormComponent){
        if(component.hasUnsavedChanges()){
            return confirm('Any unsaved changes will be lost. Continue?');
        }
        return true;
    }

}