import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { UserService } from "../../user/user.service";

@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor(private router: Router, private userService: UserService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if(this.userService.getUser()) {
            this.router.navigate(['/home']);
        }
        return true;

    }

}