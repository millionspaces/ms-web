import {Injectable} from "@angular/core";
import { environment } from '../../../environments/environment';
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {ServerConfigService} from "../../server.config.service";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class NotificationService{

    baseUrl: string;

    constructor(private _http: Http, private _serverConfigService: ServerConfigService){
        this.baseUrl = environment.apiHostNotification+'/notification';
    }

    getUserNotifications(): Observable<string>{
        return this._http
            .get(this.baseUrl, this._serverConfigService.getNotificationHeader())
            .map((response: Response) => <string[]>response.json())
            .catch(this.handleError);
    }

    emitNotification(notification): Observable<any>{
        return this._http.post(this.baseUrl, JSON.stringify(notification), this._serverConfigService.getNotificationHeader());
    }


    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }


}