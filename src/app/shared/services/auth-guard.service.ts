import {Injectable} from "@angular/core";
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from "@angular/router";
import {User} from "../../user/user.model";

@Injectable()
export class AuthGuard implements CanActivate{
    user: User;

    constructor(
        private _router: Router,
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){

        if(localStorage.getItem('ES_USR'))
            return true;

        const returnUrl = decodeURIComponent(state.url);

        this._router.navigate(['user/signin'], { queryParams: { returnUrl: returnUrl }});

        return false;
    }

}