import {Injectable} from "@angular/core";
var CryptoJS = require("crypto-js");

@Injectable()
export class CryptoService{

    constructor(){}

    key: string = "t2s67p1a345o7rzm";

    md5Hex(message){
        return this.toHex(CryptoJS.MD5, message);
    }

    sha1Hex(message){
        return this.toHex(CryptoJS.SHA1, message);
    }

    sha256Hex(message){
        return this.toHex(CryptoJS.SHA256, message);
    }

    sha224Hex(message){
        return this.toHex(CryptoJS.SHA224, message);
    }

    sha512Hex(message){
        return this.toHex(CryptoJS.SHA512, message);
    }

    sha384Hex(message){
        return this.toHex(CryptoJS.SHA384, message);
    }

    sha3Hex(message){
        return this.toHex(CryptoJS.SHA3, message);
    }

    base64(val){
        var wordArray = CryptoJS.enc.Utf8.parse(val);
        return CryptoJS.enc.Base64.stringify(wordArray);
    }

    toHex(hasher, message){
        return hasher(message).toString(CryptoJS.enc.Hex);
    }

    encryptData(data){
        var key = CryptoJS.enc.Latin1.parse('t2s67p1a345o7rzm');
        var iv = CryptoJS.enc.Latin1.parse('abcdefghabcdefgd');
        var encrypted = CryptoJS.AES.encrypt(
                JSON.stringify(data),
                key,
                {iv:iv,mode:CryptoJS.mode.CBC,padding:CryptoJS.pad.ZeroPadding}
            );
        return encrypted.toString();
    }

    packHex(value) {
        var source = value.length % 2 ? value + '0' : value
            ,result = '';

        for( var i = 0; i < source.length; i = i + 2 ) {
            result += String.fromCharCode( parseInt( source.substr( i , 2 ) ,16 ) );
        }

        return result;
    }



}