import { Injectable } from "@angular/core";

@Injectable()
export class SessionStorageService{

    addItem(key: string, object: any): void {
        this.removeItem(key);
        sessionStorage.setItem(key, JSON.stringify(object));
    }

    getItem(key: string): any {
        if (!sessionStorage.getItem(key) || sessionStorage.getItem(key) === 'undefined') return null;
        return JSON.parse(sessionStorage.getItem(key));
    }

    removeItem(key: string): void {
        sessionStorage.removeItem(key)
    }

    clear(): void {
        sessionStorage.clear();
    }

}