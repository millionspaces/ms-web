import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {ServerConfigService} from "../../server.config.service";
import {Observable} from "rxjs/Observable";
import {User} from "../../user/user.model";
import {UserService} from "../../user/user.service";
import {CryptoService} from "./crypto.service";
import {AngularFire} from "angularfire2";
import { environment } from '../../../environments/environment';
import {NotFoundError} from "../../common/errors/not-found-error";
import {BadRequestError} from "../../common/errors/bad-request-error";
import {MSError} from "../../common/errors/ms.error";

//declare var io: any;
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthenticationService{

    private baseUrl;
    private baseUrlUserGrant;
    private baseUrlDeviceLogout;
    private baseUrlEmailExistent;
    private forgetPassword;
    private resetPassword;
    socket: any;

    constructor(
        private http: Http,
        private serverConfigService: ServerConfigService,
        private userService: UserService,
        private crpto: CryptoService,
        private af: AngularFire
    ){
        this.baseUrl = environment.apiHost+'/user';
        this.baseUrlUserGrant = environment.apiHost+'/grant_eventspace_security';
        this.baseUrlDeviceLogout = environment.apiHost+'/device_logout';
        this.baseUrlEmailExistent = environment.apiHost+'/user/emailcheck';
        this.forgetPassword = environment.apiHost+'/forgetPassword';
        this.resetPassword = environment.apiHost+'/reset';
        //this.socket = io(environment.apiHostNotification);
    }

    isEmailAlreadyExist(email):Observable<any>{
        return this.http.post(
            this.baseUrlEmailExistent,'email='+email,
            this.serverConfigService.getLoginHeader()
        ).map(resp => {
            return resp.json();
        });
    }

    login(user: User): Observable<User>{
        return this.http.post(
            this.baseUrlUserGrant,
            'username='+user.email+'&password='+this.crpto.sha256Hex(user.password)
            ,
            this.serverConfigService.getLoginHeader()

        ).map(resp => {
            let user:User = resp.json();
            localStorage.setItem('ES_USR', 'Y');
            this.userService.setLoggedUser(user);
            return user;
        });
    }

    guestLogin(email: string) {
        return this.http.post(
            this.baseUrlUserGrant,
            'username='+email
            ,
            this.serverConfigService.getLoginHeader()

        ).map(resp => {
            let user:User = resp.json();
            localStorage.setItem('ES_USR', 'Y');
            localStorage.setItem('ES_GUEST_USR', 'Y');
            this.userService.setLoggedUser(user);
            return user;
        });
    }

    getLoggedUser(): Observable<User>{
        return this.http.get(
            this.baseUrl,
            this.serverConfigService.getLoginHeader())
            .map(resp => {
                let user:User = resp.json();
                return user;
            });
    }

    logout(): Observable<any>{
        return this.http.get(this.baseUrlDeviceLogout,
            this.serverConfigService.getCookieHeader(),
        ).map(resp => {
            localStorage.removeItem('ES_USR');
            localStorage.removeItem('ES_GUEST_USR');
            this.userService.clearUser();
            this.af.auth.logout();
            if(localStorage.getItem('ES_G_USR'))
                localStorage.removeItem('ES_G_USR');
            if(localStorage.getItem('ES_FB_USR'))
                localStorage.removeItem('ES_FB_USR');

            return resp;
        });
    }

    loginWithGoogle(gToken: string): Observable<User>{
        return this.http.post(this.baseUrlUserGrant,
            'username=&password=&googleToken='+gToken,
            this.serverConfigService.getLoginHeader()
        ).map(resp => {
            let user:User = resp.json();
            localStorage.setItem('ES_USR', 'Y');
            localStorage.setItem('ES_G_USR', 'Y');
            this.userService.setLoggedUser(user);
            return user;
        });
    }

    loginWithFB(fbToken: string): Observable<User>{
        return this.http.post(this.baseUrlUserGrant,
            'username=&password=&facebookToken='+fbToken,
            this.serverConfigService.getLoginHeader()
        ).map(resp => {
            let user:User = resp.json();
            localStorage.setItem('ES_USR', 'Y');
            localStorage.setItem('ES_FB_USR', 'Y');
            this.userService.setLoggedUser(user);
            return user;
        });
    }

    isRegisteredEmail(email: string){
        return this.http
            .get(this.forgetPassword+'?email='+email, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    resetUserPassword(key: string, requestBody: any){
        return this.http
            .post(this.resetPassword+'?key='+key, requestBody, this.serverConfigService.getJsonHeader())
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }




    private handleError(error: Response): Observable<any> {

        if(error.status === 404)
            return Observable.throw(new NotFoundError());

        if(error.status === 400)
            return Observable.throw(new BadRequestError(error.json()));

        return Observable.throw(new MSError(error))
    }



}