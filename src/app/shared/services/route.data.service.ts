import {Injectable} from "@angular/core";
import {Space} from "../../spaces/shared/space.model";

@Injectable()
export class RouteDataService {

    public selectedEvents: any;
    public filtersToBeApplied: any;
    public userSpace: any;
    public coordinatesToBeApplied:any;
    public searchDataRequest: any; // homepage search request data
    public ipgData: any;
    public bookingData: any;

    //payment summary page data
    public paymentSummaryData: any;
    constructor(){}

    clearData(){
        this.selectedEvents = null;
        this.filtersToBeApplied = null;
        this.userSpace = null;
        this.coordinatesToBeApplied=null;
        this.searchDataRequest = null;
        this.ipgData = null;
        this.paymentSummaryData = null;
        this.bookingData = null;
    }
}