

import {PipeTransform, Pipe} from "@angular/core";

@Pipe({
    name: 'dateSort'
})
export class DateSortPipe implements PipeTransform{
    transform(array: Array<any>, args: string){

        if(!array){
            return;
        }

        array.sort((a: any, b: any) => {
            if (new Date(a.create_at) < new Date(b.create_at)) {
                return -1;
            } else if (new Date(a.create_at) > new Date(b.create_at)) {
                return 1;
            } else {
                return 0;
            }
        });
        return array;
    }
}