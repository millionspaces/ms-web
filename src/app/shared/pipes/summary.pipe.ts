import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
    name: 'summary'
})
export class SummaryPipe implements PipeTransform {

    transform(value: string, limit?: number){
        if(!value)
            return null;

        if(value.length > limit){
            let actualLimit = (limit) ? limit : 30;
            return value.substr(0, actualLimit) + '...';
        }else{
            return value;
        }

    }
}