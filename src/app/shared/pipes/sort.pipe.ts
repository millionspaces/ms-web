import {PipeTransform, Pipe} from "@angular/core";

@Pipe({
    name: 'sort'
})
export class SortPipe implements PipeTransform{
    transform(array: Array<any>, currentField: string) {

        if(!array){
            return;
        }
        if(currentField[0] === '-'){
            //descending order
            currentField = currentField.split('-')[1];
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return 1;
                } else if (a[currentField] > b[currentField]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return array;

        }else{
            //ascending order
            array.sort((a: any, b: any) => {
                if (a[currentField] < b[currentField]) {
                    return -1;
                } else if (a[currentField] > b[currentField]) {
                    return 1;
                } else {
                    return 0;
                }
            });
            return array;
        }


    }
}