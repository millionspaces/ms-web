import {PipeTransform, Pipe} from "@angular/core";
declare var moment: any;

@Pipe({
    name: 'formatDate'
})
export class DateFormatPipe implements PipeTransform{
    transform(value: string, args?: string){
        let returnVal: string;

        if(!value)
            return "";

        if (args === 'full'){
            returnVal = new moment(value).format('MMMM Do YYYY, h:mm A');
        }else if(args === '12h'){
            returnVal = this.formatTo12h(value);
        }else if(args === 'weekday'){
            returnVal = this.getWeekday(value);
        }else if(args === 'date'){
            returnVal = new moment(value).format('MMMM Do YYYY');
        }else if(args === 'calendar-view'){
            returnVal = new moment(value).format('Do MMMM YYYY - dddd');
        }else if(args === 'caledar-format'){
            returnVal = new moment(value).format('DD MMMM YYYY');
        }else if(args === 'caledar-format-with-time'){
            returnVal = new moment(value).format('Do MMMM YYYY, h:00 A');
        }else if(args === 'time'){
            returnVal = new moment(value).format('h:mm A');
        }else{
            returnVal = new moment(value).format('MMMM Do YYYY, h:00 A');
        }
        return returnVal;
    }

    formatTo12h(input: string){

        let hours = +input.split(':')[0];
        let minutes = input.split(':')[1];

        let formatted = (hours - ((hours == 0)? -12 : (hours <= 12)? 0 : 12)) + ':' + minutes + (hours < 12 ? "AM" : "PM");

        return formatted
    }

    getWeekday(input){
        let weekday = null;
        switch (input) {
            case 1 : weekday = 'Monday'; break;
            case 2 : weekday = 'Tuesday'; break;
            case 3 : weekday = 'Wednesday'; break;
            case 4 : weekday = 'Thursday'; break;
            case 5 : weekday = 'Friday'; break;
            case 6 : weekday = 'Saturday'; break;
            case 7 : weekday = 'Sunday'; break;
            default:
                weekday = 'Weekday'
        }
        return weekday;
    }
}