
export class MsUtil{

    static zeroPad(num, places): string {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    static replaceWhiteSpaces(text: string, replaceWith: string): string {
        return text.trim().replace(/\s/g, replaceWith);
    }

    static createGroupedArray(arr: any[], chunkSize: number): any[] {
        var groups = [], i;
        for (i = 0; i < arr.length; i += chunkSize) {
            groups.push(arr.slice(i, i + chunkSize));
        }
        return groups;
    }

}