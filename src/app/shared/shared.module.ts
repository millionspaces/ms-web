import { NgModule } from "@angular/core";
import { ShowHideInputComponent } from "./components/show-hide-input.component";
import { SortPipe } from "./pipes/sort.pipe";
import { DateFormatPipe } from "./pipes/date-format.pipe";
import { PreventUnsavedChangesGuard } from "./services/prevent-unsaved-changes-guard.service";
import { SpaceRatingDirectiveComponent } from "./directives/space-rating-directive.component";
import { DateSortPipe } from "./pipes/date-sort.pipe";
import { CommonModule } from "@angular/common";
import { OnlyNumber } from "./directives/onlynumber.directive";
import { SummaryPipe } from "./pipes/summary.pipe";
import { PreloadingComponent } from "./components/preloading.component";
import { ThumbnailPreviewComponent } from "./components/thumbnail-preview.component";
import { SpaceProfileMapComponent } from "../spaces/profile/space-profile-map.component";
import { SpinnerComponent } from "./components/spinner.component";
import {MainPreLoaderComponent} from "./components/main-pre-loader.component";
import {MSModalComponent} from "./directives/ms-modal.component";
import {ForgotPasswordComponent} from "./components/forgotpassword.component";
import {LoggedInGuard} from "./services/loggedin.guard";
import {AlphaNumericDirective} from "./directives/alphanumeric.directive";
import {NumberDirective} from "./directives/number.directive";
import {FilterBlocksPipe} from "../booking/select-date/filter-blocks.pipe";

@NgModule({
    imports:[ CommonModule ],
    declarations: [
        ShowHideInputComponent,
        SortPipe,
        DateFormatPipe,
        DateSortPipe,
        SpaceRatingDirectiveComponent,
        OnlyNumber,
        SummaryPipe,
        PreloadingComponent,
        ThumbnailPreviewComponent,
        SpaceProfileMapComponent,
        SpinnerComponent,
        MainPreLoaderComponent,
        MSModalComponent,
        ForgotPasswordComponent,
        AlphaNumericDirective,
        NumberDirective,
        FilterBlocksPipe
    ],
    exports: [
        ShowHideInputComponent,
        SortPipe,
        DateFormatPipe,
        DateSortPipe,
        SpaceRatingDirectiveComponent,
        OnlyNumber,
        SummaryPipe,
        PreloadingComponent,
        ThumbnailPreviewComponent,
        SpaceProfileMapComponent,
        SpinnerComponent,
        MainPreLoaderComponent,
        MSModalComponent,
        ForgotPasswordComponent,
        AlphaNumericDirective,
        NumberDirective,
        FilterBlocksPipe
    ],
    providers: [PreventUnsavedChangesGuard, LoggedInGuard]
})
export class SharedModule{

}