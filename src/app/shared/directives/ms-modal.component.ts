import { Component, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';
import { MSModalService } from "../services/ms-modal.service";

declare let $: any;

@Component({
    selector: 'modal',
    template: '<ng-content></ng-content>',
    styles: [`
        modal {
          /* modals are hidden by default */
          display: none;
        }
        modal .modal {
          /* modal container fixed across whole screen */
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          /* z-index must be higher than .modal-background */
          z-index: 1000;
          /* enables scrolling for tall modals */
          overflow: auto;
        }
        modal .modal .modal-body {
          padding: 20px;
          background: #fff;
          /* margin exposes part of the modal background */
          margin: 40px;
        }
        modal .modal-background {
          /* modal background fixed across whole screen */
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          /* semi-transparent black  */
          background-color: #000;
          opacity: 0.75;
          /* z-index must be below .modal and above everything else  */
          z-index: 9000;
        }
        body.modal-open {
          /* body overflow is hidden to hide main scrollbar when modal window is open */
          overflow: hidden;
        }
    `]
})

export class MSModalComponent implements OnInit, OnDestroy {
    @Input() id: string;
    private element: any;

    constructor(private modalService: MSModalService, private el: ElementRef) {
        this.element = $(el.nativeElement);
    }

    ngOnInit(): void {
        let modal = this;

        // ensure id attribute exists
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        // move element to bottom of page (just before </body>) so it can be displayed above everything else
        this.element.appendTo('body');

        // close modal on background click
        this.element.on('click', function (e: any) {
            var target = $(e.target);
            if (!target.closest('.modal-body').length) {
                modal.close();
            }
        });

        // add self (this modal instance) to the modal service so it's accessible from controllers
        this.modalService.add(this);
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    // open modal
    open(): void {
        this.element.show();
        $('body').addClass('modal-open');
    }

    // close modal
    close(): void {
        this.element.hide();
        $('body').removeClass('modal-open');
    }
}