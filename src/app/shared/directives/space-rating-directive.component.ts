import {Component, Input} from "@angular/core";

@Component({
    selector: 'space-rating',
    styles: [`
        [class*=" flaticon-"]:after, [class*=" flaticon-"]:before, [class^=flaticon-]:after, [class^=flaticon-]:before{
            margin-left: 0px;
            color: gold;
        }
    `],
    template: `
        <div class="ratings p-t-5 p-b-5">
            <span *ngFor="let x of rateArray; let idx = index;">
                <i class="flaticon" [class.flaticon-star-fill]="(idx + 1) < rate || (idx + 1) == rate" [class.flaticon-star]="(idx + 1) > rate"></i>
            </span>
        </div>
    `
})
export class SpaceRatingDirectiveComponent{

    @Input() rate: number;
    rateArray;

    constructor(){
        this.rateArray = new Array(5);
    }


}

