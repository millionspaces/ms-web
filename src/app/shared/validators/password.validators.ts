import {FormControl, FormGroup} from "@angular/forms";

export class PasswordValidators{

    static shouldHaveMinimumCharacters(control: FormControl){
        const minLength = 8;
        if(control.value == undefined || control.value === '')
            return null;

        if(control.value.length < minLength)
            return { shouldHaveMinimumCharacters : { minLength: minLength }};

        return null;
    }

    static passwordShouldMatch(group: FormGroup){
        var password = group.controls['password'].value;
        var verify = group.controls['verify'].value;

        if(verify != undefined && verify !== password)
            return { passwordShouldMatch : true };

        if(password == ""  || verify == "")
            return null;

        return null;

    }

}
