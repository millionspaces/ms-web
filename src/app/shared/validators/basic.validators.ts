
import {FormControl} from "@angular/forms";
import {AuthenticationService} from "../services/authentication.service";

export class BasicValidators{

    constructor(public _authService: AuthenticationService){

    }

    static email(control: FormControl){
        var regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var valid = regEx.test(control.value);
        return valid ? null : { email: true };
    }

     emailExistent(control: FormControl){
        return new Promise((resolve, reject) => {
            this._authService.isEmailAlreadyExist(control.value).subscribe(val => {
                if(val) resolve({ alreadyExists: true})
                else resolve(null)
            });
        });
    }
}