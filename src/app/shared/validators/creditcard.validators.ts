import {FormControl} from "@angular/forms";

export class CreditCardValidators{

    static validateCard(control: FormControl) : any{
        if(!control.value)
            return null;

        let visa = CreditCardValidators.validateVisaCard(control.value);
        let american = CreditCardValidators.validateAmericanExpressCard(control.value);
        let master = CreditCardValidators.validateMasterCard(control.value);

        if(visa && american && master){
            return { validateCard : { label: 'Not a valid credit card number!' }};
        }
        /*else if(!visa){
            return { validateCard : { visa: true }};
        }
        else if(!american){
            return { validateCard : { american: true }};
        }
        else if(!master){
            return { validateCard : { master: true }};
        }*/

        return null;

    }

    static validateVisaCard(number){
        let  cardRegex = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;

        if(number.match(cardRegex))
            return null;

        return { validateVisaCard : { label: 'Not a valid Visa credit card number!' }};
    }

    static validateAmericanExpressCard(number){
        let  cardRegex = /^(?:3[47][0-9]{13})$/;

        if(number.match(cardRegex))
            return null;

        return { validateAmericanExpressCreditCard : { label: 'Not a valid Amercican Express credit card number!' }};
    }

    static validateMasterCard(number){
        let  cardRegex = /^(?:5[1-5][0-9]{14})$/;

        if(number.match(cardRegex))
            return null;

        return { validateMasterCard : { label: 'Not a valid Mastercard number!' }};
    }

}