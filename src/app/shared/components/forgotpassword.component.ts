import {Component, EventEmitter, Output} from "@angular/core";
import {AuthenticationService} from "../services/authentication.service";
import {Router} from "@angular/router";
declare let $: any;

@Component({
    selector: 'forgotpassword',
    template: `<div class="forgot-password-send-email-wrapper forgot-password-send-email-wrapper-active">
                <h3 class="send-email-heading">Reset your password</h3>
                <p class="reset-pw-tips">Enter the Email address associated with your account, and we will email you a link to reset your password.</p>
                <input class="password-reset user-input" type="email" placeholder="Email" #email/>
                <span class="could-not-find-email" *ngIf="sentEmailApiFetch && !sentResetEmail">We were unable to find any user with the given email address. </span>
                <div class="send-email-btns-wrapper">
                    <a (click)="sendResetLink(email)" >Send Reset Link</a>
                </div>
                <a (click)="hideForgotPasswordWindow()" class="text-account text-link">Back to Login</a>
            </div>
            <div class="forgot-pw-email-sent-success" [class.forgot-pw-email-sent-success-active]="sentResetEmail">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                <p class="forgot-pw-email-sent-title">Password reset link emailed successfully!</p>
                <button class="btn done-btn " (click)="goToHome()">Done</button>
            </div>

            <div class="sign-in-page-overlay"  [class.sign-in-page-overlay-active]="sentResetEmail"></div>`,
    styles: [`
.text-link{
    color:#03a3d3;
}
.text-account{
    text-align: center;
    display: block;
    font-weight: 700;
}

.send-email-heading{
  margin-bottom: 2rem;
  text-align: center;
} 
.login-box{
    font-size: 16px;
    position: relative;
    z-index: 10000;
}
.form-control{
    padding: 12px 10px 12px 50px;
    font-size: 16px;
    color: #5a5a5a;
}
.form-control-icon{
    padding: 14px 12px 8px 6px;
}
.loginBtn {
    box-sizing: border-box;
    position: relative;
    /* width: 13em;  - apply for fixed size */
    margin: 0.2em;
    padding: 0 15px 0 46px;
    border: none;
    text-align: left;
    line-height: 34px;
    white-space: nowrap;
    border-radius: 0.2em;
    font-size: 16px;
    color: #FFF;
}
.loginBtn:before {
    content: "";
    box-sizing: border-box;
    position: absolute;
    top: 0;
    left: 0;
    width: 34px;
    height: 100%;
}
.loginBtn:focus {
    outline: none;
}
.loginBtn:active {
    box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
}

/* Google */
.loginBtn--google {
    /*font-family: "Roboto", Roboto, arial, sans-serif;*/
    background: #DD4B39;
}
.loginBtn--google:before {
    border-right: #BB3F30 1px solid;
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
}
.loginBtn--google:hover,
.loginBtn--google:focus {
    background: #E74B37;
}

span.forgot-pwd {
    font-size: 12px;
}

span.forgot-pwd a:hover{
    color: #00adee;
}

/*forgot password styles*/
.sign-in-page-overlay {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
   z-index: 999;
   background: rgba(0, 0, 0, 0.6);
    background-size: cover;
}
.sign-in-page-overlay-active {
    display: inline-block;
}
/** wrapper class: Reset **/
.forgot-password-reset-window {
    display: none;
    width: 100%;
    height: 100%;
    padding: 20px;
    margin-top: 50px;
    z-index: 1000;
    position: fixed;
    background: #fff;
}
.forgot-password-reset-window-active {
    display: inline-block;
}
@media only screen and (min-width: 768px) {
    .forgot-password-reset-window {
        width: 400px;
        height: 390px;
        top: 50%;
        left: 50%;
        margin-top: -195px;
        margin-left: -230px;
    }
}
.forgot-password-reset-window  input[type=password] {
    width: 100%;
    height: 40px;
    padding: 10px;
    border: 1px solid #e0e0e0;
}
.reset-pw-heading {
    font-size: 26px;
    font-weight: bold;
    margin-top: 0;
    padding: 0px;
    color: #969696;
}
.reset-pw-tips {
    color: #9b9b9b;
    font-weight: 400;
}
.reset-pw-tips > a {
    color: #7b7b7b;
    border-bottom: 1px dotted #7b7b7b;
    text-decoration: none;
    font-style: italic;
}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    font-weight: 300;
    color: #9b9b9b;
}
::-moz-placeholder { /* Firefox 19+ */
    font-weight: 300;
    color: #9b9b9b;
}
:-ms-input-placeholder { /* IE 10+ */
    font-weight: 300;
    color: #9b9b9b;
}
:-moz-placeholder { /* Firefox 18- */
    font-weight: 300;
    color: #9b9b9b;
}
.reset-button {
    text-align: center;
    border: 1px solid #5b92f1;
    color: #ffffff;
    width: 100%;
    display: inline-block;
    margin-top: 20px;
    background: #5b92f1;
    transition: all 0.3s;
}
/** Send email **/
.forgot-password-send-email-wrapper {
    display: none;
    position: fixed;
    width: 100%;
    min-height: 476px;
    padding: 20px;
    margin-top: 50px;
    z-index: 1000;
    background: #fff;
    top: 40px;
    text-align: left;
}
.login-box, .register-box {
    padding-top: 0!important;
}
.forgot-password-send-email-wrapper-active {
    display: inline-block;
}
@media only screen and (min-width: 768px) {
    .forgot-password-send-email-wrapper {
        width: 360px;
        height: 280px;
        top: 30%;
        left: 50%;
        margin-top: -140px;
        margin-left: -230px;
    }
}
.forgot-password-send-email-wrapper input[type=email] {
    width: 100%;
    border: 1px solid #95d5f1;
    height: 38px;
    padding: 6px 12px !important;
    margin-bottom:1rem;
}
.reset-pw-heading {
    font-size: 26px;
    font-weight: bold;
    margin-top: 0;
    padding: 0px;
    color: #969696;
}
.forgot-password-send-email-wrapper h3{
    font-size: 26px;
    font-weight: bold;
    margin-top: 0;
    padding: 0px;
    color: #969696;
}
.send-email-btns-wrapper {
    display: inline-block;
    width: 100%;
    text-align: center;
}
.send-email-btns-wrapper > a {
    text-align: center;
    border: 1px solid #5b92f1;
    color: #ffffff;
    width: 100%;
    display: inline-block;
    margin-top: 20px;
    padding: 10px;
    background: #5b92f1;
    transition: all 0.3s;
    width: 48%;
}
.send-email-btns-wrapper > a:last-child {
    margin-left: 2%;
}
.send-email-btns-wrapper > a:first-child {
    color: #fcf5f4;
    background-color: #29aae3;
    border: 1px solid #34aee4;
    text-transform: uppercase;
      text-align: center;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      font-size: 1.4rem !important;
      width: 100%;
      margin: 10px 0;
      font-weight:500;
}
.forgot-pw-email-sent-success {
    height: 250px;
    width: 400px;
    z-index: 1000;
    position: fixed;
    top: 50%;
    margin-top: -125px;
    left: 50%;
    margin-left: -200px;
    background: #ffffff;
    text-align: center;
    display: none;
}
.forgot-pw-email-sent-success i {
    display: inline-block;
    padding-top: 50px;
    font-size: 50px;
    width: 100%;
    color: #4eb142;
}
.forgot-pw-email-sent-success p {
    padding-bottom: 30px;
    padding-top: 30px;
    font-size: 18px;
    color: #747474;
}
.forgot-pw-email-sent-success-active {
    display: inline-block;
}
.done-btn {
    background: #FFDD57;
    width: 50%;
    font-weight: bold;
}

.could-not-find-email {
    color: #dd6859;
    font-size: 10.5px;
    font-weight: bold;
}`]
})
export class ForgotPasswordComponent {
    sentResetEmail = false;
    sentEmailApiFetch = false;

    @Output('toggleScreen') toggleScreen = new EventEmitter();
    @Output('emailSend') emailToggle = new EventEmitter();

    constructor(private authService: AuthenticationService, private router: Router){}


    hideForgotPasswordWindow(){
        this.toggleScreen.emit(false);
    }

    sendResetLink(email: HTMLInputElement){
        this.sentEmailApiFetch = false;

        if(email.value.trim() === '') return;

        this.authService.isRegisteredEmail(email.value.trim()).subscribe(emailSent => {
            if(emailSent){
                $('.forgot-password-send-email-wrapper').css('display','none');
                this.sentResetEmail = true;
            }else{
                this.sentResetEmail = false;
            }
            this.sentEmailApiFetch = true;
        });

    }

    goToHome() {
        this.sentResetEmail = false;
        $('.sign-in-page-overlay').hide();
        this.router.navigateByUrl('/');
    }
}