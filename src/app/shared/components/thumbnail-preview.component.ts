import {Component, Input, Output, EventEmitter } from "@angular/core";
import {Headers, RequestOptions, Http} from "@angular/http";
import { Cloudinary } from '@cloudinary/angular';
import 'rxjs/add/operator/toPromise';
import {environment} from "../../../environments/environment";

@Component({
    selector: 'thumbnail-preview',
    templateUrl: 'thumbnail-preview.component.html',
    styles: [`
        .img-wrapper {
            display: inline-block;
            position: relative;
            padding-right: 10px;
        }
        .img-wrapper > img {
            width: 150px;
            height: 100px;
        }
        .thumbnail-img-close {
            position: absolute;
            top: 35px;
            right: 20px;
            color: tomato;
            cursor: pointer;
        }
    `]
})
export class ThumbnailPreviewComponent {

    @Input('menuImages') menuImages;
    @Output('removed') removed = new EventEmitter();
    isLoading = false;

    constructor(private cloudinary: Cloudinary, private http: Http){
        console.log('menuImages', this.menuImages);
    }

    deleteImage (data: any) {
        const url = `https://api.cloudinary.com/v1_1/${environment.cloudName}/delete_by_token`;
        let headers = new Headers({ 'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest' });
        let options = new RequestOptions({ headers: headers });
        const body = {
            token: data.delete_token
        };
        this.http.post(url, body, options)
            .toPromise()
            .then((response) => {
                console.log(`Deleted image - ${data.public_id} ${response.json().result}`);
                this.isLoading = false;
                this.removed.emit({ filName: data.public_id, window: data.window })
            }).catch((err: any) => {
            console.log(`Failed to delete image ${data.public_id} ${err}`);
        });
    };



}