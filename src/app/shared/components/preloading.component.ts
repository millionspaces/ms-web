import { Component } from "@angular/core";

@Component({
    selector:'preload',
    template: `
        <div class="space-grid-spinner-wrapper">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
        </div>
    `,
    styles: [`
        .space-grid-spinner-wrapper {
            position: fixed;
            height: 100%;
            width: 100%;
            background: #fff;
            top: 110px;
            right: 0;
            z-index: 9;
        }
         @media only screen and (min-width : 992px) {   
            .space-grid-spinner-wrapper {
                width: 75%;
            }
         }
        .spinner {
            width: 50px;
            height: 40px;
            text-align: center;
            font-size: 10px;
            position: absolute;
            left: 50%;
            top: 40%;
            margin-left: -25px;
            margin-top: -20px;
        }
        .spinner > div {
          background-color: #74aceb;
          height: 100%;
          width: 6px;
          display: inline-block;

          -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
          animation: sk-stretchdelay 1.2s infinite ease-in-out;
        }
        .spinner .rect2 {
          -webkit-animation-delay: -1.1s;
          animation-delay: -1.1s;
        }
        .spinner .rect3 {
          -webkit-animation-delay: -1.0s;
          animation-delay: -1.0s;
        }
        .spinner .rect4 {
          -webkit-animation-delay: -0.9s;
          animation-delay: -0.9s;
        }

        .spinner .rect5 {
          -webkit-animation-delay: -0.8s;
          animation-delay: -0.8s;
        }

        @-webkit-keyframes sk-stretchdelay {
          0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
          20% { -webkit-transform: scaleY(1.0) }
        }

        @keyframes sk-stretchdelay {
          0%, 40%, 100% {
            transform: scaleY(0.4);
            -webkit-transform: scaleY(0.4);
          }  20% {
            transform: scaleY(1.0);
            -webkit-transform: scaleY(1.0);
          }
        }

    `]
})
export class PreloadingComponent{}
