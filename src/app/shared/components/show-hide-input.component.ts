import {Component, ContentChild} from "@angular/core";

@Component({
    selector: 'show-hide-input',
    template: `
        <ng-content></ng-content>
        <span class="form-control-icon pwd-visible eye-password" (click)="toggleShow()"><i class="fa fa-eye fa-icon"></i></span>
    `,
    styles: [`
    .eye-password{
    background: transparent !important;
    }
    .fa-eye:before{
        color: #484848  !important;
        font-size:1rem;
    }
    `]  

})
export class ShowHideInputComponent{

    show = false;

    @ContentChild('password') input;

    toggleShow(){
        this.show = !this.show;
        if (this.show)
            this.input.nativeElement.type = 'text';
        else
            this.input.nativeElement.type = 'password';
    }
}