import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './user/user.service';
import { Subscription } from 'rxjs/Subscription';
import { User } from './user/user.model';
import { AuthenticationService } from './shared/services/authentication.service';

import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/pairwise';
import { RouteDataService } from './shared/services/route.data.service';
import { SocketService } from './socket.service';

declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  user: User;
  usrSubscription: Subscription;
  notificationHost: string;
  loading: boolean = true;

  private _routeScrollPositions: { [url: string]: number }[] = [];
  private _subscriptions: Subscription[] = [];

  constructor(
    private _userService: UserService,
    private _authService: AuthenticationService,
    public router: Router
  ) {
    //this.socket = io(environment.apiHostNotification);
    this.usrSubscription = this._userService
      .getUserObservable()
      .subscribe(user => {
        this.user = user;
        if (user) {
          // this.socket.emit('intiate', user.id);
        } else {
          // this.socket.emit('disconnect');
        }
      });
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });
  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true;
    }
    if (event instanceof NavigationEnd) {
      this.loading = false;
      $('.navbar-collapse')
        .removeClass('collapse in')
        .addClass('collapse');
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false;
    }
    if (event instanceof NavigationError) {
      this.loading = false;
    }

    if ($(window).innerWidth() < 992) {
      $('.menu-area').slideUp();
    }
  }

  ngOnInit() {
    let isLoggedIn = localStorage.getItem('ES_USR');

    if (isLoggedIn === 'Y')
      this._authService.getLoggedUser().subscribe(user => {
        this._userService.setLoggedUser(user);
      });

    if (this.router.url != '/' && this.router.url != '/home') {
      $('nav').addClass('default-navigation-menu');
    }

    $('.mobile-menu-activator').on('click', function() {
      $('.menu-area')
        .stop()
        .slideToggle('fast', function() {
          if ($(this).is(':visible')) $(this).css('display', 'inline-block');
        });
    });

    $(window).resize(function() {
      $('.menu-area').css('display', 'inline-block');
      if ($(window).innerWidth() < 992) {
        $('.menu-area').css('display', 'none');
      }
    });

    $('html').click(function(event) {
      if (
        $(event.target).attr('class') !== 'fa fa-bars' &&
        $(event.target).attr('class') !== 'dropdown-container' &&
        $(event.target).attr('class') !== 'logged-in-show' &&
        $(event.target).attr('class') !== 'image-placeholder' &&
        $(event.target).attr('class') !== 'nav-dropdown' &&
        $('.menu-area').css('display') == 'block'
      ) {
        $('.menu-area').slideUp();
      }
    });

    this._subscriptions.push(
      // save or restore scroll position on route change
      this.router.events
        .pairwise()
        .subscribe(([prevRouteEvent, currRouteEvent]) => {
          if (
            prevRouteEvent instanceof NavigationEnd &&
            currRouteEvent instanceof NavigationStart
          ) {
            this._routeScrollPositions[prevRouteEvent.url] = window.pageYOffset;
          }
          if (currRouteEvent instanceof NavigationEnd) {
            window.scrollTo(
              0,
              this._routeScrollPositions[currRouteEvent.url] || 0
            );
          }
        })
    );
  }

  ngOnDestroy() {
    console.log('call me');
    this.usrSubscription.unsubscribe();
    localStorage.removeItem('ES_USR');
    localStorage.removeItem('ES_GUEST_USR');
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
