import { Component, Input, OnInit } from "@angular/core";
import { Space } from "../shared/space.model";
import { AppConfigService } from "../../app.config.service";
import { MsUtil } from "../../shared/util/ms-util";
import { environment } from "../../../environments/environment";
import { SigninModalComponent } from "../../core/modal/signin-modal.component";
import { UserService } from "../../user/user.service";

declare var $;
declare var google;

@Component({
    selector: 'space',
    styleUrls : ['./space.component.css'],
    templateUrl: './space.component.html'

})
export class SpaceComponent implements OnInit{

    @Input() space: Space;
    @Input() index;
    @Input() allAmenities;
    spaceAmenities = [];
    shortDesc = false;
    appConfigService: AppConfigService;

    // modal data
    modalComponentData = null;

    constructor(
        appConfigService: AppConfigService,
        private userService: UserService
    ){
        this.appConfigService = appConfigService;
    }

    ngOnInit(){

    }

    showSpace(space, event?: any){
        event.stopPropagation();

        const spaceName = MsUtil.replaceWhiteSpaces(space.name, '-');

        if (!this.userService.getUser()) {
            this.showLoginModal(space.id, spaceName);
            return;
        }

        window.open(`#/spaces/${space.id}/${spaceName}`, '_blank');
        //this._router.navigate(['spaces', +space.id, MsUtil.replaceWhiteSpaces(space.name, '-')]);
    }

    changeShortDesc(event?: any){
        event.stopPropagation();
        this.shortDesc = !this.shortDesc;
    }

    getFeaturedImage(space){
        return environment.thumbnailPath+'/'+space.thumbnailImage;
    }

    getDiscountValue(): number {
        return Math.abs(this.space.discount);
    }

    showLoginModal(spaceId, spaceName) {
        this.modalComponentData = {
            component: SigninModalComponent,
            inputs: {
                navigateTo: 'spaces/' + spaceId + '/' + MsUtil.replaceWhiteSpaces(spaceName, '-')
            }
        }
    }


}
