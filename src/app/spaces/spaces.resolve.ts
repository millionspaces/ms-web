import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { SpaceService } from "./shared/space.service";
import { Observable } from "rxjs/Observable";
import { Space } from "./shared/space.model";
import { RouteDataService } from "../shared/services/route.data.service";
import { environment } from '../../environments/environment';

@Injectable()
export class SpacesResolve implements Resolve<any>{

    constructor(
        private _spaceService: SpaceService,
        private _routeDataService: RouteDataService
    ){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<Space []>{

        if(this._routeDataService.filtersToBeApplied){
            let result = this._spaceService.filterSpaces(this._routeDataService.filtersToBeApplied);
            return result;
        }

        if(this._routeDataService.searchDataRequest && !route.params['id']){
            let result = this._spaceService.spaceAdvancedSearch(this._routeDataService.searchDataRequest,
                (this._routeDataService.searchDataRequest.currentPage)? this._routeDataService.searchDataRequest.currentPage: 0);
            return result;
        }

        if(route.params['q']){
            let searchDataRequest = {
                "currentPage": 0,
                "participation": null,
                "budget": null,
                "amenities": null,
                "events": null,
                "isSocialMedia": 0,
                "location": null,
                "available": null,
                "rules": null,
                "seatingArrangements": null,
                "sortBy": null,
                "organizationName": route.params['q']
            }
            return this._spaceService.spaceAdvancedSearch(searchDataRequest, 0);
        }

        if(route.params['eventName'] && route.params['id']){

            let searchDataRequest = {
                "currentPage": 0,
                "participation": null,
                "budget": null,
                "amenities": null,
                "events": [+route.params['id']],
                "isSocialMedia": 1,
                "location": {address: null, latitude: null, longitude: null},
                "available": null,
                "rules": null,
                "seatingArrangements": null,
                "sortBy": null,
                "organizationName": null
            }
            return this._spaceService.spaceAdvancedSearch(searchDataRequest, 0);
        }

        if(route.params['eventId']) {
            let searchDataRequest = {
                "currentPage": 0,
                "participation": null,
                "budget": null,
                "amenities": null,
                "events": [route.params['eventId']],
                "location": {address: null, latitude: null, longitude: null},
                "available": null,
                "rules": null,
                "seatingArrangements": null,
                "sortBy": null,
                "organizationName": null
            }
            return this._spaceService.spaceAdvancedSearch(searchDataRequest, 0);
        }

        let searchDataRequest = {
            "currentPage": 0,
            "participation": null,
            "budget": null,
            "amenities": null,
            "events": null,
            "location": {address: null, latitude: null, longitude: null},
            "available": null,
            "rules": null,
            "seatingArrangements": null,
            "sortBy": null,
            "organizationName": null
        }
        //return this._spaceService.getSpacesList();
        return this._spaceService.spaceAdvancedSearch(searchDataRequest, 0);
    }
}