import {
    Component, AfterViewInit, Input, OnInit, OnDestroy, OnChanges, SimpleChanges,
    ChangeDetectionStrategy, NgZone
} from "@angular/core";
import {GoogleMapService} from "../../google-services-api/shared/google-map.service";
import {Subscription} from "rxjs/Subscription";
import {Router} from "@angular/router";
import {environment} from "environments/environment";

declare var google: any;
declare var $;
declare var InfoBox;

@Component({
    selector: 'map-view-spaces',
    template: `<div id="map-on-spaces" ></div>`,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpacesMapComponent implements AfterViewInit, OnInit, OnDestroy,OnChanges{
    @Input() spaces;
    markersOnMap: any[] = [];
    map: any;
    newMarkers = [];
    mapViewSubscription: Subscription;
    mapStyle = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];
    currentLocation;
    mapOptions;
    bucket: string;

    constructor(
        private _mapService: GoogleMapService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.bucket = environment.bucket;
    }

    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.goToSpace = this.goToSpacePublic.bind(this);

        this.newMarkers = [];
        this.spaces.forEach(space => {
            this.newMarkers.push({
                lat: space.latitude,
                lng: space.longitude,
                title: space.name,
                id: space.id,
                city: space.addressLine1,
                img: space.thumbnailImage,
                price: space.ratePerHour
            });
        })
        this.clearOldMarkers();

        this._mapService.getMarkerHighlightedAsObservable().subscribe(index => {
            if (index || index == 0)
                this.showHighlightedMarker(index);
        })
        this._mapService.getMarkerMarkerLeaveAsObservable().subscribe(index => {
            if (index || index == 0)
                this.showHighlightedMarker2(index);
        })

        this.mapViewSubscription = this._mapService.getMapViewObservableOnMobile().subscribe(
            user => {
                console.log('initializeMap...');
                this.initializeMap();
            }
        )

    }

    initializeMap(){

        this.mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(6.9333082, 79.8430947),
            styles: this.mapStyle
        };
        this.map = new google.maps.Map(document.getElementById('map-on-spaces'), this.mapOptions);

        this.setNewMarkersOnMap();
        setTimeout(() => {
           google.maps.event.trigger(this.map, 'resize');
        }, 1000);
    }

    ngAfterViewInit() {
        this.initializeMap();
    }

    showHighlightedMarker(index) {
        this.markersOnMap[index].setIcon(this.highlightedIcon());
    }

    showHighlightedMarker2(index) {
        this.markersOnMap[index].setIcon(this.normalIcon());
    }

    normalIcon() {
        return {
            url: 'https://millionspaces.com/assets/img/map/32-32-normal.png'
        };
    }

    highlightedIcon() {
        return {
            url: 'https://millionspaces.com/assets/img/map/32-32-active.png'
        };
    }

    ngOnDestroy() {
        this._mapService.clearGeoCodes();
        this.mapViewSubscription.unsubscribe();
        window.my.namespace.goToSpace = null;
    }

    setNewMarkersOnMap() {
        this.markersOnMap = [];

        for (let i = 0; i < this.newMarkers.length; i++) {

            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(this.newMarkers[i].lat, this.newMarkers[i].lng),
                title: this.newMarkers[i].title,
                icon: this.normalIcon(),
            })
            let content = `
                    <style>
                    
@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700');
.map-block{
	max-width:200px;
	height:420px;
	background-color: #EEEEEE;
	color:#000;
	margin-top: -20px;
	box-shadow: 2px 2px 2px #888888;
}

.map-block:before {
    content: " ";
    width: 0;
    height: 0;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-bottom: 10px solid rgb(235, 235, 235);
    position: absolute;
    top: -10px;
    left: 130px;
}
.modal-header{
	padding: 5px;
    border:none;
}
	
.map-block-title{
	font-family: 'Lato', sans-serif;
	font-size:20px;
	font-weight:500;
	margin-top:10px;
	color: #000;
	float:left;
	margin-left: 10%;
}
	
.map-block-price{
	font-family: 'Lato', sans-serif;
	font-size:25px;
	font-weight:900;
	margin-top:10px;
	text-align:center;
	color:#000;
}
	
.map-location-small{
	font-size:13px;
	font-weight:400;
	font-family: 'Lato', sans-serif;
	margin-top:10px;
	margin-left: 10%;
}

.event-price{
	margin-right: 5%;
}
	
.map-block-link>a{
	font-size:11px;
	font-size:11px;
	text-decoration:underline;
	font-family: 'Lato', sans-serif;
	padding-top:15px;
	margin-bottom:10px;
	margin-left:5%;
}
                    </style>
                    <div class="modal-dialog modal-sm">
                        <div class="map-block">
                              <div class="modal-header">
                                 <button type="button" onclick="this.parentNode.parentNode.parentNode.style.display = 'none';" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                              </div>
                              <div class="map-block-img"><img src="${this.bucket}/${this.newMarkers[i].img}" width="200px"></div>
                              <div class="row">
                                 <div class="map-block-title">${this.newMarkers[i].title.length > 16 ? this.newMarkers[i].title.substring(0,16)+'...' : this.newMarkers[i].title}</div>
                              </div>
                              <div class="row">
                                 <span class="map-location-small">${this.newMarkers[i].city.length > 30 ? this.newMarkers[i].city.substring(0,30)+'...' : this.newMarkers[i].city}</span>
                              </div>
                              <div class="map-block-link pul-left"><a onclick="my.namespace.goToSpace(${this.newMarkers[i].id})" target="_blank" >Events Details & Map</a></div>
                              <div class="row event-price pull-right">
                                 <span>from </span><span class="map-block-price">LKR ${this.newMarkers[i].price}</span><span> /hr</span>
                              </div>
                              <div class="clearfix"></div>
                              </div>
                        </div>
                    </div>
                `;

            let windowOptions = {
                content: content,
                maxWidth: 200,
                pixelOffset: new google.maps.Size(-140, 0),
                zIndex: null,
                closeBoxURL : "",
                infoBoxClearance: new google.maps.Size(1, 1),
                isHidden: false,
                pane: "floatPane",
                enableEventPropagation: false
            };
            let ib = new InfoBox(windowOptions);

            marker.setMap(this.map);
            marker.addListener('click', function () {
                ib.open(this.map, marker);
            }.bind(this));

            google.maps.event.addListener(marker, 'mouseover', function () {
                marker.setIcon(this.highlightedIcon());
            }.bind(this));

            google.maps.event.addListener(marker, 'mouseout', function () {
                marker.setIcon(this.normalIcon());
            }.bind(this));

            this.markersOnMap.push(marker);
        }

        let currentPositionMarker: any;
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(position => {
                console.log(position);
                this.mapOptions.center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                currentPositionMarker  = new google.maps.Marker({
                    position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    icon: 'https://stg.millionspaces.com/assets/img/map//marker.png',
                })
                currentPositionMarker.setMap(this.map);
                this.markersOnMap.push(currentPositionMarker);
            });
        }


        this._mapService.setSpacesMap(this.map);


    }


    clearOldMarkers() {
        for (let marker of this.markersOnMap)
            marker.setMap(null);
        this.markersOnMap = [];
    }

    ngOnChanges(changes: SimpleChanges) {
        this.newMarkers = [];
        this.spaces.forEach(space => {
            this.newMarkers.push({
                lat: space.latitude,
                lng: space.longitude,
                title: space.name,
                id: space.id,
                city: space.addressLine1,
                img: space.thumbnailImage,
                price: space.ratePerHour
            });
        })
        this.clearOldMarkers();
        this.setNewMarkersOnMap();
    }

    goToSpacePublic(id){
        this.ngZone.run(() => this.goToSpacePrivate(id));
    }

    goToSpacePrivate(id){
        this.router.navigate(['spaces/show',id]);
    }
}