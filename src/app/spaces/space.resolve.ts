import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {SpaceService} from "./shared/space.service";
import {Observable} from "rxjs/Observable";
import {Space} from "./shared/space.model";

@Injectable()
export class SpaceResolve implements Resolve<any>{

    constructor(
        private spaceService: SpaceService,
    ){}

    resolve(route: ActivatedRouteSnapshot) : Observable<Space>{
        return this.spaceService.getSpace(+route.params['id']);
    }


}