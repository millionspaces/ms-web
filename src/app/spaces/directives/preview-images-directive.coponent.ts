import {Component, Input, Output, EventEmitter} from "@angular/core";
import {AWSS3Service} from "../../shared/services/aws-s3.service";
import {Headers, RequestOptions, Http} from "@angular/http";
import { Cloudinary } from '@cloudinary/angular';
import 'rxjs/add/operator/toPromise';


@Component({
    selector: 'preview-image',
    styles: [`
        .space-image-box-thumbnail{
          height: 400px;
          width: 280px;
          background-size: cover;
          overflow: hidden;
        }
        .space-image-box-cover{
          height: 200px;
          width: 480px;
          background-size: cover;
          overflow: hidden;
          background-size: contain;
          background-position: center;
          background-repeat: no-repeat;
        }
        .space-image-box-cover img{
          width: auto;
          height: auto;
          display: block;
          max-height: 200px;
          max-width: 480px;
        }
        .delete-photo {
          position: absolute;
          z-index: 1001;
          top: 10px;
          right: 10px;
          padding: 5px 9px;
          border: 1px solid rgba(0, 0, 0, 0.2);
          background-color: #fff;
          border-radius: 50%;
          color: tomato;
          display: none;
        }
        .delete-photo i:before {
          margin: 0;
          font-size: 20px;
        }
        .delete-photo:hover {
          background-color: #ccc;
        }
        .space-image-box-thumbnail:hover .delete-photo{
            display: block;
        }
        .space-image-box-cover:hover .delete-photo{
            display: block;
        }

    `],
    template: `
        <div class="col-xs-12 col-sm-8 m-b-10 p-x-0 jbox-img space-image-box-cover"
                *ngIf="imageData?.url" [style.backgroundImage]="'url('+imageData?.url+')'" style="background-size: cover">

            <!--<img [src]="imageData?.response.Location" alt="your image">-->
            <a class="delete-photo" (click)="deleteImageFromCloud()"><i class="fa fa-times"></i></a>
        </div>
        <div *ngIf="isLoading">
            <main-pre-loader></main-pre-loader>
        </div>
    `
})
export class PreviewImagesDirectiveComponent{
    @Input() imageData;
    @Output() removed = new EventEmitter();
    isLoading = false;

    constructor(private awsService: AWSS3Service, private cloudinary: Cloudinary, private http: Http){

    }

    deleteImageFromCloud(){
        this.isLoading = true;

        this.deleteImage(this.imageData)


        /*this.awsService.removeData({
            Key: this.imageData.response.Key,
        }).
        then(
            res => {
                this.imageData.response.Location = null;
                this.removed.emit({filName: this.imageData.response.Key, window: this.imageData.window})
                this.isLoading = false;
                console.log("DELETE SUCCESS");
            })*/


    }


    deleteImage (data: any) {
        const url = `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/delete_by_token`;
        let headers = new Headers({ 'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest' });
        let options = new RequestOptions({ headers: headers });
        const body = {
            token: data.delete_token
        };
        this.http.post(url, body, options)
            .toPromise()
            .then((response) => {
                console.log(`Deleted image - ${data.public_id} ${response.json().result}`);
                this.isLoading = false;
                this.removed.emit({ filName: data.public_id, window: data.window })
            }).catch((err: any) => {
            console.log(`Failed to delete image ${data.public_id} ${err}`);
        });
    };

}
