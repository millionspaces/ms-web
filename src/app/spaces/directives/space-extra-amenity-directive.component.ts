import {Component, Input, OnInit, Output, EventEmitter} from "@angular/core";
import {AmenitiesService} from "../../amenities/shared/amenities.service";

@Component({
    selector: 'extra-amenity',
    templateUrl: './space-extra-amenity-directive.html'
})
export class SpaceExtraAmenityDirectiveComponent implements OnInit{
    @Input() amenities;
    @Input() extraAmenity;
    @Output() save = new EventEmitter();
    @Output() remove = new EventEmitter();

    extraRate: number;

    selectedAmenity;

    selectAmenity(amenity){
        this.selectedAmenity = amenity;
    }

    ngOnInit(){
        this.selectedAmenity = this.amenities[0];
    }

    saveExtraAmenity(){
        if(this.extraRate){
            this.extraAmenity.saved = true;
            this.extraAmenity.name = this.selectedAmenity.name;
            this.extraAmenity.id = this.selectedAmenity.id;
            this.extraAmenity.icon = this.selectedAmenity.icon;
            this.extraAmenity.rate = this.extraRate;
            this.extraAmenity.unit = this.selectedAmenity.amenityUnitsDto.name;
            //this.extraAmenities.push(this.extraAmenity);
            this.save.emit({extraAmenity: this.extraAmenity})
        }
        console.log(this.extraAmenity);
    }

    _keyPress(event: any) {
        const pattern = /[0-9\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    updateExtraAmenity(){
        this.extraAmenity.saved = false;
        this.extraAmenity.updated = true;
    }

    removeExtraAmenity(extraAmenity){
        this.remove.emit({extraAmenity: extraAmenity})
    }

}

export class ExtraAmenity{
    id: number;
    name: string;
    rate: number;
    icon: string;
    rowCount: number;

}