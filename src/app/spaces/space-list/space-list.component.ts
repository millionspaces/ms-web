import { Component, Input, OnInit, AfterViewInit, OnChanges, Output, EventEmitter, SimpleChanges } from "@angular/core";
import { PagerService } from "../shared/pager.service";
import { SpaceService } from "../shared/space.service";

declare let $;

@Component({
  selector: 'event-spaces',
  template: `
    <div class="spaces-blocks-wrapper" [class.no-results]="spaces.length === 0">
        <div class="space-blocks-landingpage-banner" *ngIf="searchDataRequest.isSocialMedia && searchDataRequest.isSocialMedia === 1">
          <div class="space-blocks-landingpage-banner-inner space-blocks-landing-banner-party" *ngIf="searchDataRequest.events[0] === 1"><img src="https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-party.jpg"/></div>
          <div class="space-blocks-landingpage-banner-inner space-blocks-landing-banner-meeting" *ngIf="searchDataRequest.events[0] === 4"><img src="https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-meeting.jpg"/></div>
          <div class="space-blocks-landingpage-banner-inner space-blocks-landing-banner-photoshoot" *ngIf="searchDataRequest.events[0] === 7"><img src="https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-photoshoot.jpg"/></div>
          <div class="space-blocks-landingpage-banner-inner space-blocks-landing-banner-wedding" *ngIf="searchDataRequest.events[0] === 2"><img src="https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-wedding.jpg"/></div>
          <div class="space-blocks-landingpage-banner-inner space-blocks-landing-banner-sports" *ngIf="searchDataRequest.events[0] === 9"><img src="https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-sports.jpg"/></div>
        </div>
        <div class="spaces-blocks-wrapper-inner spaces-blocks-wrapper-inner-landing-page">
           <div *ngIf="isLoading"><preload></preload></div>
           <div *ngIf="spaces.length > 0">
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-4" *ngFor="let space of spaces; let i=index;">
                      <space [space]="space" [allAmenities]="allAmenities" [index]="i" ></space>
                  </div>
              </div>
              <footer class="spaces-list-footer">
                <div class="row">
                    <div class="col-md-4 hide-on-mobile">
                        <p class="copy-rights-info">All rights reserved &copy; <a href="">MillionSpaces.com</a></p>
                    </div>
                    <div class="col-md-8">
                        <ul *ngIf="pager.pages && pager.pages.length" class="pagination">
                            <li [ngClass]="{disabled:pager?.currentPage === 1}">
                                <a (click)="setPage(1)"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li [ngClass]="{disabled:pager?.currentPage === 1}">
                                <a (click)="setPage(pager?.currentPage - 1)"><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li *ngFor="let page of pager?.pages" [ngClass]="{active:pager.currentPage === page}">
                                <a (click)="setPage(page)">{{page}}</a>
                            </li>
                            <li [ngClass]="{disabled:pager?.currentPage === pager.totalPages}">
                                <a (click)="setPage(pager?.currentPage + 1)"><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li [ngClass]="{disabled:pager?.currentPage === pager.totalPages}">
                                <a (click)="setPage(pager.totalPages)"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                         </ul>
                    </div>
                </div>
            </footer>
            </div>
           <div *ngIf="spaces.length === 0">
              <div class="no-results-wrapper">
                <h2>We couldn't find any spaces</h2>
              </div>
           </div>
        </div>
    </div>

  `,
  styles: [`
       .space-blocks-landingpage-banner-inner img {
            width: 100%;
       }
       .space-blocks-landing-banner-party {
            background: url('https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-party.jpg');
            background-size: cover;            
       }
       .space-blocks-landing-banner-meeting {
            background: url('https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-meeting.jpg');
            background-size: cover;            
       }
       .space-blocks-landing-banner-photoshoot {
            background: url('https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-photoshoot.jpg');
            background-size: cover;            
       }
       .space-blocks-landing-banner-wedding {
            background: url('https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-wedding.jpg');
            background-size: cover;            
       }
       .space-blocks-landing-banner-sports {
            background: url('https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/banners/m-sports.jpg');
            background-size: cover;            
       }
      .space-blocks-landingpage-banner{
        text-align: right;
        display: inline-block;
        width: 100%;
      }
      @media only screen and (min-width : 992px) {
        .space-blocks-landingpage-banner{
          text-align: right;
          display: inline-block;
          width: 75%;
        }
      }
        
     .spaces-blocks-wrapper {
        width: 100%;
        text-align: right;
        margin-top: 110px;
        margin-bottom: 100px;
        background: #f7f7f7;
     }
     @media only screen and (min-width: 992px) {
        .spaces-blocks-wrapper {
            margin-top: 110px;
        }
     }
     .spaces-list-footer {
        position: fixed;
        bottom: 0;
        right: 0;
        width: 100%;
        background: #fff;
        padding: 10px;
        text-align: center;
     }
     .pagination {
        margin: 0;
        border-radius: 0;
        display: flex;
        overflow: auto;
        justify-content: center;
     }
     @media only screen and (min-width: 992px) {
      .pagination {
        display: inline-block;
      }
     }
     .pagination > li {
        display: inline-block;
        margin: 0;
     }
     .pagination > li:first-child > a, .pagination > li:first-child > span{
        border-radius: 0;
     }
     .pagination > li:last-child > a, .pagination > li:last-child > span {
        border-radius: 0;
     }
     .pagination > .active > a {
        background: #74aceb;
        border: 1px solid #74aceb;
        color: #fff;
     }
     .pagination > .active > a:hover {
        color: #fff!important;
     }
     .pagination-wrapper {
        padding: 0;
        margin: 0;
        list-style: none;
        text-align: center;
        padding-top: 10px;
     }
     .pagination-wrapper > li {
        display: inline-block;
        padding: 10px;
        border: 1px solid #e2e2e2;
     }
     .copy-rights-info {
        padding-top: 10px;
        text-align: left;
     }
     .copy-rights-info:hover > a {
        padding-top: 10px;
        text-align: left;
        color: #5b92f1;
     }
     .hide-on-mobile {
        display: none;
     }
     .no-results-wrapper {
        text-align: center;
        margin-top: 100px;
     }
     .no-padding-on-mobile {
        padding: 0!important;
     }
     .spaces-blocks-wrapper-inner {
       padding-top: 60px;
     }
     .spaces-blocks-wrapper-inner.spaces-blocks-wrapper-inner-landing-page {
       padding-top: 0;
       margin-top: 165px;
     }
      
     @media only screen and (min-width : 992px) {
        .no-padding-on-mobile {
            padding: 0px 15px!important;
        }
        .spaces-blocks-wrapper-inner.spaces-blocks-wrapper-inner-landing-page {
          padding-top: 0;
          margin-top: 0;
        }
        .spaces-blocks-wrapper-inner {
            width: 74.5%;
            display: inline-block;
            padding-top: 0;
        }
        .spaces-list-footer {
            width: 75%;
        }
        .hide-on-mobile {
            display: block;
        }
        .spaces-list-footer {
            text-align: right;
        }
     }
     .spaces-blocks-wrapper.no-results {
        background: #fff;
     }

  `],
})

export class SpaceListComponent implements OnInit, AfterViewInit, OnChanges{
  @Input('spaces') spaces;
  @Input('spaceCount') spaceCount;
  @Input('allAmenities') allAmenities;
  @Input('request') searchDataRequest;
  @Input('currentPage') currentPage;
  @Input('isLoading') isLoading;
  @Input('sortBy') sortBy;

  @Output('pageStartEnd') pageStartEnd = new EventEmitter();
  @Output('pagedSpaces') pagedSpaces = new EventEmitter();

  spacesPerPage = 18;

  pager: any = {};

  constructor(private pagerService: PagerService, private spaceService: SpaceService){}

  ngOnInit(){
    let currentPage = 1;
    console.log('this.searchDataRequest', this.searchDataRequest);
    if(this.searchDataRequest){
      currentPage = this.searchDataRequest.currentPage + 1;
    }
    this.pager = this.pagerService.getPager(this.spaceCount, currentPage);
    let pageStart = 1;
    let pageEnd = null;

      let page = currentPage;
      if(page * this.spacesPerPage < this.spaceCount){
        pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
        pageEnd = page * this.spacesPerPage;
      }else{
        pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
        pageEnd = this.spaceCount;
      }

   /* else{
      if(this.spacesPerPage > this.spaceCount){
        pageEnd = this.spaceCount;
      }else{
        pageEnd = this.spacesPerPage;
      }
    }*/


    this.pageStartEnd.emit({start: pageStart, end: pageEnd, currentPage: page - 1});
  }

  ngAfterViewInit () {
    $('.spaces-blocks-wrapper').on('scroll', function () {
      $('.dropdown-menu').hide();
    });
  }

  ngOnChanges(changes: SimpleChanges){
      let pageStart = 1;
      let pageEnd = null;
      let spaceCountChng = changes['spaceCount'];
      let sortedValue = changes['sortBy'];

      if(spaceCountChng){
        let cur  = +spaceCountChng.currentValue;
        let prev = +spaceCountChng.previousValue;

        if((!isNaN(prev) && cur !== prev)){
          let page = 1;
          if(page * this.spacesPerPage < this.spaceCount){
            pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
            pageEnd = page * this.spacesPerPage;
          }else{
            pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
            pageEnd = this.spaceCount;
          }
          this.pager = this.pagerService.getPager(this.spaceCount, page);
          this.pageStartEnd.emit({start: pageStart, end: pageEnd, currentPage: (page - 1)});
        }
      }
      else if(sortedValue){
        let page = 1;
        if(page * this.spacesPerPage < this.spaceCount){
          pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
          pageEnd = page * this.spacesPerPage;
        }else{
          pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
          pageEnd = this.spaceCount;
        }
        this.pager = this.pagerService.getPager(this.spaceCount, page);
        this.pageStartEnd.emit({start: pageStart, end: pageEnd, currentPage: (page - 1)});
      }
      
    window.scrollTo(0, 0);
  }

  setPage(page: number) {

    //update browser url
    //this.location.go('/spaces/list/page/'+page);

    let pageStart = 1;
    let pageEnd = null;

    if (page < 1 || page > this.pager.totalPages)
      return;

    if(page * this.spacesPerPage < this.spaceCount){
      pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
      pageEnd = page * this.spacesPerPage;
    }else{
      pageStart = ((page * this.spacesPerPage) - (this.spacesPerPage) + 1);
      pageEnd = this.spaceCount;
    }

    this.pager = this.pagerService.getPager(this.spaceCount, page);

    this.pageStartEnd.emit({start: pageStart, end: pageEnd, currentPage: (page - 1)});
    this.getPagedSpaces(page - 1)
  }

  //get paginated spaces
  getPagedSpaces(pageId){
    this.isLoading = true;
    this.currentPage = pageId;
    this.searchOnFilters(this.searchDataRequest, pageId);
  }

  searchOnFilters(filterRequest: any, pageId: number){
    this.spaceService.spaceAdvancedSearch(filterRequest, pageId).subscribe(pagedSpaces => {
      this.pagedSpaces.emit({allCount: pagedSpaces.count, filteredSpaces: pagedSpaces.spaces})
      this.isLoading = false;
    })
  }




}
