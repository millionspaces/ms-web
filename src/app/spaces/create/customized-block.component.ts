import {
    Component, AfterViewInit, Input, Output, EventEmitter,  DoCheck
} from "@angular/core";

declare let $;

@Component({
    selector: 'custom-block',
    styles: [`
        .input-group {
            background: #fff;
        }
        .custom-block-space-count-zero-validation {
            border: 1px solid red!important;
        }
    `],
    templateUrl: './customized-block.component.html'
})
export class CustomizedBlockComponent implements AfterViewInit, DoCheck {

    @Input() index;
    @Input() block;
    @Input() menus;
    @Input() chargeType;
    @Input() minGuest;
    @Input() blockList;
    selectedMenu;

    @Output() removeBlock = new EventEmitter();

    dayRange = [
        {value:8, display: 'Weekday'},
        {value:6, display: 'Saturday'},
        {value:7, display: 'Sunday'}
        ]

    setStartTime(event){
        let fromTime = event.target.value;
        this.block.from = fromTime;

        this.validFromAndEndTime();

        if(this.chargeType == 1) {
            this.uniqueTimeRange();
        }

    }

    setEndTime(event){
        let endTime = event.target.value;
        this.block.to = endTime;

        this.validFromAndEndTime();

        if(this.chargeType == 1) {
            this.uniqueTimeRange();
        }
    }

    validFromAndEndTime() {
        this.block.valid = true;

        let from = +this.block.from.split(':')[0]
        let end = +this.block.to.split(':')[0]

        if(from >= end){
            this.block.valid = false;
        }
    }

    uniqueTimeRange() {

        let boolArray = this.blockList.map((block, index) => {
            if(index === this.index) return;

            if(block.from == this.block.from && block.to == this.block.to && block.day.value == this.block.day.value) {
                return true;
            } else {
                return false;
            }
        })

        if(boolArray.length > 1) {
            if(boolArray.some(value => value)) {
                this.block.valid = false;
            }
        }
    }

    uniquePriceRange() {
        if(this.chargeType == 1) {
            return true;
        }
        const boolArray = this.blockList.map((block, index) => {

            if(index === this.index) return;

            if(block.minPriceGuest == this.block.minPriceGuest && block.minPriceGuest == this.block.minPriceGuest) {
                return true;
            } else {
                return false;
            }
        })

        if(boolArray.length > 1) {
            if(boolArray.some(value => value)) {
                return false;
            }
        }
        return true;
    }

    // MS-672
    identicalBlocks() {
        if(this.chargeType == 1) {
            return true;
        }
        const boolArray = this.blockList.map((block, index) => {

            if(index === this.index) return;

            if(block.minPriceGuest == this.block.minPriceGuest && block.to == this.block.to && block.from == this.block.from) {
                return true;
            } else {
                return false;
            }
        })

        if(boolArray.length > 1) {
            if(boolArray.some(value => value)) {
                return false;
            }
        }
        return true;
    }

    changeDay(selectedDay){
        this.block.day = selectedDay;
    }

    disableProperties(e){
        e.preventDefault();
        return false;
    }

    ngAfterViewInit(){
        $('#startTimeBlock'+this.index).timepicker({
            'step': 60 ,
            'timeFormat': 'H:i:s'
        });
        $('#endTimeBlock'+this.index).timepicker({
            'step': 60 ,
            'timeFormat': 'H:i:s'
        });

        $('.pricing-availability-wrapper').parent().find('.form-control').css ({
            zIndex: 99,
            background: 'rgba(0, 0, 0, 0)'
        })

        $('input[name=bufferTime]').css({
            background: '#fff'
        });

        /*$('input[name=reimbursableBill]').change(function() {
            var ischecked= $(this).is(':checked');
            if(ischecked){
                $('input[name=reimbursableBill]').prop('checked', true);
            }else{
                $('input[name=reimbursableBill]').prop('checked', false);
            }
        });*/
    }

    // The life cycle of ui wrapper initialization can not be traced. Hence ngDoCheck was used.
    ngDoCheck () {
        $('.ui-timepicker-wrapper').css('width', '179px');
    }

    removeSelectedBlock(index){
        this.removeBlock.emit(index);
    }

    selectMenu(menu, idx){
        this.selectedMenu = menu;
        this.block.menuImage = {menuId: idx + 1, url: menu.public_id.split('/')[1]};
    }
}
