import {Component, OnInit, AfterViewInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {environment} from "../../../environments/environment";

declare let $: any;

@Component({
    templateUrl: './space-listing-success.component.html',
})

export class SpaceListingSuccessComponent implements OnInit, AfterViewInit{

    firstCoverImage = null;
    env;

    constructor(private _route: ActivatedRoute){
        this.env = environment;
    }

    ngOnInit(){
        document.getElementsByTagName('body')[0].className = '';

        this._route.params.subscribe(params => {
            this.firstCoverImage = params['firstCover']
        })
    }

    ngAfterViewInit () {
      $('.create-help-body').parentsUntil('html').css ('height', '100%');
      $('html').css ('height', '100%');
    }

}
