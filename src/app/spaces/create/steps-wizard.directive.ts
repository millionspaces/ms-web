import { Component, Input } from "@angular/core";

@Component({
    selector: 'steps-wizard',
    styles: [`
        /*top steps*/
        .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #999; font-size: 14px; margin-bottom: 3px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 20px; height: 20px; display: block; background: #caefff; top: 29px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 10px; height: 10px; background: #03a3d3; border-radius: 50px; position: absolute; top: 5px; left: 5px; } 
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none;}
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
        .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        .steps-bar-wrapper { text-align: center; }
        .steps-bar-item { display: inline-block; width: 14.28%; vertical-align: top; }
        .bs-wizard-step > a { cursor: default; }
        .progress-bar{ background: #caefff; }
        .progress-bar-steps-number {
            position: relative;
            left: -5px;
        }
        /*top stepsd*/
    `],
    template: `
        <!--step wizard-->
        <div class="row bs-wizard steps-bar-wrapper" style="border-bottom:0;">
            <div class="steps-bar-item bs-wizard-step" [ngClass]="steps[0]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
               <div class="bs-wizard-info text-center hidden-xs">Host Details</div>
               
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step2-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[1]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center hidden-xs">Basic Space Information</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step3-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[2]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center hidden-xs">Space Details</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step4-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[3]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center hidden-xs">Pricing & Availability</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step5-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[4]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
               <div class="bs-wizard-info text-center hidden-xs">Preferences</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step6-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[5]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center hidden-xs">Fee Structure</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step7-ico.png" alt=""></div>
            </div
            ><div class="steps-bar-item bs-wizard-step" [ngClass]="steps[6]">
                <div class="progress"><div class="progress-bar"></div></div>
                <a class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center hidden-xs">Bank Account Details</div>
                <div class="bs-wizard-info text-center visible-xs"><img class="process-steps-icon" src="../../../assets/img/listing/step8-ico.png" alt=""></div>
            </div>
        </div>
    `
})
export class StepsWizard {
    @Input() steps;




}