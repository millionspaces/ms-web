import {Component, OnInit, AfterViewInit, OnDestroy} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../user/user.model";
import {UserService} from "../../user/user.service";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BasicValidators} from "../../shared/validators/basic.validators";
import {PasswordValidators} from "../../shared/validators/password.validators";
import {CryptoService} from "../../shared/services/crypto.service";
import {SigninModalComponent} from "../../core/modal/signin-modal.component";

declare var $;

@Component({
    templateUrl :'./new-space-home.component.html',
    styles: [`
        .login-box{
            font-size: 16px;
        }
        .form-control{
            padding: 12px 10px 12px 50px;
            font-size: 16px;
            color: #5a5a5a;
        }
        .form-control-icon{
            padding: 14px 12px 8px 6px;
        }
        .about-box-right {
             font-size: 15px;
             line-height: 25px;
             color: #fff;
             padding: 60px 37px 27px 26px;
         }
         .about-box-left {
             font-size: 15px;
             line-height: 25px;
             color: #fff;
             padding: 60px 34px 7px 40px;
         }
         .about-title{
             font-weight: 700;
             color: #2eade3;
         }
         .list-home-row-1 .become-host-section {
             height: 400px;
         }
         @media (min-width: 768px){
            .list-home-row-1 .become-host-section{
                height: 650px;
                background-image: url(../../../assets/img/listing/about-m.jpg);
                background-size: cover;
                background-position: 70% 100%;
                background-repeat: no-repeat;
                padding-top: 100px;
                padding-left: 50px;
            }
        }

        .about-container{
             padding: 50px;
             font-size: 22px;
             line-height: 1.38;
             text-align: center;
        }


         .about-box-left ul li{
             padding-bottom: 15px;
             font-size: 20px;
         }
        
         .about-box-right ul li{
             padding-bottom: 15px;
             font-size: 20px;
         }
        
         .about-img-left{
             min-height: 510px;
             background-image:url(../../../assets/img/listing/about-m2.jpg);
             background-size:cover;
             background-repeat: no-repeat;
             background-position: center center;
         }
        
         .about-img-right{
             min-height: 510px;
             background-image:url(../../../assets/img/listing/about-m3.jpg);
             background-size:cover;
             background-repeat: no-repeat;
             background-position: center center;
         }
        
         .about-sub-title{
             font-size: 20px;
         }
        
         .about-box-title{
             padding-bottom: 20px;
             font-size: 38px;
             font-weight: 700;
         }
        
         /* Info Bar */
         .info-bar {
             margin:0;
             border:0;
             color:#000;
             background-color:rgba(0,0,0,0.05);
             padding:10px 0;
             padding-left: 20px;
         }
         @media only screen and (min-width: 992px) {
            padding-left: 10px;
         }
         .info-bar div.row>div {
             padding-top:20px;
             padding-bottom:20px;
             margin:0 !important;
             border-right:rgba(0,0,0,0.1) 1px solid;
         }
         .info-bar div.row>div:last-child {
             border:0;
         }
         .info-bar div.row>div i {
             color:#333;
             font-size:32px;
             line-height: 1.2;
             margin-right:10px;
             float:left;
         }
         .info-bar h2,
         .info-bar h4,
         .info-bar h5,
         .info-bar h6
         {
             color:#333;
             font-size:16px;
             line-height:1.5;
             margin:0;
             padding:0;
         }
        
         .info-bar h1{
             color:#333;
             font-size:32px;
             line-height:1.5;
             margin:0;
             padding:0;
             font-weight: bold;
         }
         @media only screen and (min-width: 992px) {
             .info-bar h1{
               font-size: 42px;
            }
         }
        
         .info-bar h3{
             color:#333;
             font-size:16px;
             line-height:1.5;
             margin:0;
             padding:0;
             font-weight: bold;
         }
         .info-bar p {
             font-size: 14px;
             line-height:1;
         }

         /* ------ responsive --------- */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .about-box-left {
                font-size: 15px;
                line-height: 25px;
                color: #fff;
                padding: 60px 34px 7px 40px;
            }
n
            .about-box-right{
                font-size: 15px;
                line-height: 25px;
                color: #fff;
                padding: 60px 37px 27px 26px;
            }

            a.advantage-link-btn {
                background-color: #ffce4b;
                font-weight: 500;
                text-align: center;
                color: #262629;
                height: 50px;
                line-height: 50px;
                width: 100%;
                font-size: 19px;
                display: inline-block;
                margin-bottom: 40px;
            }
        }

        @media only screen and (min-width: 568px) {
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 60px 34px 7px 40px;
             }
        
              .about-box-right{
                     font-size: 15px;
                     line-height: 25px;
                     color: #fff;
                     padding: 60px 37px 27px 26px;
                 }
        
              a.advantage-link-btn {
                     background-color: #ffce4b;
                     font-weight: 500;
                     text-align: center;
                     color: #262629;
                     height: 50px;
                     line-height: 50px;
                     width: 300px;
                     font-size: 19px;
                     display: inline-block;
                     margin-left:40px;
              }

        /* ------ END (min-width: 568px) --------- */
        
        
         @media only screen and (min-width: 769px) {
        
        
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 50px 20px 20px 20px ;
             }
        
        
             .about-box-right{
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 60px 34px 18px 10px;
             }
        
        
             a.advantage-link-btn {
                 background-color: #ffce4b;
                 font-weight: 500;
                 text-align: center;
                 color: #262629;
                 height: 50px;
                 line-height: 50px;
                 width: 300px;
                 margin-left: 90px;
                 font-size: 19px;
                 display: inline-block;
        
         }
        
         } /* ------ END (min-width: 768px) --------- */
        
        
        
         @media only screen and (min-width: 992px) {
        
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 70px 80px 16px 85px;
             }
        
             .about-box-right{
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 70px 54px 38px 50px;
             }
        
         } /* ------ END (min-width: 992px) --------- */
        
        
         @media only screen and (min-width: 1200px) {
        
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 72px 53px 10px 140px;
             }
        
             .about-box-right{
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 81px 50px 30px 55px;
             }
        
             a.advantage-link-btn {
                 background-color: #ffce4b;
                 font-weight: 500;
                 text-align: center;
                 color: #262629;
                 height: 50px;
                 line-height: 50px;
                 width: 300px;
                 margin-left:140px;
                 font-size: 19px;
                 display: inline-block;
        
             }
        
         } /* ------ END (min-width: 1200px) --------- */
        
        
         @media only screen and (min-width: 1400px) {
        
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 72px 53px 10px 140px;
             }
        
             .about-box-right{
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 81px 50px 30px 55px;
             }
        
             a.advantage-link-btn {
                 background-color: #ffce4b;
                 font-weight: 500;
                 text-align: center;
                 color: #262629;
                 height: 50px;
                 line-height: 50px;
                 width: 300px;
                 margin-left:140px;
                 font-size: 19px;
                 display: inline-block;
        
             }
        
         }
             }/* ------ END (min-width: 1400px) --------- */
        
         @media only screen and (min-width: 1600px) {
             .about-box-left {
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 72px 53px 10px 140px;
             }
        
             .about-box-right{
                 font-size: 15px;
                 line-height: 25px;
                 color: #fff;
                 padding: 81px 50px 30px 55px;
             }
        
             a.advantage-link-btn {
                 background-color: #ffce4b;
                 font-weight: 500;
                 text-align: center;
                 color: #262629;
                 height: 50px;
                 line-height: 50px;
                 width: 300px;
                 margin-left:140px;
                 font-size: 19px;
                 display: inline-block;
        
             }
         } /* ------ END (min-width: 1600px) --------- */
         .list-home-row-1 .become-host-section .get-start {
             display: none;
         }
         @media only screen and (min-width: 992px) {
            .list-home-row-1 .become-host-section .get-start {
                display: inline-block;
            }
         }
         .desktop-site-request {
             font-size: 20px;
             color: #fff;
             margin-left: 50px;
         }
         @media only screen and (min-width: 768px) {
             .desktop-site-request {
                color: #26363e;
                margin-left: 0;
             }
         }
         @media only screen and (min-width: 992px) {
             .desktop-site-request {
                 display: none;
             }
         }
         .advantage-link-btn {
             display: none!important;
         }
         @media only screen and (min-width: 992px) {
            .advantage-link-btn {
                display: inline-block!important;
            }
         }
         .hoverEft:hover {
                color: #51c5e8!important;
                cursor: pointer;
         }
     `]
})
export class NewSpaceHomeComponent implements OnInit, AfterViewInit, OnDestroy{
    user: User;
    loginForm: FormGroup;
    signupForm: FormGroup;
    isLoading = false;
    formData = {
        email : null,
        password: null
    }
    signupFormData = {
        email : null,
        password: null,
        name: null,
    }
    showSignin = true;
    showSignup = false;
    showForgotPasswordScreen = false;

    // modal data
    modalComponentData = null;


    constructor(
        private router: Router,
        private _userService: UserService,
        private _authService: AuthenticationService,
        private _crpto: CryptoService,
        private fb: FormBuilder,
        private _router: Router
    ){
        this.loginForm = fb.group({
            email: ['', BasicValidators.email],
            password: ['', Validators.required]
        });
        this.signupForm = fb.group({
                email : ['', BasicValidators.email,
                    new BasicValidators(this._authService).emailExistent.bind(this)],
                name : ['', Validators.required],
                password : ['', Validators.compose([
                    Validators.required,
                    PasswordValidators.shouldHaveMinimumCharacters
                ])],
            },
        );
    }

    ngOnInit(){
        //document.getElementsByTagName('body')[0].className = 'map-view';

        let user = this._userService.getUser();

        if(user){
            this.user = user;
            return;
        }

        let isLoggedIn = localStorage.getItem('ES_USR');

        if(!user && isLoggedIn && isLoggedIn === 'Y')
            this._authService.getLoggedUser().subscribe(
                user => {
                    this.user = user;
                    this._userService.setLoggedUser(user);
                }
            );
    }

    ngAfterViewInit(){
        var $window = $(window),
            $html = $('html');

        function resize() {
            if ($window.width() < 768) {
                return $html.addClass('mobile');
            }

            $html.removeClass('mobile');
            $html.addClass('desktop');
        }

        $window
            .resize(resize)
            .trigger('resize');

        $(window).scrollTop(0, 0);
    }

    showNewSpaceWizard(){

        if(!this.user){
            this.showLoginModal()
            return;
        }
        this.router.navigate(['spaces/listing/new'])
    }


    ngOnDestroy(){
        console.log('destrouinfg....');
    }

    toggleScreen(visible){
        console.log('visible', visible);
        this.showForgotPasswordScreen = visible;
        $("#loginModal").modal('show');
    }

    showTermsAndConditions(){
        var win = window.open("/#/terms/privacy", '_blank');
        win.focus();
    }

    showLoginModal() {
        this.modalComponentData = {
            component: SigninModalComponent,
            inputs: {
                navigateTo: 'spaces/listing/new'
            }
        }
    }

}