import {Component, Input, AfterViewInit, Output, EventEmitter} from "@angular/core";

declare let $: any;

@Component({
    selector: 'rate-per-hour-block',
    styles: [`
        .rate-per-hour-component-wrapper input {
            background: rgba(255, 255, 255, 0);
            z-index: 99;
        }
        .input-group {
            background: #fff;
        }
        .charge-per-hour-block-space-count-zero-validation {
            border: 1px solid red!important;
        }
    `],
    template: `
        <div style="margin-bottom: 10px;" class="rate-per-hour-component-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <div class="col-xs-12 p-x-0" *ngIf="index === 0">
                        Availability
                    </div>
                    <div class="input-group">
                        <input type="checkbox" [(ngModel)]="block.available"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="col-xs-12 p-x-0" *ngIf="index === 0">
                        Day
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" [(ngModel)]="block.day.display" readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="col-xs-12 p-x-0" *ngIf="index === 0">
                        From
                    </div>
                    <div class="input-group" [class.has-error]="!isValidTimeRange()">
                        <input id="startTime{{index}}"
                        class="form-control" type="text" 
                         [disabled]="!block.available"
                         [(ngModel)]="block.from"
                         (blur)="setStartTime($event)"
                         (keydown)="disableProperties($event)"
                         />
                         <span style="color: #b2b2b2; position: absolute; top: 15px; right: 10px; z-index: 9;" class="what-r-u-pl-drop-down-caret"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="col-xs-12 p-x-0" *ngIf="index === 0">
                        To
                    </div>
                    <div class="input-group" [class.has-error]="!isValidTimeRange()">
                        <input id="endTime{{index}}"
                        
                         class="form-control" type="text"
                         [disabled]="!block.available"
                         [(ngModel)]="block.to"
                         (blur)="setEndTime($event)"
                         (keydown)="disableProperties($event)"
                         />
                         <span style="color: #b2b2b2; position: absolute; top: 15px; right: 10px; z-index: 9;" class="what-r-u-pl-drop-down-caret"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="col-xs-12 p-x-0" *ngIf="index === 0">
                        Charge per hour
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="price">LKR</span>
                        <input type="text" class="form-control" 
                         [disabled]="!block.available"
                         [value]="block.charge"
                         [(ngModel)]="block.charge"
                         (blur)="setCharge($event)"
                         OnlyNumber="true"
                         maxlength="7"
                         aria-describedby="basic-addon1"
                         #chargePerHour = "ngModel"
                         [class.charge-per-hour-block-space-count-zero-validation]="chargePerHour.touched && (chargePerHour.value < 1 || chargePerHour.value == '')">
                    </div>
                </div>
            </div>
        </div>
    `
})
export class RatePerHourComponent implements AfterViewInit{

    @Input() index;
    @Input() block;
    @Output() repeatStart = new EventEmitter();
    @Output() repeatEnd = new EventEmitter();
    @Output() repeatCharge = new EventEmitter();

    ngAfterViewInit(){
        $('#startTime'+this.index).timepicker({
            'step': 60 ,
            'timeFormat': 'H:i:s'
        });
        $('#endTime'+this.index).timepicker({
            'step': 60 ,
            'timeFormat': 'H:i:s'
        });
    }

    setStartTime(event){
        let fromTime = event.target.value;
        this.block.from = fromTime;
    }

    setEndTime(event){
        let endTime = event.target.value;
        this.block.to = endTime;
    }

    setCharge(event){
        if(this.index === 0 && this.block.charge === ''){
            this.repeatCharge.emit({charge: event.target.value})
        }else{
            this.block.charge = event.target.value;
        }
    }

    disableProperties(e){
        e.preventDefault();
        return false;
    }

    isValidTimeRange(){

        let isValid = true;

        let from = +this.block.from.split(':')[0] // 9
        let end = +this.block.to.split(':')[0] // 14

        if(from >= end){
            isValid = false;
        }

        this.block.valid = isValid;
        return isValid;
    }

    /*getTimeArray(){
        return [
            {value: '12.00AM', label: '00:00'},
            {value: '1.00AM', label: '01:00'},
            {value: '2.00AM', label: '02:00'},
            {value: '3.00AM', label: '03:00'},
            {value: '4.00AM', label: '04:00'},
            {value: '5.00AM', label: '05:00'},
            {value: '6.00AM', label: '06:00'},
            {value: '7.00AM', label: '07:00'},
            {value: '8.00AM', label: '08:00'},
            {value: '9.00AM', label: '09:00'},
            {value: '10.00AM', label: '10:00'},
            {value: '11.00AM', label: '11:00'},
            {value: '12.00PM', label: '12:00'},
            {value: '1.00PM', label: '13:00'},
            {value: '2.00PM', label: '14:00'},
            {value: '3.00PM', label: '15:00'},
            {value: '4.00PM', label: '16:00'},
            {value: '5.00PM', label: '17:00'},
            {value: '6.00PM', label: '18:00'},
            {value: '7.00PM', label: '19:00'},
            {value: '8.00PM', label: '20:00'},
            {value: '9.00PM', label: '21:00'},
            {value: '10.00PM', label: '22:00'},
            {value: '11.00PM', label: '23:00'}
        ]

    }*/
}