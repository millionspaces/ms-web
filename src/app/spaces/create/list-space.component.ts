import {Component, AfterViewInit, OnInit, DoCheck, OnDestroy} from "@angular/core";
import {ImageUploadModalComponent} from "../../core/modal/image-upload-modal.component";
import {ImageUploadService} from "../../core/modal/image-upload.service";
import {Subject} from "rxjs/Subject";
import {Subscription} from "rxjs/Subscription";
import {ActivatedRoute, Router} from "@angular/router";
import {GoogleMapService} from "../../google-services-api/shared/google-map.service";
import {SpaceService} from "../shared/space.service";
import {UserService} from "../../user/user.service";
import {Response} from "@angular/http";
import {ToasterService} from "../../shared/services/toastr.service";
import { environment } from '../../../environments/environment';

declare let $: any;
declare let google: any;
declare let moment: any;
declare let cloudinary: any;

@Component({
    templateUrl: './list-space.component.html',
    styles: [`
         .input-group-btn .btn{ min-height: 48px;}
        /*top steps*/
        
        /*top stepsd*/
        .control__indicator{top: 14px;}
        label{ padding-top: 20px;}
        a.add-list-btn {background-color: #fff;font-weight: 500;text-align: center;color: #262629;margin-top: 10px;padding: 10px;border: 1px solid #000;font-size: 16px;display: inline-block;float: right;}
        .control--radio .control__indicator:after{top: 6px; left:6px;}
        .process-steps-icon{height: 25px;margin: 2px;}
                
        .pricing-info-wrapper p {
            margin-bottom: 10Wpx!important;
            max-width: 80%!important;
        }
        .pricing-info-wrapper p > span {
            font-weight: bold;
        }
        .amenities-info {
            margin-bottom: 20px;
        }
        .amenities-info > span {
            font-weight: bold;
        }
        .pricing-availability-wrapper > h3 {
            font-size: 16px;
            margin-top: 0;
        }
        .pricing-availability-wrapper > p {
            margin-bottom: 10px;
            color: #898989;
        }
        .pricing-availability-wrapper > p > span {
            font-weight: bold;
        }
        .input-group-addon{ border-radius: 0px; padding: 6px 12px;}
        input.form-control, button {
            font-size: 16px;
            font-weight: 400;
            color: #565656;
        }
        input[readonly=readonly] {
            background: #f8f8f8;
        }
        input[disabled] {
            background: #f8f8f8;
        }
        textarea {
            resize: none;
        }
        .space-des-txt-area-wrapper, .space-title-txt-area-wrapper {
            position: relative;
        }
        .operating-hours-title {
            font-size: 18px;
        }
        #myFile {
            bordeR: none;
            width: 93px;
        }
        
        /***
           jquery steps plugin overrides most of the custom styles
           To overcome this custom selectors should have higher specificity
           There are several ways to fix this but the easiest way is to put an !important rule with .
           Even though  this is treated as a bad practice, this solution is picked. Else, even for a very
           small css styling change it takes few minutes.
        */
        .pricing-model-tabs-header {
            list-style: none!important;
            padding-left: 0!important;
            margin-left: 0!important;
        }
        .pricing-model-tabs-header > li {
            display: inline-block!important;
            width: 50%!important;
            text-align: center;
            border: 1px solid #2aa1c0;
            padding: 0 10px 10px 0!important;
        }
        .pricing-model-tabs-header > li:first-child {
            border-right: none;
        }
        .pricing-model-tabs-header > li:last-child {
            border-left: none;
        }
        .pricing-model-tabs-header > li > div {            
            padding: 0 10px 10px 0!important;
        }
        .pricing-modal-tabs-body > li {
            padding: 0!important;
        }
        .pricing-modal-tabs-body > li > div {
            padding: 0!important;
        }
        .input-minimum-number-of-guests {
            width: 100%;
            height: 50px;
            border: 1px solid rgba(0, 0, 0, 0.1)!important;
            padding: 0 20px;
        }
        .select-block-charge-type {
            width: 100%;
            height: 50px;
            display: inline-block;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }
        .pricing-model-tabs-header > li.active {
            background: #2aa1c0;
            color: #fff;
        }
        .pricing-model-tabs-header .active .control input:checked:focus~.control__indicator, 
        .pricing-model-tabs-header .active .control input:checked~.control__indicator, 
        .pricing-model-tabs-header .active .control:hover input:not([disabled]):checked~.control__indicator {
            border: 1px solid #fff;
        }
        .pricing-model-tabs-header .active .control--radio .control__indicator:after {
            color: #fff
        }
        .pricing-model-tabs-header .control--radio .control__indicator:after {
            color: #e2e2e2;
            content: '';
        }
        .pricing-availability-wrapper > p {
            margin-bottom: 0;
            margin-top: 20px;
        }
        .position-checkbox {
            position: relative;
            top: 13px;
            left: 60px;
        }
        .space-count-zero-validation {
            border: 1px solid red!important;
        }
        p.content-title, p.content-body {
            text-align: center;   
        }
    `]
})
export class ListSpaceComponent implements OnInit, AfterViewInit, DoCheck, OnDestroy{
    response: Response;
    isLoading: boolean = false;
    secondaryContact = false;

    //route snapshot data
    user: any;
    eventTypes = [];
    amenities = [];
    amenityUnits = [];
    seatingOptions = [];
    spaceRules = [];
    cancellationPolicies = [];
    cloudDir: string;
    Math : any;

    selectedMeasure = {id:1, name:'sq ft'};
    spaceMeasure = [{id:1, name:'sq ft'},{id:2, name:'sq m'}];
    blockChargeTypes = [{id: 1, name: 'Space only charge'}, {id: 2, name: 'Per guest base charge'}];

    selectedBlockCharge;

    hostDetails: any = {
        hostname: <string> '',
        mobileNum: <string> '',
        companyNum: <string> '',
        profileImgData: <any> null,
        imageUrl: <string> null,
        bankInfo: {
            accountHolderName: <string> '',
            accountNumber: <string> '',
            name: <string> '',
            branch: <string> ''
        },
        contact2Name: <string> '',
        contact2Email: <string> '',
        contact2Number: <string> ''
    };

    feeStructureAgreement = false;
    selectFeeAgreement(){
        this.feeStructureAgreement = !this.feeStructureAgreement;
    }

    spaceInformation: any = {
        title: <string> null,
        address: <string> null,
        organization: <string> null,
        description: <string> null,
        thumbnailImgData: <any> null,
        coverImgData: <Array<any>> [],
    };

    basicSpaceInfo: any = {
        spaceSize: <number> null,
        guestCount: <number> 0,
        seatingOptions:<any> [],
        eventTypes: <any> [],
        amenities: <any> [],
        spaceTypes: <any> { indoor: true, outdoor: false, both: false }
    }

    pricingAndAvailability: any = {
        availability: 'PH',
        blockChargeType: 1,
        noticePeriod: 0,
        perHour : {
            bufferTime: null,
            validTill: null,
            blocks : [
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:1, display: 'Monday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:2, display: 'Tuesday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:3, display: 'Wednesday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:4, display: 'Thursday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:5, display: 'Friday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:6, display: 'Saturday'}},
                {available: true, from: '00:00:00', to: '00:00:00', charge: '', day: {value:7, display: 'Sunday'}},
            ]
        },
        perBlock : {
            validTill: null,
            minimumGuest: 0,
            menuImageData: <Array<any>> [],
            reimbursable: false, //space only charge
            blocks: [
                {
                    day: {value:8, display: 'Weekday'},
                    from: '00:00:00',
                    to: '00:00:00',
                    valid: true,
                    charge: '', //space only charge
                    minPriceGuest: 0, //per guest charge
                    menuImage: null  //per guest charge
                }
            ],
        },
    }

    preferences: any = {
        spaceRules: [],
        cancellationPolicy: null
    }

    //map data
    autoComplete: any;

    // modal data
    modalComponentData = null;
    imageDataSubscription: Subscription;

    oneBlockErrorMsg = false;

    constructor(
        private uploadService: ImageUploadService,
        private route: ActivatedRoute,
        private mapService: GoogleMapService,
        private spaceService: SpaceService,
        private userService: UserService,
        private toastr: ToasterService,
        private router: Router,
    ){
        this.user = this.route.snapshot.data['user'];
        this.eventTypes = this.route.snapshot.data['eventTypes'];
        this.amenities = this.route.snapshot.data['amenities'];
        this.amenityUnits = this.route.snapshot.data['amenityUnits'];
        this.cancellationPolicies = this.route.snapshot.data['cancellationPolicies'];
        this.seatingOptions = this.route.snapshot.data['seatingOptions'];
        this.spaceRules = this.route.snapshot.data['spaceRules'].sort(function(a,b){
            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
        this.cloudDir = environment.cldFolder;
        this.Math = Math;
    }

    selectPhotos(window){
        this.createImageUploadModal(window);
    }


    createImageUploadModal(window) {
        this.modalComponentData = {
            component: ImageUploadModalComponent,
            inputs: {
                window : window
            }
        }
    }

    ngOnInit(){

        this.seatingOptions.forEach(option => {
            option.count = option.count || '';
            option.selected = option.selected || false;
        })

        this.spaceRules.forEach(rule => {
            rule.selected = rule.selected || false;
        })

        this.imageDataSubscription = this.uploadService.getImageAsObservable().subscribe(
            imageData => {
                if (imageData.window === 'cover') {
                    this.spaceInformation.coverImgData.push(imageData);
                }
                else if (imageData.window === 'thumbnail') {
                     this.spaceInformation.thumbnailImgData = imageData;
                }
                else if (imageData.window === 'menus') {
                    let idx = this.pricingAndAvailability.perBlock.menuImageData.length;
                    imageData.name = imageData.name || 'Menu-'+(idx + 1);
                    this.pricingAndAvailability.perBlock.menuImageData.push(imageData);
                    this.isLoading = false;
                }
                else if (imageData.window === 'profile') {
                    this.hostDetails.profileImgData = imageData;
                    this.hostDetails.imageUrl = imageData.response.Location;
                }
            });

        this.amenities.forEach((amenity) => {
            amenity.selected = amenity.selected || false;
            amenity.complementary = amenity.complementary || false;
            amenity.chargable = amenity.chargable || false;
            amenity.unitPrice = amenity.unitPrice || 0;
        })

        this.cancellationPolicies.forEach(policy => {
            policy.selected = policy.selected || false;
        });

        this.selectedBlockCharge = this.blockChargeTypes[0];

        this.hostDetails.hostname = this.user.name;

        if(this.user.companyPhone){
            this.hostDetails.companyNum = this.user.companyPhone;
        }
        if(this.user.mobileNumber){
            this.hostDetails.mobileNum = this.user.mobileNumber;
        }

    }

    private hostDataSrc = new Subject<any>();
    private spaceInfoSrc = new Subject<any>();
    private basicInfoSrc = new Subject<any>();
    private availabilityInfoSrc = new Subject<any>();
    private bankInfoSrc = new Subject<any>();
    private feeStructureSrc = new Subject<any>();
    private preferencesInfoSrc = new Subject<any>();

    setSpaceDataObservable(){
        if(this.currentIndex === 0){
            this.hostDataSrc.next(this.hostDetails);
        }
        if(this.currentIndex === 1){
            this.spaceInfoSrc.next(this.spaceInformation);
        }
        if(this.currentIndex === 2){
            this.basicInfoSrc.next(this.basicSpaceInfo);
        }
        if(this.currentIndex === 3){
            this.availabilityInfoSrc.next(this.pricingAndAvailability);
        }
        if(this.currentIndex === 4){
            this.preferencesInfoSrc.next(this.preferences);
        }
        if(this.currentIndex === 5){
            this.feeStructureSrc.next(this.feeStructureAgreement);
        }
        if(this.currentIndex === 6){
            this.bankInfoSrc.next(this.hostDetails.bankInfo);
        }
    }

    ngDoCheck(){
        this.setSpaceDataObservable();
    }

    isValidHostDetails(hostDetails){
        let valid = false;
        let valid1 = true;

        if(hostDetails.hostname === '' || hostDetails.mobileNum.trim() === ' ' || !/^[a-zA-Z\s]+$/.test(hostDetails.hostname)){
            valid = false;
        }else if(hostDetails.mobileNum.length != 10){
            valid = false;
        }else if(hostDetails.companyNum && hostDetails.companyNum.length != 10){
            valid = false;
        }else {
            valid = true;
        }

        if(this.secondaryContact) {
            if(
                // MS-673
                this.hostDetails.contact2Name && /^[a-zA-Z\s]+$/.test(hostDetails.contact2Name) && this.hostDetails.contact2Name.length > 4 &&
                this.hostDetails.contact2Email &&  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.hostDetails.contact2Email) &&
                this.hostDetails.contact2Number && this.hostDetails.contact2Number.length === 10
            ) {
                valid1 = true;
            } else {
                valid1 = false
            }
        }

        if(valid && valid1){
            return true;
        }
        else{
            return false;
        }
    }

    isValidSpaceInfo(spaceInfo){
        let valid = false;
        if(
            !spaceInfo.title || spaceInfo.title.trim() === ''
            || !spaceInfo.address || spaceInfo.address.trim() === ''
            || !spaceInfo.description || spaceInfo.description.trim() === ''
            || !spaceInfo.thumbnailImgData
            || !spaceInfo.coverImgData[0]
        ){
            valid = false;
        }else {
            valid = true;
        }

        return valid;
    }

    isValidBasicInfo(basicInfo){
        let valid = false;

        let countArray = basicInfo.seatingOptions.filter(option => option.selected === true ).map(option => option.count);

        if(countArray.length > 0){
            let guestCount = Math.max.apply(Math, countArray);
            if(guestCount) basicInfo.guestCount = guestCount
            else basicInfo.guestCount = 0;
        }
        else if(countArray.length === 0)
            basicInfo.guestCount = 0;

        let includedAmenities = this.amenities.filter(amenity => amenity.selected === true);

        let validAmenities = this.isAmenitiesValid(includedAmenities);

        let validCount = this.isSeatingCountValid(countArray)
        if(
            !basicInfo.spaceSize || basicInfo.spaceSize.trim() === '' ||
            basicInfo.spaceSize < 1 ||
            !basicInfo.guestCount ||
            !validCount ||
            basicInfo.spaceTypes.length === 0 ||
            basicInfo.eventTypes.length  === 0 ||
            !validAmenities
        ){
            valid = false;
        }else{
            valid = true;
        }
        return valid;
    }

    isSeatingCountValid(seatingArray){
        let zeroCount = seatingArray.filter(count => (count[0] === '0' || count === '') );
        return (zeroCount.length > 0) ? false: true;
    }

    isAmenitiesValid(obj){
        let zeroChargeableCount = obj.filter(function(obj2){
            return obj2.chargable && (obj2.unitPrice == 0 || obj2.unitPrice === '');
        });
        return (zeroChargeableCount.length) ? false : true;
    }

    isValidAvailability(availabilityInfo){
        let valid = false;

        if(availabilityInfo.noticePeriod < 0 || availabilityInfo.noticePeriod.toString().trim() === ''){
            return false;
        }

        if(availabilityInfo.availability === 'PH'){
            if(!availabilityInfo.perHour.bufferTime || !availabilityInfo.perHour.validTill){
                return false;
            }
            else if(availabilityInfo.perHour.bufferTime < 0 || availabilityInfo.perHour.bufferTime > 72){
                return false;
            }
        }

        if(availabilityInfo.availability === 'PB'){
            if(!availabilityInfo.perBlock.validTill){
                return false;
            }
        }

        let isAllNotSelected: boolean = availabilityInfo.perHour.blocks.map(block => block.available).every(val => val === false);

        if(isAllNotSelected){
            return false;
        }

        if(availabilityInfo.availability === 'PH'){
            let booleanFlags = [];
            availabilityInfo.perHour.blocks.forEach(block => {
                if( block.available &&
                    (!block.valid
                    || block.from.trim() === ''
                    || block.to.trim() === ''
                    || block.charge.trim() === ''
                    || block.charge < 1)
                ){
                    booleanFlags.push(false);
                }else{
                    booleanFlags.push(true);
                }
            })
            valid = booleanFlags.every( val => val === true)
        }
        else if(availabilityInfo.availability === 'PB'){
            let booleanFlags = [];
            if(availabilityInfo.blockChargeType === 1){
                availabilityInfo.perBlock.blocks.forEach(block => {
                    if( !block.valid
                        || block.from.trim() === ''
                        || block.to.trim() === ''
                        || block.charge.trim() === ''
                        || block.charge < 1
                    ){
                        booleanFlags.push(false);
                    }else{
                        booleanFlags.push(true);
                    }
                })
            }else if(availabilityInfo.blockChargeType === 2){
                availabilityInfo.perBlock.blocks.forEach(block => {
                    if(
                        availabilityInfo.perBlock.minimumGuest < 1
                        || availabilityInfo.perBlock.minimumGuest.trim() == ''
                        || availabilityInfo.perBlock.minimumGuest > this.basicSpaceInfo.guestCount
                        || !block.valid
                        || block.from.trim() === ''
                        || block.to.trim() === ''
                        || block.minPriceGuest == 0
                        || block.minPriceGuest.trim() == ''
                        || !block.menuImage
                    ){
                        booleanFlags.push(false);
                    }else{
                        booleanFlags.push(true);
                    }
                })
            }

            valid = booleanFlags.every( val => val === true)
        }
        return valid;
    }

    isValidBankInfo(bankInfo){
        let valid = false;

        if(bankInfo.accountHolderName.trim() === ''
            || bankInfo.accountNumber.trim() === ''
            || bankInfo.name.trim() === ''
            || bankInfo.branch.trim() === ''
            ){
            valid = false;
        }else{
            valid = true;
        }

        return valid;
    }

    isFeeStructureAgreed(agreement){
        return agreement;
    }

    isValidPreferences(preferences){
        let valid = false;
        if(!preferences.cancellationPolicy){
            valid = false;
        }else{
            valid = true;
        }
        return valid;
    }

    currentIndex = 0;

    ngAfterViewInit(){
        /*auto fill google map*/
        let input: HTMLInputElement = (<HTMLInputElement>document.getElementById("search-box"));
        this.initAutoComplete(input);

        $("#valid-till-ph").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            format: "dd MM yyyy",
            minView: '2',
            onRender: function () {

            }
        }).on('changeDate', function (ev) {
            console.log(ev);

            this.pricingAndAvailability.perHour.validTill = moment(ev.date).format("YYYY-MM-DD");
        }.bind(this));

        $("#valid-till-pb").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            format: "dd MM yyyy",
            minView: '2',
        }).on('changeDate', function (ev) {
            this.pricingAndAvailability.perBlock.validTill = moment(ev.date).format("YYYY-MM-DD");
        }.bind(this));


        /*script 1*/
        var $window = $(window),
            $html = $('html');

        function resize() {
            if ($window.width() < 768) {
                return $html.addClass('mobile');
            }

            $html.removeClass('mobile');
            $html.addClass('desktop');
        }

        $window
            .resize(resize)
            .trigger('resize');

        let actualPage = 0;

        /*script 2*/
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            enableFinishButton :false,

            onInit: function () {
                $('.actions > ul > li:first-child').attr('style', 'display:none');

                this.hostDataSrc.asObservable().subscribe(hostDetails => {

                    let valid = this.isValidHostDetails(hostDetails);

                    if(valid){
                        enableContinue();
                    }else {
                        disableContinue();
                    }
                })

            }.bind(this),

            onStepChanging: function(event, currentIndex, newIndex){
                if(newIndex > currentIndex && newIndex != actualPage + 1){
                    return false;
                }else if(newIndex < currentIndex && newIndex != actualPage - 1){
                    return false;
                }
                return true;
            }.bind(this),

            onStepChanged: function (event, currentIndex) {
                console.log('currentIndex', currentIndex);
                actualPage = currentIndex;
                this.currentIndex = currentIndex;

                if (currentIndex > 0) {
                    $('.actions > ul > li:first-child').attr('style', '');

                } else {
                    $('.actions > ul > li:first-child').attr('style', 'display:none');
                }


                this.hostDataSrc.asObservable().subscribe(hostDetails => {
                    if(currentIndex == 0){
                        let valid = this.isValidHostDetails(hostDetails);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.spaceInfoSrc.asObservable().subscribe(spaceInfo => {
                    if(currentIndex == 1){
                        let valid = this.isValidSpaceInfo(spaceInfo);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.basicInfoSrc.asObservable().subscribe(basicInfo => {
                    if(currentIndex == 2){
                        let valid = this.isValidBasicInfo(basicInfo);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.availabilityInfoSrc.asObservable().subscribe(availabilityInfo => {
                    if(currentIndex == 3){
                        let valid = this.isValidAvailability(availabilityInfo);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.preferencesInfoSrc.asObservable().subscribe(preferences => {
                    if(currentIndex == 4){
                        let valid = this.isValidPreferences(preferences);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.feeStructureSrc.asObservable().subscribe(agreement => {
                    if(currentIndex == 5){
                        let valid = this.isFeeStructureAgreed(agreement);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })
                this.bankInfoSrc.asObservable().subscribe(bankInfo => {
                    if(currentIndex == 6){
                        let valid = this.isValidBankInfo(bankInfo);

                        if(valid){
                            enableContinue();
                        }else {
                            disableContinue();
                        }
                    }
                })


                if(currentIndex == 1){
                    var mapOptions = {
                        zoom: 15,
                        center: new google.maps.LatLng(6.936735, 79.845168),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
                    };

                    if(this.mapService.getSelectedGeoCodes()){
                        this.mapService.targetLocation(this.mapService.getSelectedGeoCodes());
                    }else{
                        this.mapService.setMap(mapOptions, 'map-on-create');
                    }
                }


            }.bind(this),

            labels: {
                next: "Continue"
            }
        });

        /*script 3*/
        $(document).ready(function(){
            resizeContent();

            $(window).resize(function() {
                resizeContent();
            });
        });

        function resizeContent() {
            let height = $(window).height();
            $('#wizard').height(height - 49);
            $('.mobile #wizard').height(height - 50);
            $('#wizard .content').height(height - 50);
            $('.mobile #wizard .content').height(height - 51);
            $('#wizard .actions').height(50);
        }

        function disableContinue(){
            $('.actions > ul > li:nth-of-type(2)').addClass('disabled');
        }
        function enableContinue(){
            $('.actions > ul > li:nth-of-type(2)').removeClass('disabled');
        }

        $('#wizard .content>.body .create-wizard-body .completed .progress').css ({
            background: '#caefff'
        });

        $('#wizard .content>.body .create-wizard-body .completed').css ({
            float: 'none'
        });
        $('#wizard .content>.body .create-wizard-body').css ('height', '94%');
        $('#wizard .content>.body .create-help-body').css ('padding', '20px 30px 30px');
        $('#wizard .content > .body .create-wizard-body .dropdown .dropdown-toggle').css ('padding', '13px 15px');

        $('.create-wizard-body').on ('scroll', () => {
            let newTop = $('#search-box').offset().top + $('#search-box').outerHeight();
            $('.pac-container').css('top', newTop + 'px');
        });

        $('.create-wizard-body').on ('scroll', () => {
            $('.datetimepicker, .ui-timepicker-wrapper').fadeOut ();
        });

        /*$('.wizard>.actions a').one("click", function() {
            $(this).click(function () { return false; });
        });*/


    }

    completeOptions = {
        componentRestrictions: {country: "lk"}
    };

    imageRemoved($event){

        console.log('$event',$event);

        if($event.window === 'cover'){
            let index = 0;
            this.spaceInformation.coverImgData.forEach(imgData => {
                if($event.filName === imgData.public_id){
                    this.spaceInformation.coverImgData.splice(index, 1);
                    return;
                }
                index++;
            });
        }
        else if($event.window === 'menus'){
            let index = 0;
            this.pricingAndAvailability.perBlock.menuImageData.forEach(imgData => {
                if($event.filName === imgData.public_id){
                    this.pricingAndAvailability.perBlock.menuImageData.splice(index, 1);
                }
                index++;
            });

            // reorder menu names
            this.pricingAndAvailability.perBlock.menuImageData.forEach((imgData, idx) => {
                imgData.name = 'Menu-'+(idx+1);
            });
        }
        else if($event.window === 'thumbnail'){
            if($event.filName === this.spaceInformation.thumbnailImgData.public_id){
                this.spaceInformation.thumbnailImgData = null;
            }
        }

    }

    private initAutoComplete(input: HTMLInputElement): void {
        this.autoComplete = new google.maps.places.Autocomplete(input, this.completeOptions);
        google && google.maps && google.maps.event && google.maps.event.addListener(this.autoComplete, 'place_changed', () => {

            this.mapService.findGeoCode(input.value)
                .subscribe(
                    geoCode => {
                        console.log('geoCode',geoCode);
                        this.mapService.setSelectedGeoCodes({
                            lat: geoCode.lat,
                            lng: geoCode.lng
                        });
                        this.mapService.targetLocation(this.autoComplete.getPlace().geometry.location);
                        console.log(this.autoComplete.getPlace());
                        this.spaceInformation.address = this.autoComplete.getPlace().formatted_address;
                        $("#search-box").val
                      //  this.subject.next(this.spaceData);
                    },
                    error => { console.log(error) }
                );
        });
    }

    selectMeasure(measure){
        this.selectedMeasure = measure;
    }

    selectEvent(event){
        event.selected = event.selected || false;
        if(event.selected){
            //remove event
            let arrayIndex = 0;
            this.basicSpaceInfo.eventTypes.forEach((eventType, index) => {
                if(eventType.id === event.id){
                    arrayIndex = index;
                    return;
                }
            });
            this.basicSpaceInfo.eventTypes.splice(arrayIndex, 1);
            event.selected = false;

        }else{
            //add event
            event.selected = true;
            this.basicSpaceInfo.eventTypes.push(event);
        }
    }


    selectSeatingOption(option){
        option.selected = !option.selected;

        let seatingOption = this.basicSpaceInfo.seatingOptions.find(soption => soption.id === option.id);

        if(seatingOption){
            this.basicSpaceInfo.seatingOptions.forEach((sOption, idx) => {
                if(sOption.id === option.id) {
                    this.basicSpaceInfo.seatingOptions.splice(idx, 1)
                }
            })
        }else{
            this.basicSpaceInfo.seatingOptions.push(option);
        }

    }

    resetAmenity(amenity){
        amenity.selected = !amenity.selected;
        amenity.chargable = false;
        amenity.complementary = false;
    }

    makeComplementAmenity(amenity){
        amenity.complementary = !amenity.complementary;
        amenity.selected = true;
        amenity.chargable = false;
    }

    makeChargableAmenity(amenity){
        amenity.chargable = !amenity.chargable;
        amenity.selected = true;
        amenity.complementary = false;
    }

    selectAmenityUnit(amenity, unit){
        amenity.amenityUnitsDto = unit;
        amenity.amenityUnit = unit.id;
    }

    selectRule(rule){

        let spaceRule = this.preferences.spaceRules.find(spaceRule => spaceRule.id === rule.id);

        if(spaceRule){
            this.preferences.spaceRules.forEach((spaceRule, idx) => {
                if(spaceRule.id === rule.id) {
                    this.preferences.spaceRules.splice(idx, 1)
                }
            })
        }else{
            this.preferences.spaceRules.push(rule);
        }

    }

    selectCancellationPolicy(policy){
        if(policy.selected){
            policy.selected = false;
            this.preferences.cancellationPolicy = null;
        }else{
            this.cancellationPolicies.forEach(policy => policy.selected = false);
            policy.selected = true;
            this.preferences.cancellationPolicy = policy;
        }
    }

    addNewBlock(){
        this.pricingAndAvailability.perBlock.blocks.push({
            day: {value:8, display: 'Weekday'},
            from: '00:00:00',
            to: '00:00:00',
            valid: true,
            charge: '', //space only charge
            minPriceGuest: 0, //per guest charge
            minBlockPrice: 0, //per guest charge
            menuImage: null //per guest charge

        });
        if(this.oneBlockErrorMsg){
            this.oneBlockErrorMsg = false;
        }
    }


    removeSelectedBlock(index){
        if(this.pricingAndAvailability.perBlock.blocks.length > 1){
            this.pricingAndAvailability.perBlock.blocks.splice(index,1);
            this.oneBlockErrorMsg = false;
        }else{
            this.oneBlockErrorMsg = true;
        }
    }

    createSpace(){
        this.create();
    }

    getRules(rules){
        return rules.map(rule => rule.id);
    }

    getSelectedEventTypes(eventTypes){
        return eventTypes.map(eventType => eventType.id)
    }

    getCoverImages(imagesDataArray){
        let imageNames = [];

        imagesDataArray.forEach(imageData => {
            imageNames.push(imageData.public_id.split("/")[1]);
        });

        return imageNames;
    }

    getSelectedAmenities(amenities){
        let filteredAmenities = amenities.filter(amenity => amenity.selected === true && amenity.chargable === false);
        return filteredAmenities.map(amenity => amenity.id);
    }

    getExtraAmenities(amenities){
        let filteredAmenities = amenities.filter(amenity => amenity.selected === true && amenity.chargable === true);
        let exAmenities = [];

        filteredAmenities.forEach(amenity => {
            let obj = {
                amenityId: amenity.id,
                amenityUnit: amenity.amenityUnit,
                extraRate: amenity.unitPrice,
            }
            exAmenities.push(obj)
        });

        return exAmenities;

    }

    getSeatingOptions(seatingOptions){
        let seatingArrangements = [];

        seatingOptions.forEach(option => {
            let obj = {
                id: option.id,
                participantCount: option.count
            }
            seatingArrangements.push(obj);
        });

        return seatingArrangements;
    }

    getAvailableBlocks(pricing){
        let customBlocks = [];

        try {
            if(pricing.availability === 'PH'){
                let obj = null;
                pricing.perHour.blocks.forEach(block => {
                     obj = {
                        from: block.from,
                        to: block.to,
                        charge: block.charge,
                        day: block.day.value,
                        isAvailable: block.available ? 1 : 0
                    }
                    customBlocks.push(obj);
                });
            }

            else if(pricing.availability === 'PB'){
                let obj = null;

                if(pricing.blockChargeType === 1){
                    pricing.perBlock.blocks.forEach(block => {
                        obj = {
                            day: block.day.value,
                            from: block.from,
                            to: block.to,
                            charge: block.charge,
                        }
                        customBlocks.push(obj);
                    });
                }

                else if(pricing.blockChargeType === 2){
                    pricing.perBlock.blocks.forEach(block => {
                        obj = {
                            day: block.day.value,
                            from: block.from,
                            to: block.to,
                            charge: block.minPriceGuest,
                            menuFiles: [block.menuImage]
                        }
                        customBlocks.push(obj);
                    });
                }
            }
        }catch (e){
            console.error(e);
            throw e;
        }



        return customBlocks;
    }

    /*getCustomizeDateRange(time){
        if(!time) return '';

        let momentObj = null;
        if(time.slice(':')[0].length === 1){
            let sliceObject = time.slice(0, 4) + ":00" + time.slice(4);
            momentObj = moment(sliceObject, ["h:mmA"])
        }
        else if(time.slice(':')[0].length === 2){
            let sliceObject = time.slice(0, 2) + ":00" + time.slice(2);
            momentObj = moment(sliceObject, ["h:mmA"])
        }
        return momentObj.format("HH:mm:ss");
    }*/

    create(){
        console.log('this.spaceInformation', this.spaceInformation);
        this.isLoading = true;
        let spaceData;
        try{
            spaceData = {
                name: this.spaceInformation.title,//
                measurementUnit: {id: this.selectedMeasure.id},//
                addressLine1: this.spaceInformation.address,//
                addressLine2: this.spaceInformation.organization,//
                description: this.spaceInformation.description,//
                size: this.basicSpaceInfo.spaceSize,//
                participantCount: this.basicSpaceInfo.guestCount,//
                user: this.user.id,
                cancellationPolicy: this.preferences.cancellationPolicy.id,//
                longitude: this.mapService.selectedGeoCodes.lng,//
                latitude: this.mapService.selectedGeoCodes.lat,//
                thumbnailImage: this.spaceInformation.thumbnailImgData.public_id.split("/")[1],//
                rules: this.getRules(this.preferences.spaceRules),//
                noticePeriod: this.pricingAndAvailability.noticePeriod,//
                bufferTime: this.pricingAndAvailability.availability === 'PH' ? this.pricingAndAvailability.perHour.bufferTime : null,//
                menuFiles: this.getAllMenus(this.pricingAndAvailability.perBlock.menuImageData),
                availabilityMethod: this.pricingAndAvailability.availability === 'PH' ? 'HOUR_BASE' : 'BLOCK_BASE',//
                reimbursableOption: (this.pricingAndAvailability.perBlock.reimbursable) ? 1 : 0,
                availability: this.getAvailableBlocks(this.pricingAndAvailability),//
                seatingArrangements: this.getSeatingOptions(this.basicSpaceInfo.seatingOptions),//
                eventType: this.getSelectedEventTypes(this.basicSpaceInfo.eventTypes),//
                amenity: this.getSelectedAmenities(this.amenities),//
                extraAmenity: this.getExtraAmenities(this.amenities),//
                images: this.getCoverImages(this.spaceInformation.coverImgData),//
                calendarStart: '2017-01-01',//
                calendarEnd: this.pricingAndAvailability.availability === 'PH' ? this.pricingAndAvailability.perHour.validTill : this.pricingAndAvailability.perBlock.validTill,//
                calendarEventSize: "10000", //
                contactPersonName: this.hostDetails.hostname,//
                mobileNumber: this.hostDetails.mobileNum, //
                companyPhone: this.hostDetails.companyNum, //
                commissionPercentage: 20,//
                accountHolderName: this.hostDetails.bankInfo.accountHolderName,//
                accountNumber: this.hostDetails.bankInfo.accountNumber,//
                bank: this.hostDetails.bankInfo.name,//
                bankBranch: this.hostDetails.bankInfo.branch,//
                hostLogo: this.hostDetails.profileImgData ? this.hostDetails.profileImgData.url : null, //
                blockChargeType: this.pricingAndAvailability.availability === 'PB' ? this.pricingAndAvailability.blockChargeType : null,
                minParticipantCount: this.pricingAndAvailability.availability === 'PB' ? this.pricingAndAvailability.perBlock.minimumGuest : null,
                spaceType: this.getSpaceTypes(),
                imageDetails: this.getCldnryImageDetails(),
                contactPersonName2: (this.secondaryContact) ? this.hostDetails.contact2Name.trim() : '',
                mobileNumber2: (this.secondaryContact) ? this.hostDetails.contact2Number.trim() : '',
                email2: (this.secondaryContact) ? this.hostDetails.contact2Email.trim() : '',
            }

        }catch (e){
            this.isLoading = false;
            this.toastr.error('Incorrect space request!');
            console.log('space request error', e);
            return;
        }

       /* try{
            hostData = {
                name: this.hostDetails.hostname,
                companyPhone: this.hostDetails.companyNum,
                mobileNumber: this.hostDetails.mobileNum,
                imageUrl: (this.hostDetails.imageUrl) ? this.hostDetails.imageUrl : '',
                email: (this.user.email)? this.user.email : '',
                address: (this.user.address)? this.user.address : '',
                companyName: (this.user.companyName)? this.user.companyName : '',
                about: (this.user.about)? this.user.about : '',
                jobTitle: (this.user.jobTitle)? this.user.jobTitle : '',
                lastName: (this.user.lastName)? this.user.lastName : '',
                accountHolderName: this.hostDetails.bankInfo.accountHolderName,
                accountNumber: this.hostDetails.bankInfo.accountNumber,
                bank: this.hostDetails.bankInfo.name,
                bankBranch: this.hostDetails.bankInfo.branch
            }
        }catch(e){
            this.isLoading = false;
            this.toastr.error('Incorrect host request!');
            console.log('host data request error', e);
            return;
        }*/

        let spaceRequest = Object.freeze(spaceData) ;

        console.log('spaceRequest', spaceRequest);

        this.spaceService.createSpace(spaceRequest)
            .subscribe(
                data =>{
                    this.response = data;
                    console.log('data', data);
                },
                error => {
                    console.log('error',error);
                    this.isLoading = false;
                    this.toastr.error('Something went wrong!');
                },
                () =>{
                    this.isLoading = false;
                    this.router.navigate(['spaces/listing/success',{firstCover: this.spaceInformation.coverImgData[0].public_id.split("/")[1]}]);
                }
            );
    }

    getCldnryImageDetails() {

        let imageDetails = [];

        if(this.spaceInformation.thumbnailImgData){
            let data = this.spaceInformation.thumbnailImgData;
            let imgData = {
                'url': data.public_id.split('/')[1],
                'publicId':data.public_id,
                'deleteToken':data.delete_token,
                'etag':data.etag,
                'signature':data.signature,
                'secureUrl':data.secure_url
            }
            imageDetails.push(imgData);
        }
        if(this.spaceInformation.coverImgData){
            this.spaceInformation.coverImgData.forEach(data => {
                let imgData = {
                    'url': data.public_id.split('/')[1],
                    'publicId':data.public_id,
                    'deleteToken':data.delete_token,
                    'etag':data.etag,
                    'signature':data.signature,
                    'secureUrl':data.secure_url
                }
                imageDetails.push(imgData);
            });
        }
        if(this.pricingAndAvailability.perBlock.menuImageData){
            this.pricingAndAvailability.perBlock.menuImageData.forEach(data => {
                let imgData = {
                    'url': data.public_id.split('/')[1],
                    'publicId':data.public_id,
                    'deleteToken':data.delete_token,
                    'etag':data.etag,
                    'signature':data.signature,
                    'secureUrl':data.secure_url
                }
                imageDetails.push(imgData);
            });
        }

        return imageDetails;
    }

    repeatStart(event){
        this.pricingAndAvailability.perHour.blocks.forEach(block => {
            block.from = event.from
        });
    }

    repeatEnd(event){
        this.pricingAndAvailability.perHour.blocks.forEach(block => {
            block.to = event.to
        });
    }

    repeatCharge(event){
        this.pricingAndAvailability.perHour.blocks.forEach(block => {
            block.charge = event.charge
        });
    }

    ngOnDestroy(){
        this.hostDataSrc.unsubscribe();
        this.spaceInfoSrc.unsubscribe();
        this.basicInfoSrc.unsubscribe();
        this.availabilityInfoSrc.unsubscribe();
        this.bankInfoSrc.unsubscribe();
        this.feeStructureSrc.unsubscribe();
        this.preferencesInfoSrc.unsubscribe();
    }

    uploadFile(event){
        this.isLoading = true;
        this.uploadService.uploadFile(event.srcElement.files[0]);
    }

    getAllMenus(menuImageData){
        let menus = [];
        if(menuImageData.length > 0){
            menuImageData.forEach((menuData, idx) => {
                menus.push( {menuId: idx + 1, url: menuData.public_id.split('/')[1]} );
            });
        }
        return menus;
    }

    selectBlockChargeType(chargeType: any){
        this.selectedBlockCharge = chargeType;
        this.pricingAndAvailability.blockChargeType = chargeType.id;
    }

    uploadThumbnailImage(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg"],
                // max_file_size: 3000000,
                min_image_width: 1580,
                min_image_height: 814,
                multiple: false,
                folder: this.cloudDir
            },
            (error, result) => {
                if(result){
                  console.log('result', result);
                  result[0].window = 'thumbnail';
                  this.spaceInformation.thumbnailImgData = result[0];
                }
            });
    }

    uploadCoverImages(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg"],
                // max_file_size: 3000000,
                min_image_width: 1580,
                min_image_height: 814,
                multiple: false,
                folder: this.cloudDir
            },
            (error, result) => {
                if(result){
                    console.log('result', result);
                    result[0].window = 'cover';
                    this.spaceInformation.coverImgData.push(result[0]);
                }
            });
    }

    uploadMenus(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg", "pdf"],
                // max_file_size: 3000000,
                multiple: false,
                folder: this.cloudDir
            },
            (error, result) => {
                if(result){
                    let idx = this.pricingAndAvailability.perBlock.menuImageData.length;
                    result[0].window = 'menus';
                    let imageData = result[0];
                    imageData.name = imageData.name || 'Menu-'+(idx + 1);
                    this.pricingAndAvailability.perBlock.menuImageData.push(imageData);
                }
            });
    }

    uploadHostLogo(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg"],
                // max_file_size: 3000000,
                min_image_width: 200,
                min_image_height: 200,
                multiple: false,
                folder: this.cloudDir
            },
            (error, result) => {
                if(result){
                    console.log('result', result);
                    result[0].window = 'profile';
                    this.hostDetails.profileImgData = result[0];
                    this.hostDetails.imageUrl = result[0].url;
                }
            });
    }

    getSpaceTypes(){
        let types = [];
        if(this.basicSpaceInfo.spaceTypes.indoor){
            types.push({ id: 1 });
        }
        else if(this.basicSpaceInfo.spaceTypes.outdoor){
            types.push({ id: 2 });
        }
        else if(this.basicSpaceInfo.spaceTypes.both){
            types.push({ id: 3 });
        }
        return types;
    }

    setSpaceType(id: number) {
        if(id === 1){
            this.basicSpaceInfo.spaceTypes.outdoor = false;
            this.basicSpaceInfo.spaceTypes.both = false;
            this.basicSpaceInfo.spaceTypes.indoor = true;
        }
        else if(id === 2){
            this.basicSpaceInfo.spaceTypes.indoor = false;
            this.basicSpaceInfo.spaceTypes.both = false;
            this.basicSpaceInfo.spaceTypes.outdoor = true;
        }
        else if(id === 3){
            this.basicSpaceInfo.spaceTypes.indoor = false;
            this.basicSpaceInfo.spaceTypes.outdoor = false;
            this.basicSpaceInfo.spaceTypes.both = true;
        }
    }

    toggleSecondaryContact() {
        this.secondaryContact = !this.secondaryContact;
    }
}