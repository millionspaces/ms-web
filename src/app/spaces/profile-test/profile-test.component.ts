import {Component, OnInit, AfterViewInit, ViewChild, NgZone, HostListener} from "@angular/core";
import { ActivatedRoute,  Router } from "@angular/router";
import { EventService } from "../../events/shared/event.service";
import { AmenitiesService } from "../../amenities/shared/amenities.service";
import { UserService } from "../../user/user.service";
import { SigninModalComponent } from "../../core/modal/signin-modal.component";
import { InfoModalComponent } from "../../core/modal/info-modal.component";
import { SpaceService } from "../shared/space.service";
import { environment } from '../../../environments/environment';
import { RouteDataService } from "../../shared/services/route.data.service";
import { ToasterService } from "../../shared/services/toastr.service";
import { BookingService } from "../../user/my-activities/booking.service";
import { MSError } from "../../common/errors/ms.error";
import { BadRequestError } from "../../common/errors/bad-request-error";
import { BlockItemsComponent } from "../calendar/block-items.component";
import { MsUtil } from "../../shared/util/ms-util";
import "../../../assets/owl/js/owl.carousel.min.js"
import { SessionStorageService } from "../../shared/services/session-storage.service";
import {FifaMascotService} from "../../core/fifa-mascot.service";

declare let moment: any;
declare let $;

@Component({
    templateUrl: './profile-test.component.html',
    styleUrls: ['./profile-test.component.css']
})
export class ProfileTestComponent implements OnInit, AfterViewInit {

    // route resolvers
    space: any;
    rules: any;
    cancellationPolicies: any;

    // for the calendar
    blockPeriods: any = [];
    eventDaysCalendar: any = [];
    availabilityMethod: string;
    availability: any = [];
    today: any;

    eventTypes: any = [];
    amenities: any = [];
    spaceRules: any = [];
    extraAmenities: any = [];
    cancellationPolicy: any;
    menus: any = [];
    selectedUserReview: any;
    selectedUserReviewIndex: number;

    selectedEvent: any;
    selectedSeatingOption: any;
    selectedGuestCount: any;
    seatingParticipantCount: any;

    // modal data
    modalComponentData = null;

    //per hour inputs
    spaceAvailability = {from:null, to:null};
    startTimeHours = 0;
    endTimeHours = 0;

    isAvailabilityChecking = false;
    spaceAvailableForBooking = false;
    checkedAvailability = false;
    reservationError;

    bucket: string;

    @ViewChild(BlockItemsComponent)
    blockItems: BlockItemsComponent;

    constructor(
        private route: ActivatedRoute,
        private eventService: EventService,
        private amenityService: AmenitiesService,
        private userService: UserService,
        private router: Router,
        private spaceService: SpaceService,
        private toaster: ToasterService,
        private routeDataService: RouteDataService,
        private storageService: SessionStorageService,
        private bookingService: BookingService,
        private fifaService: FifaMascotService,
        private zone: NgZone
    ){
        this.bucket = environment.bucket;
    }


    /**
     * Determine the mobile operating system.
     * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
     *
     * @returns {String}
     */
    getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || (<any>window).opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !(<any>window).MSStream) {
            return "iOS";
        }

        return "unknown";
    }

    futureBookingDates;
    similarSpaces;
    userReviews;

    ngOnInit(){
        this.space = this.route.snapshot.data['space'];

        // clear session storage
        this.storageService.removeItem('b_data');
        this.storageService.removeItem('s_date');

        if(this.space.approved === 0) {
            alert('This space is no longer available');
            this.router.navigateByUrl('/spaces/list');
        }


        this.rules = this.route.snapshot.data['spaceRules'];
        this.cancellationPolicies = this.route.snapshot.data['cancellationPolicies'];

        this.availabilityMethod = this.space.availabilityMethod;

        this.spaceService.getFutureBookingDates(this.space.id).subscribe(futureBookingDates => {
            this.futureBookingDates = futureBookingDates;

            this.blockPeriods = this.futureBookingDates.map(block =>
                { return { from: block.from, to: block.to, id: block.id } }
            );

            if(this.availabilityMethod === 'HOUR_BASE'){
                this.futureBookingDates.forEach(date => {

                    let startDate = moment(date.from).format('YYYY-MM-DD');
                    let endDate = moment(date.to).format('YYYY-MM-DD');

                    //same day booking
                    if(startDate === endDate){
                        this.eventDaysCalendar.push({date: startDate});
                    }else{
                        // not same day
                        let dates = getDates(new Date(startDate), new Date(endDate));
                        dates = dates.map(date =>  {
                            return { date: moment(date).format('YYYY-MM-DD') }
                        });
                        this.eventDaysCalendar.push(...dates);
                    }
                    //console.log('this.eventDaysCalendar', this.eventDaysCalendar);
                })
            }
            else if(this.availabilityMethod === 'BLOCK_BASE'){

                this.futureBookingDates.forEach(date => {

                    let startDate = moment(date.from).format('YYYY-MM-DD');
                    let endDate = moment(date.to).format('YYYY-MM-DD');

                    let endDateTime = moment(date.to).format('HH:mm');

                    //same day booking
                    if(startDate === endDate || endDateTime == "00:00"){
                        this.eventDaysCalendar.push({date: startDate});
                    }else{
                        // not same day
                        let dates = getDates(new Date(startDate), new Date(endDate));
                        dates = dates.map(date =>  {
                            return { date: moment(date).format('YYYY-MM-DD') }
                        });
                        this.eventDaysCalendar.push(...dates);
                    }
                })
            }

        })

        this.spaceService.getSimilarSpaces(this.space.id).subscribe(similarSpaces => {
            this.similarSpaces = similarSpaces;
        })

        this.spaceService.getUserReviews(this.space.id).subscribe(reviews => {
            this.userReviews = reviews;
            // set default review
            if(reviews.length > 0) {
                this.selectedUserReview = reviews[0];
                this.selectedUserReviewIndex = 0;
            }
        })

        console.log('space', this.space);

        this.route.fragment.subscribe(f => {
            const element = document.querySelector("#" + f)
            if (element) element.scrollIntoView(element)
        })

        function addDays(days, currentDate) {
            var dat = new Date(currentDate.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = addDays(1, currentDate);
            }
            return dateArray;
        }

        this.availability = this.space.availability.map(availability =>
            { return {
                day: availability.day,
                from: availability.from,
                to: availability.to,
                diff: getTimeDifference(availability),
                charge: availability.charge,
                menus: (availability.menuFiles)? availability.menuFiles : null
            } }
        )

        function getTimeDifference(availability){
            return +availability.to.split(':')[0] - +availability.from.split(':')[0];
        }

        //get space related eventTypes
        this.eventService.getEventTypes().subscribe(eventTypes => {
            eventTypes.forEach(eventType => {
                let result = $.grep(this.space.eventType, function (id) {
                    return id == eventType.id;
                });
                if (result && result.length== 1) {
                    this.eventTypes.push(eventType);
                }
            });

            // auto select event type if space event types === 1
            if (this.space.eventType.length === 1) {
                this.selectedEvent = this.eventTypes[0];
            }
        });

        // get space related amenities
        this.amenityService.getAmenitiesList().subscribe(amenities => {
            amenities.forEach(amenity => {
                let result = $.grep(this.space.amenity, function (id) {
                    return id == amenity.id;
                });
                if (result && result.length == 1) {
                    this.amenities.push(amenity);
                }
            });
        });

        //get space related extra amenities
        this.amenityService.getAmenitiesList().subscribe(amenities => {
            amenities.forEach(amenity => {
                let result = $.grep(this.space.extraAmenity, function (exAmenity) {
                    return exAmenity.amenityId == amenity.id;
                });
                if (result && result.length == 1) {
                    this.extraAmenities.push({amenity: amenity, rate: result[0].extraRate, count: 0, selected: false, amenityUnit: result[0].amenityUnit});
                }
            });

            console.log('extraAmenities', this.extraAmenities);
        });


        // get space rules
        this.rules.forEach(rule => {
            let result = $.grep(this.space.rules, function (id) {
                return id == rule.id;
            });
            if (result && result.length == 1) {
                this.spaceRules.push(rule);
            }
        });

        // get space cancellations policy
        this.cancellationPolicies.forEach(policy => {
            if (this.space.cancellationPolicy === policy.id) {
                this.cancellationPolicy = policy;
            }
        });

        this.menus = this.space.menuFiles.sort(function(a, b){
            return parseInt(a.menuId) - parseInt(b.menuId);
        });

        //initilize date
        this.today = moment();

        this.seatingParticipantCount = this.space.participantCount;

        // adding default seating arrangement if size is 1
        if (this.space.seatingArrangements.length === 1) {
            this.selectedSeatingOption = this.space.seatingArrangements[0];
            this.selectedGuestCount = +this.space.participantCount;
        }

    }

    ngAfterViewInit(){

        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            slideshow: true,
            slideshowSpeed: 3000
        });
        // Image carouse in banner
        var imgCarousel = $('#space-images-carousel').owlCarousel ({
            items: 10,
            nav: true,
            dots: false
        });

        $('.owl-carousel .owl-item a').css({
            display: 'inline-block',
            background: 'rgba(0, 0, 0, 0.2)'
        })

        $('.space-profile-banner').css({
            background: 'url('+environment.bucket+'/'+this.space.images[0]+')',
            backgroundSize: 'cover'
        })

        // Similar spaces owl carousel
        var similarSp = $('#similar-places-carousel').owlCarousel ({
            items: 3,
            nav: true,
            dots: false,
            navText: [
                "<i class='navigation-arrow fa fa-chevron-left'></i>",
                "<i class='navigation-arrow fa fa-chevron-right'></i>"
            ],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 2,
                    nav: true,
                    loop: false
                }
            }
        });

        similarSp.find('.navigation-arrow ').css({
            fontSize: '22px'
        });

        $('.space-images-section').css('display', 'inline-block');
        
        
        let $window = $(window);
        
        let viewPortHeight = $window.innerHeight(),
        viewportWidth = $window.innerWidth(),
        $bookingCalendarWrapper = $('.booking-calendar-wrapper'),
        $parallaxBanner = $('.parallax-slider');


        // Duplicated code should be removed
        if ($window.innerWidth () > 992) {

            // parallax banner takes viewport width and viewport height.
            $parallaxBanner.css({
                'height': $window.innerHeight() + 20,
                'width': $window.innerWidth()
            });

            // makes calendar booking stick to top border of viewport
            $window.on ('scroll', function () {
                if ($window.scrollTop () + 40 >= $window.innerHeight()) {
                    $bookingCalendarWrapper.addClass ('booking-calendar-wrapper-show').css('top', '50px');
                } else {
                    $bookingCalendarWrapper.removeClass ('booking-calendar-wrapper-show').css('top', 0);
                }
            });

            let navigationHeight = $('.main-navigation').outerHeight(),
                bookingCalendarFulldayWrapperHeight = $('.booking-calendar-header').outerHeight(),
                //bookingCalendarTotalWrapperHeight = $('.total-price-wrapper').outerHeight(),
                eventSelectorWrapperHeight = $('.event-type-calendar-selectors-wrapper').outerHeight(),
                bookingBtnHeight = $('.book-this-space').outerHeight(),
                selectedGuestCountHeight = $('.guest-count').outerHeight (),
                scrollableHeight = viewPortHeight - (10 + navigationHeight + bookingBtnHeight + bookingCalendarFulldayWrapperHeight + selectedGuestCountHeight + eventSelectorWrapperHeight);


        } else {


            $parallaxBanner.css({
                'height': ($window.innerHeight() / 5) * 4
            });

        }

        // targets if viewport size is greater than 678px ---> desktop
        $window.on('resize', function () {
        
            let viewPortHeight = $window.innerHeight(),
                viewportWidth = $window.innerWidth(),
                $bookingCalendarWrapper = $('.booking-calendar-wrapper'),
                $parallaxBanner = $('.parallax-slider');

            if ($window.innerWidth () > 992) {

                // parallax banner takes viewport width and viewport height.
                $parallaxBanner.css({
                    'height': $window.innerHeight() + 20,
                    'width': $window.innerWidth()
                });

                // makes calendar booking stick to top border of viewport
                $window.on ('scroll', function () {
                    if ($window.scrollTop () + 40 >= $window.innerHeight()) {
                        $bookingCalendarWrapper.addClass ('booking-calendar-wrapper-show').css('top', '50px');
                    } else {
                        $bookingCalendarWrapper.removeClass ('booking-calendar-wrapper-show').css('top', 0);
                    }
                });

                let navigationHeight = $('.main-navigation').outerHeight(),
                    bookingCalendarFulldayWrapperHeight = $('.booking-calendar-header').outerHeight(),
                    //bookingCalendarTotalWrapperHeight = $('.total-price-wrapper').outerHeight(),
                    eventSelectorWrapperHeight = $('.event-type-calendar-selectors-wrapper').outerHeight(),
                    bookingBtnHeight = $('.book-this-space').outerHeight(),
                    selectedGuestCountHeight = $('.guest-count').outerHeight (),
                    scrollableHeight = viewPortHeight - (10 + navigationHeight + bookingBtnHeight + bookingCalendarFulldayWrapperHeight + selectedGuestCountHeight + eventSelectorWrapperHeight);


            } else {


                $parallaxBanner.css({
                    'height': ($window.innerHeight() / 5) * 4
                });

            }
        
        })

        $('.map-wrapper').height(180);

        // Owl carousel specific stuff
        $('.owl-stage').css('display', 'inline-block').parent().css('text-align', 'center');
        $('.owl-carousel .owl-item').css({
            cursor: 'pointer'
        });
        $('.owl-carousel .owl-item img').css({
            padding: '5px'
        });
        $('.space-images-section .owl-theme .owl-nav, .similar-places-carousel .owl-theme .owl-nav').css({
            textAlign: 'right'
        });
        $(".owl-theme .owl-nav [class*='owl-']").css({
            background: '#64a4ff'
        });
        $('.owl-prev').css({
            background: 'none',
            position: 'absolute',
            top: '25%',
            left: '-30px',
            color: '#5b92f1'
        });
        $('.owl-next').css({
            background: 'none',
            position: 'absolute',
            top: '25%',
            right: '-30px',
            color: '#5b92f1'
        });
        $( ".space-images-section .owl-prev").html('<i class="fa fa-chevron-left"></i>').css({'color': '#fff', top: '20%'});
        $( ".space-images-section .owl-next").html('<i class="fa fa-chevron-right"></i>').css({'color': '#fff', top: '20%'});
        // Handle book now in mobile devices
        /* $('.mobile-book-now-btn-wrapper').on('click', function (e) {
         e.preventDefault ();
         $('.booking-calendar-wrapper').toggleClass('booking-calendar-wrapper-mobile');
         $('.mobile-book-now-btn-wrapper').hide();
         });*/

        // Handle calendar close hit
        /*$('.calendar-close').on ('click', function (e) {
         e.preventDefault ();
         $('.booking-calendar-wrapper').removeClass('booking-calendar-wrapper-mobile');
         $('.mobile-book-now-btn-wrapper').show();
         });*/

        $('.per-block-wrapper, .per-hour-wrapper, .extra-wrapper').hide();

        // magnificPopup initialization - Banner
        var spaceImageMagnificPopupState = null;

        $('.space-images-carousel-item').magnificPopup({
            type: 'image',
            gallery: {
                enabled:true
            },
            zoom: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    spaceImageMagnificPopupState = this;
                }
                ,close: function() {
                    spaceImageMagnificPopupState = null;
                }
            }
        });

        // magnificPopup initialization - menu
        var menuItemImageMagnificPopupState = null;

        $('.menu-items').magnificPopup({
            type: 'image',
            gallery: {
                enabled:true
            },
            zoom: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    menuItemImageMagnificPopupState = this;
                }
                ,close: function() {
                    menuItemImageMagnificPopupState = null;
                }
            }
        });

        $(window).on("popstate", function (event, state) {
            if(spaceImageMagnificPopupState != null){
                spaceImageMagnificPopupState.close();
            }
            if(menuItemImageMagnificPopupState != null){
                menuItemImageMagnificPopupState.close();
            }
        });

        $('.mobile-book-now-btn').on ('click', function () {
            if(menuItemImageMagnificPopupState != null){
                menuItemImageMagnificPopupState.close();
            }
        });

        $('.go-to-arrow-wrapper').on ('click', function () {
            $('html,body').animate({scrollTop: $(".space-profile-information-wrapper").offset().top - 40},'slow');
        });


        if (this.getMobileOperatingSystem () === 'iOS') {
            $('.mobile-book-now-btn-wrapper').css({
                bottom: '20px'
            })
        }

        window.scrollTo(0, 0);

        if(this.storageService.getItem('ipgTime')){
            this.storageService.removeItem('ipgTime');
        }

    }

    displayBookingEngine(){
        $('.booking-calendar-wrapper').toggleClass('booking-calendar-wrapper-mobile');
        $('.mobile-book-now-btn-wrapper').hide();
        $('.space-profile-information-wrapper').hide();
    }

    displaySpaceProfile(){
        $('.booking-calendar-wrapper').removeClass('booking-calendar-wrapper-mobile');
        $('.mobile-book-now-btn-wrapper').show();
        $('.space-profile-information-wrapper').show();
    }

    showScheduler(event){
        this.calendarView = false;
        this.spaceTotal = 0;
        this.discountAmount = 0;
        this.promoError = null;
        this.promoSuccess = null;

        if(this.availabilityMethod === 'HOUR_BASE'){
            this.spaceAvailableForBooking = false;
            this.isAvailabilityChecking = false;
            this.checkedAvailability = false;
            this.isTimeRangeValid = true;
            this.isInsideNoticePeriod = false;
            this.havingNetworkError = false;

        } else if(this.availabilityMethod === 'BLOCK_BASE'){
            this.resetAmenityValues(this.extraAmenities);
        }



    }

    showCalendar(){

        this.calendarView = true;
        this.spaceTotal = 0;
        this.totalHoursCount = 0;
        this.amenityCost = 0;
        this.perHourAmenityCost = 0;
        this.isAvailabilityChecking = false;
        this.checkedAvailability = false;
        this.selectedBlocks = [];
        this.discountAmount = 0;
        this.promoError = null;
        this.promoSuccess = null;

        if(this.extraAmenities.length > 0){
            this.extraAmenities.forEach(exAmenity => {
                if(exAmenity.selected && exAmenity.amenityUnit === 1){
                    exAmenity.selected = false;
                }
            })
        }

        // reset from and end event to 'FROM' 'END' when select scheduler => calendar
        // we use 'schedulerData' from and end time for the timepicker
        if(this.availabilityMethod === "HOUR_BASE"){

            //this will prevent calling checkState()
            this.startTimeHours = 0;
            this.endTimeHours = 0;
        }

        $('.calendar-component-wrapper').show();
        $('.per-block-wrapper, .per-hour-wrapper, .extra-wrapper').hide();

    }

    spaceTotal: number = 0;
    calendarView = true;
    selectedBlocks = [];


    amenityCost: number = 0; // without per hour amenities
    increaseAmenityCount(exAmenity){

        console.log('amenity', exAmenity);
        exAmenity.count++;

        //per person or per unit
        if(exAmenity.amenityUnit === 2 || exAmenity.amenityUnit === 3){
            this.amenityCost += exAmenity.rate;
        }
        /*else if(exAmenity.amenity.amenityUnitsDto.id === 1){
         this.amenityCost += (exAmenity.rate * this.totalHoursCount);
         }*/
    }

    decreaseAmenityCount(exAmenity){

        if(exAmenity.count === 0){
            return;
        }
        exAmenity.count--;
        //per person
        if(exAmenity.amenityUnit === 2 || exAmenity.amenityUnit === 3){
            this.amenityCost -= exAmenity.rate;
        }
        /*else if(exAmenity.amenity.amenityUnitsDto.id === 1){
         this.amenityCost -= (exAmenity.rate * this.totalHoursCount);
         }*/
    }

    calculateAmenityCost(exAmenity){
        return exAmenity.count * exAmenity.rate;
    }

    selectEvent(event){
        this.selectedEvent = event;
    }

    fieldsNotValid = false;
    isLoading = false;

    proceedToPayment() {

        try {


            if (!this.selectedGuestCount || !this.selectedEvent || !this.selectedSeatingOption) {
                this.fieldsNotValid = true;
                alert('Please enter required fields');
                return;
            }

            if (this.selectedGuestCount < 1) {
                this.fieldsNotValid = true;
                alert('Expected guest count should be greater than 0.');
                return;
            }

            //validation 1
            if (!this.userService.getUser()) {
                this.showLoginModal();
                return;
            }

            // validation 2
            if (this.space.user === this.userService.getUser().id) {
                this.createInfoModalComponent(`Hi ${this.userService.getUser().name}`, 'You can not request your own spaces');
                return;
            }

            this.fieldsNotValid = false;

            let spaceId = this.space.id;
            let spaceName = this.space.name;
            let spaceAddress = this.space.addressLine1;
            let organization = this.space.addressLine2;
            let grandTotal = (this.spaceTotal + this.amenityCost + this.perHourAmenityCost) - this.discountAmount;
            let eventId = this.selectedEvent.id;
            let seatingId = this.selectedSeatingOption.id;
            let guestCount = this.selectedGuestCount;

            let exAmenityArray = this.extraAmenities.map(extraAmenity => {
                if (extraAmenity.amenityUnit === 1 && extraAmenity.selected) {
                    return {amenityId: extraAmenity.amenity.id, number: this.totalHoursCount};
                }
                return {amenityId: extraAmenity.amenity.id, number: extraAmenity.count}
            });

            let dates = [];
            if (this.availabilityMethod === 'BLOCK_BASE') {
                dates = this.selectedBlocks.map(selecetBlock => {
                    return {
                        fromDate: moment(selecetBlock.day).format('YYYY-MM-DDT') + selecetBlock.block.from.substring(0, 5),
                        toDate: moment(selecetBlock.day).format('YYYY-MM-DDT') + selecetBlock.block.to.substring(0, 5)
                    }
                });

            } else if (this.availabilityMethod === 'HOUR_BASE') {
                dates.push({
                });
            }

            var fromDate;
            if (dates.length > 1) {
                fromDate = new Date(dates[0].fromDate.replace("T", " "));
                for (var i = 0; dates.length < i; i++) {
                    if (fromDate.getTime() > new Date(dates[i].fromDate.replace("T", " ")).getTime()) {
                        fromDate = new Date(dates[i].fromDate.replace("T", " "));
                    }
                }
            } else {
                fromDate = new Date(dates[0].fromDate.replace("T", " "))
            }

            var timeDiff = Math.abs(fromDate.getTime() - new Date().getTime());
            var diffDays = Math.ceil(timeDiff / 36e5);

            const dateString = dates
                .map(date => moment(date.fromDate).format('h:00 A') + '-' + moment(date.toDate).format('h:00 A'))
                .join();

            let bookingRequest = {
                "space": spaceId,
                "eventType": eventId,
                "seatingArrangement": seatingId,
                "guestCount": guestCount,
                "dates": dates,
                "bookingWithExtraAmenityDtoSet": exAmenityArray,
                "bookingCharge": grandTotal.toFixed(2),
                "promoCode": this.promoCode
            }

            let paymentSummaryData = {
                bookingId: null,
                referenceId: null,
                space: spaceName,
                organization: organization,
                address: spaceAddress,
                reservationTime: dateString,
                total: grandTotal.toFixed(2),
                dateDiff: diffDays
            }
            console.info('paymentSummaryData', paymentSummaryData)
            this.isLoading = true;

            setTimeout(() => {
                this.bookNow(bookingRequest, paymentSummaryData);
            }, 1000)

        } catch (error) {
            console.error('error', error)
        }

    }

    bookNow(bookingRequest: any, paymentSummaryData: any){
        this.bookingService.bookSpace(bookingRequest).subscribe(
            response => {
                if(response.id){


                    /*if (response.bookingCharge === 0 && response.promotion && response.promotion.type === 'NORMAL') {

                        this.promotionPay(response.id).subscribe(res => {
                            if (res.old_state === 'INITIATED' && res.new_state === 'PAYMENT_DONE') {
                                this.router.navigate(['spaces/promotion/booking/success']);
                                return;
                            } else {
                                alert('Something went wrong');
                                this.router.navigate(['home']);
                                return;
                            }
                        });
                    }*/

                    paymentSummaryData.bookingId = response.id;
                    paymentSummaryData.referenceId = response.orderId;
                    paymentSummaryData.payLaterEnabled = response.isPayLaterEnabled;
                    paymentSummaryData.manualPaymentAllowed = response.isBookingTimeEligibleForManualPayment;
                    paymentSummaryData.promotion = (response.promotion) ? JSON.stringify(response.promotion) : null;

                    this.routeDataService.clearData();
                    this.routeDataService.paymentSummaryData = paymentSummaryData;

                    this.router.navigate(['payments/order/summary']);

                }else if(response.error){
                    this.toaster.error(response.error, 'Error');
                    //booking not allowed
                    //this.spaceAvailableForBooking = false;
                }else{
                    this.toaster.error("Something went wrong!", "Error");
                }
            },
            (error: MSError) => {
                console.log('error', error)
                if(error instanceof BadRequestError){
                    this.toaster.error('Something went wrong with the request', 'Error');
                }
                else throw error;
            },
            () => {
                this.isLoading = false;
            });
    }


    showLoginModal() {
        this.modalComponentData = {
            component: SigninModalComponent,
            inputs: {
                navigateTo: 'spaces/show/' + this.space.id,
            }
        }
    }

    createInfoModalComponent(heading, body) {
        this.modalComponentData = {
            component: InfoModalComponent,
            inputs: {
                heading: heading,
                body: body
            }
        }
    }

    isTimeRangeValid = true;
    isInsideNoticePeriod = false;
    havingNetworkError: boolean = false;

    resetAmenityValues(exAmenities){
        this.amenityCost = 0;
        exAmenities.forEach(amenity => amenity.count = 0);
    }

    canProceedToPayments(){
        //return this.spaceAvailableForBooking && this.spaceTotal != 0
        return this.spaceAvailableForBooking && this.totalHoursCount != 0
    }

    totalHoursCount: number;

    resetSpaceStates(){
        this.isAvailabilityChecking = false;
        this.checkedAvailability = true;
        this.resetAmenityValues(this.extraAmenities);
    }

    goToSimilarSpace(space){
        var win = window.open("/#/spaces/"+space.id+'/'+ MsUtil.replaceWhiteSpaces(space.name, '-'), '_blank');
        win.focus();
    }

    onSelectBlock(event){
        this.spaceTotal = event.blockCost;
        this.selectedBlocks = event.selectedBlocks;
        this.totalHoursCount = +event.totalHoursCount;

        this.perHourAmenityCost = 0;

        if(this.spaceTotal === 0){
            this.extraAmenities.forEach(exAmenity => {
                exAmenity.selected = false;
                exAmenity.count = 0;
                this.amenityCost = 0;
            });
        }

        this.extraAmenities.forEach(exAmenity => {
            exAmenity.count = 0;
            this.amenityCost = 0;
        });

        this.extraAmenities.forEach(exAmenity => {
            if(exAmenity.selected && exAmenity.amenityUnit === 1){
                //exAmenity.selected = false;
                this.perHourAmenityCost += exAmenity.rate * this.totalHoursCount;
            }
        })

        if(event.selectedBlocks.length === 0){
            this.checkedAvailability = false;
            return;
        }

        let selectedDates = event.selectedBlocks.map(dayBlock => {
           return {
               fromDate: moment(dayBlock.day).format('YYYY-MM-DDT') + dayBlock.block.from,
               toDate: moment(dayBlock.day).format('YYYY-MM-DDT') + dayBlock.block.to
           }
        });

        let availabilityRequest = {
            space: this.space.id,
            dates: selectedDates
        }

        //console.log('event.selectedBlocks', availabilityRequest);

        setTimeout(() => {
            this.spaceService.getSpaceAvailablility(availabilityRequest).subscribe(resp => {
                console.log('available', resp);
                if(resp.response){
                    this.spaceAvailableForBooking = true;
                }else{
                    this.spaceAvailableForBooking = false;
                    this.reservationError = resp.reason;
                }
                this.resetSpaceStates();
            }, err => {
                this.resetSpaceStates();
            });
        }, 500)

    }

    setSeatingOption(option){
        console.log(option);
        this.zone.run(() => {
            this.selectedSeatingOption = option;
            this.seatingParticipantCount = +option.participantCount;
        });

    }

    perHourAmenityCost: number = 0;

    onChangeAmenity(exAmenity){

        exAmenity.selected = exAmenity.selected || null;

        if(exAmenity.selected){
            exAmenity.selected = false;
            this.perHourAmenityCost -= exAmenity.rate * this.totalHoursCount;
        }else{
            exAmenity.selected = true;
            this.perHourAmenityCost += exAmenity.rate * this.totalHoursCount;
        }

    }

    onSelectTimeRange(event){
        console.log('eventData', event.eventData);
        let eventData = event.eventData;

        if(!eventData){
            this.spaceTotal = 0;
            this.checkedAvailability = false;
            return;
        }

        this.spaceTotal = 0;
        this.amenityCost = 0;
        this.perHourAmenityCost = 0;


        this.isAvailabilityChecking = true;


        // reset hour base amenities
        if(this.extraAmenities.length > 0) {
            this.extraAmenities.forEach(exAmenity => {
                exAmenity.selected = false;
            })
        }


        this.spaceAvailability.from = eventData.from;
        this.spaceAvailability.to = eventData.to;

        let availabilityRequest = {
            space: this.space.id,
            dates:[{
                fromDate: moment(eventData.day).format('YYYY-MM-DDT') + eventData.from + 'Z',
                toDate: moment(eventData.day).format('YYYY-MM-DDT') + eventData.to + 'Z'
            }],
        }

        console.log('availabilityRequest', availabilityRequest);

        setTimeout(() => {
            this.spaceService.getSpaceAvailablility(availabilityRequest).subscribe(resp => {
                console.log('available', resp);
                if(resp.response){
                    this.spaceAvailableForBooking = true;
                    let hoursCount: number = +eventData.to.split(':')[0] - +eventData.from.split(':')[0];
                    this.totalHoursCount = hoursCount;
                }else{
                    this.spaceTotal = 0;
                    this.spaceAvailableForBooking = false;
                    this.reservationError = resp.reason;
                }

                this.resetSpaceStates();
            }, err => {
                console.log('network error!!', err);
                this.havingNetworkError = true;
                this.resetSpaceStates();
            });
        }, 500)

    }

    formatTo12h(input: string){

        let hours = +input.split(':')[0];
        let minutes = input.split(':')[1];

        let formatted = (hours - ((hours == 0)? -12 : (hours <= 12)? 0 : 12)) + ':' + minutes + (hours < 12 ? "AM" : "PM");

        return formatted
    }

    getAmenityUnitName(unitId){
        let name = '';
        switch (unitId) {
            case 1: name= 'Per hour'; break;
            case 2: name= 'Per person'; break;
            case 3: name= 'Per unit'; break;
            default: name = 'Per unit'; break;
        }
        return name;
    }



    setSelectedGuestCount(event){
        this.zone.run(() => {
            let tempGuestCount = event.target.value;
            if(tempGuestCount.charAt(0) == '0'){
                tempGuestCount = parseInt(tempGuestCount, 10);
            }
            if(tempGuestCount.length > event.target.attributes["maxlength"].value){
                tempGuestCount = tempGuestCount.slice(0,event.target.attributes["maxlength"].value);
            }
            event.target.value = tempGuestCount;

            // uncheck all chargable amenities
            if(this.extraAmenities.length > 0){
                this.extraAmenities.forEach(exAmenity => {
                    if(exAmenity.selected && exAmenity.amenityUnit === 1){
                        exAmenity.selected = false;
                    }
                })
            }

            // if(this.space.availabilityMethod === 'BLOCK_BASE'){
            //     if(this.schedulerData.availability){
            //         this.schedulerData.availability.forEach(block => {
            //             block.selected = false;
            //         });
            //         this.blockItems.resetBlockCost();
            //     }
            // }else if(this.space.availabilityMethod === 'HOUR_BASE' && this.scheduler){
            //     this.scheduler.clearActiveSelectors();
            // }

            this.totalHoursCount = 0;
            this.selectedBlocks = [];
            this.spaceTotal = 0;
            this.perHourAmenityCost = 0;
            this.selectedGuestCount = event.target.value;
            this.discountAmount = 0;
            this.promoError = null;
            this.promoSuccess = null;
        })
    }

    isPromoCodeChecking = false;
    discountAmount: number = 0;
    promoError: string;
    promoSuccess: string;
    promoCode: string;

    verifyPromo(promoCode: string){
        console.log('promoCode', promoCode);
        this.isPromoCodeChecking = true;

        let payload = {
            space: this.space.id,
            promoCode
        }

        this.spaceService.verifyPromoCode(payload)
            .subscribe(resp => {
                console.log('resp', resp.status);

                if (resp.status === 1) {
                    //promo code is valid
                    const flatDiscount = resp.promoDetails.isFlatDiscount;

                    if (flatDiscount === 0) {
                        this.discountAmount = Math.floor( this.getGrandTotal() * (resp.promoDetails.discount / 100) );

                    } else if (flatDiscount === 1) {
                        this.discountAmount = this.getFlatDiscountAmount(resp.promoDetails.discount);
                    }
                    this.promoError = null;
                    this.promoCode = promoCode;
                    this.promoSuccess = resp.message;
                    this.isPromoCodeChecking = false;
                }

                else if (resp.status === 2) {
                    //promo code is invalid
                    this.promoSuccess = null;
                    this.discountAmount = 0;
                    this.promoError = "Invalid promo code";
                    this.isPromoCodeChecking = false;
                }

                else if (resp.status === 3) {
                    //promo code is invalid
                    this.promoSuccess = null;
                    this.discountAmount = 0;
                    this.promoError = "Invalid promo code";
                    this.isPromoCodeChecking = false;
                }
            })
    }


    getFlatDiscountAmount (discountAmount: number) {
        let calAmount = 0;
        let totalAmount = this.getGrandTotal();

        if (totalAmount < discountAmount) {
            calAmount = totalAmount;
        } else {
            calAmount = discountAmount;
        }
        return calAmount;
    }


    getGrandTotal(): number {
        return this.spaceTotal + this.amenityCost + this.perHourAmenityCost;
    }

    get totalAmount(): number {

        const totalBookingAmount = this.spaceTotal + this.amenityCost + this.perHourAmenityCost;

        if (totalBookingAmount < this.discountAmount) {
            return 0;
        }

        return totalBookingAmount - this.discountAmount;
    }

    showPreviousReview() {
        if(this.selectedUserReviewIndex === 0) {
            return;
        }
        this.selectedUserReview = this.userReviews[--this.selectedUserReviewIndex];
        console.log('this.selectedUserReviewIndex', this.selectedUserReviewIndex);
    }

    showNextReview() {
        if(this.selectedUserReviewIndex === (this.userReviews.length - 1)) {
            return;
        }
        this.selectedUserReview = this.userReviews[++this.selectedUserReviewIndex];
        console.log('this.selectedUserReviewIndex', this.selectedUserReviewIndex);
    }



    newBookNow() {
        const bookingData = {
            spaceName: this.space.name,
            seatingTypes: this.space.seatingArrangements,
            eventTypes: this.eventTypes,
            extraAmenities: this.extraAmenities,
            participantCount: this.space.participantCount,
            minGuestCount: this.space.minParticipantCount ? this.space.minParticipantCount: 1,
            orgName: this.space.addressLine2,
            address: this.space.addressLine1,
            spaceId: this.space.id,
            ownerId: this.space.user,
            availability: this.space.availability,
            futureBookingDates: this.futureBookingDates,
            availabilityMethod: this.space.availabilityMethod,
            advanceOnlyEnable: this.space.advanceOnlyEnable,
            blockChargeType: this.space.blockChargeType,
            calendarEnd: this.space.calendarEnd
        }
        this.routeDataService.bookingData = bookingData;
        this.router.navigate(['/booking/select-date']);
    }

    promotionPay(bookingId: number) {
        return this.bookingService.updateBooking({
            booking_id : bookingId,
            event: 1,
            method: 'MANUAL',
            status: 4
        });
    }

}
