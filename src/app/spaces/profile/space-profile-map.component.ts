import {Component, AfterViewInit, Input} from "@angular/core";

declare let google: any;

@Component({
    selector: 'space-map',
    template: `<div id="space-profile-map"></div>`,
    styles: [`
        #space-profile-map {
            height: 200px;
            width: 100%;
       }
    `]
})

export class SpaceProfileMapComponent implements AfterViewInit{

    @Input('lat') lat;
    @Input('lng') lng;
    @Input('address') address;

    ngAfterViewInit(){
        this.initMap();
    }

    initMap() {
        let location = {lat: +this.lat, lng: +this.lng};
        let map = new google.maps.Map(document.getElementById('space-profile-map'), {
            zoom: 15,
            center: location,
            gestureHandling: 'greedy',
            styles: [{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"},{"hue":"#ff0000"}]}]
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: this.normalIcon()
        });

        var infowindow = new google.maps.InfoWindow({
            content: this.address
        });

        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

        setTimeout(function() {
            google.maps.event.trigger(map, "resize");
        }, 100);
    }

    normalIcon() {
        return {
            url: 'https://millionspaces.com/assets/img/map/32-32-normal.png'
        };
    }
}