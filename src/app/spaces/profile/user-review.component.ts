import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";

declare let moment;

@Component({
    selector: 'user-review',
    templateUrl: './user-review.component.html',
    styleUrls: ['./space-profile.component.css']

})
export class UserReviewComponent implements OnChanges {

    starServices: number;
    startCleanliness: number;
    starValueForMoney: number;
    startMSExp: number;

    starCountArray = [];

    @Input('reviewDetail') reviewDetail;

    ngOnChanges(changes: SimpleChanges) {

        const rateArray = changes['reviewDetail'].currentValue.rate.split('|');

        this.starServices = +rateArray[1];
        this.startCleanliness = +rateArray[2];
        this.starValueForMoney = +rateArray[3];
        this.startMSExp = +rateArray[4];
        this.starCountArray = Array(5).fill(0);
    }

    get reviewedDate() {
        return moment(this.reviewDetail.createdAt).format('MMMM YYYY');
    }

    get profileImgUrl() {

        if(this.reviewDetail.userImg) {
            return this.reviewDetail.userImg;
        } else {
            return '../../assets/img/no-prof-pic.png';
        }
    }


}