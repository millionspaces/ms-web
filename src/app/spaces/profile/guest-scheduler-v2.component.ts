import {
    Component, ElementRef, Output, Input, EventEmitter, OnChanges,
    SimpleChanges, NgZone
} from "@angular/core";

declare let $: any;
declare let moment: any;

@Component({
    selector: 'guest-scheduler-v2',
    template: `
        <div id="scheduler-wrapper"></div>
    `,
    styles:[`
        
    `]
})
export class GuestSchedulerComponentV2 implements OnChanges {

    guestShe: any;
    @Input('schedulerData') schedulerData: any;
    @Output('onSelectTimeRange') onSelectTimeRange = new EventEmitter<any>();
    dayData;
    selectedDate = moment();
    disabledPeriods = [];
    startTime: number = 0;
    endTime: number = 24;

    constructor(private _elementRef: ElementRef, private ngZone: NgZone){

    }


    ngOnChanges(changes: SimpleChanges){

        this.resetGlobalValues();

        console.log('schedulerData', changes['schedulerData'].currentValue);

        this.dayData = changes['schedulerData'].currentValue;

        if(!this.dayData){
            return;
        }

        let dayData = this.dayData;

        if(dayData.eventsPerDay){
            if(dayData.eventsPerDay.length > 0){
                this.disabledPeriods = dayData.eventsPerDay.map(event => {
                    return {
                        from: +event.from.split(':')[0] - dayData.bufferTime,
                        to: +event.to.split(':')[0] + dayData.bufferTime,
                        guest: !event.isManual
                    }
                });
            }

            // disable same day past times
            if(moment(dayData.fullDay).date() === moment().date()){
                let dayStartHour = +dayData.from.split(':')[0];
                let nowHour = moment().hour() + 1;

                this.disabledPeriods.push({
                    from: dayStartHour,
                    to: nowHour,
                    guest: true
                });
            }

            this.startTime = +dayData.from.split(':')[0];
            this.endTime = +dayData.to.split(':')[0];
        }

        let scheduler = this._elementRef.nativeElement.querySelector('#scheduler-wrapper');

        this.guestShe = (<any>$(scheduler)).msScheduler({
            disabledHours: [],
            getHours:  (from, to, isRange) => {
                if(!from || !to){
                    this.ngZone.run(() => {
                        console.log('XX');
                        this.onSelectTimeRange.emit({eventData: null});
                    });
                    return;
                }
                //console.log(from, to, isRange);
                this.ngZone.run(() => {
                    this.onSelectTimeRange.emit({eventData: { day:this.dayData.fullDay, from: this.zeroPad(from, 2)+':00', to: this.zeroPad(to, 2)+':00' }});
                });
            }
        });

        this.reRenderScheduler(this.getSchedulerData());
    }

    reRenderScheduler({selectedDate, startTime, endTime, disabledPeriods}){
        let scheduler = this._elementRef.nativeElement.querySelector('#scheduler-wrapper');

        (<any>$(scheduler)).msScheduler("options", 'selectedDate', moment(selectedDate).format('Do MMMM YYYY - dddd'));
        (<any>$(scheduler)).msScheduler("options", 'startTime', startTime);
        (<any>$(scheduler)).msScheduler("options", 'endTime', endTime);
        (<any>$(scheduler)).msScheduler("options", 'disabledHours', disabledPeriods);
    }

    getSchedulerData(){
        return {
            selectedDate: this.dayData.date,
            startTime: this.startTime,
            endTime: this.endTime,
            disabledPeriods: this.disabledPeriods
        }
    }

    resetGlobalValues(){
        this.selectedDate = moment();
        this.startTime = 0;
        this.endTime = 23;
        this.disabledPeriods = [];
    }

    clearActiveSelectors(){
        let scheduler = this._elementRef.nativeElement.querySelector('#scheduler-wrapper');
        (<any>$(scheduler)).msScheduler('options', 'clearActive')
    }

    zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }


}