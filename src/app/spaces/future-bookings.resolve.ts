import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {SpaceService} from "./shared/space.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class FutureBookingsResolve implements Resolve<any>{

    constructor(
        private spaceService: SpaceService,
    ){}

    resolve(route: ActivatedRouteSnapshot) : Observable<any>{
        return this.spaceService.getFutureBookingDates(route.params['id']);
    }


}