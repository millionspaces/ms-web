import {PipeTransform, Pipe} from "@angular/core";
import {Space} from "../shared/space.model";

@Pipe({name:"eventTypes"})
export class SpacesEventTypePipe implements PipeTransform{

    transform(spaces: Space[], args: any[]){
        let filteredSpaces = [];
        //let selectedEventTypeIds: any[] = args[0];
        let selectedEventTypeIds = [7];
        let keepGoing = true;

        spaces.forEach(space => {
            console.log(space.eventType);
            keepGoing = true;
            space.eventType.forEach(eventTypeId => {
                selectedEventTypeIds.forEach(selectedId => {
                    if(keepGoing){
                        if(selectedId === eventTypeId){
                            filteredSpaces.push(space);
                            keepGoing = false;
                        }
                    }
                });
            })
        })

        return filteredSpaces;
    }
}