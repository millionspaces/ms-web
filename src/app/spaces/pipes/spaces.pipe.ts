import {PipeTransform, Pipe} from "@angular/core";
import {Space} from "../shared/space.model";



@Pipe({name:"spaces"})
export class SpacesPipe implements PipeTransform{

    transform(spaces: Space[], name: string): any {
        if(!spaces || !name)
            return spaces;

        let returnSpaces = [];

        spaces.filter(space => {
            if(space.name.toUpperCase().indexOf(name.toUpperCase()) >= 0){
                returnSpaces.push(space);
            }
        })

        return returnSpaces;
    }

}