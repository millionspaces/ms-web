import { Component, OnInit, NgZone, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Space } from "../shared/space.model";
import { RouteDataService } from "../../shared/services/route.data.service";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

declare let $: any;
declare let google: any;
declare let moment: any;

@Component({
    templateUrl: 'space-grid.component.html',
    styles: [`

         /* Common */
         #page-content-wrapper {
            background: #e2e2e2;
         }

         /* Toolbar **/
         .space-list-toolbar {
            height: 60px;
            width: 100%;
            background: #64a4ff;
            position: fixed;            
            top: 49px;
            left: 0;
            z-index: 999;
         }
         .space-list-toolbar.space-list-toolbar-landing {
            display: none;
         }
         .spaces-list-wrapper.spaces-list-wrapper-landing {
            margin-top: -60px;
         }
         .page-count-info {
            font-size: 12px;
            margin-bottom: 10px;
            color: #fff;
            margin-bottom: 0;
            margin-left: 0;
         }
         .reset-button-wrapper {
            height: 100%;
            display: inline-block;
            width: 0;
            background: #f8f8f8;
            vertical-align: middle;
            position: relative;
            cursor: pointer;
            transition: background 0.2s;
            display: inline-block;
         }
         .reset-button-wrapper {
           width: 100%;
         }
         @media only screen and (min-width: 992px) {
            .reset-button-wrapper {
                width: 25%;
            }
         }
         .reset-button-wrapper:hover {
            background: #d6d6d6;
         }
         .reset-button-wrapper > a {
            display: inline-block;
            position: absolute;
            top: 32%;
            left: 50%;
            margin-left: -56px;
            color: #515153;
            transition: color 0.2s;
         }
         .reset-button-wrapper:hover > a {
            color: #787878;
         }
         /* Sorting constrols and desctiption */
         .sorting-ctrls-and-description-bar-wrapper {
            display: inline-block;
            width: 100%;
            background: #64a4ff;
            padding: 10px;
         }
         .sorting-ctrls-and-description-bar-wrapper.hide-desc-bar {
             display: none;
         }
         @media only screen and (min-width: 992px) {
            .sorting-ctrls-and-description-bar-wrapper {
                display: inline-block;
                width: 75%;
             }
         }
         .sorting-ctrls-and-description-bar-wrapper > div {
             display: inline-block;
         }
         .pagination > li > a {
            border-radius: 0;
         }
         .sorting-controls-wrapper {
             box-sizing: border-box;
             display: inline-block;
             width: 50%;
             text-align: right;
             padding-right: 0;
             color: #fff;
         }
         @media only screen and (min-width: 992px) {
            .sorting-controls-wrapper {
                width: 60%;
            }
         }
         .sorting-select > p {
            display: inline-block;
         }
         .sorting-select > select {
             border: 1px solid #fff;
             background: rgba(0, 0, 0, 0);
             padding: 10px;
             width: 200px;
             margin-left: 10px;
         }
         .sorting-select > select > option {
             color: #000;
         }
         .page-count-info-wrapper {
             display: inline-block;
             width: 50%;
         }
         @media only screen and (min-width : 992px) {
            .page-count-info-wrapper {
                width: 40%;
            }
            .pagination-wrapper {
                text-align: right;
                adding-top: 0;
            }
            .hide-on-mobile {
                display: block;
            }
         }
         .reset-button-wrapper input {
            margin-top: 6px;
            margin-left: 3px;
            padding-left: 5px;
            border: none;
            height: 50px;
            width: 98.5%;
         }
         @media only screen and (min-width : 992px) {
            .reset-button-wrapper input {
                width: 97%
            }
         }
         .reset-button-wrapper i {
            position: absolute;
            top: 20px;
            right: 20px;
            color: #d6d6d6;
            font-size: 20px;
         }
         
        .sorting-controls-wrapper .dropdown-menu {
            right: 0;
        }
        .sorting-controls-wrapper .dropdown, .sorting-controls-wrapper .dropup {
            display: inline-block;
        }
        .sorting-controls-wrapper button {
            width: 159px;
            border: 1px solid #fff;
        }
        .spaces-list-wrapper.no-results,
        .spaces-list-wrapper.no-results .spaces-list-wrapper-inner {
            background: #fff;
        }
    `]
})
export class SpaceGridComponent implements OnInit, OnDestroy {

    spaces: Space [];
    eventTypes: any[];
    amenities: any[];
    spaceCount: number;
    rules: any[];
    seatingOptions: any[];

    isLoading = false;
    currentPage = 0;
    pageStart;
    pageEnd;

    searchDataRequest;

    organizationKeyUp = new Subject<string>();
    orgNameSubscription = new Subscription();
    organizationName: string = '';

    sortingOptions = [
        {
            id: 1,
            status: 'priceLowestFirst',
            label: 'Price(low to high)'
        },
        {
            id: 2,
            status: 'priceHighestFirst',
            label: 'Price(high to low)'
        },
        {
            id: 3,
            status: 'distance',
            label: 'Distance'
        }
    ]

    sortOption: any;
    initialEventId: number;
    spaceOrOrgName: string;

    constructor(
        private route: ActivatedRoute,
        public _routeDataService:  RouteDataService,
        public zone: NgZone
    ){

        route.params.subscribe(params => {
            this.initialEventId = +params['id']
            this.spaceOrOrgName = params['q'];
        });

        let spacesObject = route.snapshot.data['spaces'];
        this.spaces = spacesObject.spaces;
        this.spaceCount = spacesObject.count;
        this.eventTypes = route.snapshot.data['eventTypes'];
        this.amenities = route.snapshot.data['amenities'];
        this.rules = route.snapshot.data['rules'];
        this.seatingOptions = route.snapshot.data['seatingOptions'];

        this.orgNameSubscription = this.organizationKeyUp
            .map((event:any) => event.target.value)
            .debounceTime(1000)
            .distinctUntilChanged()
            .flatMap(search => Observable.of(search).delay(500))
            .subscribe((orgName) => {
                this.organizationName = orgName;
            });

    }

    ngOnInit() {

       /* this.searchDataRequest =  {
            "currentPage": 0,
            "participation": null,
            "budget": null,
            "amenities": null,
            "events": (this.initialEventId) ? [this.initialEventId] : [],
            "isSocialMedia": (this.initialEventId) ? 1 : 0,
            "location": {address: null, latitude: null, longitude: null},
            "available": null,
            "rules": null,
            "seatingArrangements": null,
            "sortBy": null,
            "organizationName": null,
            "spaceType": []
        }

        console.log('searchDataRequest', this.searchDataRequest);*/


        if(this._routeDataService.searchDataRequest) {
            this.searchDataRequest = this._routeDataService.searchDataRequest;
        }else{
            this.searchDataRequest = {
                "currentPage": 0,
                "participation": null,
                "budget": null,
                "amenities": null,
                "events": (this.initialEventId) ? [this.initialEventId] : [],
                "isSocialMedia": (this.initialEventId) ? 1 : 0,
                "location": {address: null, latitude: null, longitude: null},
                "available": null,
                "rules": null,
                "seatingArrangements": null,
                "sortBy": null,
                "organizationName": null,
                "spaceType": []
            }
        }



    }

    setOrganization(event){
        this.isLoading = true;
            this.organizationKeyUp.next(event);

    }

    sortByOption(sortOption: any){
        if(sortOption.id === 3){
            //console.log(this.searchDataRequest.location);
            const {address, latitude, longitude} = this.searchDataRequest.location;
            if(!address || !latitude || !longitude){
                alert('Please select a location to sort by distance');
                return;
            }
        }
        this.sortOption = sortOption;
    }

    setPageStartAndEnd(event){
        this.pageStart = event.start;
        this.pageEnd = event.end;
        this.searchDataRequest.currentPage = event.currentPage;
        this._routeDataService.searchDataRequest = this.searchDataRequest;
    }

    setPageSpaces(event){
        this.zone.run(() => {
            this.spaces = event.filteredSpaces;
            this.spaceCount = event.allCount;
            this.isLoading = false;
        })
    }

    setLoading(state){
        this.isLoading = state;
    }

    ngOnDestroy(){
        this.orgNameSubscription.unsubscribe();
    }


}
