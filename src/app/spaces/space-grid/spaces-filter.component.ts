import { Component, Input, Output, EventEmitter, AfterViewInit, OnInit } from "@angular/core";
import { SpaceService } from "../shared/space.service";
import { GoogleMapService } from "../../google-services-api/shared/google-map.service";

declare let $: any;
declare let google: any;
declare let moment: any;

@Component({
    selector: 'filters',
    templateUrl: './spaces-filter.component.html',
    styles: [`
        .main-aside {
            position: fixed;
            left: 0;
            top: 111px;
            z-index: 999;
            background: #fff;
            height: calc(100vh - 120px);
            width: 100%;
            overflow-y: auto;
            transition: width 0.1s linear;
        }
        
         .main-aside.main-aside-landing { 
            top: 49px
         }
        @media only screen and (min-width: 992px) {
            .main-aside {
                height: calc(100% - 120px);
            }
            .main-aside.main-aside-landing {
                height: calc(100% - 53px);
            }
        }
        .main-aside {
            width: 0;
        }
        .main-aside.main-aside-mobile-active {
            width: 100%;
        }
        .show-filters-handler {
            position: fixed;
            z-index: 9999;
            padding: 10px 14px;
            background: #74aceb;
            border: 1px solid #e2e2e2;
            cursor: pointer;
            border-radius: 50%;
            bottom: 70px;
            right: 30px;
            color: #fff;
        }
        .filter-panel-header {
            padding: 20px 10px;
            position: relative;
            cursor: pointer;
            background: #ffffff;
            border-bottom: 1px solid #f3f3f3;
        }
        .filter-panel-header:hover {
            background: #000000;
            background: #ffffff;
        }
        .filter-panel-header.filter-panel-header-open {
            background: #ffffff;
        }
        .filter-panel-header.filter-panel-header-open > i {
            color: #74aceb;
            font-weight: bold;
        }
        .filter-panel-header h3 {
            margin: 0;
            padding: 0;
            font-size: 16px;
        }
        .filter-panel-header h3 > i {
            color: #808080;
            font-size: 18px;
            margin-right: 7px;
            width: 20px;
        }
        .filter-panel-header > i {
            position: absolute;
            right: 0;
            top: 30px;
            right: 10px;
            color: #a9a9a9;
            font-size: 14px;
        }
        .filter-panel-body {
            background: #f7f7f7;
            height: auto;
            transition: height 0.3s;
        }
        .filter-panel-body.open-filter-panel {
            padding: 10px;
            height: auto;
        }
        .filter-panel-body.close-filter-panel {
            height: 0;
            overflow: hidden;
        }
        .control__indicator {
            position: absolute;
            top: 6px;
            left: 0;
            height: 17px;
            width: 17px;
            background: #fff;
            border: 1px solid #d2d2d2;
        }
        .control.control--checkbox {
            font-size: 14px;
            color: #515153;
        }
        .form-group {
            margin-bottom: 0;
        }
        .control--checkbox .control__indicator:after {
            left: 5px;
            height: 11px;
        }
        @media only screen and (min-width : 992px) {
        .main-aside {
            width: 25%;
        }
        .show-filters-handler {
            display: none;
        }
        }
        .explore-all {
            width: 100%;
            text-align: center;
            display: inline-block;
            padding: 20px 0;
            background: #5c92f1;
            color: #fff;
            font-weight: bold;
            text-transform: uppercase;
            transition: all 0.3s ease-in;
            border-bottom: 4px solid #4c78c5;
        }
    `]
})
export class SpacesFilterComponent implements OnInit, AfterViewInit{

    @Input('eventTypes') eventTypes;
    @Input('amenities') amenities;
    @Input('rules') rules;
    @Input('seatingOptions') seatingOptions;
    @Input('spaces') spaces;
    @Input('request') searchDataRequest;
    @Input('currentPage') currentPage;

    @Output('pagedSpaces') pagedSpaces = new EventEmitter();
    @Output('loading') loading = new EventEmitter();

    eventFilterEnabled = true;
    spaceTypeFilterEnabled = false;
    locationFilterEnabled = false;
    dateFilterEnabled = false;
    guestCountFilterEnabled = false;
    rateFilterEnabled = false;
    amenityFilterEnabled = false;
    rulesFilterEnabled = false;
    seatingFilterEnabled = false;

    autoComplete: any;
    availableFrom: string; // display purposes only
    showFilterSMobile: boolean = false;
    filterCount: number = 0;

    changeEventFilter : boolean = false;
    changeAmenityFilter: boolean = false;
    changeRuleFilter : boolean = false;
    changeSeatingFilter: boolean = false;
    changeGuestFilter: boolean = false;
    changeBudgetFilter: boolean = false;
    changeLocationFilter: boolean = false;
    changeOrganizationFilter: boolean = false;
    changeSortByFilter: boolean = false;
    changeDateFilter: boolean = false;
    changeSpaceTypeFilter: boolean = false;

    guestCountOptions = [
        {id: 0, label: '1-10', values: [1,10]},
        {id: 1, label: '10-50', values: [10, 50]},
        {id: 2, label: '50-100', values: [50, 100]},
        {id: 3, label: '100-250', values: [100, 250]},
        {id: 4, label: '250-500', values: [250, 500]},
        {id: 5, label: 'Above 500', values: [500, 5000]},
    ]
    ratePerHourOptions = [
        {id: 0, label: 'Below LKR 5,000', values: [1,5000]},
        {id: 1, label: 'LKR 5,000 - LKR 10,000', values: [5000,10000]},
        {id: 2, label: 'LKR 10,000 - LKR 15,000', values: [10000,15000]},
        {id: 3, label: 'Above LKR 15,000', values: [15000,10000000]}
    ]

    selectedEvents:any = [];
    selectedAmenities:any = [];
    selectedRules:any = [];
    selectedSeatOptions:any = [];
    budgetArray:any = [];
    guestCountArray:any = [];

    spaceCount:number;

    constructor(
        private spaceService: SpaceService,
        private mapService: GoogleMapService
    ){}

    @Input()
    set organizationName(orgName: string) {
        const organizationName = orgName.trim();
        //console.log('my organizationName => ', organizationName);
        this.changeOrganization(organizationName);
    }

    @Input()
    set sortBy(sortBy: string) {
        this.changeSort(sortBy || null);
    }

    initialized = false;

    ngOnInit(){

       this.initialized = true;

       if(this.eventTypes && this.searchDataRequest && this.searchDataRequest.events && this.searchDataRequest.events.length > 0){
           this.searchDataRequest.events.forEach((event) => {
               let eventType = this.eventTypes.find(eventType =>  eventType.id === event);
               eventType.checked = eventType.checked || true;
               this.selectedEvents.push(eventType);
           });
       }

       if(this.searchDataRequest && this.searchDataRequest.amenities && this.searchDataRequest.amenities.length > 0){
           this.searchDataRequest.amenities.forEach((amenity) => {
               let amenityType = this.amenities.find(amenityType => amenityType.id == amenity);
               amenityType.checked = amenityType.checked || true;
               this.selectedEvents.push(amenityType);
           });
       }

    }

    ngAfterViewInit(){
        let input: HTMLInputElement = (<HTMLInputElement>document.getElementById("search-box"));
        this.initAutoComplete(input);

        $(".available-from").datetimepicker({
            autoclose: true,
            pickerPosition: "bottom-right",
            showMeridian: true,
            startDate: new Date(),
            forceParse: false,
            minView: 1,
        }).on('changeDate', function (ev) {
            this.availableFrom = ev.date;
            let from = moment(ev.date).format("YYYY-MM-DDTHH:00:00");
            this.searchDataRequest.available = from;
            if(!this.changeDateFilter && from != ''){
                this.changeDateFilter = true;
                this.filterCount++;
            }
            this.executeFilter();
        }.bind(this));

        $('.filters-wrapper').on('scroll', function () {
            $('.datetimepicker.dropdown-menu').hide();
            $('.pac-container').hide();
        });

        $(window).on('resize', function () {
            
            let windowHeight = $(window).innerHeight();
            let resetBtnWrapperHeight = 60;
            let navBarHeight = 50;

            //$('.main-aside').css('height', windowHeight - (resetBtnWrapperHeight + navBarHeight));
            
        })

    }

    onClickEventsFilter(){
        this.eventFilterEnabled = !this.eventFilterEnabled;
    }

    onClickSpaceTypeFilter(){
        this.spaceTypeFilterEnabled = !this.spaceTypeFilterEnabled;
    }

    onClickLocationFilter(){
        this.locationFilterEnabled = !this.locationFilterEnabled;
    }

    onClickDateFilter(){
        this.dateFilterEnabled = !this.dateFilterEnabled;
    }

    onClickGuestCountFilter(){
        this.guestCountFilterEnabled = !this.guestCountFilterEnabled;
    }

    onClickRateFilter(){
        this.rateFilterEnabled = !this.rateFilterEnabled;
    }

    onClickAmenityFilter(){
        this.amenityFilterEnabled = !this.amenityFilterEnabled;
    }

    onClickRulesFilter(){
        this.rulesFilterEnabled = !this.rulesFilterEnabled;
    }

    onClickSeatingFilter(){
        this.seatingFilterEnabled = !this.seatingFilterEnabled;
    }

    //sorting
    changeSort(sortBy: string){
        this.searchDataRequest.sortBy = sortBy;
        if(!this.initialized)
            return;
        if(!this.changeSortByFilter){
            this.changeSortByFilter = true;
            this.filterCount++;
        }
        this.executeFilter();
    }

    //organization filter
    changeOrganization(organizationName: string){
        if(!this.initialized)
            return;
        this.searchDataRequest.organizationName = organizationName;
        if(!this.changeOrganizationFilter){
            if(organizationName != ''){
                this.changeOrganizationFilter = true;
                this.filterCount++;
            }
        }
        if(organizationName == ''){
            this.changeOrganizationFilter = false;
            this.filterCount--;
        }
        this.executeFilter();
    }

    //event types filter
    changeEvent(selectedEvent){
        selectedEvent.checked = (selectedEvent.checked) ? false : true;
        let foundIndex = 0;
        let found = this.selectedEvents.find((event, index) => {
            foundIndex = index;
            return event.id === selectedEvent.id
        });

        if(found) this.selectedEvents.splice(foundIndex, 1);
        else this.selectedEvents.push(selectedEvent);

        this.searchDataRequest.events = this.selectedEvents.map(event => event.id);
        if(!this.changeEventFilter){
            if(this.selectedEvents.length > 0){
                this.changeEventFilter = true;
                this.filterCount++;
            }
        }
        if(this.selectedEvents.length == 0){
            this.changeEventFilter = false;
            this.filterCount--;
        }
        this.executeFilter();
    }

    //amenity filter
    changeAmenity(selectedAmenity){
        selectedAmenity.checked = (selectedAmenity.checked) ? false : true;
        let foundIndex = 0;
        let found = this.selectedAmenities.find((amenity, index) => {
            foundIndex = index;
            return selectedAmenity.id === amenity.id
        });

        if(found) this.selectedAmenities.splice(foundIndex, 1);
        else this.selectedAmenities.push(selectedAmenity);

        this.searchDataRequest.amenities = this.selectedAmenities.map(amenity => amenity.id);
        if(!this.changeAmenityFilter){
            if(this.selectedAmenities.length > 0){
                this.changeAmenityFilter = true;
                this.filterCount++;
            }
        }
        if(this.selectedAmenities.length == 0){
            this.changeAmenityFilter = false;
            this.filterCount--;
        }
        this.executeFilter();
    }

    //rules filter
    changeRule(selectedRule){
        selectedRule.checked = (selectedRule.checked) ? false : true;
        let foundIndex = 0;
        let found = this.selectedRules.find((rule, index) => {
            foundIndex = index;
            return selectedRule.id === rule.id
        });

        if(found) this.selectedRules.splice(foundIndex, 1);
        else this.selectedRules.push(selectedRule);

        this.searchDataRequest.rules = this.selectedRules.map(rule => rule.id);

        if(!this.changeRuleFilter){
            if(this.selectedRules.length > 0){
                this.changeRuleFilter = true;
                this.filterCount++;
            }
        }
        if(this.selectedRules.length == 0){
            this.changeRuleFilter = false;
            this.filterCount--;
        }

        this.executeFilter();
    }

    //seating filter
    changeSeatingOption(selectedOption){
        selectedOption.checked = (selectedOption.checked) ? false : true;
        let foundIndex = 0;
        let found = this.selectedSeatOptions.find((option, index) => {
            foundIndex = index;
            return selectedOption.id === option.id
        });

        if(found) this.selectedSeatOptions.splice(foundIndex, 1);
        else this.selectedSeatOptions.push(selectedOption);

        this.searchDataRequest.seatingArrangements = this.selectedSeatOptions.map(option => option.id);

        if(!this.changeSeatingFilter){
            if(this.selectedSeatOptions.length > 0){
                this.changeSeatingFilter = true;
                this.filterCount++;
            }
        }
        if(this.selectedSeatOptions.length == 0){
            this.changeSeatingFilter = false;
            this.filterCount--;
        }

        this.executeFilter();
    }

    //guest filter
    changeGuest(selectedRange){
        selectedRange.checked = (selectedRange.checked) ? false : true;
        let foundIndex = 0;
        let found = this.guestCountArray.find((option, index) => {
            foundIndex = index;
            return selectedRange.id === option.id
        });

        if(found) this.guestCountArray.splice(foundIndex, 1);
        else this.guestCountArray.push(selectedRange);

        let valArray = [];
        this.guestCountArray.forEach(option => {
            valArray.push(option.values);
        })

        valArray = valArray.join().split(',');

        let format = Math.min.apply(Math, valArray) +'-'+ Math.max.apply(Math, valArray);

        if(format === '0-0')
            format = null;

        if(!this.changeGuestFilter){
            if(this.guestCountArray.length > 0){
                this.changeGuestFilter = true;
                this.filterCount++;
            }
        }
        if(this.guestCountArray.length == 0){
            this.changeGuestFilter = false;
            this.filterCount--;
        }

        this.searchDataRequest.participation = format;
        this.executeFilter();
    }

    //budgets filter
    changeBudget(selectedRange){
        selectedRange.checked = (selectedRange.checked) ? false : true;
        let foundIndex = 0;
        let found = this.budgetArray.find((range, index) => {
            foundIndex = index;
            return selectedRange.id === range.id
        });

        if(found) this.budgetArray.splice(foundIndex, 1);
        else this.budgetArray.push(selectedRange);

        let valArray = [];
        this.budgetArray.forEach(option => {
            valArray.push(option.values);
        })

        valArray = valArray.join().split(',');

        let format = Math.min.apply(Math, valArray) +'-'+ Math.max.apply(Math, valArray);

        if(!this.changeBudgetFilter){
            if(this.budgetArray.length > 0){
                this.changeBudgetFilter = true;
                this.filterCount++;
            }
        }
        if(this.budgetArray.length == 0){
            this.changeBudgetFilter = false;
            this.filterCount--;
        }

        if(format === '0-0')
            format = null;

        this.searchDataRequest.budget = format;
        this.executeFilter();
    }

    //change location
    private initAutoComplete(input: HTMLInputElement): void {
        this.autoComplete = new google.maps.places.Autocomplete(input, {componentRestrictions: {country: "lk"}});
        google && google.maps && google.maps.event && google.maps.event.addListener(this.autoComplete, 'place_changed', () => {

            if(this.searchDataRequest){
                this.searchDataRequest.location = {address: null, latitude: null, longitude: null};
                this.searchDataRequest.location.address = input.value;

                this.mapService.findGeoCode(input.value).subscribe(
                    geoCode => {
                        this.searchDataRequest.location.latitude = geoCode.lat;
                        this.searchDataRequest.location.longitude = geoCode.lng;
                        this.executeFilter();
                    },
                    error => { console.log(error)}
                );
            }
        });
    }

    //reset location
    changeLocation(event){
        this.loading.emit(true); // loading when input blur
        if(!this.changeLocationFilter){
            this.changeLocationFilter = true;
            this.filterCount++;
        }
        this.changeLocationFilter = true;
        if(event.target.value.trim() === ''){
            this.searchDataRequest.location = {address: null, latitude: null, longitude: null};
            if(this.changeLocationFilter){
                this.changeLocationFilter = false;
                this.filterCount--;
            }
            this.executeFilter();
        }
    }

    executeFilter(){
        this.loading.emit(true);
        this.searchOnFilters(this.searchDataRequest, 0);
    }

    searchOnFilters(filterRequest: any, pageId: number){
        this.spaceService.spaceAdvancedSearch(filterRequest, pageId).subscribe(pagedSpaces => {
            this.pagedSpaces.emit({allCount: pagedSpaces.count, filteredSpaces: pagedSpaces.spaces})
        })
    }

    toggleFilters(){
        this.showFilterSMobile = !this.showFilterSMobile;
    }

    //reset filters
    resetEventsFilter(){
        this.eventTypes = this.eventTypes.map(eventType => {
            if (eventType.checked) eventType.checked = false;
            else eventType.checked = eventType.checked || false;
            return eventType;
        });
        this.searchDataRequest.events = null;
        this.selectedEvents = [];
        this.executeFilter();
    }


    changeSpaceType(id: number){

        if(this.searchDataRequest.spaceType.indexOf(id) === -1){
            this.searchDataRequest.spaceType.push(id);
        } else {
            this.searchDataRequest.spaceType.splice(this.searchDataRequest.spaceType.indexOf(id), 1);
        }
        if(!this.changeSpaceTypeFilter){
            if(this.searchDataRequest.spaceType.length > 0){
                this.changeSpaceTypeFilter = true;
                this.filterCount++;
            }
        }
        if(this.searchDataRequest.spaceType.length == 0){
            this.changeSpaceTypeFilter = false;
            this.filterCount--;
        }

        this.executeFilter();
    }

    exploreAllEventTypes() {
        var win = window.open('/#/spaces/list', '_blank');
        win.focus();
    }
}
