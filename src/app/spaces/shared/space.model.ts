export class Space{
    id:number;
    name:string = '';
    location:string;
    addressLine1:string;
    addressLine2:string = "";
    description:string;
    size:number;
    participantCount:number;
    ratePerHour:number;
    user:number;
    cancellationPolicy:number;
    images: string[] = new Array();
    eventType: number[] = new Array();
    amenity: number[] = new Array();
    extraAmenity: any[] = new Array();
    futureBookingDates: any[] = new Array();
    latitude: string;
    longitude: string;
    thumbnailImage: string;
    rating: number;
    similarSpaces :Space[]=new Array();
    securityDeposite: any;
    rules: any;
    bufferTime: any;
    availabilityBlocks: any;
    availabilityMethod: any;
    seatingArrangements: any;
    availability: any;
    noticePeriod : any;
    preStaringFrom: number;
    discount: number;
}


export class NewSpace{
    name:string = '';
    location:string;
    addressLine1:string;
    addressLine2:string;
    description:string;
    size:number;
    participantCount:number;
    ratePerHour:number;
    user:number;
    cancellationPolicy:number;
    images: string[] = new Array();
    eventType: number[] = new Array();
    amenity: number[] = new Array();
    extraAmenity: any[] = new Array();
    latitude: string;
    longitude: string;
    thumbnailImage: string;
    rating: number;
    securityDeposite: any;
    rules: any;
    bufferTime: any;
    availabilityBlocks: any;
    seatingArrangements: any;
    calendarStart: any;
    calendarEnd: any;
    calendarEventSize: any;

}
