import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SpaceFilterService {
    private filterSrc = new Subject<any>();

    setRequest(filterRequest: any) {
        this.filterSrc.next(filterRequest);
    }

    clearFilters() {
        this.filterSrc.next();
    }

    getFilterRequestAsObservable(): Observable<any> {
        return this.filterSrc.asObservable();
    }
}