import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { ServerConfigService } from '../../server.config.service';
import { Observable } from "rxjs/Observable";
import { Space } from "./space.model";
import { GoogleMapService } from "../../google-services-api/shared/google-map.service";
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class SpaceService {
    private space: Space;
    baseUrl: string;
    spaceLocation: any = null;

    constructor(private _http: Http,
                private _serverConfigService: ServerConfigService,
                private _gMapService: GoogleMapService,) {
        this.baseUrl = environment.apiHost + '/space';

    }

    getSpacesList(): Observable<Space[]> {
        return this._http
            .get(this.baseUrl, this._serverConfigService.getCookieHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getFeaturedSpaces() : Observable<Space[]>{
        return this._http
            .get(this.baseUrl + '/featured', this._serverConfigService.getCookieHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getSpace(id): Observable<Space> {
        return this._http
            .get(this.baseUrl + '/' + id, this._serverConfigService.getCookieHeader())
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }

    getSpaceRules(): Observable<any>{
        return this._http
            .get(environment.apiHost + '/common/rules', this._serverConfigService.getCookieHeader())
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }

    getSeatingOptions(): Observable<any>{
        return this._http
            .get(environment.apiHost + '/common/seatingArrangement', this._serverConfigService.getCookieHeader())
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }

    getSpaceAvailablility(data): Observable<any> {
        // return this._http.post(this.baseUrl, JSON.stringify(space), this._serverConfigService.getJsonHeader());
        return this._http
            .post(this.baseUrl + '/isAvailable', JSON.stringify(data), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    updateSpace(id, space): Observable<any> {
        return this._http
            .put(this.baseUrl + id, JSON.stringify(space), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    createSpace(spaceRequest): Observable<any> {

       return this._http.post(this.baseUrl , JSON.stringify(spaceRequest), this._serverConfigService.getJsonHeader())
           .map((response: Response) => response.json())
           .catch(this.handleError);

        /*return this._http
            .post(this.baseUrl, JSON.stringify(spaceRequest.spaceData), this._serverConfigService.getJsonHeader())
            .catch(this.handleError);*/

        /*return this._http
            .post( environment.apiHost+'/user', JSON.stringify(spaceRequest.hostData), this._serverConfigService.getJsonHeader())
            .catch(this.handleError);*/
    }

    deleteSpace(id): Observable<any> {
        return this._http
            .delete(this.baseUrl + id, this._serverConfigService.getCookieHeader())
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    getSpaceLocation() {
        return this._gMapService.getSelectedGeoCodes();
    }

    filterSpaces(body) {
        return this._http
            .post(
                this.baseUrl + '/search',
                JSON.stringify(body),
                this._serverConfigService.getJsonHeader()
            )
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }

    // search without filters
    spaceSearch (pageId: number) {
        return this._http
            .get(this.baseUrl + '/page/' + pageId, this._serverConfigService.getCookieHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    //search with filters
    spaceAdvancedSearch(body, pageId) {
        return this._http
            .post(
                this.baseUrl + '/asearch/page/' + pageId,
                JSON.stringify(body),
                this._serverConfigService.getJsonHeader()
            )
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }


    searchSpaces(body) {
        return this._http
            .post(
                this.baseUrl + '/coordinates',
                JSON.stringify(body),
                this._serverConfigService.getJsonHeader()
            )
            .map((response: Response) => <Space>response.json())
            .catch(this.handleError);
    }

    addImages(data): Observable<any> {
        return this._http
            .post(this.baseUrl + '/addImages', JSON.stringify(data), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    addChildSpace(requestBody): Observable<any> {
        return this._http
            .post(this.baseUrl + '/addChilds', JSON.stringify(requestBody), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getSimilarSpaces(spaceId: number){
        return this._http
            .get(environment.apiHost + '/similarSpaces/space/v2/' + spaceId, this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getHomepageSpaces(requestBody) {
        return this._http
            .post(this.baseUrl + '/batch', JSON.stringify(requestBody), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getFutureBookingDates(spaceId: number){
        return this._http
            .get(environment.apiHost + '/futureBookedDates/space/' + spaceId, this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getUserReviews(spaceId: number) {
        return this._http
            .get(environment.apiHost + '/reviews/' + spaceId, this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    verifyPromoCode(requestBody: any){
        return this._http
            .post(this.baseUrl + '/promo', JSON.stringify(requestBody), this._serverConfigService.getJsonHeader())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }


    private handleError(error: any): Observable<any> {
        return Observable.throw(error.json().error || 'Server Error');
    }


}


