import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {SpaceService} from "./space.service";

@Injectable()
export class SpaceRulesResolve implements Resolve<any>{

    constructor(
        private spaceService: SpaceService,
    ){}

    resolve(route: ActivatedRouteSnapshot) : Observable<any>{
        return this.spaceService.getSpaceRules();
    }


}