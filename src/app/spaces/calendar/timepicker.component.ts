import {Component, Input, OnInit, Output, EventEmitter, OnDestroy} from "@angular/core";

declare let moment: any;

@Component({
    selector: 'timepicker',
    template: `
        <div class="dropdown" *ngIf="isSameDaySelection()">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{(selectedTime) ? selectedTime.label : label}}
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li (click)="selectTime(time)" [class.inactive]="isTimePassed(time)" *ngFor="let time of timeArray"><a>{{time.label}}</a></li>
          </ul>
        </div>
        <div class="dropdown" *ngIf="!isSameDaySelection()">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{(selectedTime) ? selectedTime.label : label}}
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li (click)="selectTime(time)" *ngFor="let time of timeArray"><a>{{time.label}}</a></li>
          </ul>
        </div>
    `,
    styles: [`
        .dropdown > button {
            box-sizing: border-box;
            display: inline-block;
            width: 100%;
            background: #fff;
            border: 1px solid #e2e2e2;
            height: 40px;
            color: #3f414f;
            padding: 5px;
            text-align: left;
        }
        .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .open>.dropdown-toggle.btn-primary.focus, .open>.dropdown-toggle.btn-primary:focus, .open>.dropdown-toggle.btn-primary:hover {
            background-color: #fff;
            color: #3f414f;
            border-color: #e2e2e2;
        }
        .dropdown-menu {
            left: 10px;
            width: 94%;
            max-height: 200px;
            overflow-y: auto;
        }
        .dropdown-menu > li.inactive {
            background-color: #fdfdfd;
        }
        .dropdown-menu > li.inactive > a {
            color: #cecece !important;
            cursor: not-allowed;
        }
        .caret {
            position: absolute;
            right: 10px;
            bottom: 17px;
        }
    `]
})
export class TimepickerComponent implements OnInit, OnDestroy{

    _start: string;
    _end: string;
    _schedularDay: any;
    step = 1;
    selectedTime: any;
    timeArray = [];
    @Input('label') label;

    refreshIntervalId;

    @Input()
    set start(start: string) {
        if(start === 'start') this.selectedTime = null; //this reset start to 'FROM'
        this._start = start;
        this.fillTimeArray(this._start, this._end);
    }

    @Input()
    set end(end: string) {
        if(end === 'end') this.selectedTime = null; //this reset start to 'END'
        this._end = end;
        this.fillTimeArray(this._start, this._end);
    }

    @Input()
    set schedularDay(schedularDay: any) {
        this._schedularDay = schedularDay;
    }

    @Input('today') today;
    @Output('setTime') setTime = new EventEmitter();

    ngOnInit(){
        this.fillTimeArray(this._start, this._end);

        // reset times every 30 seconds
        setInterval(function(){
            this.refreshIntervalId = this.fillTimeArray(this._start, this._end);
        }.bind(this), 30000)
    }

    fillTimeArray(startTime:string, endTime: string){

        if(!startTime || !endTime){
            return;
        }

        this.timeArray = [];

        let startHour = startTime.split(':')[0];
        let startMin = startTime.split(':')[1];
        let start = +startHour + ((+startMin === 0) ? 0 : +startMin / 60);

        let endHour = endTime.split(':')[0];
        let endMin = endTime.split(':')[1];
        let end = +endHour + ((+endMin === 0) ? 0 : +endMin / 60);

        let step = this.step;

        for (var i = start; i < end || i == end; ){
            let label = formatLabel(''+i) + (i >= 12 ? "PM":"AM") // 1:30PM
            let valuePass = formatValue(''+i) // 13.30
            let value = i; // 13.5
            this.timeArray.push({label, valuePass, value});

            i += step;
        }

        function formatLabel(time){
            let hour = +time.split('.')[0];
            let min = +time.split('.')[1];

            hour = ((hour + 11) % 12 + 1)
            hour = pad(hour, 2);

            if(min) return hour + ':30'
            else return hour + ':00'
        }

        function formatValue(time){
            let hour = +time.split('.')[0];
            let min = +time.split('.')[1];

            hour = pad(hour, 2);

            if(min) return hour + ':30'
            else return hour + ':00'
        }

        function pad(n, width, z = '0') {
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        //console.log('this.timeArray', this.timeArray)
    }

    selectTime(time){

        if(this.isSameDaySelection() && this.isTimePassed(time)){
            return;
        }

        this.selectedTime = time;
        this.setTime.emit({time})
    }

    isTimePassed(time){

        let timestamp = moment(time.valuePass, 'HH:mm');
        let timeElapsed = moment.duration(timestamp.diff(moment())).asHours();

        if(timeElapsed < 0){
            return true;
        }

        return false;
    }

    isSameDaySelection(){
        if(!this._schedularDay){
            return false;
        }

        if(this._schedularDay.format('YYYY-MM-DD') === this.today.format('YYYY-MM-DD')){
            return true;
        }

        return false;
    }

    ngOnDestroy(){
        clearInterval(this.refreshIntervalId);
    }




}