import {Component, Input, Output, EventEmitter, AfterViewInit } from "@angular/core";

declare let moment: any;
declare let $: any;

@Component({
    selector: 'block-items',
    styles: [`
        /**  Time blocks legend **/
        .time-blocks-legend {
            text-align: center;
        }
        .time-blocks-legend > div {
            display: inline-block;
            width: 33.33%;
            text-align: center;
        }
        .time-blocks-legend > div > span {
            display: block;
            margin-top: 5px;
            color: #636363;
        }
        .time-blocks-legend > div > span:first-of-type {
            display: inline-block;
            width: 18px;
            height: 18px;
            border-radius: 50%;
            font-size: 12px;
        }
        .time-blocks-legend > div > span:first-of-type > span {
            display: inline-block;
            vertical-align: middle;
            position: relative;
            top: 0;
            color: #fff;
        }
        .legend-available > span:first-of-type {
            background: #2ea710;
        }
        .legend-booked > span:first-of-type {
            background: #f54a3a;
        }
        .legend-past > span:first-of-type, .legend-booked-past  > span:first-of-type {
            background: #636261;
        }
        .per-block-avl-wrapper > div {
            padding: 10px 5px;
            margin-bottom: 5px;
        }
        .per-block-avl-wrapper > div > div > span, .per-block-avl-wrapper > div > div > span > input {
            vertical-align: middle;
            margin: 0;
        }
        .per-block-avl-wrapper > div > div > span {
            display: inline-block;
        }
        .per-block-avl-wrapper > div > div > span:first-of-type {
            width: 34%;
            padding-right: 10px;
        }
        .per-block-avl-wrapper > div > div > span:nth-child(2) {
            width: 33.5%;
        }
        .per-block-avl-wrapper > div > div > span:nth-child(3) {
            width: 30%;
            padding-left: 7px;
        }
        .availability-indicator {
            display: inline-block;
            width: 18px;
            height: 18px;
            border-radius: 50%;
            text-align: center;
            font-size: 12px;
        }
        .availability-indicator > span {
            display: inline-block;
            top: 1px;
            position: relative;
            color: #fff;
        }
        .ai-available {
            background: #2ea710;
        }
        .ai-not-available {
            background: #636261;
        }
        .ai-booked {
            background: #f54a3a;
        }
        .avl-wrapper {
            margin-top: 10px;
            box-sizing: border-box;
        }
        .avl-menus-wrapper > div {
            width: auto;
            display: inline-block;
        }
        .booked-past, .available-past, .booked {
            background: #f9f9f9;
        }
        .available {
            background: #eafaff;
        }
        .avl-wrapper-titles {
            margin-top: 10px;
        }
        .avl-wrapper-titles > span {
            display: inline-block;
            font-size: 12px;
        }
        .avl-wrapper-titles > span:first-child {
            width: 34%;
        }
        .avl-wrapper-titles > span:nth-child(2) {
            width: 35%;
        }

        .block-menu-info-ico-wrapper {
            display: inline-block;
            position: absolute;
            bottom: 10px;
            right: 10px;
        }
        .per-block-avl-wrapper {
            position: relative;
        }
        .avl-menus-wrapper {
            position: absolute;
            bottom: -10px;
            right: 10px;
            background: #fff;
            border: 1px solid #e2e2e2;
        }
    `],
    templateUrl: './block-items.component.html'
})

export class BlockItemsComponent implements  AfterViewInit  {

    _schedulerData: any;
    state: any;
    blocksCost: number = 0;
    totalHoursCount: number = 0;
    _guestCount: number;

    @Input('selectedBlocks') selectedBlocks;
    @Output('onSelectBlock') onSelectBlock = new EventEmitter();
    @Input('noticePeriod') noticePeriod;
    @Input('minGuestCount') minGuestCount;
    @Input('chargeType') chargeType;

    @Input()
    set guestCount(guestCount: number){
        //console.log('guestcount', guestCount);
        this.totalHoursCount = 0;
        this._guestCount = guestCount;
    }

    availArray;

    @Input()
    set schedulerData(schedulerData: any) {

        if(!schedulerData.availability){
            return;
        }

        this._schedulerData = schedulerData;
        this.blocksCost = 0;
        this.totalHoursCount = 0;

        schedulerData.availability.forEach(block => {
            if(block.state)
                block.state = null;

            if(block.selected)
                block.selected = false;
        });

        schedulerData.availability.forEach(block => {
            block.state = this.getState(block)
        });

        this.availArray = this._schedulerData.availability.map(event => {
            return {
                key: event.from + event.to,
                value: []
            }
        });

        console.log('schedulerData.availability', schedulerData.availability);
    }


    ngAfterViewInit(){
        $('[data-toggle="tooltip"]').tooltip();
    }

    getState(block){
        return {
            booked : this.isBooked(block),
            blocked: false,
            past : this.isPast(block)
        }
    }

    isBlocked(block) {
        let isBlock = false;
        this.availArray.forEach(item => {
            if(item.key === (block.from + block.to)) {
                isBlock = item.value.some(x => {
                    return x === true;
                })
            }
        });

        return isBlock;
    }

    isBooked(block){
        let format = 'hh:mm:ss';
        let exists = false;

        this._schedulerData.eventsPerDay.forEach(event => {

            let wholeBlockStartWithinRange = moment(block.from, format)
                .isBetween(moment(event.from, format), moment(event.to, format),'hours', '[)'); // left inclusive
            let wholeBlockEndWithinRange = moment(block.to, format)
                .isBetween(moment(event.from, format), moment(event.to, format),'hours', '(]') // right inclusive


            let blockStartWithinRange = moment(event.from, format)
                .isBetween(moment(block.from, format), moment(block.to, format),'hours', '[)'); // left inclusive
            let blockEndWithinRange = moment(event.to, format)
                .isBetween(moment(block.from, format), moment(block.to, format),'hours', '(]') // right inclusive
            //moment doc https://stackoverflow.com/a/29495647/1390894

            if((blockStartWithinRange || blockEndWithinRange) && event.to != "00:00:00"){
                exists = true;
                return;
            }else if(wholeBlockStartWithinRange || wholeBlockEndWithinRange){
                exists = true;
                return;
            }
        })

        return exists;
    }

    isPast(block){
        let isPast = false;
        if(this.isSameDaySelection()){
            let timestamp = moment(block.from, 'HH:mm');
            let timeElapsed = moment.duration(timestamp.diff(moment())).asHours();
            if(timeElapsed < 0){
                isPast = true;
            }
        }
        return isPast;
    }

    isSameDaySelection(){
        if(!this._schedulerData.fullDay)
            return false;

        if(this._schedulerData.fullDay.format('YYYY-MM-DD') === moment().format('YYYY-MM-DD'))
            return true;

        return false;
    }

    selectBlock(block){
        let format = 'hh:mm:ss';

        //space only chage
        if(this.chargeType === 1){
            if(block.selected){
                block.selected = false;
                this.blocksCost -= block.charge;
                this.totalHoursCount -= block.diff;
                this.selectedBlocks = this.selectedBlocks.filter(sblock => {
                    return sblock.block.from != block.from
                });

            }else{
                block.selected = true;
                this.blocksCost += block.charge;
                this.totalHoursCount += block.diff;
                this.selectedBlocks.push({block: block, day: this._schedulerData.fullDay});
            }
        }
        // per guest charge
        else if(this.chargeType === 2){
            let blockCharge = (this.minGuestCount > (this._guestCount || 0)) ? (block.charge * this.minGuestCount) : (block.charge * (this._guestCount || 0))
            if(block.selected){
                block.selected = false;
                this.blocksCost -= blockCharge;
                this.totalHoursCount -= block.diff;
                this.selectedBlocks = this.selectedBlocks.filter(sblock => {
                    return sblock.block.from != block.from
                });

            }else{
                block.selected = true;
                this.blocksCost += blockCharge;
                this.totalHoursCount += block.diff;
                this.selectedBlocks.push({block: block, day: this._schedulerData.fullDay});
            }
        }

        //console.log('this.selectedBlocks', this.selectedBlocks);


        if(this.selectedBlocks.length){

            this.selectedBlocks.forEach(selectedBlock => {
                const blockFrom = moment(selectedBlock.block.from ,format);
                const blockEnd = moment(selectedBlock.block.to, format);

                this._schedulerData.availability.forEach(event => {

                    if(event.from == selectedBlock.block.from && event.to == selectedBlock.block.to){
                        return;
                    }

                    const eventFrom = moment(event.from, format);
                    const eventEnd = moment(event.to, format);

                    const eventRange1 = moment.range(eventFrom, eventEnd);
                    const eventRange2 = moment.range(blockFrom, blockEnd);

                    let overlaping = eventRange1.overlaps(eventRange2);

                    if(overlaping){
                        event.state.blocked = true;
                        //this.setEventBlockTimes(event.from + event.to, true);

                    }else if(!overlaping && !event.state.blocked){
                        event.state.blocked = false;
                        //this.setEventBlockTimes(event.from + event.to, false);

                    }
                    else if(!overlaping && event.state.blocked && !block.selected){
                        event.state.blocked = false;
                        //this.setEventBlockTimes(event.from + event.to, false);

                    }
                    else if(!overlaping && event.state.blocked && block.selected){
                        event.state.blocked = true;
                        //this.setEventBlockTimes(event.from + event.to, true);

                    }

                });
            })
        }

        else {
            this._schedulerData.availability.forEach(event => {
                if(!event.state.booked){
                    event.state.blocked = false;
                }
            });
        }

        this.onSelectBlock.emit({
            blockCost: this.blocksCost,
            selectedBlocks: this.selectedBlocks,
            totalHoursCount: this.totalHoursCount
        })
    }

    isWithinNoticePeriod(block){
        let isWithinNoticePeriod = false;
        if(this.isSameDaySelection()){
            let timestamp = moment(block.from, 'HH:mm');
            let timeElapsed = moment.duration(timestamp.diff(moment())).asHours();
            if(this.noticePeriod >  timeElapsed){
                isWithinNoticePeriod = true;
            }
        }
        return isWithinNoticePeriod;
    }

    setEventBlockTimes(key, blocked) {
        this.availArray.forEach(item => {
            if(item.key === key) {
                item.value.push(blocked);
            }
        });
    }

    resetBlockCost(){
        this.blocksCost = 0;
    }

    getMenusName(menus){
        return menus.map(menu => 'Menu-'+menu.menuId ).join();
    }

}
