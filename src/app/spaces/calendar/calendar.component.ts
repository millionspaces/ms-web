import {Component, AfterViewInit, Input, Output, EventEmitter, OnInit} from "@angular/core";

declare let clndr: any;
declare let moment: any;
declare let $: any;

@Component({
    selector: 'booking-calendar',
    template: `
        <div class="book-cal"></div>
        `,
    styles: [`
        :host >>> .clndr-controls {
            font-size: 20px;
            padding-top: 20px;
        }
        :host >>> .clndr-controls {
            font-size: 14px;
            margin-bottom: 20px;
        }
        :host >>> .clndr-control-button, :host >>> .clndr-control-button {
            display: inline-block;
            width: 22%;
        }
        :host >>> .month {
          display: inline-block;
          width: 52%;
        }
        /*:host >>> .month {*/
            /*text-align: center;*/
        /*}*/
        /*:host >>> .clndr-next-btn-wrapper {*/
            /*text-align: right;*/
        /*}*/
        :host >>> .header-days {
            /*display: inline-block;*/
        }
        :host >>> .header-days > div {
            display: inline-block;
            width: 14.2%;
        }
        :host >>> .clndr-weeks-wrapper > div {

        }
        :host >>> .clndr-weeks-wrapper > div > div {
            display: inline-block;
            width: 14.2%;
            height: 50px;
        }
        :host >>> .book-cal {
            text-align: center;
            background: #ffffff;
            margin: 10px;
            margin-top: 0;
        }
        :host >>> .day-contents {
            display: table;
            margin: 0 auto;
            height: 100%;
        }
        :host >>> .day-cell {
            display: table-cell;
            vertical-align: middle;
            height: 100%;
            position: relative;
            width: 15px;
        }
        :host >>> .adjacent-month {
            color: #e2e2e2;
        }
        :host >>> .day-contents {
            cursor: pointer;
        }
        :host >>> .month {
            font-size: 16px;
        }
        :host >>> .clndr-next-button, :host >>> .clndr-previous-button {
            cursor: pointer;
            font-size: 22px;
            padding: 10px 20px;
        }
        :host >>> .event > div > div > span {
            position: absolute;
            background: #388fff;
            bottom: 7px;
            left: 7px;
            width: 5px;
            height: 5px;
            display: inline-block;
            border-radius: 50%;
        }
        :host >>> .today {
            border-radius: 50%;
            color: #388fff;
            width: 49px;
            height: 40px;
            font-weight: bold;
        }

        /*:host >>> .today.event > div > div > span {*/
            /*left: 23px;*/
        /*}*/

        :host >>> .inactive {
            color: #989898;
        }
        :host >>> .past {
            border-radius: 50%;
            color: #989898;
            width: 49px;
            height: 40px;
        }

    `]
})
export class CalendarComponent implements OnInit, AfterViewInit{

    @Input('blockPeriods') blockPeriods;
    @Input('eventDaysCalendar') eventDaysCalendar;
    @Input('availability') availability;
    @Input('availabilityMethod') availabilityMethod;
    @Input('noticePeriod') noticePeriod;
    @Input('bufferTime') bufferTime;
    @Input('calendarEnd') calendarEnd;
    @Input('spaceRatePerHour') spaceRatePerHour;

    @Output('showScheduler') showScheduler = new EventEmitter();


    bookingCalendar: any;

    clndrTemplate = `
        <div class='clndr-controls'>
            <div class='clndr-control-button'>
                <span class='clndr-previous-button'><i class="fa fa-angle-left" aria-hidden="true"></i></span>
            </div
            ><div class='month'><%= month %> <%= year %></div
            ><div class='clndr-control-button clndr-next-btn-wrapper'>
                <span class='clndr-next-button'><i class="fa fa-angle-right" aria-hidden="true"></i></span>
            </div>
        </div>
        <div>
            <div>
                <div class='header-days'><% for(var i = 0; i < daysOfTheWeek.length; i++) { %><div class='header-day'><%= daysOfTheWeek[i] %></div><% } %></div>
            </div>
            <div class="clndr-weeks-wrapper"><% for(var i = 0; i < numberOfRows; i++){ %><div><% for(var j = 0; j < 7; j++){ %><% var d = j + i * 7; %><div class='<%= days[d].classes %>'><div class='day-contents'><div class="day-cell"><%= days[d].day %><span></span></div></div></div><% } %></div><% } %></div>
        </div>
    `

    ngOnInit() {

        console.log('blockPeriods', this.blockPeriods);
        console.log('eventDaysCalendar', this.eventDaysCalendar);
        console.log('availability', this.availability);
        console.log('availabilityMethod', this.availabilityMethod);
        console.log('noticePeriod', this.noticePeriod);
        console.log('bufferTime', this.bufferTime);
        console.log('calendarEnd', this.calendarEnd);
        console.log('spaceRatePerHour', this.spaceRatePerHour);


    }

    ngAfterViewInit(){

        this.bookingCalendar = (<any>$('.book-cal')).clndr({
            constraints: {
                endDate: this.calendarEnd,
                startDate: moment().format('YYYY-MM-DD')
            },
            events: this.eventDaysCalendar,
            template: this.clndrTemplate,
            daysOfTheWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            weekOffset: 1,
            classes: {
                past: "past",
                today: "today",
                event: "event",
                inactive: "inactive",
                selected: "selected",
                lastMonth: "last-month",
                nextMonth: "next-month",
                adjacentMonth: "adjacent-month",
            },
            clickEvents: {
                click: function (target) {

                    if ($(target.element).hasClass('inactive')) {
                        return;
                    }

                    $('.calendar-component-wrapper').hide();
                    $('.per-block-wrapper, .per-hour-wrapper,  .extra-wrapper').show();

                    let selectedDate = moment(target.date).format('DD/MM/YYYY');

                    let eventsPerDay = [];
                    let availabilityPerDayHourBase = { from: null, to: null, day: null, charge: null };
                    let availabilityPerDayBlockBase = [];
                    let schedularData = {};


                    //fill day availability
                    if(this.availabilityMethod === "HOUR_BASE"){
                        this.availability.forEach(dayAvailability => {
                            if(target.date.weekday() === dayAvailability.day){
                                availabilityPerDayHourBase = dayAvailability;
                                return;
                            }
                            else if(target.date.weekday() === 0 && dayAvailability.day === 7){
                                availabilityPerDayHourBase = dayAvailability;
                                return;
                            }
                        })
                    }

                    //fill day availability - per block
                    else if(this.availabilityMethod === "BLOCK_BASE"){
                        this.availability.forEach(dayAvailability => {
                            //handle weekdays blocks
                            if( (target.date.weekday() === 1 || target.date.weekday() === 2 || target.date.weekday() === 3
                                || target.date.weekday() === 4 || target.date.weekday() === 5 ) && dayAvailability.day === 8
                            ){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                            //handle saturday blocks
                            else if(target.date.weekday() === 6 && dayAvailability.day === 6){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                            //handle sunday blocks
                            else if(target.date.weekday() === 0 && dayAvailability.day === 7){
                                availabilityPerDayBlockBase.push(dayAvailability);
                            }
                        })
                    }

                    // fill schedularData
                    if(this.availabilityMethod === "HOUR_BASE"){
                        //fill events per day for both PH and PB
                        this.blockPeriods.forEach(block => {

                            var startDate = moment(block.from).format('DD/MM/YYYY');
                            var endDate = moment(block.to).format('DD/MM/YYYY');

                            // same date
                            if(selectedDate === startDate && selectedDate === endDate)
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: moment(block.to).format('HH:mm:00'),
                                    id: block.id
                                });

                            // partially booked from start date
                            else if(selectedDate === startDate && selectedDate !== endDate){
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: availabilityPerDayHourBase.to,
                                    id: block.id
                                });
                            }

                            // fullday booked
                            else if(moment(selectedDate, 'DD/MM/YYYY').isBetween(moment(startDate, 'DD/MM/YYYY'), moment(endDate, 'DD/MM/YYYY'))){
                                eventsPerDay.push({
                                    from: availabilityPerDayHourBase.from,
                                    to: availabilityPerDayHourBase.to,
                                    fullDay: true,
                                    id: block.id
                                });
                            }

                            // partially booked from end date
                            else if(selectedDate === endDate && selectedDate !== startDate){
                                eventsPerDay.push({
                                    from: availabilityPerDayHourBase.from,
                                    to: moment(block.to).format('HH:mm:00'),
                                    id: block.id
                                });
                            }
                        })

                        schedularData = {
                            fullDay: moment(target.date),
                            day: target.date.weekday(),
                            from: availabilityPerDayHourBase.from,
                            to: availabilityPerDayHourBase.to,
                            charge: availabilityPerDayHourBase.charge,
                            eventsPerDay: eventsPerDay,
                            noticePeriod: this.noticePeriod,
                            bufferTime: this.bufferTime,
                            availabilityMethod: this.availabilityMethod
                        }
                    }


                    else if(this.availabilityMethod === "BLOCK_BASE"){

                        this.blockPeriods.forEach(block => {
                            /*if(selectedDate === moment(block.from).format('DD/MM/YYYY'))
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: moment(block.to).format('HH:mm:00'),
                                    id: block.id
                                });*/
                            var startDate = moment(block.from).format('DD/MM/YYYY');
                            var endDate = moment(block.to).format('DD/MM/YYYY');

                            // same date
                            if(selectedDate === startDate && selectedDate === endDate)
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: moment(block.to).format('HH:mm:00'),
                                    id: block.id
                                });

                            // partially booked from start date
                            else if(selectedDate === startDate && selectedDate !== endDate){
                                eventsPerDay.push({
                                    from: moment(block.from).format('HH:mm:00'),
                                    to: '24:00:00',
                                    id: block.id
                                });
                            }

                            // fullday booked
                            else if(moment(selectedDate, 'DD/MM/YYYY').isBetween(moment(startDate, 'DD/MM/YYYY'), moment(endDate, 'DD/MM/YYYY'))){
                                eventsPerDay.push({
                                    from: '00:00:00',
                                    to: '24:00:00',
                                    fullDay: true,
                                    id: block.id
                                });
                            }

                            // partially booked from end date
                            else if(selectedDate === endDate && selectedDate !== startDate){
                                eventsPerDay.push({
                                    from: '00:00:00',
                                    to: moment(block.to).format('HH:mm:00'),
                                    id: block.id
                                });
                            }
                        })

                        schedularData = {
                            fullDay: moment(target.date),
                            day: target.date.weekday(),
                            availability: availabilityPerDayBlockBase,
                            eventsPerDay: eventsPerDay,
                            noticePeriod: this.noticePeriod,
                            availabilityMethod: this.availabilityMethod
                        }

                    }



                    this.showScheduler.emit({schedularData});

                }.bind(this),
                today: function () {
                    console.log('Cal-1 today');
                },
                nextMonth: function () {
                    console.log('Cal-1 next month');
                },
                previousMonth: function () {
                    console.log('Cal-1 previous month');
                },
                onMonthChange: function () {
                    console.log('Cal-1 month changed');
                },
                nextYear: function () {
                    console.log('Cal-1 next year');
                },
                previousYear: function () {
                    console.log('Cal-1 previous year');
                },
                onYearChange: function () {
                    console.log('Cal-1 year changed');
                },
                nextInterval: function () {
                    console.log('Cal-1 next interval');
                },
                previousInterval: function () {
                    console.log('Cal-1 previous interval');
                },
                onIntervalChange: function () {
                    console.log('Cal-1 interval changed');
                },
            },
            multiDayEvents: {
                singleDay: 'date',
                endDate: 'endDate',
                startDate: 'startDate'
            },
            showAdjacentMonths: true,
            adjacentDaysChangeMonth: false
        });

        // set calendar month to avengers promo month
        if(this.spaceRatePerHour == 0) {
            this.bookingCalendar.setMonth(+this.calendarEnd.split('-')[1] - 1);
            this.bookingCalendar.setEvents([{date: '2018-04-27'}]);
        }
    }

}
