import {Component} from "@angular/core";
import {ImageUploadModalComponent} from "../../core/modal/image-upload-modal.component";
import {SpaceService} from "../shared/space.service";
import {environment} from '../../../environments/environment';
declare let cloudinary: any;
@Component({
    templateUrl: './add-images.component.html'
})
export class AddImagesComponent {
    isLoading = false;

    // modal data
    modalComponentData = null;

    spaceId: number;
    thumbnailImage: any;
    covers: any [] = [];

    constructor(private spaceService: SpaceService){}

    createImageUploadModal(window) {
        this.modalComponentData = {
            component: ImageUploadModalComponent,
            inputs: {
                window : window
            }
        }
    }


    imageRemoved($event){

        /*if($event.window === 'cover'){
            let index = 0;
            this.covers.forEach(imgData => {
                if($event.filName === imgData.response.Key){
                    this.covers.splice(index, 1);
                    return;
                }
                index++;
            });
        }
        else if($event.window === 'thumbnail'){
            if($event.filName === this.thumbnailImage.response.Key){
                this.thumbnailImage = null;
            }
        }*/


        console.log('$event',$event);

        if($event.window === 'cover'){
            let index = 0;
            this.covers.forEach(imgData => {
                if($event.filName === imgData.public_id){
                    this.covers.splice(index, 1);
                    return;
                }
                index++;
            });
        }

        else if($event.window === 'thumbnail'){
            if($event.filName === this.thumbnailImage.public_id){
                this.thumbnailImage = null;
            }
        }

    }


    submitImages(){
        if(!this.spaceId){
            alert('space id required');
            return;
        }
        else if(!this.thumbnailImage){
            alert('Thumbnail Image required');
            return;
        }
        else if(this.covers.length === 0){
            alert('Cover images required');
            return;
        }

        let apiRequest = {
            id: +this.spaceId,
            thumbnailImage: this.thumbnailImage.public_id.split('/')[1],
            images : this.covers.map(cover => cover.public_id.split('/')[1])
        }


        console.log('apiRequest', apiRequest);

        this.spaceService.addImages(apiRequest).subscribe(isSuccess => {
            if(isSuccess){
                alert('image upload successful');
                location.reload();
            }else{
                alert('image upload fail');
            }
        })

    }


    uploadThumbnailImage(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg"],
                max_file_size: 3000000,
                multiple: false,
                folder: 'millionspaces'
            },
            (error, result) => {
                if(result){
                    console.log('result', result);
                    result[0].window = 'thumbnail';
                    this.thumbnailImage = result[0];
                }
            });
    }

    uploadCoverImages(){
        cloudinary.openUploadWidget({
                cloud_name: environment.cloudName,
                upload_preset: environment.preset,
                theme: 'white',
                sources: [ 'local', 'url'],
                client_allowed_formats: ["png", "jpeg"],
                max_file_size: 3000000,
                multiple: false,
                folder: 'millionspaces'
            },
            (error, result) => {
                if(result){
                    console.log('result', result);
                    result[0].window = 'cover';
                    this.covers.push(result[0]);
                }
            });
    }



}