import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NewSpaceHomeComponent } from "./create/new-space-home.component";
import { UserResolve } from "../user/user.resolve";
import { EventTypeResolve } from "../events/shared/event-type.resolve";
import { AmenitiesResolve } from "../amenities/shared/amenities.resolve";
import { SpaceListingSuccessComponent } from "./create/space-listing-success.component";
import { SpacesResolve } from "./spaces.resolve";
import { SpaceResolve } from "./space.resolve";
import { CancellationPolicyResolve } from "../core/cancellation-policy/shared/cancellation-policy.resolve";
import { ListSpaceComponent } from "./create/list-space.component";
import { AmenityUnitsResolve } from "../amenities/shared/amenity-units.resolve";
import { SpaceRulesResolve } from "./shared/space-rules.resolve";
import { SeatingOptionsResolver } from "./shared/seating-options.resolver";
import { SpaceGridComponent } from "./space-grid/space-grid.component";
import {SpaceProfileComponent} from "./profile/space-profile.component";
import {AddImagesComponent} from "./add-images/add-images.component";
import {AuthGuard} from "../shared/services/auth-guard.service";
import { ProfileTestComponent } from "./profile-test/profile-test.component";

const SPACE_ROUTES = [
    {
        path: 'images',
        component: AddImagesComponent,
    },
    {
        path: 'listing/new',
        component: ListSpaceComponent,
        canActivate:[AuthGuard],
        resolve: {
            user: UserResolve,
            eventTypes: EventTypeResolve,
            amenities: AmenitiesResolve,
            amenityUnits: AmenityUnitsResolve,
            cancellationPolicies: CancellationPolicyResolve,
            seatingOptions: SeatingOptionsResolver,
            spaceRules: SpaceRulesResolve
        },
        /*canDeactivate: [PreventUnsavedChangesGuard]*/
    },
    { path: 'listing/home', component: NewSpaceHomeComponent },
    { path: 'listing/success', component: SpaceListingSuccessComponent},
    {
        path: 'list',
        component: SpaceGridComponent,
        resolve: {
            spaces: SpacesResolve,
            eventTypes: EventTypeResolve,
            amenities: AmenitiesResolve,
            rules: SpaceRulesResolve,
            seatingOptions: SeatingOptionsResolver
        }
    },

    {
        path: 'event/:eventId',
        component: SpaceGridComponent,
        resolve: {
            spaces: SpacesResolve,
            eventTypes: EventTypeResolve,
            amenities: AmenitiesResolve,
            rules: SpaceRulesResolve,
            seatingOptions: SeatingOptionsResolver
        }
    },
    // {
    //     path: 'events/:eventName/:id',
    //     component: SpaceGridComponent,
    //     resolve: {
    //         spaces: SpacesResolve,
    //         amenities: AmenitiesResolve,
    //         rules: SpaceRulesResolve,
    //         seatingOptions: SeatingOptionsResolver
    //     }
    // },
    {
        path: ':id/:name',
        component: SpaceProfileComponent,
        canActivate:[AuthGuard],
        resolve:{
            space: SpaceResolve,
            cancellationPolicies: CancellationPolicyResolve,
            spaceRules: SpaceRulesResolve
        }
    },
    {
        path: 'test/route/:id',
        component: ProfileTestComponent,
        resolve:{
            space: SpaceResolve,
            cancellationPolicies: CancellationPolicyResolve,
            spaceRules: SpaceRulesResolve
        }
    }


]

@NgModule({
    imports: [RouterModule.forChild(SPACE_ROUTES)],
    exports: [RouterModule]
})

export class SpacesRouteModule{}
