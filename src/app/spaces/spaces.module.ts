import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";

import { SpaceComponent } from "./space/space.component";
import { SpacesPipe } from "./pipes/spaces.pipe";
import { SpaceListComponent } from "./space-list/space-list.component";
import { SpacesRouteModule } from "./spaces.route.module";
import { NewSpaceHomeComponent } from "./create/new-space-home.component";
import { SpacesMapComponent } from "./map/spaces-map.component";
import { SpaceResolve } from "./space.resolve";
import { SpaceExtraAmenityDirectiveComponent } from "./directives/space-extra-amenity-directive.component";
import { SpaceListingSuccessComponent } from "./create/space-listing-success.component";
import { SpacesEventTypePipe } from "./pipes/spaces-eventype.pipe";

import { GoogleServicesAPIModule } from "../google-services-api/google-services-api.module";
import { AmenitiesService } from "../amenities/shared/amenities.service";
import { CancellationPolicyService } from "../core/cancellation-policy/shared/cancellation-policy.service";
import { FilterPipe } from '../filter.pipe';
import { PreviewImagesDirectiveComponent } from "./directives/preview-images-directive.coponent";
import { FbServicesAPIModule } from "../fb-auth/fb-services-api.module";
import { SharedModule } from "../shared/shared.module";
import { CoreModule } from "../core/core.module";
import { ListSpaceComponent } from "./create/list-space.component";
import { CustomizedBlockComponent } from "./create/customized-block.component";
import { StepsWizard } from "./create/steps-wizard.directive";
import { SpaceGridComponent } from "./space-grid/space-grid.component";
import { SpaceFilterService } from "./shared/space-filter.service";
import { RatePerHourComponent } from "./create/rate-per-hour-block.component";
import { SpacesFilterComponent } from "./space-grid/spaces-filter.component";
import { CalendarComponent } from "./calendar/calendar.component";
import { SpaceProfileComponent } from "./profile/space-profile.component";
import { TimepickerComponent } from "./calendar/timepicker.component";
import { BlockItemsComponent } from "./calendar/block-items.component";
import { GuestSchedulerComponent } from "./profile/guest-scheduler.component";
import {AddImagesComponent} from "./add-images/add-images.component";

import { CloudinaryModule } from '@cloudinary/angular';
import { Cloudinary } from 'cloudinary-core';
import {GuestSchedulerComponentV2} from "./profile/guest-scheduler-v2.component";

import { environment } from '../../environments/environment'
import {UserReviewComponent} from "./profile/user-review.component";
import { ProfileTestComponent } from "./profile-test/profile-test.component";

export const cloudinaryLib = {
    Cloudinary: Cloudinary
};

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        GoogleServicesAPIModule,
        FbServicesAPIModule,
        SharedModule,
        CoreModule,
        SpacesRouteModule,
        CloudinaryModule.forRoot(cloudinaryLib, { cloud_name: environment.cloudName })
    ],
    declarations:[
        ListSpaceComponent,
        SpaceListComponent,
        SpaceComponent,
        SpacesPipe,
        SpaceGridComponent,
        SpacesMapComponent,
        NewSpaceHomeComponent,
        SpaceListingSuccessComponent,
        SpaceExtraAmenityDirectiveComponent,
        PreviewImagesDirectiveComponent,
        FilterPipe,
        SpacesEventTypePipe,
        CustomizedBlockComponent,
        RatePerHourComponent,
        StepsWizard,
        SpacesFilterComponent,
        CalendarComponent,
        SpaceProfileComponent,
        TimepickerComponent,
        BlockItemsComponent,
        GuestSchedulerComponent,
        AddImagesComponent,
        GuestSchedulerComponentV2,
        UserReviewComponent,
        ProfileTestComponent
    ],
    providers: [
        AmenitiesService,
        CancellationPolicyService,
        SpaceResolve,
        SpaceFilterService,
    ]
})
export class SpacesModule{}